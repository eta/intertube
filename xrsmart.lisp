;;;; Network Rail (and Crossrail) SMART berth offset processing

(in-package :trackernet)

(defparameter *xr-smart-areas*
  '("D1" "D6" "D4" "D3" "DA" ; XR West
    "Q2" "Q1" "Q4" ; XR East
    ))
(defparameter *xr-smart-data*
  (alexandria:alist-hash-table
   (read-from-string (uiop:read-file-string "./refdata/smart.sexp"))
   :test 'equal))

(defun read-json-stanoxen-table (path)
  (with-open-file (stream path)
    (let* ((cl-json:*json-identifier-name-to-lisp* #'identity))
      (alexandria:alist-hash-table
       (mapcar
        (lambda (x) (cons (symbol-name (car x)) (cdr x)))
        (cl-json:decode-json stream))
       :test #'equal))))

(defparameter *stanoxen-table* (read-json-stanoxen-table "./refdata/stanoxen.json"))

(defun lookup-stanox (stanox)
  (gethash stanox *stanoxen-table*))

(defun prettify-station-name (name)
  "Transform NAME, an all-caps station name, into something not all caps."
  (format nil "~{~A~^ ~}"
          (loop
            for word in (split-sequence:split-sequence #\Space name)
            collect (cond
                      ((string-equal word "and") "and")
                      ((string-equal word "on") "on")
                      ((string-equal word "by") "by")
                      ((< (length word) 2) word)
                      (t (format nil "~A~A"
                                 (string-upcase (subseq word 0 1))
                                 (string-downcase (subseq word 1))))))))

;; This should not be needed any more, and imports to redis instead of the json file,
;; but is kept here for historical reference
(defun import-stanoxen-from-cif (path)
  (with-open-file (file path)
    (itps::read-cif-file
     file
     (lambda (item)
       (when (typep item 'itps::cif-header)
         (format t "; Header: ~A~%" item))
       (when (and
              (typep item 'itps::cif-tiploc-insert)
              (itps::stanox item))
         (format t "; Item: ~A~%" item)
         (red:set (format nil "stanox:~A" (itps::stanox item))
                  (prettify-station-name (itps::description item))))))))

;; https://stackoverflow.com/a/6004194 CC-BY-SA 3.0
(defun list-string< (a b)
  (cond ((null a) (not (null b)))
        ((null b) nil)
        ((string= (first a) (first b)) (list-string< (rest a) (rest b)))
        (t (string< (first a) (first b)))))

(defun transform-smart-data (nr-in-path refdata-out-path)
  (with-open-file (st nr-in-path)
    (let* ((data (cl-json:decode-json st))
           (actual-data (cdr (assoc :+BERTHDATA+ data)))
           (ret (make-hash-table :test #'equal)))
      (dolist (rec actual-data)
        (let* ((step-type (nilify-zeros (cdr (assoc :+STEPTYPE+ rec))))
               (from-berth (nilify-zeros (cdr (assoc :+FROMBERTH+ rec))))
               (to-berth (nilify-zeros (cdr (assoc :+TOBERTH+ rec))))
               (stanox (nilify-zeros (cdr (assoc :+STANOX+ rec))))
               (event-type (nilify-zeros (cdr (assoc :+EVENT+ rec))))
               (platform (nilify-zeros (cdr (assoc :+PLATFORM+ rec))))
               (to-line (nilify-zeros (cdr (assoc :+TOLINE+ rec))))
               (from-line (nilify-zeros (cdr (assoc :+FROMLINE+ rec))))
               (offset (parse-integer (cdr (assoc :+BERTHOFFSET+ rec))))
               (area (nilify-zeros (cdr (assoc :+TD+ rec))))
               (comment (nilify-zeros (cdr (assoc :+COMMENT+ rec))))
               (key (list area from-berth to-berth step-type)))
          (when (and area offset stanox event-type step-type
                     (member area *xr-smart-areas* :test #'equal))
            (alexandria:ensure-gethash key ret)
            (push `((:stanox . ,stanox)
                    (:event-type . ,event-type)
                    (:platform . ,platform)
                    (:to-line . ,to-line)
                    (:from-line . ,from-line)
                    (:offset . ,offset)
                    (:comment . ,comment))
                  (gethash key ret)))))
      (with-open-file (out refdata-out-path
                           :direction :output
                           :if-exists :supersede
                           :element-type 'character)
        (write
         (sort
          (alexandria:hash-table-alist ret)
          #'list-string<
          :key #'car)
         :case :downcase
         :stream out))
      ret)))

(defun translate-smart-event-type (type)
  (cond
    ((string= type "A")
     (values "Arrived at" "up"))
    ((string= type "B")
     (values "Departed" "up"))
    ((string= type "C")
     (values "Arrived at" "down"))
    ((string= type "D")
     (values "Departed" "down"))
    (t (error "Invalid SMART event type ~A" type))))

(defun translate-smart-data (smart-data)
  "Translates a set of raw SMART entries to a more useful list."
  (mapcar
   (lambda (entry)
     (let* ((stanox (cdr (assoc :stanox entry)))
            (station-name
              (or
               (lookup-stanox stanox)
               (format nil "STANOX ~A" stanox)))
            (platform (cdr (assoc :platform entry)))
            (event-type (cdr (assoc :event-type entry)))
            (translated-event-type
              (multiple-value-list (translate-smart-event-type event-type)))
            (arrival-p (member event-type
                               ;; A -> Arrive Up; C -> Arrive Down
                               '("A" "C") :test #'string=))
            (to-line (cdr (assoc :to-line entry)))
            (from-line (cdr (assoc :from-line entry)))
            (description
              (format nil "~A ~A~@[ platform ~A~] (~A~@[ to ~A~]~@[ from ~A~])"
                      (first translated-event-type)
                      station-name
                      platform
                      (second translated-event-type)
                      to-line
                      from-line)))
       (list (if arrival-p :arrive :depart)
             station-name
             (or
              platform
              (when (or to-line from-line)
                (format nil "~A ~A~A~A"
                        (second translated-event-type)
                        (or to-line "")
                        (if from-line "/" "-")
                        (or from-line "-")))
              (second translated-event-type))
             (cdr (assoc :offset entry))
             description)))
   (sort smart-data
         #'<
         :key (lambda (x) (cdr (assoc :offset x))))))

(defun smart-operations-for-step (area step-type to-berth from-berths &key previous-area)
  "Returns a list of SMART operations if a SMART event corresponds to a movement from any of the berths in FROM-BERTHS to TO-BERTH. (If FROM-BERTHS is left unspecified, the movement is treated as an interpose.)"
  (flet ((lookup (from to kind &optional override-area)
           (let ((data (gethash (list (or override-area area) from to kind)
                                *xr-smart-data*)))
             (when data
               (translate-smart-data data)))))
    (when (string= step-type "CC") ; interpose
      (return-from smart-operations-for-step
        (or
         (lookup nil to-berth "I")
         ;; When moving into a new area, we might get the interpose from
         ;; the new area before we get the cancel from the old.
         ;; We therefore treat an interpose as a cancel of the previous
         ;; area's berth if that produces a useful result.
         (lookup (first from-berths) nil "C" previous-area))))
    (when (and
           (equal to-berth "XXCB") ; HACK(eta): used for CB messages
           from-berths)
      (return-from smart-operations-for-step
        (lookup (first from-berths) nil "C")))
    (or
     ;; The most common: movement between two berths
     (lookup (first from-berths) to-berth "B")
     ;; Movement into a berth from anywhere
     (lookup nil to-berth "T")
     ;; Movement from a berth anywhere
     (lookup (first from-berths) nil "F")
     ;; Movement from a berth via a bunch of intermediate berths
     ;; NOTE(eta): This is timed wrongly!
     (lookup (car (last from-berths)) to-berth "D")
     ;; Movement from a berth via a bunch of intermediate berths
     (lookup (car (last from-berths)) to-berth "E")
     ;; In case something went wrong: intermediate or between movements for
     ;; *any* of the berths in FROM-BERTHS
     (let ((fallbacks
             (loop for berth in (cdr from-berths)
                   append (list
                           ;; The way it's supposed to happen
                           (list berth to-berth "D")
                           (list berth to-berth "E")
                           ;; A missed between
                           (list berth to-berth "B")))))
       (loop
         for args in fallbacks
         do (alexandria:when-let ((ret (apply #'lookup args)))
              (log:warn "used SMART fallback entry ~A for ~A -> ~A (area ~A)"
                        args from-berths to-berth area)
              (return-from smart-operations-for-step ret)))))))

(defun read-station-berths-table (path)
  (with-open-file (stream path)
    (loop
      for (station pfm berth)
      in (mapcan (function cdr) (cl-json:decode-json stream))
      collect (cons berth (format nil "At ~A Platform ~A" station pfm)))))

(defparameter *smart-station-descriptions-table*
  (alexandria:alist-hash-table
   (read-station-berths-table "./refdata/station-berths.json")
   :test 'equal))
