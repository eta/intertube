(in-package :intertube-web)

(defmacro html-str (&body body)
  `(who:with-html-output-to-string (*standard-output* nil
                                    :prologue nil)
     ,@body))

(defparameter *dropdown-direction-map*
  '(("N"
     ("edgware-bank" . "Edgware")
     ("edgware-cx" . "Edgware")
     ("high-barnet-bank" . "High Barnet")
     ("high-barnet-cx" . "High Barnet")
     ("morden-edgware-bank" . "Morden")
     ("morden-edgware-cx" . "Morden")
     ("morden-high-barnet-bank" . "Morden")
     ("morden-high-barnet-cx" . "Morden"))
    ("M"
     ("westbound-amersham" . "Amersham")
     ("westbound-watford" . "Watford")
     ("westbound-uxbridge" . "Uxbridge")
     ("eastbound-amersham" . "Aldgate")
     ("eastbound-watford" . "Aldgate")
     ("eastbound-uxbridge" . "Aldgate"))
    ("D"
     ("ealing-upminster" . "Ealing Broadway")
     ("richmond-upminster" . "Richmond")
     ("wimbledon-edgware" . "Wimbledon")
     ("wimbledon-upminster" . "Wimbledon")
     ("richmond-edgware" . "Richmond")
     ("upminster-ealing" . "Upminster")
     ("upminster-richmond" . "Upminster")
     ("edgware-wimbledon" . "Edgware Road")
     ("upminster-wimbledon" . "Upminster")
     ("edgware-richmond" . "Edgware Road"))
    ("C"
     ("hainault-newbury-ealing" . "Hainault")
     ("hainault-woodford-ealing" . "Hainault")
     ("hainault-newbury-ruislip" . "Hainault")
     ("hainault-woodford-ruislip" . "Hainault")
     ("epping-ealing" . "Epping")
     ("epping-ruislip" . "Epping")
     ("ruislip-newbury-hainault" . "West Ruislip")
     ("ruislip-woodford-hainault" . "West Ruislip")
     ("ruislip-epping" . "West Ruislip")
     ("ealing-newbury-hainault" . "Ealing Broadway")
     ("ealing-woodford-hainault" . "Ealing Broadway")
     ("ealing-epping" . "Ealing Broadway"))
    ("J"
     ("eastbound" . "Stratford")
     ("westbound" . "Stanmore"))
    ("X"
     ("shenfield-reading" . "Shenfield")
     ("shenfield-heathrow" . "Shenfield")
     ("abbey-wood-reading" . "Abbey Wood")
     ("abbey-wood-heathrow" . "Abbey Wood")
     ("reading-shenfield" . "Reading")
     ("reading-abbey-wood" . "Reading")
     ("heathrow-shenfield" . "Heathrow")
     ("heathrow-abbey-wood" . "Heathrow"))
    ("P"
     ("eastbound-uxbridge" . "Cockfosters")
     ("eastbound-heathrow" . "Cockfosters")
     ("westbound-uxbridge" . "Uxbridge")
     ("westbound-heathrow" . "Heathrow Airport"))
    ("V"
     ("northbound" . "Walthamstow")
     ("southbound" . "Brixton"))
    ("A"
     ("eastbound" . "Barking")
     ("westbound" . "Hammersmith"))
    ("H"
     ("clockwise" . "clockwise")
     ("anticlockwise" . "anticlockwise"))
    ("W"
     ("eastbound" . "Bank")
     ("westbound" . "Waterloo"))
    ("B"
     ("eastbound" . "Elephant and Castle")
     ("westbound" . "Harrow and Wealdstone"))))

;; FIXME(eta): This entire thing is very copypasta, and need not be!
(defparameter *line-model-data*
  `(("M"
     ("westbound-amersham"
      "Westbound <i>towards Amersham</i>"
      ("Aldgate" 2 3)
      ("Liverpool Street" 2)
      ("Moorgate" 2)
      ("Barbican" 2)
      ("Farringdon" 2)
      ("King's Cross St. Pancras" 1)
      ("Euston Square" 1)
      ("Great Portland Street" 1)
      ("Baker Street" 1 2 4)
      ("Finchley Road" 1)
      ("Wembley Park" 1 2)
      ("Preston Road" 1)
      ("Northwick Park" 1)
      ("Harrow-on-the-Hill" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=westbound-uxbridge#middle-switcher"
           :id "middle-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Uxbridge instead")))
      ("North Harrow" 1)
      ("Pinner" 1)
      ("Northwood Hills" 1)
      ("Northwood" 1)
      ("Moor Park" 3)
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=westbound-watford#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Watford instead")))
      ("Rickmansworth" 1)
      ("Chorleywood" 1)
      ("Chalfont & Latimer" 1)
      ("Amersham" 1 2 3))
     ("westbound-watford"
      "Westbound <i>towards Watford</i>"
      ("Aldgate" 2 3)
      ("Liverpool Street" 2)
      ("Moorgate" 2)
      ("Barbican" 2)
      ("Farringdon" 2)
      ("King's Cross St. Pancras" 1)
      ("Euston Square" 1)
      ("Great Portland Street" 1)
      ("Baker Street" 1 2 4)
      ("Finchley Road" 1)
      ("Wembley Park" 1 2)
      ("Preston Road" 1)
      ("Northwick Park" 1)
      ("Harrow-on-the-Hill" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=westbound-uxbridge#middle-switcher"
           :id "middle-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Uxbridge instead")))
      ("North Harrow" 1)
      ("Pinner" 1)
      ("Northwood Hills" 1)
      ("Northwood" 1)
      ("Moor Park" 3)
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=westbound-amersham#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Amersham instead")))
      ("Croxley" 1)
      ("Watford" 1 2))
     ("westbound-uxbridge"
      "Westbound <i>towards Uxbridge</i>"
      ("Aldgate" 2 3)
      ("Liverpool Street" 2)
      ("Moorgate" 2)
      ("Barbican" 2)
      ("Farringdon" 2)
      ("King's Cross St. Pancras" 1)
      ("Euston Square" 1)
      ("Great Portland Street" 1)
      ("Baker Street" 1 2 4)
      ("Finchley Road" 1)
      ("Wembley Park" 1 2)
      ("Preston Road" 1)
      ("Northwick Park" 1)
      ("Harrow-on-the-Hill" 4)
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=westbound-amersham#middle-switcher"
           :id "middle-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes to Amersham and Watford instead")))
      ("West Harrow" 1)
      ("Rayners Lane" 1)
      ("Eastcote" 1)
      ("Ruislip Manor" 1)
      ("Ruislip" 1)
      ("Ickenham" 1)
      ("Hillingdon" 1)
      ("Uxbridge" 1 2 3 4))
     ("eastbound-amersham"
      "Eastbound <i>towards Aldgate via Amersham</i>"
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=eastbound-watford#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Watford ")
          (:a
           :href "/line/M?route=eastbound-uxbridge#west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Uxbridge")))
      ("Amersham" 1 2 3)
      ("Chalfont & Latimer" 2)
      ("Chorleywood" 2)
      ("Rickmansworth" 2)
      ("Moor Park" 2 4)
      ("Northwood" 2)
      ("Northwood Hills" 2)
      ("Pinner" 2)
      ("North Harrow" 2)
      ("Harrow-on-the-Hill" 5 6)
      ("Northwick Park" 2)
      ("Preston Road" 2)
      ("Wembley Park" 5 6)
      ("Finchley Road" 4)
      ("Baker Street" 3)
      ("Great Portland Street" 2)
      ("Euston Square" 2)
      ("King's Cross St. Pancras" 2)
      ("Farringdon" 1)
      ("Barbican" 1)
      ("Moorgate" 1)
      ("Liverpool Street" 1)
      ("Aldgate" 2 3))
     ("eastbound-watford"
      "Eastbound <i>towards Aldgate via Watford</i>"
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=eastbound-amersham#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Amersham ")
          (:a
           :href "/line/M?route=eastbound-uxbridge#second-west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Uxbridge")))
      ("Watford" 1 2)
      ("Croxley" 2)
      ("Moor Park" 2 4)
      ("Northwood" 2)
      ("Northwood Hills" 2)
      ("Pinner" 2)
      ("North Harrow" 2)
      ("Harrow-on-the-Hill" 5 6)
      ("Northwick Park" 2)
      ("Preston Road" 2)
      ("Wembley Park" 5 6)
      ("Finchley Road" 4)
      ("Baker Street" 3)
      ("Great Portland Street" 2)
      ("Euston Square" 2)
      ("King's Cross St. Pancras" 2)
      ("Farringdon" 1)
      ("Barbican" 1)
      ("Moorgate" 1)
      ("Liverpool Street" 1)
      ("Aldgate" 2 3))
     ("eastbound-uxbridge"
      "Eastbound <i>towards Aldgate via Uxbridge</i>"
      (:html
       ,(html-str
          (:a
           :href "/line/M?route=eastbound-amersham#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Amersham ")
          (:a
           :href "/line/M?route=eastbound-watford#second-west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Watford")))
      ("Uxbridge" 1 2 3 4)
      ("Hillingdon" 2)
      ("Ickenham" 2)
      ("Ruislip" 2)
      ("Ruislip Manor" 2)
      ("Eastcote" 2)
      ("Rayners Lane" 2)
      ("West Harrow" 2)
      ("Harrow-on-the-Hill" 5 6)
      ("Northwick Park" 2)
      ("Preston Road" 2)
      ("Wembley Park" 5 6)
      ("Finchley Road" 4)
      ("Baker Street" 3)
      ("Great Portland Street" 2)
      ("Euston Square" 2)
      ("King's Cross St. Pancras" 2)
      ("Farringdon" 1)
      ("Barbican" 1)
      ("Moorgate" 1)
      ("Liverpool Street" 1)
      ("Aldgate" 2 3)))
    ("W"
     ("eastbound"
      "Eastbound to Bank"
      ("Waterloo" 25)
      ("Bank" 7 8))
     ("westbound"
      "Westbound to Waterloo"
      ("Bank" 7 8)
      ("Waterloo" 26)))
    ("D"
     ("ealing-upminster"
      "Westbound <i>towards Ealing Broadway via Upminster</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-edgware#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes from Edgware Road instead")))
      ("Upminster" 3 4 5)
      ("Upminster Bridge" 1)
      ("Hornchurch" 1)
      ("Elm Park" 1)
      ("Dagenham East" 1)
      ("Dagenham Heathway" 1)
      ("Becontree" 1)
      ("Upney" 1)
      ("Barking" 3 6)
      ("East Ham" 1)
      ("Upton Park" 1)
      ("Plaistow" 1)
      ("West Ham" 1)
      ("Bromley-by-Bow" 1)
      ("Bow Road" 1)
      ("Mile End" 2)
      ("Stepney Green" 1)
      ("Whitechapel" 2)
      ("Aldgate East" 1)
      ("Tower Hill" 1 2)
      ("Monument" 1)
      ("Cannon Street" 1)
      ("Mansion House" 1)
      ("Blackfriars" 1)
      ("Temple" 1)
      ("Embankment" 1)
      ("Westminster" 1)
      ("St James's Park" 1)
      ("Victoria" 1)
      ("Sloane Square" 1)
      ("South Kensington" 1)
      ("Gloucester Road" 1)
      ("Earls Court" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-upminster#wimble-switcher"
           :id "wimble-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Wimbledon instead")))
      ("West Kensington" 1)
      ("Barons Court" 1)
      ("Hammersmith" 1)
      ("Ravenscourt Park" 1)
      ("Stamford Brook" 1)
      ("Turnham Green" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=richmond-upminster#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Richmond instead")))
      ("Chiswick Park" 1)
      ("Acton Town" 1)
      ("Ealing Common" 1)
      ("Ealing Broadway" 7 8 9)
      )
     ("richmond-upminster"
      "Westbound <i>towards Richmond via Upminster</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=richmond-edgware#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Edgware Road instead")))
      ("Upminster" 3 4 5)
      ("Upminster Bridge" 1)
      ("Hornchurch" 1)
      ("Elm Park" 1)
      ("Dagenham East" 1)
      ("Dagenham Heathway" 1)
      ("Becontree" 1)
      ("Upney" 1)
      ("Barking" 3 6)
      ("East Ham" 1)
      ("Upton Park" 1)
      ("Plaistow" 1)
      ("West Ham" 1)
      ("Bromley-by-Bow" 1)
      ("Bow Road" 1)
      ("Mile End" 2)
      ("Stepney Green" 1)
      ("Whitechapel" 2)
      ("Aldgate East" 1)
      ("Tower Hill" 1 2)
      ("Monument" 1)
      ("Cannon Street" 1)
      ("Mansion House" 1)
      ("Blackfriars" 1)
      ("Temple" 1)
      ("Embankment" 1)
      ("Westminster" 1)
      ("St James's Park" 1)
      ("Victoria" 1)
      ("Sloane Square" 1)
      ("South Kensington" 1)
      ("Gloucester Road" 1)
      ("Earls Court" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-upminster#wimble-switcher"
           :id "wimble-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Wimbledon instead")))
      ("West Kensington" 1)
      ("Barons Court" 1)
      ("Hammersmith" 1)
      ("Ravenscourt Park" 1)
      ("Stamford Brook" 1)
      ("Turnham Green" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=ealing-upminster#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Ealing Broadway instead")))
      ("Gunnersbury" 1)
      ("Kew Gardens" 1)
      ("Richmond" 4 5 6 7))
     ("richmond-edgware"
      "Westbound <i>towards Richmond via Edgware Road</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=richmond-upminster#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Upminster instead")))
      ("Edgware Road" 1 2 3 4)
      ("Paddington" 1)
      ("Bayswater" 1)
      ("Notting Hill Gate" 1)
      ("High Street Kensington" 1)
      ("Earls Court" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-edgware#wimble-switcher"
           :id "wimble-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Wimbledon instead")))
      ("West Kensington" 1)
      ("Barons Court" 1)
      ("Hammersmith" 1)
      ("Ravenscourt Park" 1)
      ("Stamford Brook" 1)
      ("Turnham Green" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=ealing-upminster#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes to Ealing Broadway instead")))
      ("Gunnersbury" 1)
      ("Kew Gardens" 1)
      ("Richmond" 4 5 6 7))
     ("wimbledon-upminster"
      "Westbound <i>towards Wimbledon via Upminster</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-edgware#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Edgware Road instead")))
      ("Upminster" 3 4 5)
      ("Upminster Bridge" 1)
      ("Hornchurch" 1)
      ("Elm Park" 1)
      ("Dagenham East" 1)
      ("Dagenham Heathway" 1)
      ("Becontree" 1)
      ("Upney" 1)
      ("Barking" 3 6)
      ("East Ham" 1)
      ("Upton Park" 1)
      ("Plaistow" 1)
      ("West Ham" 1)
      ("Bromley-by-Bow" 1)
      ("Bow Road" 1)
      ("Mile End" 2)
      ("Stepney Green" 1)
      ("Whitechapel" 2)
      ("Aldgate East" 1)
      ("Tower Hill" 1 2)
      ("Monument" 1)
      ("Cannon Street" 1)
      ("Mansion House" 1)
      ("Blackfriars" 1)
      ("Temple" 1)
      ("Embankment" 1)
      ("Westminster" 1)
      ("St James's Park" 1)
      ("Victoria" 1)
      ("Sloane Square" 1)
      ("South Kensington" 1)
      ("Gloucester Road" 1)
      ("Earls Court" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=ealing-upminster#wimble-switcher"
           :id "wimble-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes to Ealing Broadway and Richmond instead")))
      ("West Brompton" 1)
      ("Fulham Broadway" 1)
      ("Parsons Green" 1)
      ("Putney Bridge" 2)
      ("East Putney" 2)
      ("Southfields" 2)
      ("Wimbledon Park" 2)
      ("Wimbledon" 1 2 3 4))
     ("wimbledon-edgware"
      "Westbound <i>towards Wimbledon via Edgware Road</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=wimbledon-upminster#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Upminster instead")))
      ("Edgware Road" 1 2 3 4)
      ("Paddington" 1)
      ("Bayswater" 1)
      ("Notting Hill Gate" 1)
      ("High Street Kensington" 1)
      ("Earls Court" 3 4)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=ealing-upminster#wimble-switcher"
           :id "wimble-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes to Ealing Broadway and Richmond instead")))
      ("West Brompton" 1)
      ("Fulham Broadway" 1)
      ("Parsons Green" 1)
      ("Putney Bridge" 2)
      ("East Putney" 2)
      ("Southfields" 2)
      ("Wimbledon Park" 2)
      ("Wimbledon" 1 2 3 4))
     ("upminster-ealing"
      "Eastbound <i>towards Upminster via Ealing Broadway</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=upminster-richmond#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Richmond ")
          (:a
           :href "/model-line?code=D&direction=upminster-wimbledon#west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Wimbledon")))
      ("Ealing Broadway" 7 8 9)
      ("Ealing Common" 2)
      ("Acton Town" 4)
      ("Chiswick Park" 2)
      ("Turnham Green" 4)
      ("Stamford Brook" 3)
      ("Ravenscourt Park" 4)
      ("Hammersmith" 4)
      ("Barons Court" 4)
      ("West Kensington" 2)
      ("Earls Court" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=edgware-wimbledon#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes to Edgware Road instead")))
      ("Gloucester Road" 3)
      ("South Kensington" 2)
      ("Sloane Square" 2)
      ("Victoria" 2)
      ("St James's Park" 2)
      ("Westminster" 2)
      ("Embankment" 2)
      ("Temple" 2)
      ("Blackfriars" 2)
      ("Mansion House" 3)
      ("Cannon Street" 2)
      ("Monument" 2)
      ("Tower Hill" 2 3)
      ("Aldgate East" 2)
      ("Whitechapel" 1)
      ("Stepney Green" 2)
      ("Mile End" 3)
      ("Bow Road" 2)
      ("Bromley-by-Bow" 2)
      ("West Ham" 2)
      ("Plaistow" 2)
      ("Upton Park" 2)
      ("East Ham" 2)
      ("Barking" 2)
      ("Upney" 2)
      ("Becontree" 2)
      ("Dagenham Heathway" 2)
      ("Dagenham East" 2)
      ("Elm Park" 2)
      ("Hornchurch" 2)
      ("Upminster Bridge" 2)
      ("Upminster" 3 4 5)
      )
     ("upminster-richmond"
      "Eastbound <i>towards Upminster via Richmond</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=upminster-ealing#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Ealing Broadway ")
          (:a
           :href "/model-line?code=D&direction=upminster-wimbledon#second-west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Wimbledon")))
      ("Richmond" 4 5 6 7)
      ("Kew Gardens" 2)
      ("Gunnersbury" 2)
      ("Turnham Green" 4)
      ("Stamford Brook" 3)
      ("Ravenscourt Park" 4)
      ("Hammersmith" 4)
      ("Barons Court" 4)
      ("West Kensington" 2)
      ("Earls Court" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=edgware-richmond#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Edgware Road instead")))
      ("Gloucester Road" 3)
      ("South Kensington" 2)
      ("Sloane Square" 2)
      ("Victoria" 2)
      ("St James's Park" 2)
      ("Westminster" 2)
      ("Embankment" 2)
      ("Temple" 2)
      ("Blackfriars" 2)
      ("Mansion House" 3)
      ("Cannon Street" 2)
      ("Monument" 2)
      ("Tower Hill" 2 3)
      ("Aldgate East" 2)
      ("Whitechapel" 1)
      ("Stepney Green" 2)
      ("Mile End" 3)
      ("Bow Road" 2)
      ("Bromley-by-Bow" 2)
      ("West Ham" 2)
      ("Plaistow" 2)
      ("Upton Park" 2)
      ("East Ham" 2)
      ("Barking" 2)
      ("Upney" 2)
      ("Becontree" 2)
      ("Dagenham Heathway" 2)
      ("Dagenham East" 2)
      ("Elm Park" 2)
      ("Hornchurch" 2)
      ("Upminster Bridge" 2)
      ("Upminster" 3 4 5)
      )
     ("edgware-richmond"
      "Eastbound <i>towards Edgware Road via Richmond</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=edgware-wimbledon#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Wimbledon instead")))
      ("Richmond" 4 5 6 7)
      ("Kew Gardens" 2)
      ("Gunnersbury" 2)
      ("Turnham Green" 4)
      ("Stamford Brook" 3)
      ("Ravenscourt Park" 4)
      ("Hammersmith" 4)
      ("Barons Court" 4)
      ("West Kensington" 2)
      ("Earls Court" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=upminster-richmond#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Upminster instead")))
      ("High Street Kensington" 2 3 4)
      ("Notting Hill Gate" 2)
      ("Bayswater" 2)
      ("Paddington" 2)
      ("Edgware Road" 1 2 3 4))
     ("upminster-wimbledon"
      "Eastbound <i>towards Upminster via Wimbledon</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=upminster-ealing#second-west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Ealing Broadway ")
          (:a
           :href "/model-line?code=D&direction=upminster-richmond#second-west-switcher"
           :id "second-west-switcher"
           :class "govuk-button !-no-bottom-margin second-jump-link"
           (who:str *git-branch-svg*)
           "or Richmond")))
      ("Wimbledon" 1 2 3 4)
      ("Wimbledon Park" 1)
      ("Southfields" 1)
      ("East Putney" 1)
      ("Putney Bridge" 1)
      ("Parsons Green" 2)
      ("Fulham Broadway" 2)
      ("West Brompton" 2)
      ("Earls Court" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=edgware-wimbledon#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Edgware Road instead")))
      ("Gloucester Road" 3)
      ("South Kensington" 2)
      ("Sloane Square" 2)
      ("Victoria" 2)
      ("St James's Park" 2)
      ("Westminster" 2)
      ("Embankment" 2)
      ("Temple" 2)
      ("Blackfriars" 2)
      ("Mansion House" 3)
      ("Cannon Street" 2)
      ("Monument" 2)
      ("Tower Hill" 2 3)
      ("Aldgate East" 2)
      ("Whitechapel" 1)
      ("Stepney Green" 2)
      ("Mile End" 3)
      ("Bow Road" 2)
      ("Bromley-by-Bow" 2)
      ("West Ham" 2)
      ("Plaistow" 2)
      ("Upton Park" 2)
      ("East Ham" 2)
      ("Barking" 2)
      ("Upney" 2)
      ("Becontree" 2)
      ("Dagenham Heathway" 2)
      ("Dagenham East" 2)
      ("Elm Park" 2)
      ("Hornchurch" 2)
      ("Upminster Bridge" 2)
      ("Upminster" 3 4 5))
     ("edgware-wimbledon"
      "Eastbound <i>towards Edgware Road via Wimbledon</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=edgware-richmond#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Richmond instead")))
      ("Wimbledon" 1 2 3 4)
      ("Wimbledon Park" 1)
      ("Southfields" 1)
      ("East Putney" 1)
      ("Putney Bridge" 1)
      ("Parsons Green" 2)
      ("Fulham Broadway" 2)
      ("West Brompton" 2)
      ("Earls Court" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=D&direction=upminster-wimbledon#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Upminster instead")))
      ("High Street Kensington" 2 3 4)
      ("Notting Hill Gate" 2)
      ("Bayswater" 2)
      ("Paddington" 2)
      ("Edgware Road" 1 2 3 4)))
    ("A"
     ("eastbound"
      "Eastbound <i>towards Barking</i>"
      ("Hammersmith" 1 2 3)
      ("Goldhawk Road" 1)
      ("Shepherd's Bush Market" 1)
      ("Wood Lane" 1)
      ("Latimer Road" 1)
      ("Ladbroke Grove" 1)
      ("Westbourne Park" 2)
      ("Royal Oak" 2)
      ("Paddington (Suburban)" 16)
      ("Edgware Road" 1)
      ("Baker Street" 5)
      ("Great Portland Street" 2)
      ("Euston Square" 2)
      ("Kings Cross St. Pancras" 2)
      ("Farringdon" 1)
      ("Barbican" 1)
      ("Moorgate" 1)
      ("Liverpool Street" 1)
      (:html
       ,(html-str
          (:a
           :href "/line/H?route=clockwise#switcher"
           :id "switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View Circle line via Aldgate instead")))
      ("Aldgate East" 2)
      ("Whitechapel" 1)
      ("Stepney Green" 2)
      ("Mile End" 3)
      ("Bow Road" 2)
      ("Bromley-by-Bow" 2)
      ("West Ham" 2)
      ("Plaistow" 2)
      ("Upton Park" 2)
      ("East Ham" 2)
      ("Barking" 3))
     ("westbound"
      "Westbound <i>towards Hammersmith</i>"
      (:html
       ,(html-str
          (:a
           :href "/line/H?route=anticlockwise#switcher"
           :id "switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View Circle line anticlockwise instead")))
      ("Barking" 6)
      ("East Ham" 1)
      ("Upton Park" 1)
      ("Plaistow" 1)
      ("West Ham" 1)
      ("Bromley-by-Bow" 1)
      ("Bow Road" 1)
      ("Mile End" 2)
      ("Stepney Green" 1)
      ("Whitechapel" 2)
      ("Aldgate East" 1)
      ("Liverpool Street" 2)
      ("Moorgate" 2)
      ("Barbican" 2)
      ("Farringdon" 2)
      ("Kings Cross St. Pancras" 1)
      ("Euston Square" 1)
      ("Great Portland Street" 1)
      ("Baker Street" 6)
      ("Edgware Road" 4)
      ("Paddington (Suburban)" 15)
      ("Royal Oak" 1)
      ("Westbourne Park" 1)
      ("Ladbroke Grove" 2)
      ("Latimer Road" 2)
      ("Wood Lane" 2)
      ("Shepherd's Bush Market" 2)
      ("Goldhawk Road" 2)
      ("Hammersmith" 1 2 3)))
    ("H"
     ("clockwise"
      "Clockwise <i>towards Edgware Road</i>"
      ("Hammersmith" 1 2 3)
      ("Goldhawk Road" 1)
      ("Shepherd's Bush Market" 1)
      ("Wood Lane" 1)
      ("Latimer Road" 1)
      ("Ladbroke Grove" 1)
      ("Westbourne Park" 2)
      ("Royal Oak" 2)
      ("Paddington (Suburban)" 16)
      ("Edgware Road" 1)
      ("Baker Street" 5)
      ("Great Portland Street" 2)
      ("Euston Square" 2)
      ("Kings Cross St. Pancras" 2)
      ("Farringdon" 1)
      ("Barbican" 1)
      ("Moorgate" 1)
      ("Liverpool Street" 1)
      (:html
       ,(html-str
          (:a
           :href "/line/A?route=eastbound#switcher"
           :id "switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View H&C to Barking instead")))
      ("Aldgate" 1)
      ("Tower Hill" 1)
      ("Monument" 1)
      ("Cannon Street" 1)
      ("Mansion House" 1)
      ("Blackfriars" 1)
      ("Temple" 1)
      ("Embankment" 1)
      ("Westminster" 1)
      ("St James's Park" 1)
      ("Victoria" 1)
      ("Sloane Square" 1)
      ("South Kensington" 1)
      ("Gloucester Road" 2)
      ("High Street Kensington" 2)
      ("Notting Hill Gate" 2)
      ("Bayswater" 2)
      ("Paddington" 2)
      ("Edgware Road" 3))
     ("anticlockwise"
      "Anticlockwise <i>towards Hammersmith</i>"
      (:html
       ,(html-str
          (:a
           :href "/line/A?route=westbound#switcher"
           :id "switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View H&C from Barking instead")))
      ("Edgware Road" 3)
      ("Paddington" 1)
      ("Bayswater" 1)
      ("Notting Hill Gate" 1)
      ("High Street Kensington" 1)
      ("Gloucester Road" 3)
      ("South Kensington" 2)
      ("Sloane Square" 2)
      ("Victoria" 2)
      ("St James's Park" 2)
      ("Westminster" 2)
      ("Embankment" 2)
      ("Temple" 2)
      ("Blackfriars" 2)
      ("Mansion House" 3)
      ("Cannon Street" 2)
      ("Monument" 2)
      ("Tower Hill" 3)
      ("Aldgate" 4)
      ("Liverpool Street" 2)
      ("Moorgate" 2)
      ("Barbican" 2)
      ("Farringdon" 2)
      ("Kings Cross St. Pancras" 1)
      ("Euston Square" 1)
      ("Great Portland Street" 1)
      ("Baker Street" 6)
      ("Edgware Road" 4)
      ("Paddington (Suburban)" 15)
      ("Royal Oak" 1)
      ("Westbourne Park" 1)
      ("Ladbroke Grove" 2)
      ("Latimer Road" 2)
      ("Wood Lane" 2)
      ("Shepherd's Bush Market" 2)
      ("Goldhawk Road" 2)
      ("Hammersmith" 1 2 3)))
    ("X"
     ("abbey-wood-reading"
      "Eastbound <i>towards Abbey Wood via Reading</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=abbey-wood-heathrow#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Heathrow Airport instead")))
      ("Reading" 12 13 14 15)
      ("Twyford" 4)
      ("Maidenhead" 4)
      ("Taplow" 4)
      ("Burnham" 2)
      ("Slough" 5)
      ("Langley" 4)
      ("West Drayton" 4)
      ("Hayes & Harlington" 4)
      ("Southall" 4)
      ("Hanwell" 3)
      ("West Ealing" 4)
      ("Ealing Broadway" 4)
      ("Acton Main Line" 3)
      ("Westbourne Park Sidings" "A" "B" "C" "E")
      ("Paddington" "A")
      ("Bond Street" "A")
      ("Tottenham Court Road" "A")
      ("Farringdon" "A")
      ("Liverpool Street" "A")
      ("Whitechapel" "A")
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=shenfield-reading#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Shenfield instead")))
      ("Canary Wharf" "A")
      ("Custom House" "A")
      ("Woolwich" "A")
      ("Abbey Wood" 3 4))
     ("shenfield-reading"
      "Eastbound <i>towards Shenfield via Reading</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=shenfield-heathrow#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Heathrow Airport instead")))
      ("Reading" 12 13 14 15)
      ("Twyford" 4)
      ("Maidenhead" 4)
      ("Taplow" 4)
      ("Burnham" 2)
      ("Slough" 5)
      ("Langley" 4)
      ("West Drayton" 4)
      ("Hayes & Harlington" 4)
      ("Southall" 4)
      ("Hanwell" 3)
      ("West Ealing" 4)
      ("Ealing Broadway" 4)
      ("Acton Main Line" 3)
      ("Westbourne Park Sidings" "A" "B" "C" "E")
      ("Paddington" "A")
      ("Bond Street" "A")
      ("Tottenham Court Road" "A")
      ("Farringdon" "A")
      ("Liverpool Street" "A")
      ("Whitechapel" "A")
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=abbey-wood-reading#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Abbey Wood instead")))
      ("Stratford" 8)
      ("Maryland" 2)
      ("Forest Gate" 2)
      ("Manor Park" 2)
      ("Ilford" 4)
      ("Seven Kings" 4)
      ("Goodmayes" 4)
      ("Chadwell Heath" 4)
      ("Romford" 5)
      ("Gidea Park" 4)
      ("Harold Wood" 4)
      ("Brentwood" 4)
      ("Shenfield" 4 5 6))
     ("shenfield-heathrow"
      "Eastbound <i>towards Shenfield via Heathrow</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=shenfield-reading#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Reading instead")))
      ("Heathrow Terminal 4" 1)
      (:html
       ,(html-str
          (:span
           :class "!-error-colour"
           "Terminals 4 and 5 are on separate branches; shown together for simplicity")))
      ("Heathrow Terminal 5" 4)
      ("Heathrow Terminals 2 & 3" 2)
      ("Hayes & Harlington" 4)
      ("Southall" 4)
      ("Hanwell" 3)
      ("West Ealing" 4)
      ("Ealing Broadway" 4)
      ("Acton Main Line" 3)
      ("Westbourne Park Sidings" "A" "B" "C" "E")
      ("Paddington" "A")
      ("Bond Street" "A")
      ("Tottenham Court Road" "A")
      ("Farringdon" "A")
      ("Liverpool Street" "A")
      ("Whitechapel" "A")
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=abbey-wood-heathrow#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Abbey Wood instead")))
      ("Stratford" 8)
      ("Maryland" 2)
      ("Forest Gate" 2)
      ("Manor Park" 2)
      ("Ilford" 4)
      ("Seven Kings" 4)
      ("Goodmayes" 4)
      ("Chadwell Heath" 4)
      ("Romford" 5)
      ("Gidea Park" 4)
      ("Harold Wood" 4)
      ("Brentwood" 4)
      ("Shenfield" 4 5 6))
     ("abbey-wood-heathrow"
      "Eastbound <i>towards Abbey Wood via Heathrow</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=abbey-wood-reading#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Reading instead")))
      ("Heathrow Terminal 4" 1)
      (:html
       ,(html-str
          (:span
           :class "!-error-colour"
           "Terminals 4 and 5 are on separate branches; shown together for simplicity")))
      ("Heathrow Terminal 5" 4)
      ("Heathrow Terminals 2 & 3" 2)
      ("Hayes & Harlington" 4)
      ("Southall" 4)
      ("Hanwell" 3)
      ("West Ealing" 4)
      ("Ealing Broadway" 4)
      ("Acton Main Line" 3)
      ("Westbourne Park Sidings" "A" "B" "C" "E")
      ("Paddington" "A")
      ("Bond Street" "A")
      ("Tottenham Court Road" "A")
      ("Farringdon" "A")
      ("Liverpool Street" "A")
      ("Whitechapel" "A")
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=shenfield-heathrow#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Shenfield instead")))
      ("Canary Wharf" "A")
      ("Custom House" "A")
      ("Woolwich" "A")
      ("Abbey Wood" 3 4))
     ("reading-abbey-wood"
      "Westbound <i>towards Reading via Abbey Wood</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=reading-shenfield#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Shenfield instead")))
      ("Abbey Wood" 3 4)
      ("Woolwich" "B")
      ("Custom House" "B")
      ("Canary Wharf" "B")
      ("Whitechapel" "B")
      ("Liverpool Street" "B")
      ("Farringdon" "B")
      ("Tottenham Court Road" "B")
      ("Bond Street" "B")
      ("Paddington" "B")
      ("Westbourne Park Sidings" "A" "B" "C" "W")
      ("Acton Main Line" 4)
      ("Ealing Broadway" 3)
      ("West Ealing" 3)
      ("Hanwell" 2)
      ("Southall" 3)
      ("Hayes & Harlington" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=heathrow-abbey-wood#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Heathrow Airport instead")))
      ("West Drayton" 3)
      ("Iver" 3)
      ("Langley" 3)
      ("Slough" 4)
      ("Burnham" 1)
      ("Taplow" 3)
      ("Maidenhead" 3)
      ("Twyford" 3)
      ("Reading" 12 13 14 15))
     ("reading-shenfield"
      "Westbound <i>towards Reading via Shenfield</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=reading-abbey-wood#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Abbey Wood instead")))
      ("Shenfield" 4 5 6)
      ("Brentwood" 3)
      ("Harold Wood" 3)
      ("Gidea Park" 3)
      ("Romford" 4)
      ("Chadwell Heath" 3)
      ("Goodmayes" 3)
      ("Seven Kings" 3)
      ("Ilford" 3)
      ("Manor Park" 1)
      ("Forest Gate" 1)
      ("Maryland" 1)
      ("Stratford" 5)
      ("Whitechapel" "B")
      ("Liverpool Street" "B")
      ("Farringdon" "B")
      ("Tottenham Court Road" "B")
      ("Bond Street" "B")
      ("Paddington" "B")
      ("Westbourne Park Sidings" "A" "B" "C" "W")
      ("Acton Main Line" 4)
      ("Ealing Broadway" 3)
      ("West Ealing" 3)
      ("Hanwell" 2)
      ("Southall" 3)
      ("Hayes & Harlington" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=heathrow-shenfield#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Heathrow Airport instead")))
      ("West Drayton" 3)
      ("Iver" 3)
      ("Langley" 3)
      ("Slough" 4)
      ("Burnham" 1)
      ("Taplow" 3)
      ("Maidenhead" 3)
      ("Twyford" 3)
      ("Reading" 12 13 14 15))
     ("heathrow-abbey-wood"
      "Westbound <i>towards Heathrow via Abbey Wood</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=heathrow-shenfield#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Shenfield instead")))
      ("Abbey Wood" 3 4)
      ("Woolwich" "B")
      ("Custom House" "B")
      ("Canary Wharf" "B")
      ("Whitechapel" "B")
      ("Liverpool Street" "B")
      ("Farringdon" "B")
      ("Tottenham Court Road" "B")
      ("Bond Street" "B")
      ("Paddington" "B")
      ("Westbourne Park Sidings" "A" "B" "C" "W")
      ("Acton Main Line" 4)
      ("Ealing Broadway" 3)
      ("West Ealing" 3)
      ("Hanwell" 2)
      ("Southall" 3)
      ("Hayes & Harlington" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=reading-abbey-wood#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Reading instead")))
      ("Heathrow Terminals 2 & 3" 1)
      ("Heathrow Terminal 4" 1)
      (:html
       ,(html-str
          (:span
           :class "!-error-colour"
           "Terminals 4 and 5 are on separate branches; shown together for simplicity")))
      ("Heathrow Terminal 5" 4))
     ("heathrow-shenfield"
      "Westbound <i>towards Heathrow via Shenfield</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=heathrow-abbey-wood#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Abbey Wood instead")))
      ("Shenfield" 4 5 6)
      ("Brentwood" 3)
      ("Harold Wood" 3)
      ("Gidea Park" 3)
      ("Romford" 4)
      ("Chadwell Heath" 3)
      ("Goodmayes" 3)
      ("Seven Kings" 3)
      ("Ilford" 3)
      ("Manor Park" 1)
      ("Forest Gate" 1)
      ("Maryland" 1)
      ("Stratford" 5)
      ("Whitechapel" "B")
      ("Liverpool Street" "B")
      ("Farringdon" "B")
      ("Tottenham Court Road" "B")
      ("Bond Street" "B")
      ("Paddington" "B")
      ("Westbourne Park Sidings" "A" "B" "C" "W")
      ("Acton Main Line" 4)
      ("Ealing Broadway" 3)
      ("West Ealing" 3)
      ("Hanwell" 2)
      ("Southall" 3)
      ("Hayes & Harlington" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=X&direction=reading-shenfield#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Reading instead")))
      ("Heathrow Terminals 2 & 3" 1)
      ("Heathrow Terminal 4" 1)
      (:html
       ,(html-str
          (:span
           :class "!-error-colour"
           "Terminals 4 and 5 are on separate branches; shown together for simplicity")))
      ("Heathrow Terminal 5" 4)))
    ("N"
     ("edgware-bank"
      "Northbound <i>towards Edgware via Bank</i>"
      ("Morden" 1 2 3 4 5)
      ("South Wimbledon" 1)
      ("Colliers Wood" 1)
      ("Tooting Broadway" 1)
      ("Tooting Bec" 1)
      ("Balham" 1)
      ("Clapham South" 1)
      ("Clapham Common" 1)
      ("Clapham North" 1)
      ("Stockwell" 2)
      ("Oval" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=edgware-cx#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Charing Cross branch")))
      ("Kennington" 3)
      ("Elephant and Castle" 1)
      ("Borough" 1)
      ("London Bridge" 1)
      ("Bank" 4)
      ("Moorgate" 7)
      ("Old Street" 1)
      ("Angel" 2)
      ("King's Cross St. Pancras" 7)
      ("Euston" 3)
      (:html
      ,(html-str
         (:a
          :href "/model-line?code=N&direction=high-barnet-bank#north-switcher"
          :id "north-switcher"
          :class "govuk-button !-no-bottom-margin"
          (who:str *git-branch-svg*)
          "View route to High Barnet instead")))
      ("Camden Town" 1)
      ("Chalk Farm" 1)
      ("Belsize Park" 1)
      ("Hampstead" 1)
      ("Golders Green" 2)
      ("Brent Cross" 1)
      ("Hendon Central" 1)
      ("Colindale" 1)
      ("Burnt Oak" 1)
      ("Edgware" 1 2 3)
      )
     ("edgware-cx"
      "Northbound <i>towards Edgware via Charing Cross</i>"
      ("Morden" 1 2 3 4 5)
      ("South Wimbledon" 1)
      ("Colliers Wood" 1)
      ("Tooting Broadway" 1)
      ("Tooting Bec" 1)
      ("Balham" 1)
      ("Clapham South" 1)
      ("Clapham Common" 1)
      ("Clapham North" 1)
      ("Stockwell" 2)
      ("Oval" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=edgware-bank#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Bank branch")))
      ("Kennington" 2)
      ("Waterloo" 1)
      ("Embankment" 3)
      ("Charing Cross" 5)
      ("Leicester Square" 4)
      ("Tottenham Court Road" 3)
      ("Goodge Street" 1)
      ("Warren Street" 1)
      ("Euston" 1)
      ("Mornington Crescent" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=high-barnet-cx#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to High Barnet instead")))
      ("Camden Town" 1)
      ("Chalk Farm" 1)
      ("Belsize Park" 1)
      ("Hampstead" 1)
      ("Golders Green" 2)
      ("Brent Cross" 1)
      ("Hendon Central" 1)
      ("Colindale" 1)
      ("Burnt Oak" 1)
      ("Edgware" 1 2 3)
      )
     ("high-barnet-bank"
      "Northbound <i>towards High Barnet and Mill Hill East via Bank</i>"
      ("Morden" 1 2 3 4 5)
      ("South Wimbledon" 1)
      ("Colliers Wood" 1)
      ("Tooting Broadway" 1)
      ("Tooting Bec" 1)
      ("Balham" 1)
      ("Clapham South" 1)
      ("Clapham Common" 1)
      ("Clapham North" 1)
      ("Stockwell" 2)
      ("Oval" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=high-barnet-cx#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Charing Cross branch")))
      ("Kennington" 3)
      ("Elephant and Castle" 1)
      ("Borough" 1)
      ("London Bridge" 1)
      ("Bank" 4)
      ("Moorgate" 7)
      ("Old Street" 1)
      ("Angel" 2)
      ("King's Cross St. Pancras" 7)
      ("Euston" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=edgware-bank#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Edgware instead")))
      ("Camden Town" 3)
      ("Kentish Town" 1)
      ("Tufnell Park" 1)
      ("Archway" 1)
      ("Highgate" 1)
      ("East Finchley" 1)
      ("Finchley Central" 2)
      ("West Finchley" 1)
      ("Woodside Park" 1)
      ("Totteridge & Whetstone" 1)
      ("High Barnet" 1 2 3)
      )
     ("high-barnet-cx"
      "Northbound <i>towards High Barnet and Mill Hill East via Charing Cross</i>"
      ("Morden" 1 2 3 4 5)
      ("South Wimbledon" 1)
      ("Colliers Wood" 1)
      ("Tooting Broadway" 1)
      ("Tooting Bec" 1)
      ("Balham" 1)
      ("Clapham South" 1)
      ("Clapham Common" 1)
      ("Clapham North" 1)
      ("Stockwell" 2)
      ("Oval" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=high-barnet-bank#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Bank branch")))
      ("Kennington" 2)
      ("Waterloo" 1)
      ("Embankment" 3)
      ("Charing Cross" 5)
      ("Leicester Square" 4)
      ("Tottenham Court Road" 3)
      ("Goodge Street" 1)
      ("Warren Street" 1)
      ("Euston" 1)
      ("Mornington Crescent" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=edgware-cx#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Edgware instead")))
      ("Camden Town" 3)
      ("Kentish Town" 1)
      ("Tufnell Park" 1)
      ("Archway" 1)
      ("Highgate" 1)
      ("East Finchley" 1)
      ("Finchley Central" 2)
      ("West Finchley" 1)
      ("Woodside Park" 1)
      ("Totteridge & Whetstone" 1)
      ("High Barnet" 1 2 3)
      )
     ("morden-edgware-bank"
      "Southbound <i>towards Morden via Bank and Edgware</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-high-barnet-bank#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from High Barnet instead")))
      ("Edgware" 1 2 3)
      ("Burnt Oak" 2)
      ("Colindale" 2)
      ("Hendon Central" 2)
      ("Brent Cross" 2)
      ("Golders Green" 3 5)
      ("Hampstead" 2)
      ("Belsize Park" 2)
      ("Chalk Farm" 2)
      ("Camden Town" 2)
      ("Euston" 6)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-edgware-cx#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Charing Cross branch")))
      ("King's Cross St. Pancras" 8)
      ("Angel" 1)
      ("Old Street" 2)
      ("Moorgate" 8)
      ("Bank" 3)
      ("London Bridge" 2)
      ("Borough" 2)
      ("Elephant and Castle" 2)
      ("Kennington" 4)
      ("Oval" 2)
      ("Stockwell" 3)
      ("Clapham North" 2)
      ("Clapham Common" 2)
      ("Clapham South" 2)
      ("Balham" 2)
      ("Tooting Bec" 2)
      ("Tooting Broadway" 2)
      ("Colliers Wood" 2)
      ("South Wimbledon" 2)
      ("Morden" 1 2 3 4 5)
      )
     ("morden-edgware-cx"
      "Southbound <i>towards Morden via Charing Cross and Edgware</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-high-barnet-cx#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from High Barnet instead")))
      ("Edgware" 1 2 3)
      ("Burnt Oak" 2)
      ("Colindale" 2)
      ("Hendon Central" 2)
      ("Brent Cross" 2)
      ("Golders Green" 3 5)
      ("Hampstead" 2)
      ("Belsize Park" 2)
      ("Chalk Farm" 2)
      ("Camden Town" 2)
      ("Mornington Crescent" 2)
      ("Euston" 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-edgware-bank#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Bank branch")))
      ("Warren Street" 2)
      ("Goodge Street" 2)
      ("Tottenham Court Road" 4)
      ("Leicester Square" 3)
      ("Charing Cross" 6)
      ("Embankment" 4)
      ("Waterloo" 2)
      ("Kennington" 2)
      ("Oval" 2)
      ("Stockwell" 3)
      ("Clapham North" 2)
      ("Clapham Common" 2)
      ("Clapham South" 2)
      ("Balham" 2)
      ("Tooting Bec" 2)
      ("Tooting Broadway" 2)
      ("Colliers Wood" 2)
      ("South Wimbledon" 2)
      ("Morden" 1 2 3 4 5)
      )
     ("morden-high-barnet-bank"
      "Southbound <i>towards Morden via Bank and High Barnet</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-edgware-bank#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Edgware instead")))
      ("High Barnet" 1 2 3)
      ("Totteridge & Whetstone" 2)
      ("Woodside Park" 2)
      ("West Finchley" 2)
      ("Finchley Central" 3)
      ("East Finchley" 4)
      ("Highgate" 2)
      ("Archway" 2)
      ("Tufnell Park" 2)
      ("Kentish Town" 2)
      ("Camden Town" 4)
      ("Euston" 6)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-high-barnet-cx#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Charing Cross branch")))
      ("King's Cross St. Pancras" 8)
      ("Angel" 1)
      ("Old Street" 2)
      ("Moorgate" 8)
      ("Bank" 3)
      ("London Bridge" 2)
      ("Borough" 2)
      ("Elephant and Castle" 2)
      ("Kennington" 4)
      ("Oval" 2)
      ("Stockwell" 3)
      ("Clapham North" 2)
      ("Clapham Common" 2)
      ("Clapham South" 2)
      ("Balham" 2)
      ("Tooting Bec" 2)
      ("Tooting Broadway" 2)
      ("Colliers Wood" 2)
      ("South Wimbledon" 2)
      ("Morden" 1 2 3 4 5)
      )
     ("morden-high-barnet-cx"
      "Southbound <i>towards Morden via Charing Cross and High Barnet</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-edgware-cx#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Edgware instead")))
      ("High Barnet" 1 2 3)
      ("Totteridge & Whetstone" 2)
      ("Woodside Park" 2)
      ("West Finchley" 2)
      ("Finchley Central" 3)
      ("East Finchley" 4)
      ("Highgate" 2)
      ("Archway" 2)
      ("Tufnell Park" 2)
      ("Kentish Town" 2)
      ("Camden Town" 4)
      ("Mornington Crescent" 2)
      ("Euston" 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=N&direction=morden-high-barnet-bank#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "Switch to Bank branch")))
      ("Warren Street" 2)
      ("Goodge Street" 2)
      ("Tottenham Court Road" 4)
      ("Leicester Square" 3)
      ("Charing Cross" 6)
      ("Embankment" 4)
      ("Waterloo" 2)
      ("Kennington" 2)
      ("Oval" 2)
      ("Stockwell" 3)
      ("Clapham North" 2)
      ("Clapham Common" 2)
      ("Clapham South" 2)
      ("Balham" 2)
      ("Tooting Bec" 2)
      ("Tooting Broadway" 2)
      ("Colliers Wood" 2)
      ("South Wimbledon" 2)
      ("Morden" 1 2 3 4 5)
      ))
    ("C"
     ("hainault-newbury-ealing"
      "Eastbound <i>towards Hainault via Newbury Park and Ealing</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ruislip#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from West Ruislip instead")))
      ("Ealing Broadway" 5 6)
      ("West Acton" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ealing#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes via Woodford (to Hainault and Epping) instead")))
      ("Wanstead" 2)
      ("Redbridge" 2)
      ("Gants Hill" 2)
      ("Newbury Park" 2)
      ("Barkingside" 2)
      ("Fairlop" 2)
      ("Hainault" 2 3)
      )
     ("hainault-woodford-ealing"
      "Eastbound <i>towards Hainault via Woodford and Ealing</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ruislip#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from West Ruislip instead")))
      ("Ealing Broadway" 5 6)
      ("West Acton" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ealing#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Snaresbrook" 2)
      ("South Woodford" 2)
      ("Woodford" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=epping-ealing#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Epping instead")))
      ("Roding Valley" 2)
      ("Chigwell" 2)
      ("Grange Hill" 2)
      ("Hainault" 1)
      )

     ("hainault-newbury-ruislip"
      "Eastbound <i>towards Hainault via Newbury Park and West Ruislip</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ealing#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Ealing Broadway instead")))
      ("West Ruislip" 1 2)
      ("Ruislip Gardens" 2)
      ("South Ruislip" 2)
      ("Northolt" 2)
      ("Greenford" 3)
      ("Perivale" 2)
      ("Hanger Lane" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ruislip#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View routes via Woodford (to Hainault and Epping) instead")))
      ("Wanstead" 2)
      ("Redbridge" 2)
      ("Gants Hill" 2)
      ("Newbury Park" 2)
      ("Barkingside" 2)
      ("Fairlop" 2)
      ("Hainault" 2 3)
      )
     ("hainault-woodford-ruislip"
      "Eastbound <i>towards Hainault via Woodford and West Ruislip</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ealing#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Ealing Broadway instead")))
      ("West Ruislip" 1 2)
      ("Ruislip Gardens" 2)
      ("South Ruislip" 2)
      ("Northolt" 2)
      ("Greenford" 3)
      ("Perivale" 2)
      ("Hanger Lane" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ruislip#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Snaresbrook" 2)
      ("South Woodford" 2)
      ("Woodford" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=epping-ruislip#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Epping instead")))
      ("Roding Valley" 2)
      ("Chigwell" 2)
      ("Grange Hill" 2)
      ("Hainault" 1)
      )

     ("epping-ealing"
      "Eastbound <i>towards Epping via Ealing</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=epping-ruislip#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from West Ruislip instead")))
      ("Ealing Broadway" 5 6)
      ("West Acton" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ealing#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Snaresbrook" 2)
      ("South Woodford" 2)
      ("Woodford" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ealing#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Hainault instead")))
      ("Buckhurst Hill" 2)
      ("Loughton" 4)
      ("Debden" 2)
      ("Theydon Bois" 2)
      ("Epping" 1 2)
      )
     ("epping-ruislip"
      "Eastbound <i>towards Epping via West Ruislip</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=epping-ealing#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Ealing Broadway instead")))
      ("West Ruislip" 1 2)
      ("Ruislip Gardens" 2)
      ("South Ruislip" 2)
      ("Northolt" 2)
      ("Greenford" 3)
      ("Perivale" 2)
      ("Hanger Lane" 2)
      ("North Acton" 3)
      ("East Acton" 2)
      ("White City" 4)
      ("Shepherd's Bush" 2)
      ("Holland Park" 2)
      ("Notting Hill Gate" 3)
      ("Queensway" 2)
      ("Lancaster Gate" 2)
      ("Marble Arch" 2)
      ("Bond Street" 2)
      ("Oxford Circus" 2)
      ("Tottenham Court Road" 2)
      ("Holborn" 2)
      ("Chancery Lane" 2)
      ("St. Paul's" 2)
      ("Bank" 6)
      ("Liverpool Street" 4)
      ("Bethnal Green" 2)
      ("Mile End" 4)
      ("Stratford" 6)
      ("Leyton" 2)
      ("Leytonstone" 3)
      ;; Switcher
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-newbury-ruislip#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Snaresbrook" 2)
      ("South Woodford" 2)
      ("Woodford" 3)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=hainault-woodford-ruislip#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Hainault instead")))
      ("Buckhurst Hill" 2)
      ("Loughton" 4)
      ("Debden" 2)
      ("Theydon Bois" 2)
      ("Epping" 1 2)
      )

     ("ruislip-newbury-hainault"
      "Westbound <i>towards West Ruislip via Newbury Park and Hainault</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-epping#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Epping instead")))
      ("Hainault" 2 3)
      ("Fairlop" 1)
      ("Barkingside" 1)
      ("Newbury Park" 1)
      ("Gants Hill" 1)
      ("Redbridge" 1)
      ("Wanstead" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-woodford-hainault#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Woodford instead")))
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-newbury-hainault#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Ealing Broadway instead")))
      ("Hanger Lane" 1)
      ("Perivale" 1)
      ("Greenford" 1)
      ("Northolt" 1)
      ("South Ruislip" 1)
      ("Ruislip Gardens" 1)
      ("West Ruislip" 1 2)
      )

     ("ruislip-woodford-hainault"
      "Westbound <i>towards West Ruislip via Woodford and Hainault</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-epping#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Epping instead")))
      ("Hainault" 1)
      ("Grange Hill" 1)
      ("Chigwell" 1)
      ("Roding Valley" 1)
      ("Woodford" 2)
      ("South Woodford" 1)
      ("Snaresbrook" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-newbury-hainault#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-woodford-hainault#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Ealing Broadway instead")))
      ("Hanger Lane" 1)
      ("Perivale" 1)
      ("Greenford" 1)
      ("Northolt" 1)
      ("South Ruislip" 1)
      ("Ruislip Gardens" 1)
      ("West Ruislip" 1 2)
      )

     ("ruislip-epping"
      "Westbound <i>towards West Ruislip via Epping</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-woodford-hainault#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Hainault instead")))
      ("Epping" 1 2)
      ("Theydon Bois" 1)
      ("Debden" 1)
      ("Loughton" 1)
      ("Buckhurst Hill" 1)
      ("Woodford" 2)
      ("South Woodford" 1)
      ("Snaresbrook" 1)
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-epping#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Ealing Broadway instead")))
      ("Hanger Lane" 1)
      ("Perivale" 1)
      ("Greenford" 1)
      ("Northolt" 1)
      ("South Ruislip" 1)
      ("Ruislip Gardens" 1)
      ("West Ruislip" 1 2)
      )

     ("ealing-newbury-hainault"
      "Westbound <i>towards Ealing via Newbury Park and Hainault</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-epping#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Epping instead")))
      ("Hainault" 2 3)
      ("Fairlop" 1)
      ("Barkingside" 1)
      ("Newbury Park" 1)
      ("Gants Hill" 1)
      ("Redbridge" 1)
      ("Wanstead" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-woodford-hainault#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Woodford instead")))
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-newbury-hainault#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to West Ruislip instead")))
      ("West Acton" 1)
      ("Ealing Broadway" 5 6)
      )
     ("ealing-woodford-hainault"
      "Westbound <i>towards Ealing via Woodford and Hainault</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-epping#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Epping instead")))
      ("Hainault" 1)
      ("Grange Hill" 1)
      ("Chigwell" 1)
      ("Roding Valley" 1)
      ("Woodford" 2)
      ("South Woodford" 1)
      ("Snaresbrook" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-newbury-hainault#branch-switcher"
           :id "branch-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route via Newbury Park instead")))
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-woodford-hainault#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to West Ruislip instead")))
      ("West Acton" 1)
      ("Ealing Broadway" 5 6)
      )
     ("ealing-epping"
      "Westbound <i>towards Ealing via Epping</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ealing-woodford-hainault#east-switcher"
           :id "east-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Hainault instead")))
      ("Epping" 1 2)
      ("Theydon Bois" 1)
      ("Debden" 1)
      ("Loughton" 1)
      ("Buckhurst Hill" 1)
      ("Woodford" 2)
      ("South Woodford" 1)
      ("Snaresbrook" 1)
      ("Leytonstone" 1)
      ("Leyton" 1)
      ("Stratford" 3)
      ("Mile End" 1)
      ("Bethnal Green" 1)
      ("Liverpool Street" 5)
      ("Bank" 5)
      ("St. Paul's" 1)
      ("Chancery Lane" 1)
      ("Holborn" 1)
      ("Tottenham Court Road" 1)
      ("Oxford Circus" 1)
      ("Bond Street" 1)
      ("Marble Arch" 1)
      ("Lancaster Gate" 1)
      ("Queensway" 1)
      ("Notting Hill Gate" 4)
      ("Holland Park" 1)
      ("Shepherd's Bush" 1)
      ("White City" 1)
      ("East Acton" 1)
      ("North Acton" 1)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=C&direction=ruislip-epping#west-switcher"
           :id "west-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to West Ruislip instead")))
      ("West Acton" 1)
      ("Ealing Broadway" 5 6)
      )
     )
    ("P"
     ("eastbound-uxbridge"
      "Eastbound <i>towards Cockfosters via Uxbridge</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=P&direction=eastbound-heathrow#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Heathrow Airport instead")))
      ("Uxbridge" 1 2 3 4)
      ("Hillingdon" 2)
      ("Ickenham" 2)
      ("Ruislip" 2)
      ("Ruislip Manor" 2)
      ("Eastcote" 2)
      ("Rayners Lane" 2)
      ("South Harrow" 2)
      ("Sudbury Hill" 2)
      ("Sudbury Town" 2)
      ("Alperton" 2)
      ("Park Royal" 2)
      ("North Ealing" 2)
      ("Ealing Common" 2)
      ("Acton Town" 3 4)
      ("Turnham Green" 3)
      ("Ravenscourt Park" 3)
      ("Hammersmith" 3)
      ("Barons Court" 3)
      ("Earl's Court" 5)
      ("Gloucester Road" 5)
      ("South Kensington" 4)
      ("Knightsbridge" 1)
      ("Hyde Park Corner" 1)
      ("Green Park" 2)
      ("Piccadilly Circus" 3)
      ("Leicester Square" 2)
      ("Covent Garden" 2)
      ("Holborn" 4)
      ("Russell Square" 1)
      ("King's Cross" 6)
      ("Caledonian Road" 1)
      ("Holloway Road" 2)
      ("Arsenal" 1)
      ("Finsbury Park" 1)
      ("Manor House" 1)
      ("Turnpike Lane" 1)
      ("Wood Green" 1)
      ("Bounds Green" 1)
      ("Arnos Grove" 1 3)
      ("Southgate" 1)
      ("Oakwood" 2)
      ("Cockfosters" 1 2 3 4))
     ("eastbound-heathrow"
      "Eastbound <i>towards Cockfosters via Heathrow Airport</i>"
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=P&direction=eastbound-uxbridge#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route from Uxbridge instead")))
      ("Heathrow Terminal 5" 6)
      ("Heathrow Terminals 2 & 3" 2)
      ("Hatton Cross" 2)
      ("Hounslow West" 2)
      ("Hounslow Central" 2)
      ("Hounslow East" 2)
      ("Osterley" 2)
      ("Boston Manor" 2)
      ("Northfields" 3 4)
      ("South Ealing" 1 2)
      ("Acton Town" 3 4)
      ("Turnham Green" 3)
      ("Ravenscourt Park" 3)
      ("Hammersmith" 3)
      ("Barons Court" 3)
      ("Earl's Court" 5)
      ("Gloucester Road" 5)
      ("South Kensington" 4)
      ("Knightsbridge" 1)
      ("Hyde Park Corner" 1)
      ("Green Park" 2)
      ("Piccadilly Circus" 3)
      ("Leicester Square" 2)
      ("Covent Garden" 2)
      ("Holborn" 4)
      ("Russell Square" 1)
      ("King's Cross" 6)
      ("Caledonian Road" 1)
      ("Holloway Road" 2)
      ("Arsenal" 1)
      ("Finsbury Park" 1)
      ("Manor House" 1)
      ("Turnpike Lane" 1)
      ("Wood Green" 1)
      ("Bounds Green" 1)
      ("Arnos Grove" 1 3)
      ("Southgate" 1)
      ("Oakwood" 2)
      ("Cockfosters" 1 2 3 4))
     ("westbound-uxbridge"
      "Westbound <i>towards Uxbridge</i>"
      ("Cockfosters" 1 2 3 4)
      ("Oakwood" 1)
      ("Southgate" 2)
      ("Arnos Grove" 4)
      ("Bounds Green" 2)
      ("Wood Green" 2)
      ("Turnpike Lane" 2)
      ("Manor House" 2)
      ("Finsbury Park" 3)
      ("Arsenal" 2)
      ("Holloway Road" 1)
      ("Caledonian Road" 2)
      ("King's Cross" 5)
      ("Russell Square" 2)
      ("Holborn" 3)
      ("Covent Garden" 1)
      ("Leicester Square" 1)
      ("Piccadilly Circus" 4)
      ("Green Park" 1)
      ("Hyde Park Corner" 2)
      ("Knightsbridge" 2)
      ("South Kensington" 3)
      ("Gloucester Road" 4)
      ("Earl's Court" 6)
      ("Barons Court" 2)
      ("Hammersmith" 2)
      ("Ravenscourt Park" 2)
      ("Stamford Brook" 2)
      ("Turnham Green" 2)
      ("Acton Town" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=P&direction=westbound-heathrow#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Heathrow Airport instead")))
      ("Ealing Common" 1)
      ("North Ealing" 1)
      ("Park Royal" 1)
      ("Alperton" 1)
      ("Sudbury Town" 1)
      ("Sudbury Hill" 1)
      ("South Harrow" 1)
      ("Rayners Lane" 1)
      ("Eastcote" 1)
      ("Ruislip Manor" 1)
      ("Ruislip" 1)
      ("Ickenham" 1)
      ("Hillingdon" 1)
      ("Uxbridge" 1 2 3 4))
     ("westbound-heathrow"
      "Westbound <i>towards Heathrow Airport</i>"
      ("Cockfosters" 1 2 3 4)
      ("Oakwood" 1)
      ("Southgate" 2)
      ("Arnos Grove" 4)
      ("Bounds Green" 2)
      ("Wood Green" 2)
      ("Turnpike Lane" 2)
      ("Manor House" 2)
      ("Finsbury Park" 3)
      ("Arsenal" 2)
      ("Holloway Road" 1)
      ("Caledonian Road" 2)
      ("King's Cross" 5)
      ("Russell Square" 2)
      ("Holborn" 3)
      ("Covent Garden" 1)
      ("Leicester Square" 1)
      ("Piccadilly Circus" 4)
      ("Green Park" 1)
      ("Hyde Park Corner" 2)
      ("Knightsbridge" 2)
      ("South Kensington" 3)
      ("Gloucester Road" 4)
      ("Earl's Court" 6)
      ("Barons Court" 2)
      ("Hammersmith" 2)
      ("Ravenscourt Park" 2)
      ("Stamford Brook" 2)
      ("Turnham Green" 2)
      ("Acton Town" 1 2)
      (:html
       ,(html-str
          (:a
           :href "/model-line?code=P&direction=westbound-uxbridge#north-switcher"
           :id "north-switcher"
           :class "govuk-button !-no-bottom-margin"
           (who:str *git-branch-svg*)
           "View route to Uxbridge instead")))
      ("South Ealing" 3 4)
      ("Northfields" 1 2)
      ("Boston Manor" 1)
      ("Osterley" 1)
      ("Hounslow East" 1)
      ("Hounslow Central" 1)
      ("Hounslow West" 1)
      ("Hatton Cross" 1)
      ("Heathrow Terminal 4" 1)
      ("Heathrow Terminals 2 & 3" 1)))
    ("V"
     ("northbound"
      "Northbound <i>towards Walthamstow Central</i>"
      ("Brixton" 1 2)
      ("Stockwell" 1)
      ("Vauxhall" 1)
      ("Pimlico" 1)
      ("Victoria" 3)
      ("Green Park" 3)
      ("Oxford Circus" 6)
      ("Warren Street" 3)
      ("Euston" 4)
      ("Kings Cross St. Pancras" 3)
      ("Highbury & Islington" 3)
      ("Finsbury Park" 2)
      ("Seven Sisters" 3 4)
      ("Tottenham Hale" 1)
      ("Blackhorse Road" 1)
      ("Walthamstow Central" 1 2))
     ("southbound"
      "Southbound <i>towards Brixton</i>"
      ("Walthamstow Central" 1 2)
      ("Blackhorse Road" 2)
      ("Tottenham Hale" 2)
      ("Seven Sisters" 5)
      ("Finsbury Park" 4)
      ("Highbury & Islington" 5)
      ("Kings Cross St. Pancras" 4)
      ("Euston" 5)
      ("Warren Street" 4)
      ("Oxford Circus" 5)
      ("Green Park" 4)
      ("Victoria" 4)
      ("Pimlico" 2)
      ("Vauxhall" 2)
      ("Stockwell" 4)
      ("Brixton" 1 2)
      ))
    ("B"
     ("northbound" "Northbound <i>towards Harrow &amp; Wealdstone</i>"
      ("Elephant & Castle" 3 4)
      ("Lambeth North" 1)
      ("Waterloo" 3)
      ("Embankment" 5)
      ("Charing Cross" 1)
      ("Piccadilly Circus" 1)
      ("Oxford Circus" 4)
      ("Regents Park" 1)
      ("Baker Street" 9)
      ("Marylebone" 1)
      ("Edgware Road" 1)
      ("Paddington" 3)
      ("Warwick Avenue" 1)
      ("Maida Vale" 1)
      ("Kilburn Park" 1)
      ("Queen's Park" 3)
      ("Kensal Green" 2)
      ("Willesden Junction" 3)
      ("Harlesden" 2)
      ("Stonebridge Park" 2)
      ("Wembley Central" 1)
      ("North Wembley" 2)
      ("South Kenton" 2)
      ("Kenton" 2)
      ("Harrow & Wealdstone" 1))
     ("southbound" "Southbound <i>towards Elephant &amp; Castle</i>"
      ("Harrow & Wealdstone" 2)
      ("Kenton" 1)
      ("South Kenton" 1)
      ("North Wembley" 1)
      ("Wembley Central" 2)
      ("Stonebridge Park" 1)
      ("Harlesden" 1)
      ("Willesden Junction" 1)
      ("Kensal Green" 1)
      ("Queen's Park" 2)
      ("Kilburn Park" 2)
      ("Maida Vale" 2)
      ("Warwick Avenue" 2)
      ("Paddington" 4)
      ("Edgware Road" 2)
      ("Marylebone" 2)
      ("Baker Street" 8)
      ("Regents Park" 2)
      ("Oxford Circus" 3)
      ("Piccadilly Circus" 2)
      ("Charing Cross" 2)
      ("Embankment" 6)
      ("Waterloo" 4)
      ("Lambeth North" 2)
      ("Elephant & Castle" 3 4)))
    ("J"
     ("eastbound" "Eastbound <i>towards Stratford</i>"
      ;; Format: (station-name platforms)
      ("Stanmore" 1 2 3)
      ("Canons Park" 2)
      ("Queensbury" 2)
      ("Kingsbury" 2)
      ("Wembley Park" 4)
      ("Neasden" 3)
      ("Dollis Hill" 2)
      ("Willesden Green" 3)
      ("Kilburn" 2)
      ("West Hampstead" 2)
      ("Finchley Road" 3)
      ("Swiss Cottage" 2)
      ("St. John's Wood" 2)
      ("Baker Street" 7)
      ("Bond Street" 4)
      ("Green Park" 6)
      ("Westminster" 3)
      ("Waterloo" 6)
      ("Southwark" 2)
      ("London Bridge" 4)
      ("Bermondsey" 2)
      ("Canada Water" 2)
      ("Canary Wharf" 2)
      ("North Greenwich" 3)
      ("Canning Town" 6)
      ("West Ham" 6)
      ("Stratford" 13 14 15))
     ("westbound" "Westbound <i>towards Stanmore</i>"
      ("Stratford" 13 14 15)
      ("West Ham" 5)
      ("Canning Town" 5)
      ("North Greenwich" 1 2)
      ("Canary Wharf" 1)
      ("Canada Water" 1)
      ("Bermondsey" 1)
      ("London Bridge" 3)
      ("Southwark" 1)
      ("Waterloo" 5)
      ("Westminster" 4)
      ("Green Park" 5)
      ("Bond Street" 3)
      ("Baker Street" 10)
      ("St. John's Wood" 1)
      ("Swiss Cottage" 1)
      ("Finchley Road" 2)
      ("West Hampstead" 1)
      ("Kilburn" 1)
      ("Willesden Green" 2)
      ("Dollis Hill" 1)
      ("Neasden" 2)
      ("Wembley Park" 3)
      ("Kingsbury" 1)
      ("Queensbury" 1)
      ("Canons Park" 1)
      ("Stanmore" 1 2 3)))))
