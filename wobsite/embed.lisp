;;;; Support functions for embedding and serving static files.

(in-package :intertube-web)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun read-file-to-array (filespec)
    "Inefficiently read the file at FILESPEC into an array, and return the array."
    (let ((ret (make-array 0
                           :element-type '(unsigned-byte 8)
                           :fill-pointer 0
                           :adjustable t)))
      (with-open-file (stream filespec
                              :direction :input
                              :element-type '(unsigned-byte 8))
        (loop
          with byte
          while (setf byte (read-byte stream nil nil))
          do (vector-push-extend byte ret 1024)))
      ret)))

(defun serve-embedded-file (data-vector content-type)
  "Serve the embedded file, whose bytes are in DATA-VECTOR, with content type CONTENT-TYPE."
  (setf (hunchentoot:content-type*) content-type)
  (setf (hunchentoot:content-length*) (length data-vector))
  (setf (hunchentoot:header-out :cache-control) "max-age=31536000, immutable")
  (let ((out (hunchentoot:send-headers)))
    (write-sequence data-vector out)
    (finish-output out)))
