;;;; Tooling for drawing pretty journey diagrams

(in-package :intertube-web)

(defclass journey-item ()
  ((era
    :reader era
    :initform nil
    :initarg :era)
   (index
    :reader index
    :initform nil)))

(defclass journey-station (journey-item)
  ((start-ts
    :initarg :start-ts
    :reader start-ts
    :initform nil)
   (end-ts
    :initarg :end-ts
    :reader end-ts
    :initform nil)
   (station-name
    :initarg :station-name
    :reader station-name)
   (platform
    :initarg :platform
    :reader platform
    :initform nil)
   (line-type
    :reader line-type
    :initform nil)
   (trainp
    :reader trainp
    :initform nil
    :initarg :trainp)
   (uri
    :reader uri
    :initform nil
    :initarg :uri)
   (timing-remark
    :reader timing-remark
    :initform nil
    :initarg :timing-remark)))

(defclass journey-train (journey-item)
  ((start-ts
    :initarg :start-ts
    :reader start-ts
    :initform nil)
   (end-ts
    :initarg :end-ts
    :reader end-ts
    :initform nil)
   (progress
    :initarg :progress
    :reader progress
    :initform nil)
   (timing-remark
    :reader timing-remark
    :initform nil
    :initarg :timing-remark)
   (text
    :initarg :text
    :reader text
    :initform nil)))

(defclass journey-filler (journey-item) ())

(defun render-journey-time (time)
  (local-time:with-decoded-timestamp
      (:minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D:~2,'0D" h m)))

(defun render-journey-station (line-code item)
  (with-slots (start-ts end-ts station-name platform line-type trainp
               uri timing-remark index era)
      item
    (who:with-html-output-to-string (*standard-output* nil
                                     :prologue nil)
      (:li
       :class "live-line-station"
       :data-journey-index (who:str index)
       :id (when start-ts (format nil "ent-~A" start-ts))
       (:span
        :class "live-line-timing"
        (cond
          (timing-remark
           (who:str timing-remark))
          ((and start-ts end-ts
                (>= start-ts end-ts))
           (who:htm
            (:span
             :class "live-timing-nonstop"
             (who:str (render-journey-time start-ts)))))
          ((and start-ts end-ts trainp)
           (who:htm
            (:span
             :class "live-timing-arr"
             (who:str (render-journey-time start-ts)))
            (:span
             :class "live-timing-dep"
             "+"
             (who:esc (format-duration-small (- end-ts start-ts))))))
          ((and start-ts end-ts
                (eql start-ts end-ts))
           (who:htm
            (:span
             :class "live-timing-dep"
             (who:str (render-journey-time start-ts)))))
          ((and start-ts end-ts)
           (who:htm
            (:span
             :class "live-timing-arr"
             (who:str (render-journey-time start-ts)))
            (:span
             :class "live-timing-dep"
             (who:str (render-journey-time end-ts)))))
          (start-ts
           (who:htm
            (:span
             :class "live-timing-dep"
             (who:str (render-journey-time start-ts)))))
          ))
       (:span
        :class (format nil "live-line-bsicon ~@A"
                       (when (eql era :past) "past-era"))
        :style (format nil "color: ~A;" *accent-colour*)
        (cond
          ((eql line-type :start)
           (who:str *bsicons-bhf-start-svg*))
          ((eql line-type :end)
           (who:str *bsicons-bhf-end-svg*))
          ((and (not trainp)
                (or
                 (uiop:string-suffix-p station-name "Jn")
                 (uiop:string-suffix-p station-name "Junction")
                 (uiop:string-suffix-p station-name "Portal")
                 (and
                  (equal station-name "Westbourne Park Sidings")
                  (or (equal platform "E")
                      (equal platform "W")))))
           (who:str *bsicons-kmw-svg*))
          ((and (not trainp)
                start-ts end-ts
                (eql start-ts end-ts))
           (who:str *bsicons-str-pass-svg*))
          (t
           (who:str *bsicons-bhf-svg*)))
        (when trainp
          (who:htm
           (:span
            :class "live-line-bsicon-train"
            (who:str *metro-svg*)))))
       (:span
        :class "govuk-heading-m live-line-station-name"
        (:a
         :href (or uri
                   (when start-ts
                     (uri-for-station-timed line-code station-name start-ts))
                   (uri-for-station line-code station-name))
         :class "station-calls-link"
         (who:esc station-name))
        (when platform
          (who:htm
           (:span
            :class "!-secondary-colour"
            (if (or
                 (uiop:string-prefix-p "up" platform)
                 (uiop:string-prefix-p "down" platform))
                (progn
                  (who:str " <small>(")
                  (who:esc platform)
                  (who:str ")</small>"))
                (progn
                  (who:str " plat. ")
                  (who:esc platform)))))))))))

(defun render-journey-filler (item)
  (who:with-html-output-to-string (*standard-output* nil
                                   :prologue nil)
    (:li
     :class "line-platform-intermediate-blank"
     (:span
      :class (format nil "live-line-bsicon-intermediate ~@A"
                     (when (eql (era item) :past) "past-era"))
      :style (format nil "color: ~A;" *accent-colour*)
      (who:str *bsicons-str-svg*)))))

(defun render-journey-train (train)
  (with-slots (start-ts end-ts progress text index timing-remark) train
    (who:with-html-output-to-string (*standard-output* nil
                                     :prologue nil)
      (:li
       :class "line-platform-intermediate-filler"
       :data-journey-index (who:str index)
       (:span
        :class "live-line-bsicon-intermediate"
        :style (format nil "color: ~A;" *accent-colour*)
        (who:str *bsicons-str-svg*)))
      (:li
       :class "line-platform-intermediate"
       :data-journey-index (who:str index)
       (:span
        :class "live-line-timing"
        (:span
         :class "live-timing-dep"
         (when timing-remark
           (who:str timing-remark))
         (when (and start-ts end-ts (not timing-remark))
           (who:str "+")
           (who:esc (format-duration-small (- end-ts start-ts))))))
       (:span
        :class "live-line-bsicon"
        :style (format nil "color: ~A;" *accent-colour*)
        (who:str *bsicons-str-svg*)
        (:span
         :class "live-line-bsicon-train"
         (who:str *metro-svg*)))
       (:span
        :class "line-intermediate-descr"
        (if progress
            (who:htm
             (:progress
              :class "line-journey-progress"
              :max "100"
              :value progress))
            (who:esc text))))
      (:li
       :class "line-platform-intermediate-filler"
       :data-journey-index (who:str index)
       (:span
        :class "live-line-bsicon-intermediate"
        :style (format nil "color: ~A;" *accent-colour*)
        (who:str *bsicons-str-svg*))))))

(defun render-journey-items (line-code item-list)
  (mapcar
   (lambda (item)
     (cons
      item
      (etypecase item
        (journey-station
         (render-journey-station line-code item))
        (journey-train
         (render-journey-train item))
        (journey-filler
         (render-journey-filler item)))))
   (loop
     for i from 0
     for item in item-list
     when (eql i 0)
       do (setf (slot-value item 'line-type) :start)
     when (eql (1+ i) (length item-list))
       do (setf (slot-value item 'line-type) :end)
     do (setf (slot-value item 'index) i)
     collect item
     when (and (not (eql (1+ i) (length item-list)))
               (typep item 'journey-station)
               (typep (elt item-list (1+ i)) 'journey-station))
       collect (make-instance 'journey-filler
                              :era (era item)))))

(defun render-journey (line-code item-list &key (past-context 0) force-split-point)
  (let* ((rendered-items (render-journey-items line-code item-list))
         (item-split-point
           (position-if-not (lambda (item)
                              (eql (era item) :past))
                            item-list))
         (item-split-point
           (or
            force-split-point
            (when item-split-point
              (max 0 (- item-split-point past-context)))
            0))
         (split-point
           (position-if (lambda (item)
                          (eql (index (car item)) item-split-point))
                        rendered-items))
         (rendered-items (mapcar (function cdr) rendered-items))
         (past-items (subseq rendered-items 0 split-point))
         (current-items (subseq rendered-items split-point)))
    (who:with-html-output-to-string (*standard-output* nil
                                     :prologue nil)
      (:ul
       :class "govuk-list live-line-list train-journey-list"
       (when past-items
         (who:htm
          (:details
           :class "govuk-details train-journey-details"
           (:summary
            (:li
             :class "live-line-station"
             (:span
              :class "live-line-bsicon show-more-arrow"
              :style (format nil "color: ~A;" *accent-colour*)
              (who:str *bsicons-str-svg*))
             (:span
              :class "view-previous-stops"
              (:span
               :class "closed-text"
               "View previous stops")
              (:span
               :class "open-text"
               "Hide previous stops"))))
           (:div
            :class "detailed-stops"
            (loop
              for item in past-items
              do (who:str item))))))
       (loop
         for item in current-items
         do (who:str item))))))
