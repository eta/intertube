;;; Some actual unit tests, what a concept

(defpackage intertube-tests
  (:use :cl :prove))

(in-package :intertube-tests)

(defun output-current-entry-diffs (previous-encoding current-encoding out)
    (dolist (slot current-encoding)
      (block continue
        (let* ((slot-name (car slot))
               (slot-value (cdr slot))
               (previous-slot-value (cdr (assoc slot-name previous-encoding))))
          ;; Similar logic to HISTORY below, but with TRACK-CODES
          (when (eql slot-name 'trackernet::track-codes)
            (dolist (code
                     (nreverse
                      (loop
                        with history = slot-value
                        while (not (equal history previous-slot-value))
                        do (unless history (error "ran off the end of history?"))
                        collect (car history)
                        do (setf history (cdr history)))))
              (format out "~&[track     ] ~A~%" code))
            (return-from continue))

          ;; It's just a boring plain-old-data slot, so compare it and
          ;; add a thingie if it's different.
          (unless (equal slot-value previous-slot-value)
            (format out "~&[event     ] ~A = ~A~%" slot-name slot-value))))))

(defun output-live-train-diffs (previous-encoding current-encoding out)
    (dolist (slot current-encoding)
      (block continue
        (let* ((slot-name (car slot))
               (slot-value (cdr slot))
               (previous-slot-value (cdr (assoc slot-name previous-encoding))))
          ;; Don't compare some fields that are annoying for now.
          ;; CURRENT-ENTRY is diffed separately to make this easier.
          (when (member slot-name '(trackernet::unique-id
                                    trackernet::current-entry
                                    trackernet::destination-ballot))
            (return-from continue))

          ;; If we're comparing the HISTORY slot, handle it specially, by
          ;; popping one history item off and adding it to RET until we
          ;; end up with our list looking the same as PREVIOUS-SLOT-VALUE.
          (when (eql slot-name 'trackernet::history)
            (dolist (item
                     (reverse
                      (loop
                        with history = slot-value
                        while (not (equal history previous-slot-value))
                        do (unless history (error "ran off the end of history?"))
                        collect (car history)
                        do (setf history (cdr history)))))
              (format out "~&[history   ] ~A~%" item))
            (return-from continue))

          ;; It's just a boring plain-old-data slot, so compare it and
          ;; add a thingie if it's different.
          (unless (equal slot-value previous-slot-value)
            (format out "~&[train     ] ~A = ~A~%" slot-name slot-value))))))

(defun make-live-train-test-case (observation-list)
  "Given a list of (UNIVERSAL-TS OBSERVER TRACKERNET-TRAIN) observations in OBSERVATION-LIST, output a human-readable log of changes."
  (with-output-to-string (log)
    (let* ((first-observation (third (car observation-list)))
           (live-train (make-instance 'trackernet::live-train
                                      :actually-live nil
                                      :line-code (trackernet::line first-observation)
                                      :trackernet-id "testcase"))
           (previous-encoding (conspack:encode-object live-train))
           (previous-entry (trackernet::current-entry live-train))
           (previous-entry-encoding nil))
      (dolist (item observation-list)
        (destructuring-bind (universal-ts observer train-object) item
          (format log "-~A- " universal-ts)
          (if (position #\· (trackernet::track-code train-object))
              (trackernet::add-smart-live-train-observation
               live-train universal-ts observer train-object)
              (trackernet::add-live-train-observation
               live-train universal-ts observer train-object))
          (let* ((current-entry (trackernet::current-entry live-train))
                 (current-entry-encoding (conspack:encode-object current-entry))
                 (current-encoding (conspack:encode-object live-train)))
            ;; Add the live-train diffs to the log.
            (output-live-train-diffs previous-encoding current-encoding log)
            (setf previous-encoding current-encoding)
            ;; If the entries are the same object, diff them.
            ;; If not, output an 'exchange' event to the log.
            (if (eql previous-entry current-entry)
                (output-current-entry-diffs previous-entry-encoding current-entry-encoding log)
                (format log "~&[new event ] ~A~%" current-entry))
            (setf previous-entry current-entry)
            (setf previous-entry-encoding current-entry-encoding)))))))

(defun write-out-test-case (name observation-list)
  (with-open-file (input-log
                   (format nil "./testcases/~A.dat" name)
                   :direction :output
                   :element-type '(unsigned-byte 8))
    (with-open-file (result-log
                     (format nil "./testcases/~A.txt" name)
                     :direction :output)
      (cpk:with-named-index 'trackernet::trackernet-cpk-v1
        (conspack:encode observation-list :stream input-log))
      (princ (make-live-train-test-case observation-list) result-log)
      name)))

(defun write-out-test-case-from-historical-train (ltid)
  (let* ((filename (intertube-web::resolve-train-filename ltid))
         (observations
           (mapcar (lambda (obs)
                     (list (trackernet::universal-ts obs)
                           (format nil "X-~A-X" (trackernet::observer obs))
                           (change-class obs 'trackernet::trackernet-train)))
                   (trackernet::fetch-clickhouse-observations filename)))
         (line (trackernet::line (third (car observations))))
         (date (trackernet::get-iso-8601-for-ts (first (car observations))))
         (testname (format nil "~A-~A-~A" date line
                           (subseq filename #.(length "lt:train_")))))
    (write-out-test-case testname observations)))

(defun write-out-test-case-from-live-train (ltid)
  (let* ((filename (intertube-web::resolve-train-filename ltid))
         (lt (trackernet::redis-raw-get-cpk filename))
         (trackernet-id (trackernet::trackernet-id lt))
         (observations (trackernet::redis-cpk-sorted-set-all
                        (format nil "~A-train-~A" (trackernet::line-code lt) trackernet-id)))
         (line (trackernet::line (third (car observations))))
         (date (trackernet::get-iso-8601-for-ts (first (car observations))))
         (testname (format nil "~A-~A-live-~A" date line
                           (subseq filename #.(length "lt:train_")))))
    (write-out-test-case testname observations)))

(defun write-out-test-case-from-existing (name)
  (with-open-file (input-log
                   (format nil "./testcases/~A.dat" name)
                   :element-type '(unsigned-byte 8))
      (let ((observation-list
              (cpk:with-named-index 'trackernet::trackernet-cpk-v1
                (conspack:decode-stream input-log))))
        (write-out-test-case name observation-list))))

(defun check-written-test-case (name)
  (with-open-file (input-log
                   (format nil "./testcases/~A.dat" name)
                   :element-type '(unsigned-byte 8))
      (let ((observation-list
              (cpk:with-named-index 'trackernet::trackernet-cpk-v1
                (conspack:decode-stream input-log)))
            (result-log (uiop:read-file-string (format nil "./testcases/~A.txt" name))))
        (equal (make-live-train-test-case observation-list) result-log))))

(defun prove-check-written-test-case (name &optional desc)
  (let ((descr (if desc
                  (format nil "~A (~A)" desc name)
                  (format nil "live train test ~A" name))))
    (if (check-written-test-case name)
        (pass descr)
        (fail descr))))

(plan 12)

;; Test MAKE-SCLASS-DELTA
(is-values
 (trackernet::make-sclass-delta :+SF-MSG+ 00 "CA")
 '(#xCA #xFF)
 "basic SF-MSG")

(is-values
 (trackernet::make-sclass-delta :+SF-MSG+ 01 "CA")
 '(#xCA00 #xFF00)
 "SF-MSG address 1")

(is-values
 (trackernet::make-sclass-delta :+SG-MSG+ 00 "06880306")
 '(#x06038806 #xFFFFFFFF)
 "basic SG-MSG")

(is-values
 (trackernet::make-sclass-delta :+SG-MSG+ 02 "06880306")
 '(#x060388060000 #xFFFFFFFF0000)
 "SG-MSG address 2")

;; Test APPLY-SCLASS-DELTA
(is
 (trackernet::apply-sclass-delta
  0
  :+SF-MSG+
  00
  "CA")
 #xCA
 "apply-sclass-delta with basic SF-MSG")

(is
 (trackernet::apply-sclass-delta
  #xBE
  :+SF-MSG+
  00
  "CA")
 #xCA
 "apply-sclass-delta with existing data")

(is
 (trackernet::apply-sclass-delta
  #xBECA
  :+SF-MSG+
  00
  "CA")
 #xBECA
 "apply-sclass-delta with unrelated existing data")

(is
 (trackernet::apply-sclass-delta
  #xBECA
  :+SF-MSG+
  01
  "CA")
 #xCACA
 "apply-sclass-delta with unrelated existing data")

(is
 (trackernet::apply-sclass-delta
  0
  :+SG-MSG+
  00
  "C0FFEEAA")
 #xAAEEFFC0
 "apply-sclass-delta with SG-MSG")

(is
 (trackernet::apply-sclass-delta
  #xC0FFEEAA
  :+SG-MSG+
  00
  "C0FFEEAA")
 #xAAEEFFC0
 "apply-sclass-delta with SG-MSG and existing data")

(is
 (trackernet::apply-sclass-delta
  #xC0FFEEAA
  :+SG-MSG+
  04
  "C0FFEEAA")
 #xAAEEFFC0C0FFEEAA
 "apply-sclass-delta with SG-MSG and unrelated existing data")

(is
 (trackernet::apply-sclass-delta
  #xC0FFEEAABBBBBBBB
  :+SG-MSG+
  00
  "C0FFEEAA")
 #xC0FFEEAAAAEEFFC0
 "apply-sclass-delta with SG-MSG and unrelated existing data")

;; Live train test cases

;;(prove-check-written-test-case
;; "2023-08-04-X-live-tWNm1ZdFQYmRzgs_Hah8LQ"
;; "simple XR test (abbey wood to heathrow and back)")


(finalize)
