;;;; Network Rail Integrated Train Planning System -- schedule processing

(defpackage :itps
  (:use :cl))

(in-package :itps)

(defparameter +europe-london-tz+
  (progn
    (local-time:reread-timezone-repository)
    (local-time:find-timezone-by-location-name "Europe/London")))

(defparameter +current-century+ 2000
  "As if you'll ever need to change this.")

;; FIXME(eta): copypasta
(defparameter +europe-london-tz+
  (progn
    (local-time:reread-timezone-repository)
    (local-time:find-timezone-by-location-name "Europe/London")))

;; FIXME(eta): copypasta
(defun get-iso-8601-for-ts (ts)
  "Returns the current date, in %Y-%m-%d format."
  (local-time:format-timestring
   nil
   (local-time:universal-to-timestamp ts)
   :format '((:year 4) #\- (:month 2) #\- (:day 2))))

;; FIXME(eta): copypasta
(defun universal-time-railway (time)
  (local-time:with-decoded-timestamp
      (:sec s :minute m :hour h
       :timezone +europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D~2,'0D~A" h m
            (cond
              ((>= s 45) "¾")
              ((>= s 30) "½")
              ((>= s 15) "¼")
              (t "")))))

(defun generate-schedule-id ()
  (concatenate
   'string
   "itps_"
   (string-right-trim
    '(#\=)
    (qbase64:encode-bytes
     (uuid:uuid-to-byte-array
      (uuid:make-v4-uuid))
     :scheme :uri))))

(defclass unknown-cif-record ()
  ((text
    :reader text
    :initarg :text)))

(defclass cif-header ()
  ((mainframe-identity
    :reader mainframe-identity
    :initarg :mainframe-identity)
   (extracted-ts
    :reader extracted-ts
    :initarg :extracted-ts)
   (reference
    :reader reference
    :initarg :reference)
   (last-reference
    :reader last-reference
    :initarg :last-reference
    :initform nil)
   (updatep
    :reader updatep
    :initarg :updatep)))

(defmethod print-object ((obj cif-header) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (mainframe-identity extracted-ts reference updatep) obj
      (format stream "for ~A file ~A, extracted ~A ~A, mainframe ID ~A"
              (if updatep "update" "full")
              reference
              (get-iso-8601-for-ts extracted-ts)
              (universal-time-railway extracted-ts)
              mainframe-identity))))

(defclass cif-tiploc-insert ()
  ((tiploc
    :reader tiploc
    :initarg :tiploc)
   (nlc
    :reader nlc
    :initarg :nlc)
   (description
    :reader description
    :initarg :description)
   (stanox
    :reader stanox
    :initarg :stanox)
   (crs
    :reader crs
    :initarg :crs)))

(defmethod print-object ((obj cif-tiploc-insert) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (tiploc description nlc stanox crs) obj
      (format stream "for ~A (\"~A\"), stanox ~A, nlc ~A, crs ~A"
              tiploc description stanox nlc crs))))

(defclass cif-schedule-deletion ()
  ((uid
    :reader uid
    :initarg :uid)
   (runs-from-ts
    :reader runs-from-ts
    :initarg :runs-from-ts)
   (stp-indicator
    :reader stp-indicator
    :initarg :stp-indicator)))

(defclass cif-schedule ()
  ((intertube-id
    :reader intertube-id
    :initform (generate-schedule-id))
   (revisedp
    :reader revisedp
    :initarg :revisedp)
   (uid
    :reader uid
    :initarg :uid
    :documentation "The ITPS UID of the schedule.")
   (runs-from-ts
    :reader runs-from-ts
    :initarg :runs-from-ts)
   (runs-to-ts
    :reader runs-to-ts
    :initarg :runs-to-ts)
   (runs-on-days
    :reader runs-on-days
    :initarg :runs-on-days)
   (bank-holiday-code
    :reader bank-holiday-code
    :initarg :bank-holiday-code)
   (train-status-code
    :reader train-status-code
    :initarg :train-status-code)
   (train-category-code
    :reader train-category-code
    :initarg :train-category-code)
   (headcode
    :reader headcode
    :initarg :headcode)
   (nrs-code
    :reader nrs-code
    :initarg :nrs-code)
   (stp-indicator
    :reader stp-indicator
    :initarg :stp-indicator)
   (toc-code
    :reader toc-code
    :initarg :toc-code)
   (locations
    :reader locations
    :initarg :locations)))

(defmethod print-object ((obj cif-schedule) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (intertube-id revisedp uid runs-from-ts runs-to-ts runs-on-days bank-holiday-code train-category-code headcode stp-indicator toc-code locations) obj
      (format stream "~A (~A, ~A…) ~A from ~A to ~A ~A, headcode ~A, for TOC ~A, ~A locations from ~A to ~A"
              uid stp-indicator
              (subseq intertube-id 0 #.(length "itps_XXXX"))
              (category-code-to-human train-category-code)
              (get-iso-8601-for-ts runs-from-ts)
              (get-iso-8601-for-ts runs-to-ts)
              (days-runs-to-human runs-on-days)
              headcode
              toc-code
              (length locations)
              (universal-time-railway
               (depart-wtt-sec (first locations)))
              (universal-time-railway
               (arrive-wtt-sec (car (last locations))))))))

(defclass cif-location ()
  ((tiploc
    :reader tiploc
    :initarg :tiploc)
   (platform
    :reader platform
    :initarg :platform
    :initform nil)
   (line
    :reader line
    :initarg :line
    :initform nil)
   (path
    :reader path
    :initarg :path
    :initform nil)
   (allowance-engineering
    :reader allowance-engineering
    :initarg :allowance-engineering
    :initform nil)
   (allowance-pathing
    :reader allowance-pathing
    :initarg :allowance-pathing
    :initform nil)
   (allowance-performance
    :reader allowance-performance
    :initarg :allowance-performance
    :initform nil)
   (activity
    :reader activity
    :initarg :activity
    :initform nil)))

(defclass cif-location-depart (cif-location)
  ((depart-wtt-sec
    :reader depart-wtt-sec
    :initarg :depart-wtt-sec)
   (depart-public-sec
    :reader depart-public-sec
    :initarg :depart-public-sec
    :initform nil)))

(defmethod print-object ((obj cif-location-depart) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (depart-wtt-sec depart-public-sec tiploc platform line allowance-engineering allowance-pathing allowance-performance) obj
      (format stream "dep ~A~@[ (~A)~] at ~A~@[ pfm ~A~]~@[, line ~A~]~@[, eng +~A~]~@[, path +~A~]~@[, perf +~A~]"
              (universal-time-railway depart-wtt-sec)
              (when (and depart-public-sec
                         (not (eql depart-public-sec depart-wtt-sec)))
                (universal-time-railway depart-public-sec))
              tiploc
              platform
              line
              allowance-engineering
              allowance-pathing
              allowance-performance))))

(defclass cif-location-arrive (cif-location)
  ((arrive-wtt-sec
    :reader arrive-wtt-sec
    :initarg :arrive-wtt-sec)
   (arrive-public-sec
    :reader arrive-public-sec
    :initarg :arrive-public-sec
    :initform nil)))

(defmethod print-object ((obj cif-location-arrive) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (arrive-wtt-sec arrive-public-sec tiploc platform path allowance-engineering allowance-pathing allowance-performance) obj
      (format stream "arr ~A~@[ (~A)~] at ~A~@[ pfm ~A~]~@[, line ~A~]~@[, eng +~A~]~@[, path +~A~]~@[, perf +~A~]"
              (universal-time-railway arrive-wtt-sec)
              (when (and arrive-public-sec
                         (not (eql arrive-public-sec arrive-wtt-sec)))
                  (universal-time-railway arrive-public-sec))
              tiploc
              platform
              path
              allowance-engineering
              allowance-pathing
              allowance-performance))))

(defclass cif-location-intermediate (cif-location-depart cif-location-arrive)
  ((pass-wtt-sec
    :reader pass-wtt-sec
    :initarg :pass-wtt-sec)))

(defmethod print-object ((obj cif-location-intermediate) stream)
  (print-unreadable-object (obj stream :identity t)
    (with-slots (arrive-wtt-sec arrive-public-sec depart-wtt-sec depart-public-sec pass-wtt-sec tiploc platform line path allowance-engineering allowance-pathing allowance-performance) obj
      (format stream "CIF-LOCATION ")
      (if pass-wtt-sec
          (format stream "pass ~A"
                  (universal-time-railway pass-wtt-sec))
          (format stream "arr ~A~@[ (~A)~] dep ~A~@[ (~A)~]"
                  (universal-time-railway arrive-wtt-sec)
                  (when (and arrive-public-sec
                             (not (eql arrive-public-sec arrive-wtt-sec)))
                    (universal-time-railway arrive-public-sec))
                  (universal-time-railway depart-wtt-sec)
                  (when (and depart-public-sec
                             (not (eql depart-public-sec depart-wtt-sec)))
                    (universal-time-railway depart-public-sec))))
      (format stream " at ~A~@[ pfm ~A~]~@[, line ~A~]~@[, path ~A~]~@[, eng +~A~]~@[, path +~A~]~@[, perf +~A~]"
              tiploc
              platform
              line
              path
              allowance-engineering
              allowance-pathing
              allowance-performance))))

(defvar *line* nil
  "The line of text currently being parsed.")

(defun chomp (n)
  "Chomps N characters from *LINE*, stripping whitespace.
If the returned string would be empty, returns NIL instead.
If there aren't N characters to chomp, signals an error."
  (assert *line* () "No line to chomp!")
  (assert (>= (length *line*) n) () "Length of line is ~A, but tried to chomp ~A" (length *line*) n)
  (let ((ret
          (string-trim
           '(#\Space #\Tab)
           (subseq *line* 0 n))))
    (setf *line* (subseq *line* n))
    (unless (eql (length ret) 0)
      ret)))

(defun int-chomp (n)
  "Calls PARSE-INTEGER on the results of CHOMP."
  (let ((chomped (chomp n)))
    (when chomped
      (parse-integer chomped :junk-allowed t))))

(defun ensure (value descr)
  "Signal an error if VALUE is NIL, with the DESCR providing a description as to what it is."
  (unless value
    (error "Failed to ENSURE a ~A" descr))
  value)

(defmacro chomping-bind (bindings &body body)
  (let ((let-bindings
          (loop
            for b in bindings
            collect (destructuring-bind
                        (varname width &rest controls) b
                      (let* ((intp (member :int controls))
                             (optionalp (member :optional controls))
                             (func-sym
                               (if intp 'int-chomp 'chomp))
                             (clause
                               `(,func-sym ,width)))
                        (unless optionalp
                          (setf clause `(ensure ,clause
                                                ,(symbol-name varname))))
                        `(,varname ,clause))))))
    `(let ,let-bindings
        ,@body)))

(defun ddmmyy-universal-ts (date)
  "Convert a date in DDMMYY format to a universal timestamp."
  (let ((dd (parse-integer date :start 0 :end 2))
        (mm (parse-integer date :start 2 :end 4))
        (yy (parse-integer date :start 4 :end 6)))
    (local-time:timestamp-to-universal
     (local-time:encode-timestamp
      0 0 0 0 dd mm (+ +current-century+ yy)
      :timezone +europe-london-tz+))))

(defun yymmdd-universal-ts (date)
  "Convert a date in YYMMDD format to a universal timestamp."
  (let ((yy (parse-integer date :start 0 :end 2))
        (mm (parse-integer date :start 2 :end 4))
        (dd (parse-integer date :start 4 :end 6)))
    (local-time:timestamp-to-universal
     (local-time:encode-timestamp
      0 0 0 0 dd mm (+ +current-century+ yy)
      :timezone +europe-london-tz+))))

(defun hhmmh-universal-ts (time)
  "Convert a time in HHMM[H] format to a number of seconds past midnight."
  ;; FIXME(eta): There are probably daylight savings issues with this.
  (let ((hh (parse-integer time :start 0 :end 2))
        (mm (parse-integer time :start 2 :end 4))
        (hp (and (> (length time) 4)
                 (string= (subseq time 4) "H"))))
    (+ (* 3600 hh)
        (* 60 mm)
        (if hp 30 0))))

(defun parse-cif-header (line)
  (let ((*line* line))
    (chomping-bind ((record-type 2)
                    (mainframe-identity 20)
                    (extract-date 6)
                    (extract-time 4)
                    (reference 7)
                    (last-reference 7 :optional)
                    (updatep 1))
      (declare (ignore record-type))
      (make-instance 'cif-header
                     :extracted-ts (+ (ddmmyy-universal-ts extract-date)
                                       (hhmmh-universal-ts extract-time))
                     :mainframe-identity mainframe-identity
                     :reference reference
                     :last-reference last-reference
                     :updatep (string= updatep "U")))))

(defun parse-tiploc-insert (line)
  (let ((*line* line))
    (chomping-bind ((record-type 2)
                    (tiploc 7)
                    (capitals-id 2 :optional)
                    (nlc 6 :optional)
                    (nlc-check 1 :optional)
                    (description 26)
                    (stanox 5 :optional)
                    (po-code 4 :optional)
                    (crs-code 3 :optional))
      (declare (ignore record-type capitals-id nlc-check po-code))
      (make-instance 'cif-tiploc-insert
                     :tiploc tiploc
                     :nlc nlc
                     :description description
                     :stanox stanox
                     :crs crs-code))))

(defun parse-days-runs (days-runs)
  "Convert a string representation of the days a train runs on to a binary number."
  (loop
    for i from 0 below 7
    sum (ash (if (eql (elt days-runs i) #\1) 1 0) (- 6 i))))

(defun category-code-to-human (value)
  (cond
    ((string= value "OL") "London Underground / Metro")
    ((string= value "OU") "Unadvertised Ordinary Passenger")
    ((string= value "OO") "Ordinary Passenger")
    ((string= value "OS") "Staff Train")
    ((string= value "OW") "Mixed Ordinary Passenger")
    ((string= value "XC") "Channel Tunnel")
    ((string= value "XD") "Sleeper (Europe)")
    ((string= value "XI") "International")
    ((string= value "XR") "Motorail")
    ((string= value "XU") "Unadvertised Express")
    ((string= value "XX") "Express Passenger")
    ((string= value "XZ") "Sleeper")
    ((string= value "BR") "Rail Replacement Bus")
    ((string= value "BS") "Bus")
    ((string= value "SS") "Ship")
    ((string= value "EE") "Empty Coaching Stock")
    ((string= value "EL") "London Underground ECS")
    ((string= value "ES") "ECS & Staff")
    ;; There are more, but they're mostly freight and such.
    (t value)))

(defun days-runs-to-human (value)
  "Convert a number representing a train's running days to a human-readable string."
  (cond
    ;; thanks to Dave McCormick, no thanks to random FOIA request
    ((eql value #b1111100) "SX") ; Saturdays excepted
    ((eql value #b0111110) "MX") ; Mondays excepted
    ((eql value #b0111100) "MSX") ; Mondays and Saturdays excepted
    ((eql value #b1111000) "FSX") ; Fridays and Saturdays excepted
    ((eql value #b1111110) "EWD") ; Every working day
    ((eql value #b0000001) "SU") ; Sunday
    (t
     ;; [series of days] only
     (let ((days
             (loop
               for day in '("M" "T" "W" "Th" "F" "S" "Su")
               for i from 0 below 7
               unless (zerop (logand value (ash 1 (- 6 i))))
                 collect day)))
       (format nil "~{~A~}O" days)))))

(defun parse-location-origin (lo-line)
  (let ((*line* lo-line))
    (chomping-bind ((tiploc 8)
                    (depart-wtt 5)
                    (depart-public 4)
                    (platform 3 :optional)
                    (line 3 :optional)
                    (allowance-engineering 2 :int :optional)
                    (allowance-pathing 2 :int :optional)
                    (activity 12 :optional)
                    (allowance-performance 2 :int :optional))
      (make-instance 'cif-location-depart
                     :tiploc tiploc
                     :platform platform
                     :line line
                     :allowance-engineering allowance-engineering
                     :allowance-pathing allowance-pathing
                     :allowance-performance allowance-performance
                     :activity activity
                     :depart-wtt-sec (hhmmh-universal-ts depart-wtt)
                     :depart-public-sec
                     ;; If a train is not for public use, its public time
                     ;; will be 0000 -- but it could also just be a train
                     ;; leaving at midnight, so we need to check...
                     (unless (and (string= depart-public "0000")
                                  (not (uiop:string-prefix-p "0000" depart-wtt)))
                       (hhmmh-universal-ts depart-public))))))

(defun parse-location-terminating (lt-line)
  (let ((*line* lt-line))
    (chomping-bind ((tiploc 8)
                    (arrive-wtt 5)
                    (arrive-public 4)
                    (platform 3 :optional)
                    (path 3 :optional)
                    (activity 12 :optional))
      (make-instance 'cif-location-arrive
                     :tiploc tiploc
                     :platform platform
                     :path path
                     :activity activity
                     :arrive-wtt-sec (hhmmh-universal-ts arrive-wtt)
                     :arrive-public-sec
                     (unless (and (string= arrive-public "0000")
                                  (not (uiop:string-prefix-p "0000" arrive-wtt)))
                       (hhmmh-universal-ts arrive-public))))))

(defun parse-location-intermediate (li-line)
  (let ((*line* li-line))
    (chomping-bind ((tiploc 8)
                    (arrive-wtt 5 :optional)
                    (depart-wtt 5 :optional)
                    (pass-wtt 5 :optional)
                    (arrive-public 4 :optional)
                    (depart-public 4 :optional)
                    (platform 3 :optional)
                    (line 3 :optional)
                    (path 3 :optional)
                    (activity 12 :optional)
                    (allowance-engineering 3 :int :optional)
                    (allowance-pathing 2 :int :optional)
                    (allowance-performance 2 :int :optional))
      (assert (or arrive-wtt depart-wtt pass-wtt) ()
              "LI line with no times at all:~%~A" li-line)
      (make-instance 'cif-location-intermediate
                     :tiploc tiploc
                     :arrive-wtt-sec
                     (when arrive-wtt
                       (hhmmh-universal-ts arrive-wtt))
                     :depart-wtt-sec
                     (when depart-wtt
                       (hhmmh-universal-ts depart-wtt))
                     :pass-wtt-sec
                     (when pass-wtt
                       (hhmmh-universal-ts pass-wtt))
                     :arrive-public-sec
                     (when arrive-public
                       (unless (and (string= arrive-public "0000")
                                    (not (uiop:string-prefix-p "0000" arrive-wtt)))
                         (hhmmh-universal-ts arrive-public)))
                     :depart-public-sec
                     (when depart-public
                       (unless (and (string= depart-public "0000")
                                    (not (uiop:string-prefix-p "0000" depart-wtt)))
                         (hhmmh-universal-ts depart-public)))
                     :platform platform
                     :line line
                     :path path
                     :activity activity
                     :allowance-engineering allowance-engineering
                     :allowance-pathing allowance-pathing
                     :allowance-performance allowance-performance))))

(defun parse-location (line)
  (let ((record-type (subseq line 0 2))
        (rest (subseq line 2)))
    (cond
      ((string= record-type "LO")
       (parse-location-origin rest))
      ((string= record-type "LI")
       (parse-location-intermediate rest))
      ((string= record-type "LT")
       (parse-location-terminating rest))
      ((string= record-type "CR")
       ;; FIXME(eta): We don't handle Change in Route yet.
       )
      (t
       (error "Unexpected ~A line in schedule:~%~A" record-type line)))))

(defun parse-schedule (bs-line bx-line other-lines)
  (let ((*line* bs-line)
        (toc-code (when bx-line (subseq bx-line 11 13)))
        (location-records
          (remove-if-not (lambda (x) (typep x 'cif-location))
                         (mapcar #'parse-location other-lines))))
    (chomping-bind ((record-type 2)
                    (tx-type 1)
                    (uid 6)
                    (date-runs-from 6)
                    (date-runs-to 6)
                    (days-run 7)
                    (bank-holiday 1 :optional)
                    (train-status 1 :optional)
                    (train-category 2 :optional)
                    (headcode 4 :optional)
                    (nrs-code 4 :optional)
                    (course-ind 1 :optional)
                    (profit-code 8 :optional)
                    (rsid-portion 1 :optional)
                    (power-type 3 :optional)
                    (timing-load 4 :optional)
                    (speed 3 :int :optional)
                    (unused 19 :optional)
                    (stp-indicator 1))
      (declare (ignore record-type course-ind profit-code
                       rsid-portion power-type timing-load
                       speed unused))
      (make-instance 'cif-schedule
                     :revisedp (string= tx-type "R")
                     :uid uid
                     :runs-from-ts (yymmdd-universal-ts date-runs-from)
                     :runs-to-ts (yymmdd-universal-ts date-runs-to)
                     :runs-on-days (parse-days-runs days-run)
                     :bank-holiday-code bank-holiday
                     :train-status-code train-status
                     :train-category-code train-category
                     :headcode headcode
                     :nrs-code nrs-code
                     :stp-indicator stp-indicator
                     :toc-code toc-code
                     :locations location-records))))

(defun read-cif-file (stream callback)
  "Read the CIF file from STREAM, calling CALLBACK with all succesfully parsed objects."
  (loop
    with current-schedule-lines
    with line
    with record-type
    while (setf line (read-line stream nil nil))
    do (setf record-type (subseq line 0 2))
    do (when current-schedule-lines
         (cond
           ((member record-type '("BX" "LO" "LI" "CR") :test #'string=)
            (setf current-schedule-lines (append current-schedule-lines (list line))))
           ((string= record-type "LT")
            (setf current-schedule-lines (append current-schedule-lines (list line)))
            (funcall callback
                     (parse-schedule (elt current-schedule-lines 0)
                                     (elt current-schedule-lines 1)
                                     (subseq current-schedule-lines 2)))
            (setf current-schedule-lines nil))
           (t
            (funcall callback
                     (parse-schedule (elt current-schedule-lines 0) nil nil))
            (setf current-schedule-lines nil))))
       ;; FIXME(eta): This would be less shit if LOOP had a `continue'.
    do (cond
         ((string= record-type "HD")
          (funcall callback
                   (parse-cif-header line)))
         ((string= record-type "TI")
          (funcall callback
                   (parse-tiploc-insert line)))
         ((string= record-type "BS")
          (setf current-schedule-lines (list line))))))

