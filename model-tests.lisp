(defpackage intertube-model-tests
  (:use :cl))

(in-package :intertube-model-tests)

(defun truthify (x)
  (when x
    t))

(defun get-model-test-results ()
  (let ((ret (make-hash-table :test 'equal)))
    (loop
      for (line-code . line-data) in intertube-web::*line-model-data*
      do (unless (equal line-code "A")
           (loop
             for (direction-code direction-desc . platform-data)
               in line-data
             do (loop
                  for (station-name . platforms)
                    in platform-data
                  do (block nil
                       (when (eql station-name :html)
                         (return))
                       (dolist (pfm platforms)
                         (let ((platform-key
                                 (format nil "~A: ~A platform ~A exists"
                                         line-code station-name (princ-to-string pfm))))
                           (setf (gethash platform-key ret)
                                 (or
                                  (truthify
                                   (gethash (list station-name (princ-to-string pfm))
                                            (trackernet::station-to-point
                                             (trackernet::line-model-for line-code))))
                                  (truthify
                                   (gethash (list station-name (format nil "~A(E)" pfm))
                                            (trackernet::station-to-point
                                             (trackernet::line-model-for line-code))))
                                  (truthify
                                   (gethash (list station-name (format nil "~A(W)" pfm))
                                            (trackernet::station-to-point
                                             (trackernet::line-model-for line-code)))))))))))))
    (alexandria:hash-table-alist ret)))

(defun run-tests ()
  (let ((results (get-model-test-results))
        (grouped (make-hash-table :test 'equal))
        (failures 0)
        (tests 0)
        (id 0))
    (loop
      for (key . result) in results
      do (incf tests)
      do (when (not result) (incf failures))
      do (alexandria:ensure-gethash (elt key 0) grouped)
      do (push (list key result) (gethash (elt key 0) grouped)))
    (format *standard-output* "<?xml version=\"1.0\"?>~%")
    (format *standard-output* "<testsuites tests=\"~A\" name=\"intertube model tests\" failures=\"~A\">~%"
            tests failures)
    (loop
      with set
      with failures
      for line in (sort (alexandria:hash-table-keys grouped)
                        #'char<)
      do (setf set (sort (gethash line grouped)
                         (lambda (a b)
                           (string< (car a) (car b)))))
      do (setf failures (count-if (lambda (x)
                                    (not (second x)))
                                  set))
      do (format *standard-output* "  <testsuite name=\"~A line\" tests=\"~A\" failures=\"~A\">~%"
                 (who:escape-string
                  (first (intertube-web::tube-line-by-code (princ-to-string line))))
                 (length set)
                 failures)
      do (loop
           for (key result) in set
           do (format *standard-output* "    <testcase name=\"~A\" id=\"~A\">~%"
                      (who:escape-string key) (incf id))
           do (if result
                  (format *standard-output* "    </testcase>~%")
                  (progn
                    (format *standard-output* "      <failure message=\"object does not exist\" type=\"fail\"/>~%")
                    (format *standard-output* "    </testcase>~%"))))
      do (format *standard-output* "  </testsuite>~%"))
    (format *standard-output* "</testsuites>")))
