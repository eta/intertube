;;;; This file was generated automatically by parse-x11.lisp
;;;; Please do not edit directly, just run make if necessary (but should not be).


(IN-PACKAGE :CL-COLORS2)
(PUSH (CONS "snow" (DEFINE-RGB-COLOR "snow" 1 50/51 50/51)) *X11-COLORS-LIST*)

(PUSH (CONS "ghost-white" (DEFINE-RGB-COLOR "ghost-white" 248/255 248/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "ghostwhite" (DEFINE-RGB-COLOR "ghostwhite" 248/255 248/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "white-smoke" (DEFINE-RGB-COLOR "white-smoke" 49/51 49/51 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "whitesmoke" (DEFINE-RGB-COLOR "whitesmoke" 49/51 49/51 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gainsboro" (DEFINE-RGB-COLOR "gainsboro" 44/51 44/51 44/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "floral-white" (DEFINE-RGB-COLOR "floral-white" 1 50/51 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "floralwhite" (DEFINE-RGB-COLOR "floralwhite" 1 50/51 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "old-lace" (DEFINE-RGB-COLOR "old-lace" 253/255 49/51 46/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "oldlace" (DEFINE-RGB-COLOR "oldlace" 253/255 49/51 46/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "linen" (DEFINE-RGB-COLOR "linen" 50/51 16/17 46/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "antique-white" (DEFINE-RGB-COLOR "antique-white" 50/51 47/51 43/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "antiquewhite" (DEFINE-RGB-COLOR "antiquewhite" 50/51 47/51 43/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "papaya-whip" (DEFINE-RGB-COLOR "papaya-whip" 1 239/255 71/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "papayawhip" (DEFINE-RGB-COLOR "papayawhip" 1 239/255 71/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "blanched-almond" (DEFINE-RGB-COLOR "blanched-almond" 1 47/51 41/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "blanchedalmond" (DEFINE-RGB-COLOR "blanchedalmond" 1 47/51 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "bisque" (DEFINE-RGB-COLOR "bisque" 1 76/85 196/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "peach-puff" (DEFINE-RGB-COLOR "peach-puff" 1 218/255 37/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "peachpuff" (DEFINE-RGB-COLOR "peachpuff" 1 218/255 37/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "navajo-white" (DEFINE-RGB-COLOR "navajo-white" 1 74/85 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "navajowhite" (DEFINE-RGB-COLOR "navajowhite" 1 74/85 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "moccasin" (DEFINE-RGB-COLOR "moccasin" 1 76/85 181/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "cornsilk" (DEFINE-RGB-COLOR "cornsilk" 1 248/255 44/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "ivory" (DEFINE-RGB-COLOR "ivory" 1 1 16/17)) *X11-COLORS-LIST*)

(PUSH (CONS "lemon-chiffon" (DEFINE-RGB-COLOR "lemon-chiffon" 1 50/51 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "lemonchiffon" (DEFINE-RGB-COLOR "lemonchiffon" 1 50/51 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "seashell" (DEFINE-RGB-COLOR "seashell" 1 49/51 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "honeydew" (DEFINE-RGB-COLOR "honeydew" 16/17 1 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "mint-cream" (DEFINE-RGB-COLOR "mint-cream" 49/51 1 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "mintcream" (DEFINE-RGB-COLOR "mintcream" 49/51 1 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "azure" (DEFINE-RGB-COLOR "azure" 16/17 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "alice-blue" (DEFINE-RGB-COLOR "alice-blue" 16/17 248/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "aliceblue" (DEFINE-RGB-COLOR "aliceblue" 16/17 248/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "lavender" (DEFINE-RGB-COLOR "lavender" 46/51 46/51 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "lavender-blush" (DEFINE-RGB-COLOR "lavender-blush" 1 16/17 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "lavenderblush" (DEFINE-RGB-COLOR "lavenderblush" 1 16/17 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "misty-rose" (DEFINE-RGB-COLOR "misty-rose" 1 76/85 15/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "mistyrose" (DEFINE-RGB-COLOR "mistyrose" 1 76/85 15/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "white" (DEFINE-RGB-COLOR "white" 1 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "black" (DEFINE-RGB-COLOR "black" 0 0 0)) *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-slate-gray"
       (DEFINE-RGB-COLOR "dark-slate-gray" 47/255 79/255 79/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslategray" (DEFINE-RGB-COLOR "darkslategray" 47/255 79/255 79/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-slate-grey"
       (DEFINE-RGB-COLOR "dark-slate-grey" 47/255 79/255 79/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslategrey" (DEFINE-RGB-COLOR "darkslategrey" 47/255 79/255 79/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "dim-gray" (DEFINE-RGB-COLOR "dim-gray" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "dimgray" (DEFINE-RGB-COLOR "dimgray" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "dim-grey" (DEFINE-RGB-COLOR "dim-grey" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "dimgrey" (DEFINE-RGB-COLOR "dimgrey" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "slate-gray" (DEFINE-RGB-COLOR "slate-gray" 112/255 128/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "slategray" (DEFINE-RGB-COLOR "slategray" 112/255 128/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "slate-grey" (DEFINE-RGB-COLOR "slate-grey" 112/255 128/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "slategrey" (DEFINE-RGB-COLOR "slategrey" 112/255 128/255 48/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-slate-gray" (DEFINE-RGB-COLOR "light-slate-gray" 7/15 8/15 3/5))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightslategray" (DEFINE-RGB-COLOR "lightslategray" 7/15 8/15 3/5))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-slate-grey" (DEFINE-RGB-COLOR "light-slate-grey" 7/15 8/15 3/5))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightslategrey" (DEFINE-RGB-COLOR "lightslategrey" 7/15 8/15 3/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray" (DEFINE-RGB-COLOR "gray" 38/51 38/51 38/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey" (DEFINE-RGB-COLOR "grey" 38/51 38/51 38/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-grey" (DEFINE-RGB-COLOR "light-grey" 211/255 211/255 211/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightgrey" (DEFINE-RGB-COLOR "lightgrey" 211/255 211/255 211/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-gray" (DEFINE-RGB-COLOR "light-gray" 211/255 211/255 211/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightgray" (DEFINE-RGB-COLOR "lightgray" 211/255 211/255 211/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "midnight-blue" (DEFINE-RGB-COLOR "midnight-blue" 5/51 5/51 112/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "midnightblue" (DEFINE-RGB-COLOR "midnightblue" 5/51 5/51 112/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "navy" (DEFINE-RGB-COLOR "navy" 0 0 128/255)) *X11-COLORS-LIST*)

(PUSH (CONS "navy-blue" (DEFINE-RGB-COLOR "navy-blue" 0 0 128/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "navyblue" (DEFINE-RGB-COLOR "navyblue" 0 0 128/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "cornflower-blue"
       (DEFINE-RGB-COLOR "cornflower-blue" 20/51 149/255 79/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "cornflowerblue"
       (DEFINE-RGB-COLOR "cornflowerblue" 20/51 149/255 79/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-slate-blue"
       (DEFINE-RGB-COLOR "dark-slate-blue" 24/85 61/255 139/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslateblue" (DEFINE-RGB-COLOR "darkslateblue" 24/85 61/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "slate-blue" (DEFINE-RGB-COLOR "slate-blue" 106/255 6/17 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "slateblue" (DEFINE-RGB-COLOR "slateblue" 106/255 6/17 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-slate-blue"
       (DEFINE-RGB-COLOR "medium-slate-blue" 41/85 104/255 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumslateblue"
       (DEFINE-RGB-COLOR "mediumslateblue" 41/85 104/255 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "light-slate-blue"
       (DEFINE-RGB-COLOR "light-slate-blue" 44/85 112/255 1))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightslateblue" (DEFINE-RGB-COLOR "lightslateblue" 44/85 112/255 1))
 *X11-COLORS-LIST*)

(PUSH (CONS "medium-blue" (DEFINE-RGB-COLOR "medium-blue" 0 0 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "mediumblue" (DEFINE-RGB-COLOR "mediumblue" 0 0 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "royal-blue" (DEFINE-RGB-COLOR "royal-blue" 13/51 7/17 15/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "royalblue" (DEFINE-RGB-COLOR "royalblue" 13/51 7/17 15/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "blue" (DEFINE-RGB-COLOR "blue" 0 0 1)) *X11-COLORS-LIST*)

(PUSH (CONS "dodger-blue" (DEFINE-RGB-COLOR "dodger-blue" 2/17 48/85 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "dodgerblue" (DEFINE-RGB-COLOR "dodgerblue" 2/17 48/85 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "deep-sky-blue" (DEFINE-RGB-COLOR "deep-sky-blue" 0 191/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "deepskyblue" (DEFINE-RGB-COLOR "deepskyblue" 0 191/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "sky-blue" (DEFINE-RGB-COLOR "sky-blue" 9/17 206/255 47/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "skyblue" (DEFINE-RGB-COLOR "skyblue" 9/17 206/255 47/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-sky-blue" (DEFINE-RGB-COLOR "light-sky-blue" 9/17 206/255 50/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightskyblue" (DEFINE-RGB-COLOR "lightskyblue" 9/17 206/255 50/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "steel-blue" (DEFINE-RGB-COLOR "steel-blue" 14/51 26/51 12/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "steelblue" (DEFINE-RGB-COLOR "steelblue" 14/51 26/51 12/17))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-steel-blue"
       (DEFINE-RGB-COLOR "light-steel-blue" 176/255 196/255 74/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsteelblue"
       (DEFINE-RGB-COLOR "lightsteelblue" 176/255 196/255 74/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "light-blue" (DEFINE-RGB-COLOR "light-blue" 173/255 72/85 46/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightblue" (DEFINE-RGB-COLOR "lightblue" 173/255 72/85 46/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "powder-blue" (DEFINE-RGB-COLOR "powder-blue" 176/255 224/255 46/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "powderblue" (DEFINE-RGB-COLOR "powderblue" 176/255 224/255 46/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "pale-turquoise" (DEFINE-RGB-COLOR "pale-turquoise" 35/51 14/15 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "paleturquoise" (DEFINE-RGB-COLOR "paleturquoise" 35/51 14/15 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-turquoise" (DEFINE-RGB-COLOR "dark-turquoise" 0 206/255 209/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkturquoise" (DEFINE-RGB-COLOR "darkturquoise" 0 206/255 209/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-turquoise"
       (DEFINE-RGB-COLOR "medium-turquoise" 24/85 209/255 4/5))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumturquoise"
       (DEFINE-RGB-COLOR "mediumturquoise" 24/85 209/255 4/5))
 *X11-COLORS-LIST*)

(PUSH (CONS "turquoise" (DEFINE-RGB-COLOR "turquoise" 64/255 224/255 208/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "cyan" (DEFINE-RGB-COLOR "cyan" 0 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "light-cyan" (DEFINE-RGB-COLOR "light-cyan" 224/255 1 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightcyan" (DEFINE-RGB-COLOR "lightcyan" 224/255 1 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "cadet-blue" (DEFINE-RGB-COLOR "cadet-blue" 19/51 158/255 32/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "cadetblue" (DEFINE-RGB-COLOR "cadetblue" 19/51 158/255 32/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-aquamarine"
       (DEFINE-RGB-COLOR "medium-aquamarine" 2/5 41/51 2/3))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumaquamarine" (DEFINE-RGB-COLOR "mediumaquamarine" 2/5 41/51 2/3))
 *X11-COLORS-LIST*)

(PUSH (CONS "aquamarine" (DEFINE-RGB-COLOR "aquamarine" 127/255 1 212/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-green" (DEFINE-RGB-COLOR "dark-green" 0 20/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkgreen" (DEFINE-RGB-COLOR "darkgreen" 0 20/51 0))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-olive-green"
       (DEFINE-RGB-COLOR "dark-olive-green" 1/3 107/255 47/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkolivegreen" (DEFINE-RGB-COLOR "darkolivegreen" 1/3 107/255 47/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-sea-green"
       (DEFINE-RGB-COLOR "dark-sea-green" 143/255 188/255 143/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkseagreen"
       (DEFINE-RGB-COLOR "darkseagreen" 143/255 188/255 143/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "sea-green" (DEFINE-RGB-COLOR "sea-green" 46/255 139/255 29/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "seagreen" (DEFINE-RGB-COLOR "seagreen" 46/255 139/255 29/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-sea-green"
       (DEFINE-RGB-COLOR "medium-sea-green" 4/17 179/255 113/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumseagreen"
       (DEFINE-RGB-COLOR "mediumseagreen" 4/17 179/255 113/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "light-sea-green"
       (DEFINE-RGB-COLOR "light-sea-green" 32/255 178/255 2/3))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightseagreen" (DEFINE-RGB-COLOR "lightseagreen" 32/255 178/255 2/3))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "pale-green" (DEFINE-RGB-COLOR "pale-green" 152/255 251/255 152/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "palegreen" (DEFINE-RGB-COLOR "palegreen" 152/255 251/255 152/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "spring-green" (DEFINE-RGB-COLOR "spring-green" 0 1 127/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "springgreen" (DEFINE-RGB-COLOR "springgreen" 0 1 127/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lawn-green" (DEFINE-RGB-COLOR "lawn-green" 124/255 84/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "lawngreen" (DEFINE-RGB-COLOR "lawngreen" 124/255 84/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "green" (DEFINE-RGB-COLOR "green" 0 1 0)) *X11-COLORS-LIST*)

(PUSH (CONS "chartreuse" (DEFINE-RGB-COLOR "chartreuse" 127/255 1 0))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-spring-green"
       (DEFINE-RGB-COLOR "medium-spring-green" 0 50/51 154/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumspringgreen"
       (DEFINE-RGB-COLOR "mediumspringgreen" 0 50/51 154/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "green-yellow" (DEFINE-RGB-COLOR "green-yellow" 173/255 1 47/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "greenyellow" (DEFINE-RGB-COLOR "greenyellow" 173/255 1 47/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lime-green" (DEFINE-RGB-COLOR "lime-green" 10/51 41/51 10/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "limegreen" (DEFINE-RGB-COLOR "limegreen" 10/51 41/51 10/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "yellow-green" (DEFINE-RGB-COLOR "yellow-green" 154/255 41/51 10/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "yellowgreen" (DEFINE-RGB-COLOR "yellowgreen" 154/255 41/51 10/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "forest-green" (DEFINE-RGB-COLOR "forest-green" 2/15 139/255 2/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "forestgreen" (DEFINE-RGB-COLOR "forestgreen" 2/15 139/255 2/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "olive-drab" (DEFINE-RGB-COLOR "olive-drab" 107/255 142/255 7/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "olivedrab" (DEFINE-RGB-COLOR "olivedrab" 107/255 142/255 7/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-khaki" (DEFINE-RGB-COLOR "dark-khaki" 63/85 61/85 107/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkkhaki" (DEFINE-RGB-COLOR "darkkhaki" 63/85 61/85 107/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "khaki" (DEFINE-RGB-COLOR "khaki" 16/17 46/51 28/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "pale-goldenrod" (DEFINE-RGB-COLOR "pale-goldenrod" 14/15 232/255 2/3))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "palegoldenrod" (DEFINE-RGB-COLOR "palegoldenrod" 14/15 232/255 2/3))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "light-goldenrod-yellow"
       (DEFINE-RGB-COLOR "light-goldenrod-yellow" 50/51 50/51 14/17))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrodyellow"
       (DEFINE-RGB-COLOR "lightgoldenrodyellow" 50/51 50/51 14/17))
 *X11-COLORS-LIST*)

(PUSH (CONS "light-yellow" (DEFINE-RGB-COLOR "light-yellow" 1 1 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightyellow" (DEFINE-RGB-COLOR "lightyellow" 1 1 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "yellow" (DEFINE-RGB-COLOR "yellow" 1 1 0)) *X11-COLORS-LIST*)

(PUSH (CONS "gold" (DEFINE-RGB-COLOR "gold" 1 43/51 0)) *X11-COLORS-LIST*)

(PUSH
 (CONS "light-goldenrod"
       (DEFINE-RGB-COLOR "light-goldenrod" 14/15 13/15 26/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrod" (DEFINE-RGB-COLOR "lightgoldenrod" 14/15 13/15 26/51))
 *X11-COLORS-LIST*)

(PUSH (CONS "goldenrod" (DEFINE-RGB-COLOR "goldenrod" 218/255 11/17 32/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-goldenrod"
       (DEFINE-RGB-COLOR "dark-goldenrod" 184/255 134/255 11/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkgoldenrod"
       (DEFINE-RGB-COLOR "darkgoldenrod" 184/255 134/255 11/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "rosy-brown" (DEFINE-RGB-COLOR "rosy-brown" 188/255 143/255 143/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "rosybrown" (DEFINE-RGB-COLOR "rosybrown" 188/255 143/255 143/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "indian-red" (DEFINE-RGB-COLOR "indian-red" 41/51 92/255 92/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "indianred" (DEFINE-RGB-COLOR "indianred" 41/51 92/255 92/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "saddle-brown" (DEFINE-RGB-COLOR "saddle-brown" 139/255 23/85 19/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "saddlebrown" (DEFINE-RGB-COLOR "saddlebrown" 139/255 23/85 19/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "sienna" (DEFINE-RGB-COLOR "sienna" 32/51 82/255 3/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "peru" (DEFINE-RGB-COLOR "peru" 41/51 133/255 21/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "burlywood" (DEFINE-RGB-COLOR "burlywood" 74/85 184/255 9/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "beige" (DEFINE-RGB-COLOR "beige" 49/51 49/51 44/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "wheat" (DEFINE-RGB-COLOR "wheat" 49/51 74/85 179/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "sandy-brown" (DEFINE-RGB-COLOR "sandy-brown" 244/255 164/255 32/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "sandybrown" (DEFINE-RGB-COLOR "sandybrown" 244/255 164/255 32/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "tan" (DEFINE-RGB-COLOR "tan" 14/17 12/17 28/51)) *X11-COLORS-LIST*)

(PUSH (CONS "chocolate" (DEFINE-RGB-COLOR "chocolate" 14/17 7/17 2/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "firebrick" (DEFINE-RGB-COLOR "firebrick" 178/255 2/15 2/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "brown" (DEFINE-RGB-COLOR "brown" 11/17 14/85 14/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "dark-salmon" (DEFINE-RGB-COLOR "dark-salmon" 233/255 10/17 122/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "darksalmon" (DEFINE-RGB-COLOR "darksalmon" 233/255 10/17 122/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "salmon" (DEFINE-RGB-COLOR "salmon" 50/51 128/255 38/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "light-salmon" (DEFINE-RGB-COLOR "light-salmon" 1 32/51 122/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightsalmon" (DEFINE-RGB-COLOR "lightsalmon" 1 32/51 122/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "orange" (DEFINE-RGB-COLOR "orange" 1 11/17 0)) *X11-COLORS-LIST*)

(PUSH (CONS "dark-orange" (DEFINE-RGB-COLOR "dark-orange" 1 28/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorange" (DEFINE-RGB-COLOR "darkorange" 1 28/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "coral" (DEFINE-RGB-COLOR "coral" 1 127/255 16/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "light-coral" (DEFINE-RGB-COLOR "light-coral" 16/17 128/255 128/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightcoral" (DEFINE-RGB-COLOR "lightcoral" 16/17 128/255 128/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "tomato" (DEFINE-RGB-COLOR "tomato" 1 33/85 71/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "orange-red" (DEFINE-RGB-COLOR "orange-red" 1 23/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orangered" (DEFINE-RGB-COLOR "orangered" 1 23/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "red" (DEFINE-RGB-COLOR "red" 1 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "hot-pink" (DEFINE-RGB-COLOR "hot-pink" 1 7/17 12/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "hotpink" (DEFINE-RGB-COLOR "hotpink" 1 7/17 12/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "deep-pink" (DEFINE-RGB-COLOR "deep-pink" 1 4/51 49/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "deeppink" (DEFINE-RGB-COLOR "deeppink" 1 4/51 49/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "pink" (DEFINE-RGB-COLOR "pink" 1 64/85 203/255)) *X11-COLORS-LIST*)

(PUSH (CONS "light-pink" (DEFINE-RGB-COLOR "light-pink" 1 182/255 193/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightpink" (DEFINE-RGB-COLOR "lightpink" 1 182/255 193/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "pale-violet-red"
       (DEFINE-RGB-COLOR "pale-violet-red" 73/85 112/255 49/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "palevioletred" (DEFINE-RGB-COLOR "palevioletred" 73/85 112/255 49/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "maroon" (DEFINE-RGB-COLOR "maroon" 176/255 16/85 32/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-violet-red"
       (DEFINE-RGB-COLOR "medium-violet-red" 199/255 7/85 133/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumvioletred"
       (DEFINE-RGB-COLOR "mediumvioletred" 199/255 7/85 133/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "violet-red" (DEFINE-RGB-COLOR "violet-red" 208/255 32/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "violetred" (DEFINE-RGB-COLOR "violetred" 208/255 32/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "magenta" (DEFINE-RGB-COLOR "magenta" 1 0 1)) *X11-COLORS-LIST*)

(PUSH (CONS "violet" (DEFINE-RGB-COLOR "violet" 14/15 26/51 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "plum" (DEFINE-RGB-COLOR "plum" 13/15 32/51 13/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "orchid" (DEFINE-RGB-COLOR "orchid" 218/255 112/255 214/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-orchid" (DEFINE-RGB-COLOR "medium-orchid" 62/85 1/3 211/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "mediumorchid" (DEFINE-RGB-COLOR "mediumorchid" 62/85 1/3 211/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-orchid" (DEFINE-RGB-COLOR "dark-orchid" 3/5 10/51 4/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorchid" (DEFINE-RGB-COLOR "darkorchid" 3/5 10/51 4/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-violet" (DEFINE-RGB-COLOR "dark-violet" 148/255 0 211/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkviolet" (DEFINE-RGB-COLOR "darkviolet" 148/255 0 211/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "blue-violet" (DEFINE-RGB-COLOR "blue-violet" 46/85 43/255 226/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "blueviolet" (DEFINE-RGB-COLOR "blueviolet" 46/85 43/255 226/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "purple" (DEFINE-RGB-COLOR "purple" 32/51 32/255 16/17))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "medium-purple" (DEFINE-RGB-COLOR "medium-purple" 49/85 112/255 73/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumpurple" (DEFINE-RGB-COLOR "mediumpurple" 49/85 112/255 73/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "thistle" (DEFINE-RGB-COLOR "thistle" 72/85 191/255 72/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "snow1" (DEFINE-RGB-COLOR "snow1" 1 50/51 50/51)) *X11-COLORS-LIST*)

(PUSH (CONS "snow2" (DEFINE-RGB-COLOR "snow2" 14/15 233/255 233/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "snow3" (DEFINE-RGB-COLOR "snow3" 41/51 67/85 67/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "snow4" (DEFINE-RGB-COLOR "snow4" 139/255 137/255 137/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "seashell1" (DEFINE-RGB-COLOR "seashell1" 1 49/51 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "seashell2" (DEFINE-RGB-COLOR "seashell2" 14/15 229/255 74/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "seashell3" (DEFINE-RGB-COLOR "seashell3" 41/51 197/255 191/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "seashell4" (DEFINE-RGB-COLOR "seashell4" 139/255 134/255 26/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "antiquewhite1" (DEFINE-RGB-COLOR "antiquewhite1" 1 239/255 73/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "antiquewhite2" (DEFINE-RGB-COLOR "antiquewhite2" 14/15 223/255 4/5))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "antiquewhite3" (DEFINE-RGB-COLOR "antiquewhite3" 41/51 64/85 176/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "antiquewhite4" (DEFINE-RGB-COLOR "antiquewhite4" 139/255 131/255 8/17))
 *X11-COLORS-LIST*)

(PUSH (CONS "bisque1" (DEFINE-RGB-COLOR "bisque1" 1 76/85 196/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "bisque2" (DEFINE-RGB-COLOR "bisque2" 14/15 71/85 61/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "bisque3" (DEFINE-RGB-COLOR "bisque3" 41/51 61/85 158/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "bisque4" (DEFINE-RGB-COLOR "bisque4" 139/255 25/51 107/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "peachpuff1" (DEFINE-RGB-COLOR "peachpuff1" 1 218/255 37/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "peachpuff2" (DEFINE-RGB-COLOR "peachpuff2" 14/15 203/255 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "peachpuff3" (DEFINE-RGB-COLOR "peachpuff3" 41/51 35/51 149/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "peachpuff4" (DEFINE-RGB-COLOR "peachpuff4" 139/255 7/15 101/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "navajowhite1" (DEFINE-RGB-COLOR "navajowhite1" 1 74/85 173/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "navajowhite2" (DEFINE-RGB-COLOR "navajowhite2" 14/15 69/85 161/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "navajowhite3" (DEFINE-RGB-COLOR "navajowhite3" 41/51 179/255 139/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "navajowhite4" (DEFINE-RGB-COLOR "navajowhite4" 139/255 121/255 94/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lemonchiffon1" (DEFINE-RGB-COLOR "lemonchiffon1" 1 50/51 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lemonchiffon2"
       (DEFINE-RGB-COLOR "lemonchiffon2" 14/15 233/255 191/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lemonchiffon3" (DEFINE-RGB-COLOR "lemonchiffon3" 41/51 67/85 11/17))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lemonchiffon4"
       (DEFINE-RGB-COLOR "lemonchiffon4" 139/255 137/255 112/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "cornsilk1" (DEFINE-RGB-COLOR "cornsilk1" 1 248/255 44/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "cornsilk2" (DEFINE-RGB-COLOR "cornsilk2" 14/15 232/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "cornsilk3" (DEFINE-RGB-COLOR "cornsilk3" 41/51 40/51 59/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "cornsilk4" (DEFINE-RGB-COLOR "cornsilk4" 139/255 8/15 8/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "ivory1" (DEFINE-RGB-COLOR "ivory1" 1 1 16/17)) *X11-COLORS-LIST*)

(PUSH (CONS "ivory2" (DEFINE-RGB-COLOR "ivory2" 14/15 14/15 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "ivory3" (DEFINE-RGB-COLOR "ivory3" 41/51 41/51 193/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "ivory4" (DEFINE-RGB-COLOR "ivory4" 139/255 139/255 131/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "honeydew1" (DEFINE-RGB-COLOR "honeydew1" 16/17 1 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "honeydew2" (DEFINE-RGB-COLOR "honeydew2" 224/255 14/15 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "honeydew3" (DEFINE-RGB-COLOR "honeydew3" 193/255 41/51 193/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "honeydew4" (DEFINE-RGB-COLOR "honeydew4" 131/255 139/255 131/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lavenderblush1" (DEFINE-RGB-COLOR "lavenderblush1" 1 16/17 49/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lavenderblush2"
       (DEFINE-RGB-COLOR "lavenderblush2" 14/15 224/255 229/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lavenderblush3"
       (DEFINE-RGB-COLOR "lavenderblush3" 41/51 193/255 197/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lavenderblush4"
       (DEFINE-RGB-COLOR "lavenderblush4" 139/255 131/255 134/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "mistyrose1" (DEFINE-RGB-COLOR "mistyrose1" 1 76/85 15/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "mistyrose2" (DEFINE-RGB-COLOR "mistyrose2" 14/15 71/85 14/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "mistyrose3" (DEFINE-RGB-COLOR "mistyrose3" 41/51 61/85 181/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "mistyrose4" (DEFINE-RGB-COLOR "mistyrose4" 139/255 25/51 41/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "azure1" (DEFINE-RGB-COLOR "azure1" 16/17 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "azure2" (DEFINE-RGB-COLOR "azure2" 224/255 14/15 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "azure3" (DEFINE-RGB-COLOR "azure3" 193/255 41/51 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "azure4" (DEFINE-RGB-COLOR "azure4" 131/255 139/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "slateblue1" (DEFINE-RGB-COLOR "slateblue1" 131/255 37/85 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "slateblue2" (DEFINE-RGB-COLOR "slateblue2" 122/255 103/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "slateblue3" (DEFINE-RGB-COLOR "slateblue3" 7/17 89/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "slateblue4" (DEFINE-RGB-COLOR "slateblue4" 71/255 4/17 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "royalblue1" (DEFINE-RGB-COLOR "royalblue1" 24/85 118/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "royalblue2" (DEFINE-RGB-COLOR "royalblue2" 67/255 22/51 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "royalblue3" (DEFINE-RGB-COLOR "royalblue3" 58/255 19/51 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "royalblue4" (DEFINE-RGB-COLOR "royalblue4" 13/85 64/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "blue1" (DEFINE-RGB-COLOR "blue1" 0 0 1)) *X11-COLORS-LIST*)

(PUSH (CONS "blue2" (DEFINE-RGB-COLOR "blue2" 0 0 14/15)) *X11-COLORS-LIST*)

(PUSH (CONS "blue3" (DEFINE-RGB-COLOR "blue3" 0 0 41/51)) *X11-COLORS-LIST*)

(PUSH (CONS "blue4" (DEFINE-RGB-COLOR "blue4" 0 0 139/255)) *X11-COLORS-LIST*)

(PUSH (CONS "dodgerblue1" (DEFINE-RGB-COLOR "dodgerblue1" 2/17 48/85 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "dodgerblue2" (DEFINE-RGB-COLOR "dodgerblue2" 28/255 134/255 14/15))
 *X11-COLORS-LIST*)

(PUSH (CONS "dodgerblue3" (DEFINE-RGB-COLOR "dodgerblue3" 8/85 116/255 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "dodgerblue4" (DEFINE-RGB-COLOR "dodgerblue4" 16/255 26/85 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "steelblue1" (DEFINE-RGB-COLOR "steelblue1" 33/85 184/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "steelblue2" (DEFINE-RGB-COLOR "steelblue2" 92/255 172/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "steelblue3" (DEFINE-RGB-COLOR "steelblue3" 79/255 148/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "steelblue4" (DEFINE-RGB-COLOR "steelblue4" 18/85 20/51 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "deepskyblue1" (DEFINE-RGB-COLOR "deepskyblue1" 0 191/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "deepskyblue2" (DEFINE-RGB-COLOR "deepskyblue2" 0 178/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "deepskyblue3" (DEFINE-RGB-COLOR "deepskyblue3" 0 154/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "deepskyblue4" (DEFINE-RGB-COLOR "deepskyblue4" 0 104/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "skyblue1" (DEFINE-RGB-COLOR "skyblue1" 9/17 206/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "skyblue2" (DEFINE-RGB-COLOR "skyblue2" 42/85 64/85 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "skyblue3" (DEFINE-RGB-COLOR "skyblue3" 36/85 166/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "skyblue4" (DEFINE-RGB-COLOR "skyblue4" 74/255 112/255 139/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightskyblue1" (DEFINE-RGB-COLOR "lightskyblue1" 176/255 226/255 1))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightskyblue2"
       (DEFINE-RGB-COLOR "lightskyblue2" 164/255 211/255 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightskyblue3" (DEFINE-RGB-COLOR "lightskyblue3" 47/85 182/255 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightskyblue4" (DEFINE-RGB-COLOR "lightskyblue4" 32/85 41/85 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "slategray1" (DEFINE-RGB-COLOR "slategray1" 66/85 226/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "slategray2" (DEFINE-RGB-COLOR "slategray2" 37/51 211/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "slategray3" (DEFINE-RGB-COLOR "slategray3" 53/85 182/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "slategray4" (DEFINE-RGB-COLOR "slategray4" 36/85 41/85 139/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsteelblue1" (DEFINE-RGB-COLOR "lightsteelblue1" 202/255 15/17 1))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsteelblue2"
       (DEFINE-RGB-COLOR "lightsteelblue2" 188/255 14/17 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsteelblue3"
       (DEFINE-RGB-COLOR "lightsteelblue3" 54/85 181/255 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsteelblue4"
       (DEFINE-RGB-COLOR "lightsteelblue4" 22/51 41/85 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightblue1" (DEFINE-RGB-COLOR "lightblue1" 191/255 239/255 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightblue2" (DEFINE-RGB-COLOR "lightblue2" 178/255 223/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightblue3" (DEFINE-RGB-COLOR "lightblue3" 154/255 64/85 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightblue4" (DEFINE-RGB-COLOR "lightblue4" 104/255 131/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightcyan1" (DEFINE-RGB-COLOR "lightcyan1" 224/255 1 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightcyan2" (DEFINE-RGB-COLOR "lightcyan2" 209/255 14/15 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightcyan3" (DEFINE-RGB-COLOR "lightcyan3" 12/17 41/51 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightcyan4" (DEFINE-RGB-COLOR "lightcyan4" 122/255 139/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "paleturquoise1" (DEFINE-RGB-COLOR "paleturquoise1" 11/15 1 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "paleturquoise2" (DEFINE-RGB-COLOR "paleturquoise2" 58/85 14/15 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "paleturquoise3" (DEFINE-RGB-COLOR "paleturquoise3" 10/17 41/51 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "paleturquoise4"
       (DEFINE-RGB-COLOR "paleturquoise4" 2/5 139/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "cadetblue1" (DEFINE-RGB-COLOR "cadetblue1" 152/255 49/51 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "cadetblue2" (DEFINE-RGB-COLOR "cadetblue2" 142/255 229/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "cadetblue3" (DEFINE-RGB-COLOR "cadetblue3" 122/255 197/255 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "cadetblue4" (DEFINE-RGB-COLOR "cadetblue4" 83/255 134/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "turquoise1" (DEFINE-RGB-COLOR "turquoise1" 0 49/51 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "turquoise2" (DEFINE-RGB-COLOR "turquoise2" 0 229/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "turquoise3" (DEFINE-RGB-COLOR "turquoise3" 0 197/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "turquoise4" (DEFINE-RGB-COLOR "turquoise4" 0 134/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "cyan1" (DEFINE-RGB-COLOR "cyan1" 0 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "cyan2" (DEFINE-RGB-COLOR "cyan2" 0 14/15 14/15)) *X11-COLORS-LIST*)

(PUSH (CONS "cyan3" (DEFINE-RGB-COLOR "cyan3" 0 41/51 41/51)) *X11-COLORS-LIST*)

(PUSH (CONS "cyan4" (DEFINE-RGB-COLOR "cyan4" 0 139/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkslategray1" (DEFINE-RGB-COLOR "darkslategray1" 151/255 1 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslategray2" (DEFINE-RGB-COLOR "darkslategray2" 47/85 14/15 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslategray3"
       (DEFINE-RGB-COLOR "darkslategray3" 121/255 41/51 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkslategray4"
       (DEFINE-RGB-COLOR "darkslategray4" 82/255 139/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "aquamarine1" (DEFINE-RGB-COLOR "aquamarine1" 127/255 1 212/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "aquamarine2" (DEFINE-RGB-COLOR "aquamarine2" 118/255 14/15 66/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "aquamarine3" (DEFINE-RGB-COLOR "aquamarine3" 2/5 41/51 2/3))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "aquamarine4" (DEFINE-RGB-COLOR "aquamarine4" 23/85 139/255 116/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkseagreen1" (DEFINE-RGB-COLOR "darkseagreen1" 193/255 1 193/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkseagreen2" (DEFINE-RGB-COLOR "darkseagreen2" 12/17 14/15 12/17))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkseagreen3" (DEFINE-RGB-COLOR "darkseagreen3" 31/51 41/51 31/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkseagreen4" (DEFINE-RGB-COLOR "darkseagreen4" 7/17 139/255 7/17))
 *X11-COLORS-LIST*)

(PUSH (CONS "seagreen1" (DEFINE-RGB-COLOR "seagreen1" 28/85 1 53/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "seagreen2" (DEFINE-RGB-COLOR "seagreen2" 26/85 14/15 148/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "seagreen3" (DEFINE-RGB-COLOR "seagreen3" 67/255 41/51 128/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "seagreen4" (DEFINE-RGB-COLOR "seagreen4" 46/255 139/255 29/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "palegreen1" (DEFINE-RGB-COLOR "palegreen1" 154/255 1 154/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "palegreen2" (DEFINE-RGB-COLOR "palegreen2" 48/85 14/15 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "palegreen3" (DEFINE-RGB-COLOR "palegreen3" 124/255 41/51 124/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "palegreen4" (DEFINE-RGB-COLOR "palegreen4" 28/85 139/255 28/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "springgreen1" (DEFINE-RGB-COLOR "springgreen1" 0 1 127/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "springgreen2" (DEFINE-RGB-COLOR "springgreen2" 0 14/15 118/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "springgreen3" (DEFINE-RGB-COLOR "springgreen3" 0 41/51 2/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "springgreen4" (DEFINE-RGB-COLOR "springgreen4" 0 139/255 23/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "green1" (DEFINE-RGB-COLOR "green1" 0 1 0)) *X11-COLORS-LIST*)

(PUSH (CONS "green2" (DEFINE-RGB-COLOR "green2" 0 14/15 0)) *X11-COLORS-LIST*)

(PUSH (CONS "green3" (DEFINE-RGB-COLOR "green3" 0 41/51 0)) *X11-COLORS-LIST*)

(PUSH (CONS "green4" (DEFINE-RGB-COLOR "green4" 0 139/255 0)) *X11-COLORS-LIST*)

(PUSH (CONS "chartreuse1" (DEFINE-RGB-COLOR "chartreuse1" 127/255 1 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "chartreuse2" (DEFINE-RGB-COLOR "chartreuse2" 118/255 14/15 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "chartreuse3" (DEFINE-RGB-COLOR "chartreuse3" 2/5 41/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "chartreuse4" (DEFINE-RGB-COLOR "chartreuse4" 23/85 139/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "olivedrab1" (DEFINE-RGB-COLOR "olivedrab1" 64/85 1 62/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "olivedrab2" (DEFINE-RGB-COLOR "olivedrab2" 179/255 14/15 58/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "olivedrab3" (DEFINE-RGB-COLOR "olivedrab3" 154/255 41/51 10/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "olivedrab4" (DEFINE-RGB-COLOR "olivedrab4" 7/17 139/255 2/15))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "darkolivegreen1"
       (DEFINE-RGB-COLOR "darkolivegreen1" 202/255 1 112/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkolivegreen2"
       (DEFINE-RGB-COLOR "darkolivegreen2" 188/255 14/15 104/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkolivegreen3" (DEFINE-RGB-COLOR "darkolivegreen3" 54/85 41/51 6/17))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkolivegreen4"
       (DEFINE-RGB-COLOR "darkolivegreen4" 22/51 139/255 61/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "khaki1" (DEFINE-RGB-COLOR "khaki1" 1 82/85 143/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "khaki2" (DEFINE-RGB-COLOR "khaki2" 14/15 46/51 133/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "khaki3" (DEFINE-RGB-COLOR "khaki3" 41/51 66/85 23/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "khaki4" (DEFINE-RGB-COLOR "khaki4" 139/255 134/255 26/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrod1"
       (DEFINE-RGB-COLOR "lightgoldenrod1" 1 236/255 139/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrod2"
       (DEFINE-RGB-COLOR "lightgoldenrod2" 14/15 44/51 26/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrod3"
       (DEFINE-RGB-COLOR "lightgoldenrod3" 41/51 38/51 112/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightgoldenrod4"
       (DEFINE-RGB-COLOR "lightgoldenrod4" 139/255 43/85 76/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightyellow1" (DEFINE-RGB-COLOR "lightyellow1" 1 1 224/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightyellow2" (DEFINE-RGB-COLOR "lightyellow2" 14/15 14/15 209/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "lightyellow3" (DEFINE-RGB-COLOR "lightyellow3" 41/51 41/51 12/17))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightyellow4"
       (DEFINE-RGB-COLOR "lightyellow4" 139/255 139/255 122/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "yellow1" (DEFINE-RGB-COLOR "yellow1" 1 1 0)) *X11-COLORS-LIST*)

(PUSH (CONS "yellow2" (DEFINE-RGB-COLOR "yellow2" 14/15 14/15 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "yellow3" (DEFINE-RGB-COLOR "yellow3" 41/51 41/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "yellow4" (DEFINE-RGB-COLOR "yellow4" 139/255 139/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "gold1" (DEFINE-RGB-COLOR "gold1" 1 43/51 0)) *X11-COLORS-LIST*)

(PUSH (CONS "gold2" (DEFINE-RGB-COLOR "gold2" 14/15 67/85 0)) *X11-COLORS-LIST*)

(PUSH (CONS "gold3" (DEFINE-RGB-COLOR "gold3" 41/51 173/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "gold4" (DEFINE-RGB-COLOR "gold4" 139/255 39/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "goldenrod1" (DEFINE-RGB-COLOR "goldenrod1" 1 193/255 37/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "goldenrod2" (DEFINE-RGB-COLOR "goldenrod2" 14/15 12/17 2/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "goldenrod3" (DEFINE-RGB-COLOR "goldenrod3" 41/51 31/51 29/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "goldenrod4" (DEFINE-RGB-COLOR "goldenrod4" 139/255 7/17 4/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkgoldenrod1" (DEFINE-RGB-COLOR "darkgoldenrod1" 1 37/51 1/17))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "darkgoldenrod2"
       (DEFINE-RGB-COLOR "darkgoldenrod2" 14/15 173/255 14/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkgoldenrod3" (DEFINE-RGB-COLOR "darkgoldenrod3" 41/51 149/255 4/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "darkgoldenrod4"
       (DEFINE-RGB-COLOR "darkgoldenrod4" 139/255 101/255 8/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "rosybrown1" (DEFINE-RGB-COLOR "rosybrown1" 1 193/255 193/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "rosybrown2" (DEFINE-RGB-COLOR "rosybrown2" 14/15 12/17 12/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "rosybrown3" (DEFINE-RGB-COLOR "rosybrown3" 41/51 31/51 31/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "rosybrown4" (DEFINE-RGB-COLOR "rosybrown4" 139/255 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "indianred1" (DEFINE-RGB-COLOR "indianred1" 1 106/255 106/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "indianred2" (DEFINE-RGB-COLOR "indianred2" 14/15 33/85 33/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "indianred3" (DEFINE-RGB-COLOR "indianred3" 41/51 1/3 1/3))
      *X11-COLORS-LIST*)

(PUSH (CONS "indianred4" (DEFINE-RGB-COLOR "indianred4" 139/255 58/255 58/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "sienna1" (DEFINE-RGB-COLOR "sienna1" 1 26/51 71/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "sienna2" (DEFINE-RGB-COLOR "sienna2" 14/15 121/255 22/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "sienna3" (DEFINE-RGB-COLOR "sienna3" 41/51 104/255 19/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "sienna4" (DEFINE-RGB-COLOR "sienna4" 139/255 71/255 38/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "burlywood1" (DEFINE-RGB-COLOR "burlywood1" 1 211/255 31/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "burlywood2" (DEFINE-RGB-COLOR "burlywood2" 14/15 197/255 29/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "burlywood3" (DEFINE-RGB-COLOR "burlywood3" 41/51 2/3 25/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "burlywood4" (DEFINE-RGB-COLOR "burlywood4" 139/255 23/51 1/3))
      *X11-COLORS-LIST*)

(PUSH (CONS "wheat1" (DEFINE-RGB-COLOR "wheat1" 1 77/85 62/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "wheat2" (DEFINE-RGB-COLOR "wheat2" 14/15 72/85 58/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "wheat3" (DEFINE-RGB-COLOR "wheat3" 41/51 62/85 10/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "wheat4" (DEFINE-RGB-COLOR "wheat4" 139/255 42/85 2/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "tan1" (DEFINE-RGB-COLOR "tan1" 1 11/17 79/255)) *X11-COLORS-LIST*)

(PUSH (CONS "tan2" (DEFINE-RGB-COLOR "tan2" 14/15 154/255 73/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "tan3" (DEFINE-RGB-COLOR "tan3" 41/51 133/255 21/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "tan4" (DEFINE-RGB-COLOR "tan4" 139/255 6/17 43/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "chocolate1" (DEFINE-RGB-COLOR "chocolate1" 1 127/255 12/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "chocolate2" (DEFINE-RGB-COLOR "chocolate2" 14/15 118/255 11/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "chocolate3" (DEFINE-RGB-COLOR "chocolate3" 41/51 2/5 29/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "chocolate4" (DEFINE-RGB-COLOR "chocolate4" 139/255 23/85 19/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "firebrick1" (DEFINE-RGB-COLOR "firebrick1" 1 16/85 16/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "firebrick2" (DEFINE-RGB-COLOR "firebrick2" 14/15 44/255 44/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "firebrick3" (DEFINE-RGB-COLOR "firebrick3" 41/51 38/255 38/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "firebrick4" (DEFINE-RGB-COLOR "firebrick4" 139/255 26/255 26/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "brown1" (DEFINE-RGB-COLOR "brown1" 1 64/255 64/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "brown2" (DEFINE-RGB-COLOR "brown2" 14/15 59/255 59/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "brown3" (DEFINE-RGB-COLOR "brown3" 41/51 1/5 1/5))
      *X11-COLORS-LIST*)

(PUSH (CONS "brown4" (DEFINE-RGB-COLOR "brown4" 139/255 7/51 7/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "salmon1" (DEFINE-RGB-COLOR "salmon1" 1 28/51 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "salmon2" (DEFINE-RGB-COLOR "salmon2" 14/15 26/51 98/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "salmon3" (DEFINE-RGB-COLOR "salmon3" 41/51 112/255 28/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "salmon4" (DEFINE-RGB-COLOR "salmon4" 139/255 76/255 19/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightsalmon1" (DEFINE-RGB-COLOR "lightsalmon1" 1 32/51 122/255))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsalmon2" (DEFINE-RGB-COLOR "lightsalmon2" 14/15 149/255 38/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsalmon3" (DEFINE-RGB-COLOR "lightsalmon3" 41/51 43/85 98/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "lightsalmon4" (DEFINE-RGB-COLOR "lightsalmon4" 139/255 29/85 22/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "orange1" (DEFINE-RGB-COLOR "orange1" 1 11/17 0)) *X11-COLORS-LIST*)

(PUSH (CONS "orange2" (DEFINE-RGB-COLOR "orange2" 14/15 154/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orange3" (DEFINE-RGB-COLOR "orange3" 41/51 133/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orange4" (DEFINE-RGB-COLOR "orange4" 139/255 6/17 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorange1" (DEFINE-RGB-COLOR "darkorange1" 1 127/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorange2" (DEFINE-RGB-COLOR "darkorange2" 14/15 118/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorange3" (DEFINE-RGB-COLOR "darkorange3" 41/51 2/5 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkorange4" (DEFINE-RGB-COLOR "darkorange4" 139/255 23/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "coral1" (DEFINE-RGB-COLOR "coral1" 1 38/85 86/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "coral2" (DEFINE-RGB-COLOR "coral2" 14/15 106/255 16/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "coral3" (DEFINE-RGB-COLOR "coral3" 41/51 91/255 23/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "coral4" (DEFINE-RGB-COLOR "coral4" 139/255 62/255 47/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "tomato1" (DEFINE-RGB-COLOR "tomato1" 1 33/85 71/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "tomato2" (DEFINE-RGB-COLOR "tomato2" 14/15 92/255 22/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "tomato3" (DEFINE-RGB-COLOR "tomato3" 41/51 79/255 19/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "tomato4" (DEFINE-RGB-COLOR "tomato4" 139/255 18/85 38/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "orangered1" (DEFINE-RGB-COLOR "orangered1" 1 23/85 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orangered2" (DEFINE-RGB-COLOR "orangered2" 14/15 64/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orangered3" (DEFINE-RGB-COLOR "orangered3" 41/51 11/51 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "orangered4" (DEFINE-RGB-COLOR "orangered4" 139/255 37/255 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "red1" (DEFINE-RGB-COLOR "red1" 1 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "red2" (DEFINE-RGB-COLOR "red2" 14/15 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "red3" (DEFINE-RGB-COLOR "red3" 41/51 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "red4" (DEFINE-RGB-COLOR "red4" 139/255 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "debianred" (DEFINE-RGB-COLOR "debianred" 43/51 7/255 27/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "deeppink1" (DEFINE-RGB-COLOR "deeppink1" 1 4/51 49/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "deeppink2" (DEFINE-RGB-COLOR "deeppink2" 14/15 6/85 137/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "deeppink3" (DEFINE-RGB-COLOR "deeppink3" 41/51 16/255 118/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "deeppink4" (DEFINE-RGB-COLOR "deeppink4" 139/255 2/51 16/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "hotpink1" (DEFINE-RGB-COLOR "hotpink1" 1 22/51 12/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "hotpink2" (DEFINE-RGB-COLOR "hotpink2" 14/15 106/255 167/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "hotpink3" (DEFINE-RGB-COLOR "hotpink3" 41/51 32/85 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "hotpink4" (DEFINE-RGB-COLOR "hotpink4" 139/255 58/255 98/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "pink1" (DEFINE-RGB-COLOR "pink1" 1 181/255 197/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "pink2" (DEFINE-RGB-COLOR "pink2" 14/15 169/255 184/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "pink3" (DEFINE-RGB-COLOR "pink3" 41/51 29/51 158/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "pink4" (DEFINE-RGB-COLOR "pink4" 139/255 33/85 36/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightpink1" (DEFINE-RGB-COLOR "lightpink1" 1 58/85 37/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightpink2" (DEFINE-RGB-COLOR "lightpink2" 14/15 54/85 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightpink3" (DEFINE-RGB-COLOR "lightpink3" 41/51 28/51 149/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightpink4" (DEFINE-RGB-COLOR "lightpink4" 139/255 19/51 101/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "palevioletred1" (DEFINE-RGB-COLOR "palevioletred1" 1 26/51 57/85))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "palevioletred2"
       (DEFINE-RGB-COLOR "palevioletred2" 14/15 121/255 53/85))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "palevioletred3"
       (DEFINE-RGB-COLOR "palevioletred3" 41/51 104/255 137/255))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "palevioletred4"
       (DEFINE-RGB-COLOR "palevioletred4" 139/255 71/255 31/85))
 *X11-COLORS-LIST*)

(PUSH (CONS "maroon1" (DEFINE-RGB-COLOR "maroon1" 1 52/255 179/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "maroon2" (DEFINE-RGB-COLOR "maroon2" 14/15 16/85 167/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "maroon3" (DEFINE-RGB-COLOR "maroon3" 41/51 41/255 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "maroon4" (DEFINE-RGB-COLOR "maroon4" 139/255 28/255 98/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "violetred1" (DEFINE-RGB-COLOR "violetred1" 1 62/255 10/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "violetred2" (DEFINE-RGB-COLOR "violetred2" 14/15 58/255 28/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "violetred3" (DEFINE-RGB-COLOR "violetred3" 41/51 10/51 8/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "violetred4" (DEFINE-RGB-COLOR "violetred4" 139/255 2/15 82/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "magenta1" (DEFINE-RGB-COLOR "magenta1" 1 0 1)) *X11-COLORS-LIST*)

(PUSH (CONS "magenta2" (DEFINE-RGB-COLOR "magenta2" 14/15 0 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "magenta3" (DEFINE-RGB-COLOR "magenta3" 41/51 0 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "magenta4" (DEFINE-RGB-COLOR "magenta4" 139/255 0 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "orchid1" (DEFINE-RGB-COLOR "orchid1" 1 131/255 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "orchid2" (DEFINE-RGB-COLOR "orchid2" 14/15 122/255 233/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "orchid3" (DEFINE-RGB-COLOR "orchid3" 41/51 7/17 67/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "orchid4" (DEFINE-RGB-COLOR "orchid4" 139/255 71/255 137/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "plum1" (DEFINE-RGB-COLOR "plum1" 1 11/15 1)) *X11-COLORS-LIST*)

(PUSH (CONS "plum2" (DEFINE-RGB-COLOR "plum2" 14/15 58/85 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "plum3" (DEFINE-RGB-COLOR "plum3" 41/51 10/17 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "plum4" (DEFINE-RGB-COLOR "plum4" 139/255 2/5 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "mediumorchid1" (DEFINE-RGB-COLOR "mediumorchid1" 224/255 2/5 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumorchid2" (DEFINE-RGB-COLOR "mediumorchid2" 209/255 19/51 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumorchid3" (DEFINE-RGB-COLOR "mediumorchid3" 12/17 82/255 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumorchid4"
       (DEFINE-RGB-COLOR "mediumorchid4" 122/255 11/51 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "darkorchid1" (DEFINE-RGB-COLOR "darkorchid1" 191/255 62/255 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "darkorchid2" (DEFINE-RGB-COLOR "darkorchid2" 178/255 58/255 14/15))
 *X11-COLORS-LIST*)

(PUSH (CONS "darkorchid3" (DEFINE-RGB-COLOR "darkorchid3" 154/255 10/51 41/51))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "darkorchid4" (DEFINE-RGB-COLOR "darkorchid4" 104/255 2/15 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "purple1" (DEFINE-RGB-COLOR "purple1" 31/51 16/85 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "purple2" (DEFINE-RGB-COLOR "purple2" 29/51 44/255 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "purple3" (DEFINE-RGB-COLOR "purple3" 25/51 38/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "purple4" (DEFINE-RGB-COLOR "purple4" 1/3 26/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "mediumpurple1" (DEFINE-RGB-COLOR "mediumpurple1" 57/85 26/51 1))
      *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumpurple2" (DEFINE-RGB-COLOR "mediumpurple2" 53/85 121/255 14/15))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumpurple3"
       (DEFINE-RGB-COLOR "mediumpurple3" 137/255 104/255 41/51))
 *X11-COLORS-LIST*)

(PUSH
 (CONS "mediumpurple4" (DEFINE-RGB-COLOR "mediumpurple4" 31/85 71/255 139/255))
 *X11-COLORS-LIST*)

(PUSH (CONS "thistle1" (DEFINE-RGB-COLOR "thistle1" 1 15/17 1))
      *X11-COLORS-LIST*)

(PUSH (CONS "thistle2" (DEFINE-RGB-COLOR "thistle2" 14/15 14/17 14/15))
      *X11-COLORS-LIST*)

(PUSH (CONS "thistle3" (DEFINE-RGB-COLOR "thistle3" 41/51 181/255 41/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "thistle4" (DEFINE-RGB-COLOR "thistle4" 139/255 41/85 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray0" (DEFINE-RGB-COLOR "gray0" 0 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "grey0" (DEFINE-RGB-COLOR "grey0" 0 0 0)) *X11-COLORS-LIST*)

(PUSH (CONS "gray1" (DEFINE-RGB-COLOR "gray1" 1/85 1/85 1/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey1" (DEFINE-RGB-COLOR "grey1" 1/85 1/85 1/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray2" (DEFINE-RGB-COLOR "gray2" 1/51 1/51 1/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey2" (DEFINE-RGB-COLOR "grey2" 1/51 1/51 1/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray3" (DEFINE-RGB-COLOR "gray3" 8/255 8/255 8/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey3" (DEFINE-RGB-COLOR "grey3" 8/255 8/255 8/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray4" (DEFINE-RGB-COLOR "gray4" 2/51 2/51 2/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey4" (DEFINE-RGB-COLOR "grey4" 2/51 2/51 2/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray5" (DEFINE-RGB-COLOR "gray5" 13/255 13/255 13/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey5" (DEFINE-RGB-COLOR "grey5" 13/255 13/255 13/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray6" (DEFINE-RGB-COLOR "gray6" 1/17 1/17 1/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey6" (DEFINE-RGB-COLOR "grey6" 1/17 1/17 1/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray7" (DEFINE-RGB-COLOR "gray7" 6/85 6/85 6/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey7" (DEFINE-RGB-COLOR "grey7" 6/85 6/85 6/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray8" (DEFINE-RGB-COLOR "gray8" 4/51 4/51 4/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey8" (DEFINE-RGB-COLOR "grey8" 4/51 4/51 4/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray9" (DEFINE-RGB-COLOR "gray9" 23/255 23/255 23/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey9" (DEFINE-RGB-COLOR "grey9" 23/255 23/255 23/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray10" (DEFINE-RGB-COLOR "gray10" 26/255 26/255 26/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey10" (DEFINE-RGB-COLOR "grey10" 26/255 26/255 26/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray11" (DEFINE-RGB-COLOR "gray11" 28/255 28/255 28/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey11" (DEFINE-RGB-COLOR "grey11" 28/255 28/255 28/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray12" (DEFINE-RGB-COLOR "gray12" 31/255 31/255 31/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey12" (DEFINE-RGB-COLOR "grey12" 31/255 31/255 31/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray13" (DEFINE-RGB-COLOR "gray13" 11/85 11/85 11/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey13" (DEFINE-RGB-COLOR "grey13" 11/85 11/85 11/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray14" (DEFINE-RGB-COLOR "gray14" 12/85 12/85 12/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey14" (DEFINE-RGB-COLOR "grey14" 12/85 12/85 12/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray15" (DEFINE-RGB-COLOR "gray15" 38/255 38/255 38/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey15" (DEFINE-RGB-COLOR "grey15" 38/255 38/255 38/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray16" (DEFINE-RGB-COLOR "gray16" 41/255 41/255 41/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey16" (DEFINE-RGB-COLOR "grey16" 41/255 41/255 41/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray17" (DEFINE-RGB-COLOR "gray17" 43/255 43/255 43/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey17" (DEFINE-RGB-COLOR "grey17" 43/255 43/255 43/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray18" (DEFINE-RGB-COLOR "gray18" 46/255 46/255 46/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey18" (DEFINE-RGB-COLOR "grey18" 46/255 46/255 46/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray19" (DEFINE-RGB-COLOR "gray19" 16/85 16/85 16/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey19" (DEFINE-RGB-COLOR "grey19" 16/85 16/85 16/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray20" (DEFINE-RGB-COLOR "gray20" 1/5 1/5 1/5)) *X11-COLORS-LIST*)

(PUSH (CONS "grey20" (DEFINE-RGB-COLOR "grey20" 1/5 1/5 1/5)) *X11-COLORS-LIST*)

(PUSH (CONS "gray21" (DEFINE-RGB-COLOR "gray21" 18/85 18/85 18/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey21" (DEFINE-RGB-COLOR "grey21" 18/85 18/85 18/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray22" (DEFINE-RGB-COLOR "gray22" 56/255 56/255 56/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey22" (DEFINE-RGB-COLOR "grey22" 56/255 56/255 56/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray23" (DEFINE-RGB-COLOR "gray23" 59/255 59/255 59/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey23" (DEFINE-RGB-COLOR "grey23" 59/255 59/255 59/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray24" (DEFINE-RGB-COLOR "gray24" 61/255 61/255 61/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey24" (DEFINE-RGB-COLOR "grey24" 61/255 61/255 61/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray25" (DEFINE-RGB-COLOR "gray25" 64/255 64/255 64/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey25" (DEFINE-RGB-COLOR "grey25" 64/255 64/255 64/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray26" (DEFINE-RGB-COLOR "gray26" 22/85 22/85 22/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey26" (DEFINE-RGB-COLOR "grey26" 22/85 22/85 22/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray27" (DEFINE-RGB-COLOR "gray27" 23/85 23/85 23/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey27" (DEFINE-RGB-COLOR "grey27" 23/85 23/85 23/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray28" (DEFINE-RGB-COLOR "gray28" 71/255 71/255 71/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey28" (DEFINE-RGB-COLOR "grey28" 71/255 71/255 71/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray29" (DEFINE-RGB-COLOR "gray29" 74/255 74/255 74/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey29" (DEFINE-RGB-COLOR "grey29" 74/255 74/255 74/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray30" (DEFINE-RGB-COLOR "gray30" 77/255 77/255 77/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey30" (DEFINE-RGB-COLOR "grey30" 77/255 77/255 77/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray31" (DEFINE-RGB-COLOR "gray31" 79/255 79/255 79/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey31" (DEFINE-RGB-COLOR "grey31" 79/255 79/255 79/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray32" (DEFINE-RGB-COLOR "gray32" 82/255 82/255 82/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey32" (DEFINE-RGB-COLOR "grey32" 82/255 82/255 82/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray33" (DEFINE-RGB-COLOR "gray33" 28/85 28/85 28/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey33" (DEFINE-RGB-COLOR "grey33" 28/85 28/85 28/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray34" (DEFINE-RGB-COLOR "gray34" 29/85 29/85 29/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey34" (DEFINE-RGB-COLOR "grey34" 29/85 29/85 29/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray35" (DEFINE-RGB-COLOR "gray35" 89/255 89/255 89/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey35" (DEFINE-RGB-COLOR "grey35" 89/255 89/255 89/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray36" (DEFINE-RGB-COLOR "gray36" 92/255 92/255 92/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey36" (DEFINE-RGB-COLOR "grey36" 92/255 92/255 92/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray37" (DEFINE-RGB-COLOR "gray37" 94/255 94/255 94/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey37" (DEFINE-RGB-COLOR "grey37" 94/255 94/255 94/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray38" (DEFINE-RGB-COLOR "gray38" 97/255 97/255 97/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey38" (DEFINE-RGB-COLOR "grey38" 97/255 97/255 97/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray39" (DEFINE-RGB-COLOR "gray39" 33/85 33/85 33/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey39" (DEFINE-RGB-COLOR "grey39" 33/85 33/85 33/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray40" (DEFINE-RGB-COLOR "gray40" 2/5 2/5 2/5)) *X11-COLORS-LIST*)

(PUSH (CONS "grey40" (DEFINE-RGB-COLOR "grey40" 2/5 2/5 2/5)) *X11-COLORS-LIST*)

(PUSH (CONS "gray41" (DEFINE-RGB-COLOR "gray41" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey41" (DEFINE-RGB-COLOR "grey41" 7/17 7/17 7/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray42" (DEFINE-RGB-COLOR "gray42" 107/255 107/255 107/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey42" (DEFINE-RGB-COLOR "grey42" 107/255 107/255 107/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray43" (DEFINE-RGB-COLOR "gray43" 22/51 22/51 22/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey43" (DEFINE-RGB-COLOR "grey43" 22/51 22/51 22/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray44" (DEFINE-RGB-COLOR "gray44" 112/255 112/255 112/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey44" (DEFINE-RGB-COLOR "grey44" 112/255 112/255 112/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray45" (DEFINE-RGB-COLOR "gray45" 23/51 23/51 23/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey45" (DEFINE-RGB-COLOR "grey45" 23/51 23/51 23/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray46" (DEFINE-RGB-COLOR "gray46" 39/85 39/85 39/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey46" (DEFINE-RGB-COLOR "grey46" 39/85 39/85 39/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray47" (DEFINE-RGB-COLOR "gray47" 8/17 8/17 8/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey47" (DEFINE-RGB-COLOR "grey47" 8/17 8/17 8/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray48" (DEFINE-RGB-COLOR "gray48" 122/255 122/255 122/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey48" (DEFINE-RGB-COLOR "grey48" 122/255 122/255 122/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray49" (DEFINE-RGB-COLOR "gray49" 25/51 25/51 25/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey49" (DEFINE-RGB-COLOR "grey49" 25/51 25/51 25/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray50" (DEFINE-RGB-COLOR "gray50" 127/255 127/255 127/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey50" (DEFINE-RGB-COLOR "grey50" 127/255 127/255 127/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray51" (DEFINE-RGB-COLOR "gray51" 26/51 26/51 26/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey51" (DEFINE-RGB-COLOR "grey51" 26/51 26/51 26/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray52" (DEFINE-RGB-COLOR "gray52" 133/255 133/255 133/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey52" (DEFINE-RGB-COLOR "grey52" 133/255 133/255 133/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray53" (DEFINE-RGB-COLOR "gray53" 9/17 9/17 9/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey53" (DEFINE-RGB-COLOR "grey53" 9/17 9/17 9/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray54" (DEFINE-RGB-COLOR "gray54" 46/85 46/85 46/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey54" (DEFINE-RGB-COLOR "grey54" 46/85 46/85 46/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray55" (DEFINE-RGB-COLOR "gray55" 28/51 28/51 28/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey55" (DEFINE-RGB-COLOR "grey55" 28/51 28/51 28/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray56" (DEFINE-RGB-COLOR "gray56" 143/255 143/255 143/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey56" (DEFINE-RGB-COLOR "grey56" 143/255 143/255 143/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray57" (DEFINE-RGB-COLOR "gray57" 29/51 29/51 29/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey57" (DEFINE-RGB-COLOR "grey57" 29/51 29/51 29/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray58" (DEFINE-RGB-COLOR "gray58" 148/255 148/255 148/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey58" (DEFINE-RGB-COLOR "grey58" 148/255 148/255 148/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray59" (DEFINE-RGB-COLOR "gray59" 10/17 10/17 10/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey59" (DEFINE-RGB-COLOR "grey59" 10/17 10/17 10/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray60" (DEFINE-RGB-COLOR "gray60" 3/5 3/5 3/5)) *X11-COLORS-LIST*)

(PUSH (CONS "grey60" (DEFINE-RGB-COLOR "grey60" 3/5 3/5 3/5)) *X11-COLORS-LIST*)

(PUSH (CONS "gray61" (DEFINE-RGB-COLOR "gray61" 52/85 52/85 52/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey61" (DEFINE-RGB-COLOR "grey61" 52/85 52/85 52/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray62" (DEFINE-RGB-COLOR "gray62" 158/255 158/255 158/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey62" (DEFINE-RGB-COLOR "grey62" 158/255 158/255 158/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray63" (DEFINE-RGB-COLOR "gray63" 161/255 161/255 161/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey63" (DEFINE-RGB-COLOR "grey63" 161/255 161/255 161/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray64" (DEFINE-RGB-COLOR "gray64" 163/255 163/255 163/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey64" (DEFINE-RGB-COLOR "grey64" 163/255 163/255 163/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray65" (DEFINE-RGB-COLOR "gray65" 166/255 166/255 166/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey65" (DEFINE-RGB-COLOR "grey65" 166/255 166/255 166/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray66" (DEFINE-RGB-COLOR "gray66" 56/85 56/85 56/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey66" (DEFINE-RGB-COLOR "grey66" 56/85 56/85 56/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray67" (DEFINE-RGB-COLOR "gray67" 57/85 57/85 57/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey67" (DEFINE-RGB-COLOR "grey67" 57/85 57/85 57/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray68" (DEFINE-RGB-COLOR "gray68" 173/255 173/255 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey68" (DEFINE-RGB-COLOR "grey68" 173/255 173/255 173/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray69" (DEFINE-RGB-COLOR "gray69" 176/255 176/255 176/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey69" (DEFINE-RGB-COLOR "grey69" 176/255 176/255 176/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray70" (DEFINE-RGB-COLOR "gray70" 179/255 179/255 179/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey70" (DEFINE-RGB-COLOR "grey70" 179/255 179/255 179/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray71" (DEFINE-RGB-COLOR "gray71" 181/255 181/255 181/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey71" (DEFINE-RGB-COLOR "grey71" 181/255 181/255 181/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray72" (DEFINE-RGB-COLOR "gray72" 184/255 184/255 184/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey72" (DEFINE-RGB-COLOR "grey72" 184/255 184/255 184/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray73" (DEFINE-RGB-COLOR "gray73" 62/85 62/85 62/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey73" (DEFINE-RGB-COLOR "grey73" 62/85 62/85 62/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray74" (DEFINE-RGB-COLOR "gray74" 63/85 63/85 63/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey74" (DEFINE-RGB-COLOR "grey74" 63/85 63/85 63/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray75" (DEFINE-RGB-COLOR "gray75" 191/255 191/255 191/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey75" (DEFINE-RGB-COLOR "grey75" 191/255 191/255 191/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray76" (DEFINE-RGB-COLOR "gray76" 194/255 194/255 194/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey76" (DEFINE-RGB-COLOR "grey76" 194/255 194/255 194/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray77" (DEFINE-RGB-COLOR "gray77" 196/255 196/255 196/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey77" (DEFINE-RGB-COLOR "grey77" 196/255 196/255 196/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray78" (DEFINE-RGB-COLOR "gray78" 199/255 199/255 199/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey78" (DEFINE-RGB-COLOR "grey78" 199/255 199/255 199/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray79" (DEFINE-RGB-COLOR "gray79" 67/85 67/85 67/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey79" (DEFINE-RGB-COLOR "grey79" 67/85 67/85 67/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray80" (DEFINE-RGB-COLOR "gray80" 4/5 4/5 4/5)) *X11-COLORS-LIST*)

(PUSH (CONS "grey80" (DEFINE-RGB-COLOR "grey80" 4/5 4/5 4/5)) *X11-COLORS-LIST*)

(PUSH (CONS "gray81" (DEFINE-RGB-COLOR "gray81" 69/85 69/85 69/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey81" (DEFINE-RGB-COLOR "grey81" 69/85 69/85 69/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray82" (DEFINE-RGB-COLOR "gray82" 209/255 209/255 209/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey82" (DEFINE-RGB-COLOR "grey82" 209/255 209/255 209/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray83" (DEFINE-RGB-COLOR "gray83" 212/255 212/255 212/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey83" (DEFINE-RGB-COLOR "grey83" 212/255 212/255 212/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray84" (DEFINE-RGB-COLOR "gray84" 214/255 214/255 214/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey84" (DEFINE-RGB-COLOR "grey84" 214/255 214/255 214/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray85" (DEFINE-RGB-COLOR "gray85" 217/255 217/255 217/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey85" (DEFINE-RGB-COLOR "grey85" 217/255 217/255 217/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray86" (DEFINE-RGB-COLOR "gray86" 73/85 73/85 73/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey86" (DEFINE-RGB-COLOR "grey86" 73/85 73/85 73/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray87" (DEFINE-RGB-COLOR "gray87" 74/85 74/85 74/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey87" (DEFINE-RGB-COLOR "grey87" 74/85 74/85 74/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray88" (DEFINE-RGB-COLOR "gray88" 224/255 224/255 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey88" (DEFINE-RGB-COLOR "grey88" 224/255 224/255 224/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray89" (DEFINE-RGB-COLOR "gray89" 227/255 227/255 227/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey89" (DEFINE-RGB-COLOR "grey89" 227/255 227/255 227/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray90" (DEFINE-RGB-COLOR "gray90" 229/255 229/255 229/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey90" (DEFINE-RGB-COLOR "grey90" 229/255 229/255 229/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray91" (DEFINE-RGB-COLOR "gray91" 232/255 232/255 232/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey91" (DEFINE-RGB-COLOR "grey91" 232/255 232/255 232/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray92" (DEFINE-RGB-COLOR "gray92" 47/51 47/51 47/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey92" (DEFINE-RGB-COLOR "grey92" 47/51 47/51 47/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray93" (DEFINE-RGB-COLOR "gray93" 79/85 79/85 79/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey93" (DEFINE-RGB-COLOR "grey93" 79/85 79/85 79/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray94" (DEFINE-RGB-COLOR "gray94" 16/17 16/17 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey94" (DEFINE-RGB-COLOR "grey94" 16/17 16/17 16/17))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray95" (DEFINE-RGB-COLOR "gray95" 242/255 242/255 242/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey95" (DEFINE-RGB-COLOR "grey95" 242/255 242/255 242/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray96" (DEFINE-RGB-COLOR "gray96" 49/51 49/51 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey96" (DEFINE-RGB-COLOR "grey96" 49/51 49/51 49/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray97" (DEFINE-RGB-COLOR "gray97" 247/255 247/255 247/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey97" (DEFINE-RGB-COLOR "grey97" 247/255 247/255 247/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray98" (DEFINE-RGB-COLOR "gray98" 50/51 50/51 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey98" (DEFINE-RGB-COLOR "grey98" 50/51 50/51 50/51))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray99" (DEFINE-RGB-COLOR "gray99" 84/85 84/85 84/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "grey99" (DEFINE-RGB-COLOR "grey99" 84/85 84/85 84/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "gray100" (DEFINE-RGB-COLOR "gray100" 1 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "grey100" (DEFINE-RGB-COLOR "grey100" 1 1 1)) *X11-COLORS-LIST*)

(PUSH (CONS "dark-grey" (DEFINE-RGB-COLOR "dark-grey" 169/255 169/255 169/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkgrey" (DEFINE-RGB-COLOR "darkgrey" 169/255 169/255 169/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-gray" (DEFINE-RGB-COLOR "dark-gray" 169/255 169/255 169/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkgray" (DEFINE-RGB-COLOR "darkgray" 169/255 169/255 169/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-blue" (DEFINE-RGB-COLOR "dark-blue" 0 0 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkblue" (DEFINE-RGB-COLOR "darkblue" 0 0 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-cyan" (DEFINE-RGB-COLOR "dark-cyan" 0 139/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkcyan" (DEFINE-RGB-COLOR "darkcyan" 0 139/255 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-magenta" (DEFINE-RGB-COLOR "dark-magenta" 139/255 0 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkmagenta" (DEFINE-RGB-COLOR "darkmagenta" 139/255 0 139/255))
      *X11-COLORS-LIST*)

(PUSH (CONS "dark-red" (DEFINE-RGB-COLOR "dark-red" 139/255 0 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "darkred" (DEFINE-RGB-COLOR "darkred" 139/255 0 0))
      *X11-COLORS-LIST*)

(PUSH (CONS "light-green" (DEFINE-RGB-COLOR "light-green" 48/85 14/15 48/85))
      *X11-COLORS-LIST*)

(PUSH (CONS "lightgreen" (DEFINE-RGB-COLOR "lightgreen" 48/85 14/15 48/85))
      *X11-COLORS-LIST*)
