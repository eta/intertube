(in-package #:cl-user)

(defpackage #:cl-colors2-tests
  (:use
   #:alexandria
   #:common-lisp
   #:cl-colors2
   #:clunit)
  (:export #:run))

(in-package #:cl-colors2-tests)

(defsuite cl-colors-suite ())

(defmacro first-value (a)
  `(first (multiple-value-list ,a)))

(defun run (&key (use-debugger nil))
  "Run all the tests for CL-COLORS2-TESTS."
  (clunit:run-suite 'cl-colors-suite :use-debugger use-debugger))

(defun rgb= (rgb1 rgb2 &optional (epsilon 1e-10))
  "Compare RGB colors for (numerical) equality."
  (color-equals rgb1 rgb2 :tolerance epsilon))

(defun random-rgb ()
  (rgb (random 1d0) (random 1d0) (random 1d0)))

(deftest rgb<->hsv-test (cl-colors-suite)
  (loop repeat 100 do
    (let ((rgb (random-rgb)))
      (assert-true (rgb= rgb (as-rgb (as-hsv rgb))))
      (assert-true (color-equals (as-hsv rgb) rgb)))))

(deftest parse-name (cl-colors-suite)
  (assert-true  (as-rgb "snow")))

;; (defun test-hue-combination (from to positivep)
;;   (dotimes (i 21)
;;     (format t "~a " (hue-combination from to (/ i 20) positivep))))

(deftest print-hex-rgb-test (cl-colors-suite)
  (let ((rgb (rgb 0.070 0.203 0.337)))
    (assert-equalp "#123456" (print-hex-rgb rgb))
    (assert-equalp "123456" (print-hex-rgb rgb :hash nil))
    (assert-equalp "#135" (print-hex-rgb rgb :short t))
    (assert-equalp "135" (print-hex-rgb rgb :hash nil :short t))
    (assert-equalp "#12345678" (print-hex-rgb rgb :alpha 0.47))
    (assert-equalp "12345678" (print-hex-rgb rgb :alpha 0.47 :hash nil))
    (assert-equalp "#1357" (print-hex-rgb rgb :alpha 0.47 :short t))
    (assert-equalp "1357" (print-hex-rgb rgb :alpha 0.47 :hash nil :short t))))

(deftest parse-hex-rgb-test (cl-colors-suite)
  (let ((rgb                     (rgb 0.070 0.203 0.337))
        (*clunit-equality-test*  (rcurry #'rgb= 0.01)))
    (assert-equality* rgb (first-value (parse-hex-rgb "#123456")))
    (assert-equality* rgb (first-value (parse-hex-rgb "123456")))
    (assert-equality* rgb (first-value (parse-hex-rgb "#135")))
    (assert-equality* rgb (first-value (parse-hex-rgb "135")))
    (let ((*clunit-equality-test* #'(lambda (list1 list2)
                                      (and (rgb= (car list1) (car list2) 0.01)
                                           (cl-colors2::eps= (cadr list1) (cadr list2)
                                                             0.01)))))
      (assert-equality* (list rgb 0.47) (multiple-value-list (parse-hex-rgb "#12345678")))
      (assert-equality* (list rgb 0.47) (multiple-value-list (parse-hex-rgb "12345678")))
      (assert-equality* (list rgb 0.47) (multiple-value-list (parse-hex-rgb "#1357")))
      (assert-equality* (list rgb 0.47) (multiple-value-list (parse-hex-rgb "1357"))))))

(deftest print-hex-rgb/format-test (cl-colors-suite)
  (assert-equalp "#123456" (with-output-to-string (*standard-output*)
                             (print-hex-rgb (rgb 0.070 0.203 0.337)
                                            :destination T))))

(deftest hex<->rgb-test (cl-colors-suite)
  (loop repeat 100 do
       (let ((rgb                     (random-rgb))
             (*clunit-equality-test*  (rcurry #'rgb= 0.01)))
         (assert-equality* rgb (first-value (parse-hex-rgb (print-hex-rgb rgb)))))))

(deftest parse-hex-rgb-ranges-test (cl-colors-suite)
  (let ((*clunit-equality-test* (rcurry #'rgb= 0.001)))
    (assert-equality* (rgb 0.070 0.203 0.337)
        (first-value (parse-hex-rgb "foo#123456zzz" :start 3 :end 10)))))

(run)
