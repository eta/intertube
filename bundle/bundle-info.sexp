(:CREATION-TIME #A((20) BASE-CHAR . "2023-05-15T23:28:28Z") :REQUESTED-SYSTEMS
 ("drakma" "cl-conspack" "cxml" "cl-statsd" "cl-redis" "qbase64" "cl-ansi-text"
  "gzip-stream" "bobbin" "cl-heap" "hunchentoot" "cl-who" "fuzzy-match"
  "cl-json" "ironclad" "fast-websocket" "cl-stomp" "trivial-backtrace" "uuid"
  "easy-routes" "prove")
 :LISP-INFO
 (:MACHINE-INSTANCE "whitechapel" :MACHINE-TYPE #A((5) BASE-CHAR . "ARM64")
  :MACHINE-VERSION #A((12) BASE-CHAR . "Apple M2 Pro")
  :LISP-IMPLEMENTATION-TYPE #A((4) BASE-CHAR . "SBCL")
  :LISP-IMPLEMENTATION-VERSION #A((5) BASE-CHAR . "2.3.2"))
 :QUICKLISP-INFO
 (:HOME #A((21) BASE-CHAR . "/Users/eta/quicklisp/") :LOCAL-PROJECT-DIRECTORIES
  (#A((36) BASE-CHAR . "/Users/eta/quicklisp/local-projects/")) :DISTS
  ((:NAME "quicklisp" :DIST-URL
    "http://beta.quicklisp.org/dist/quicklisp/2022-11-07/distinfo.txt" :VERSION
    "2022-11-07"))))
