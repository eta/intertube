# intertube reference data

`refdata.json.zst` contains intertube reference data, suitable for restoring into a blank database using `TRACKERNET::LOAD-REFDATA`.
Please note that this data is to be considered **proprietary**: you may load it into a local database for testing and development,
but running a public intertube instance based off it is prohibited.
