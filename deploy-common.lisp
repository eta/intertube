(format t "==> Loading vendored bundle~%")
(load "./bundle/bundle.lisp")
(format t "==> Loading intertube system~%")
;; Register the .asd file inside this directory with ASDF.
(push (uiop/os:getcwd) asdf:*central-registry*)
(asdf:load-system :intertube :verbose t)
(format t "==> Loaded models:~%")
(loop
  for x being the hash-values of trackernet::*line-models*
  do (format t "    ~A~%" x))
