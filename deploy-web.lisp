(load "deploy-common.lisp")
(require :sb-sprof)
(format t "==> Creating intertube-web executable~%")
(sb-ext:save-lisp-and-die "./intertube-web" :toplevel #'intertube-web::main :executable t)
