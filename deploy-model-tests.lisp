(load "deploy-common.lisp")
(format t "==> Running model tests (test output in ./model-test-output.xml)~%")
(load "model-tests.lisp")
(with-open-file (test-out "./model-test-output.xml"
                          :if-exists :supersede
                          :if-does-not-exist :create
                          :direction :output)
  (let ((*standard-output*
          (make-broadcast-stream *debug-io* test-out)))
    (intertube-model-tests::run-tests)))
