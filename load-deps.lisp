(format t "==> Loading vendored bundle~%")
(load "./bundle/bundle.lisp")
(format t "==> Loading dependencies~%")
(dolist (s '("drakma" "cl-conspack" "cxml" "cl-statsd" "cl-redis" "qbase64"
                        "cl-ansi-text" "gzip-stream" "bobbin"
			"cl-heap" "hunchentoot" "cl-who" "fuzzy-match"
			"cl-json" "ironclad" "fast-websocket"
			"cl-stomp" "trivial-backtrace"
			"uuid" "easy-routes" "prove"))
  (asdf:load-system s))
