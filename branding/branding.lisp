(in-package :intertube-web)

(defmacro define-branding (branding-data)
  `(progn
     ,@(loop
         for (file mime-type) in branding-data
         collect (let* ((sym (intern file "INTERTUBE-WEB"))
                        (pathname (format nil "./branding/~A" file))
                        (uri (format nil "/~A" file))
                        (file-data (read-file-to-array pathname)))
                   `(hunchentoot:define-easy-handler (,sym :uri ,uri) ()
                      (serve-embedded-file ,file-data ,mime-type))))))

(define-branding
    (("android-chrome-192x192.png" "image/png")
     ("android-chrome-512x512.png" "image/png")
     ("apple-touch-icon.png" "image/png")
     ("favicon-16x16.png" "image/png")
     ("favicon-32x32.png" "image/png")
     ("mstile-150x150.png" "image/png")
     ("safari-pinned-tab.svg" "image/svg+xml")
     ("logo.svg" "image/svg+xml")
     ("logo-text.svg" "image/svg+xml")
     ("logo-text-cracked.svg" "image/svg+xml")
     ("browserconfig.xml" "application/xml")
     ("favicon.ico" "image/vnd.microsoft.icon")
     ("site.webmanifest" "text/plain")))

(defvar *accent-colour* "#1d70b8")
