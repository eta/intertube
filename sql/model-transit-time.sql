SELECT p1.intertube_filename, date_diff('second', p1.t_min, p1.t_max), IF(p2.t_min IS NOT NULL, date_diff('second', p1.t_max, p2.t_min), 0)
FROM (
  SELECT t1.intertube_filename, min(t1.observation_ts) AS t_min, max(t1.observation_ts) AS t_max
	FROM trains AS t1
   WHERE t1.line = 'J'
	AND toHour(t1.observation_ts) = 12
	AND toDayOfWeek(t1.observation_ts) = 1
	AND t1.observation_ts > (now() - INTERVAL 4 week)
	AND t1.track_code IN
	['TJ10130','TJ10131','TJ10132','TJ10133','TJ10134','TJ10135','TJ10136','TJ10137','TJ10138']
	GROUP BY t1.intertube_filename
) AS p1
                                                                         LEFT JOIN (
	SELECT t1.intertube_filename, min(t1.observation_ts) AS t_min, max(t1.observation_ts) AS t_max
	FROM trains AS t1
	WHERE t1.line = 'J'
	AND toHour(t1.observation_ts) = 12
	AND toDayOfWeek(t1.observation_ts) = 1
	AND t1.observation_ts > (now() - INTERVAL 4 week)
	AND t1.track_code IN
	['TJ10140','TJ10142','TJ10144','TJ10146','TJ10148','TJ10151','TJ10153','TJ10155','TJ10154','TJ10157','TJ10156','TJ10159','TJ10158','TJ10161','TJ10160','TJ10163','TJ10162','TJ10165','TJ10164','TJ10167','TJ10166','TJ10169','TJ10168','TJ10171','TJ10170','TJ10173','TJ10172','TJ10175','TJ10174','TJ10177','TJ10176','TJ10179','TJ10178','TJ10181','TJ10180','TJ10183','TJ10182','TJ10185','TJ10184','TJ10187','TJ10186','TJ10189','TJ10188','TJ10191','TJ10190','TJ10193','TJ10192','TJ10195','TJ10194','TJ10196','TJ10198','TJ10197','TJ10200','TJ10201','TJ10202','TJ10203']
	GROUP BY t1.intertube_filename
) AS p2
ON p1.intertube_filename = p2.intertube_filename
WHERE IF(p2.t_min IS NOT NULL, p1.t_max < p2.t_min, TRUE);
