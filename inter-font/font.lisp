(in-package :intertube-web)

(defmacro define-inter-fonts (font-names)
  `(progn
     ,@(loop
         for font in font-names
         collect (let* ((sym (intern font "INTERTUBE-WEB"))
                        (uri (format nil "/fonts/~A" font))
                        (pathname (format nil "./inter-font/~A" font))
                        (file-data (read-file-to-array pathname)))
                   `(hunchentoot:define-easy-handler (,sym :uri ,uri) ()
                      (serve-embedded-file ,file-data "font/woff2"))))))

;; NOTE(eta): haha lol this is so stupid
(define-inter-fonts
    ("Inter-BlackItalic.woff"
     "Inter-BlackItalic.woff2"
     "Inter-Black.woff"
     "Inter-Black.woff2"
     "Inter-BoldItalic.woff"
     "Inter-BoldItalic.woff2"
     "Inter-Bold.woff"
     "Inter-Bold.woff2"
     "Inter-ExtraBoldItalic.woff"
     "Inter-ExtraBoldItalic.woff2"
     "Inter-ExtraBold.woff"
     "Inter-ExtraBold.woff2"
     "Inter-ExtraLightItalic.woff"
     "Inter-ExtraLightItalic.woff2"
     "Inter-ExtraLight.woff"
     "Inter-ExtraLight.woff2"
     "Inter-italic.var.woff2"
     "Inter-Italic.woff"
     "Inter-Italic.woff2"
     "Inter-LightItalic.woff"
     "Inter-LightItalic.woff2"
     "Inter-Light.woff"
     "Inter-Light.woff2"
     "Inter-MediumItalic.woff"
     "Inter-MediumItalic.woff2"
     "Inter-Medium.woff"
     "Inter-Medium.woff2"
     "Inter-Regular.woff"
     "Inter-Regular.woff2"
     "Inter-roman.var.woff2"
     "Inter-SemiBoldItalic.woff"
     "Inter-SemiBoldItalic.woff2"
     "Inter-SemiBold.woff"
     "Inter-SemiBold.woff2"
     "Inter-ThinItalic.woff"
     "Inter-ThinItalic.woff2"
     "Inter-Thin.woff"
     "Inter-Thin.woff2"
     "Inter.var.woff2"))
