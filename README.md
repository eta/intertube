<img src="https://git.eta.st/eta/intertube/-/raw/master/readme-header.png" alt="intertube, an eta.st project" height="160" width="447" />

This is the source code of **[intertube](https://intertube.eta.st/)**, a London Underground open data explorer
by [eta](https://eta.st/).

It's not really designed to be run or hacked on by others, but you're welcome to look (and copy snippets under the MIT license).
If you do want to contribute, [get in touch](https://eta.st/#contact) and we can work something out.

The main interesting bits are `trackernet.lisp` (the bulk of the scraper and backend code) and `wobsite.lisp` (the frontend).

## API access

An experimental API is available — see [api-docs.md](https://git.eta.st/eta/intertube/src/branch/master/api-docs.md).
