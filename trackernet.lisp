(in-package :trackernet)

(setf usocket:*ipv6-only-p* t)
;; How is this not the default?!
(setf drakma:*drakma-default-external-format* :utf-8)

(defparameter *trackernet-base-url* "http://cloud.tfl.gov.uk/TrackerNet")
(defparameter *unified-api-base-url* "https://api.tfl.gov.uk")
(defparameter *trackernet-app-key* nil)
(defparameter *nrod-stomp-host* "publicdatafeeds.networkrail.co.uk")
(defparameter *tfl-status-url* "https://api.tfl.gov.uk/Line/bakerloo,central,circle,district,elizabeth,hammersmith-city,jubilee,metropolitan,northern,piccadilly,victoria,waterloo-city/Status")
(defparameter *nrod-stomp-port* 61628)
(defparameter *nrod-data-key* #.(ignore-errors
                                 (read-from-string
                                  (uiop:read-file-string "./nrod-data-key"))))
(defparameter +months+ '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))
(defparameter +europe-london-tz+
  (progn
    (local-time:reread-timezone-repository)
    (local-time:find-timezone-by-location-name "Europe/London")))
(defparameter *trackernet-predictions-dir* "./predictions/")
(defparameter *trackernet-trains-archival-dir* "./trains/")
(defparameter *backoff-time-secs* 10)
(defparameter *prediction-expiry-secs* 60)
(defparameter *train-active-expiry-secs* 30)
(defparameter *rescue-grace-period-secs* 15)
(defparameter *train-active-expiry-secs-real* 360)
(defparameter *xr-train-expiry-secs* (* 3600 3))
(defparameter *xr-train-rescue-secs* 10)
(defparameter *live-train-expiry-secs* 600)
(defparameter *xr-live-train-expiry-secs* (* 3600 3))
(defparameter *train-set-code-expiry-secs* 360)
(defparameter *track-code-expiry-secs* 10)
(defparameter *rude-requests-per-minute* 700)
(defparameter *rescue-ptr-expiry-secs* (* 60 60 3))
(defparameter *train-live-data-limit* 40000)
(defparameter *max-train-age-secs* (* 60 60 24))
(defparameter *rescue-max-dist* 10.0)
(defparameter *rescue-timediff-threshold* -15)
(defparameter *max-trackernet-data-age-secs* 300)
(defparameter *lines*
  '("B" "C" "D" "H" "J" "M" "N" "P" "V" "W" "X" "S"))
(defparameter *codes-to-scrape*
  (alexandria:shuffle
   '(;; District line
     ("D" "UPM") ; Upminster (terminus)
     ("D" "BKG") ; Barking (between UPM and THL)
     ("D" "THL") ; Tower Hill (bay platform)
     ("D" "ECT") ; Earl's Court
     ("D" "EBY") ; Ealing Broadway
     ("D" "OLY") ; Kensington (Olympia)
     ("D" "HST") ; High St Kensington
     ("D" "FBY") ; Fulham Broadway
     ("D" "HMD") ; Hammersmith
     ("D" "ALE") ; Aldgate East
     ("D" "ECM") ; Ealing Common
     ("D" "ACT") ; Acton Town
     ("D" "GUN") ; Gunnersbury
     ("D" "SKN") ; South Kensington
     ("D" "EMB") ; Embankment
     ("D" "WHM") ; West Ham
     ("D" "PLW") ; Plaistow
     ("D" "EHM") ; East Ham
     ("D" "UPY") ; Upney
     ("D" "DGE") ; Dagenham East
     ("D" "ERD") ; Edgware Road
     ("D" "WKN") ; West Kensington
     ("D" "WDN") ; Wimbledon
     ("D" "RMD") ; Richmond

     ;; Victoria line
     ("V" "BRX") ; Brixton
     ("V" "WAL") ; Walthamstow Central
     ("V" "SVS") ; Seven Sisters
     ("V" "HBY") ; Highbury & Islington
     ("V" "WST") ; Warren Street
     ("V" "KXX") ; King's Cross St Pancras
     ("V" "VIC") ; Victoria

     ;; Jubilee line
     ("J" "WHD") ; West Hampstead
     ("J" "WPK") ; Wembley Park
     ("J" "STA") ; Stanmore
     ("J" "SFD") ; Stratford
     ("J" "WLO") ; Waterloo
     ("J" "NGW") ; North Greenwich
     ("J" "CPK") ; Canons Park
     ("J" "NEA") ; Neasden
     ("J" "FRD") ; Finchley Road
     ("J" "GPK") ; Green Park
     ("J" "WLO") ; Waterloo
     ("J" "LON") ; London Bridge
     ("J" "CWF") ; Canary Wharf
     ("J" "WHM") ; West Ham

     ;; Northern line
     ("N" "MOR") ; Morden
     ("N" "EUS") ; Euston
     ("N" "HBT") ; High Barnet
     ("N" "KEN") ; Kennington
     ("N" "MHE") ; Mill Hill East
     ("N" "EDG") ; Edgware
     ("N" "CTN") ; Camden Town
     ("N" "ARC") ; Archway
     ("N" "FYC") ; Finchley Central
     ("N" "EFY") ; East Finchley
     ("N" "MCR") ; Mornington Crescent
     ("N" "COL") ; Colindale
     ("N" "HMP") ; Hampstead
     ("N" "KXX") ; King's Cross St Pancras
     ("N" "CHX") ; Charing Cross
     ("N" "STK") ; Stockwell
     ("N" "TBY") ; Tooting Broadway
     ("N" "MGT") ; Moorgate
;;     ("N" "BPS") ; Battersea Power Station

     ;; Piccadilly line
     ("P" "ACT") ; Acton Town
     ("P" "HRV") ; Heathrow Terminal 5
     ("P" "CFS") ; Cockfosters
     ("P" "UXB") ; Uxbridge
     ("P" "RLN") ; Rayners Lane
     ("P" "KXX") ; King's Cross St Pancras
     ("P" "HMD") ; Hammersmith
     ("P" "NFD") ; Northfields
     ("P" "WGN") ; Wood Green
     ("P" "GPK") ; Green Park
     ("P" "HPC") ; Hyde Park Corner
     ("P" "BOS") ; Boston Manor
     ("P" "HNC") ; Hounslow Central
     ("P" "HRC") ; Heathrow Terminals 123
     ("P" "ECM") ; Ealing Common
     ("P" "SHR") ; South Harrow
     ("P" "RUI") ; Ruislip

     ;; Central line
     ("C" "WRP") ; West Ruislip
     ("C" "NHT") ; Northolt
     ("C" "EBY") ; Ealing Broadway
     ("C" "NAC") ; North Acton
     ("C" "WCT") ; White City
     ("C" "MAR") ; Marble Arch
     ("C" "LYS") ; Leytonstone
     ("C" "WFD") ; Woodford
     ("C" "HAI") ; Hainault
     ("C" "EPP") ; Epping
     ("C" "NEP") ; Newbury Park
     ("C" "LTN") ; Loughton
     ("C" "RUG") ; Ruislip Gardens
     ("C" "QWY") ; Queensway
     ("C" "LST") ; Liverpool Street
     ("C" "BNG") ; Bethnal Green
     ("C" "GRH") ; Grange Hill
     ("C" "DEB") ; Debden

     ;; Bakerloo line
     ("B" "HAW") ; Harrow & Wealdstone
     ("B" "SPK") ; Stonebridge Park
     ("B" "ELE") ; Elephant & Castle
     ("B" "QPK") ; Queen's Park
     ("B" "WEM") ; Wembley Central
     ("B" "WJN") ; Willesden Junction
     ("B" "PAD") ; Paddington
     ("B" "PIC") ; Piccadilly Circus
     ("B" "WLO") ; Waterloo
     ("B" "LAM") ; Lambeth North

     ;; Metropolitan line
     ("M" "ALD") ; Aldgate
     ("M" "AME") ; Amersham
     ("M" "CLF") ; Chalfont & Latimer
     ("M" "UXB") ; Uxbridge
     ("M" "WAT") ; Watford
     ("M" "HOH") ; Harrow-on-the-Hill
     ("M" "BST") ; Baker Street
     ("M" "RKY") ; Rickmansworth
     ("M" "MGT") ; Moorgate
     ("M" "KXX") ; King's Cross St Pancras
     ("M" "FRD") ; Finchley Road
     ("M" "WPK") ; Wembley Park
     ("M" "RLN") ; Rayners Lane
     ("M" "RUI") ; Ruislip
     ("M" "HDN") ; Hillingdon
     ("M" "NWD") ; Northwood

     ;; Hammersmith & Circle lines
     ("H" "HMS") ; Hammersmith
     ("H" "ERD") ; Edgware Road
     ("H" "WMS") ; Westminster
     ("H" "KXX") ; King's Cross St Pancras
     ("H" "BKG") ; Barking
     ("H" "PAD") ; Paddington
     ("H" "BST") ; Baker Street
     ("H" "SKN") ; South Kensington
     ("H" "HST") ; High Street Kensington
     ("H" "ALD") ; Aldgate
     ("H" "ALE") ; Aldgate East
     ("H" "WCL") ; Whitechapel
     ("H" "WHM") ; West Ham
     ("H" "PLW") ; Plaistow
     ("H" "EHM") ; East Ham

     ;; Waterloo & City line
     ("W" "WLO") ; Waterloo
     ("W" "BNK") ; Bank
     )))
(defparameter *code-version*
  (or
   (ignore-errors
    (string-right-trim
     '(#\Newline #\Tab #\Space)
     (uiop:run-program
      "git describe --always --dirty"
      :output :string)))
   (uiop:getenv "INTERTUBE_CODE_VERSION")
   "unknown"))
(defparameter *user-agent*
  (format nil "intertube-scraper/~A (contact me at intertube@eta.st)"
          *code-version*))

;; FIXME(eta): this needs to go, but I don't want to weed it out just yet
(defparameter *per-line-track-graph*
  (make-hash-table
   :test 'equal))
(defparameter *normalized-track-links*
  (make-hash-table :test 'equal))
(defparameter +manual-count+ 999999)
(defparameter *rescuable-trains* (make-hash-table :test 'equal))

(defvar *current-prediction* nil)
(defvar *current-station* nil)
(defvar *current-platform* nil)
(defvar *current-train* nil)
(defvar *trackernet-kill-switch* nil)
(defvar *current-scraper-plan* (cons nil nil))
(defvar *current-worker-count* 0)
(defvar *track-links-set* (make-hash-table
                           :test 'equal))
(defvar *replaced-track-links-set* (make-hash-table
                                    :test 'equal))
(defvar *clickhouse-out* (make-broadcast-stream))
(defvar *staging-mode-p* nil)

;; HACK(eta): USOCKET carries on swapping round conditions. Don't do that,
;;            please.
(defun usocket::handle-condition (&rest args)
  (declare (ignore args)))

(defun statsd-inc (&rest args)
  (ignore-errors
   (apply #'statsd:inc args)))

(defun statsd-gauge (&rest args)
  (ignore-errors
   (apply #'statsd:gauge args)))

(defun statsd-counter (&rest args)
  (ignore-errors
   (apply #'statsd:counter args)))

(defun bgcol (col)
  (cl-ansi-text:make-color-string col :style :background))

(defun colreset ()
  cl-ansi-text:+reset-color-string+)

(defun read-all-from (file)
  (with-open-file (source file
                          :element-type '(unsigned-byte 8))
    (let ((xs (cxml:make-source source))
          (next))
      (loop
        while (first (setf next (multiple-value-list (klacks:consume xs))))
        do (format t "~A~%" next)))))

;; stolen from https://bugs.launchpad.net/sbcl/+bug/1399727/comments/5
(defun set-native-thread-name (thread name)
  #+(and sb-thread linux)
  (when (and (stringp name) (not (equal "" name)))
    (let ((n (with-output-to-string (s)
               (dotimes (i (min (length name) 15))
                 (let ((c (char name i)))
                   (if (<= 32 (char-code c) 126)
                       (write-char c s)
                       (return-from set-native-thread-name)))))))
      (sb-alien:with-alien ((fn (sb-alien:function sb-alien:integer sb-alien:unsigned sb-alien:c-string)
                                :extern "pthread_setname_np"))
        (values (sb-alien:alien-funcall fn
                               (sb-thread::thread-os-thread thread)
                               n))))))

(defun update-native-thread-names ()
  #+sb-thread
  (dolist (x (sb-thread:list-all-threads))
    (set-native-thread-name x (sb-thread:thread-name x))))

(defun set-thread-name (name &optional thread)
  #+sb-thread
  (let ((thread (or thread sb-thread:*current-thread*)))
    (setf (sb-thread:thread-name thread) name)
    (set-native-thread-name thread name)))
;; end stolen section

(defclass trackernet-prediction ()
  ((created-ts
    :reader created-ts)
   (line-code
    :reader line-code)
   (line-name
    :reader line-name)
   (stations
    :reader stations
    :initform nil)))

(conspack:defencoding trackernet-prediction
  created-ts line-code line-name stations)

(defclass trackernet-station ()
  ((code
    :reader code)
   (name
    :reader name)
   (cur-time
    :reader cur-time)
   (mess
    :reader mess
    :initform "")
   (platforms
    :reader platforms
    :initform nil)))

(conspack:defencoding trackernet-station
  code name cur-time platforms)

(defmethod print-object ((obj trackernet-station) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (name code cur-time platforms) obj
      (format stream "\"~A\" (~A) at ~A; ~A platforms"
              name code cur-time (length platforms)))))

(defclass trackernet-platform ()
  ((name
    :reader name)
   (num
    :reader num)
   (track-code
    :reader track-code)
   (next-train
    :reader next-train)
   (trains
    :reader trains
    :initform nil)))

(conspack:defencoding trackernet-platform
  name num track-code next-train trains)

(defmethod print-object ((obj trackernet-platform) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (name num track-code trains) obj
      (format stream "\"~A\" (~A - ~A); ~A trains"
              name num track-code (length trains)))))

(defclass trackernet-train ()
  ((train-id
    :reader train-id
    :initform nil)
   (lcid
    :reader lcid
    :initform nil)
   (set-no
    :reader set-no)
   (trip-no
    :reader trip-no)
   (seconds-to
    :reader seconds-to)
   (time-to
    :reader time-to)
   (location-desc
    :reader location-desc
    :initform nil)
   (destination-desc
    :reader destination-desc
    :initform nil)
   (dest-code
    :reader dest-code)
   (order
    :reader order)
   (depart-time
    :reader depart-time)
   (depart-interval
    :reader depart-interval)
   (departed
    :reader departed)
   (direction
    :reader direction)
   (is-stalled
    :reader is-stalled)
   (track-code
    :reader track-code
    :initform nil)
   (line
    :reader line
    :initform nil)
   (input-dest
    :reader input-dest
    :initform nil)
   (leading-car-no
    :reader leading-car-no
    :initform nil)))

(defmethod print-object ((obj trackernet-train) stream)
  (if *print-readably*
      (format stream
              "#.(CONSPACK:DECODE-OBJECT 'TRACKERNET::TRACKERNET-TRAIN '~A)"
              (write-to-string (conspack:encode-object obj)))
      (print-unreadable-object (obj stream :type t)
        (with-slots (train-id lcid set-no trip-no track-code location-desc destination-desc leading-car-no) obj
          (format
           stream
           "~A (l=~A/s=~A/t=~A/n=~A) ~A (~A) to ~A"
           (or train-id "???")
           (or lcid "???")
           set-no
           trip-no
           (or leading-car-no "?")
           (or location-desc "<somewhere>")
           (or track-code "???")
           (or destination-desc "<somewhere>"))))))

(conspack:defencoding trackernet-train
  train-id lcid set-no trip-no seconds-to time-to location-desc
  destination-desc dest-code order depart-time depart-interval
  departed direction is-stalled track-code line input-dest
  leading-car-no)

(defmethod make-load-form ((obj trackernet-train) &optional environment)
  (declare (ignore environment))
  `(conspack:decode-object
    'trackernet-train
    (quote ,(conspack:encode-object obj))))

(conspack:define-index trackernet-cpk-v1
  ;; trackernet-train
  train-id lcid set-no trip-no seconds-to time-to location-desc
  destination-desc dest-code order depart-time depart-interval
  departed direction is-stalled track-code line input-dest
  leading-car-no
  ;; trackernet-platform
  name num next-train trains
  ;; trackernet-station
  code cur-time platforms
  ;; trackernet-prediction
  created-ts line-code line-name stations)

;; this one's only used for live train objects at the time of writing
(conspack:define-index trackernet-cpk-v2
  ;; package name
  trackernet
  ;; trackernet-train
  trackernet-train
  train-id lcid set-no trip-no seconds-to time-to location-desc
  destination-desc dest-code order depart-time depart-interval
  departed direction is-stalled track-code line input-dest
  leading-car-no
  ;; trackernet-platform
  trackernet-platform
  name num next-train trains
  ;; trackernet-station
  trackernet-station
  code cur-time platforms
  ;; trackernet-prediction
  trackernet-prediction
  created-ts line-code line-name stations
  ;; train-history-entry
  train-history-entry
  universal-ts
  ;; train-identity-change
  train-identity-change
  wtt-id wtt-trip
  ;; train-destination-change
  train-destination-change
  destination
  ;; train-rescue
  train-rescue
  new-key
  ;; train-transit
  train-transit
  end-ts track-codes
  ;; train-station-stop
  train-station-stop
  station-name platform
  ;; majority-ballot
  majority-ballot
  inner
  ;; live-train
  live-train
  unique-id train-identity first-updated last-updated last-updated
  destination current-entry trackernet-id destination-ballot line-code
  history)

(defmethod print-object ((obj trackernet-prediction) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (line-name line-code created-ts stations) obj
      (if (eql (length stations) 1)
          (format stream "for \"~A\" (~A) on the ~A (~A) created at ~A"
                  (name (first stations))
                  (code (first stations))
                  line-name
                  line-code
                  created-ts)
          (format stream "for ~A station(s) on the ~A (~A) created at ~A"
                  (length stations)
                  line-name
                  line-code
                  created-ts)))))

(defun get-trains (pred)
  "Extract and return all the train objects from the prediction PRED."
  (with-slots (stations) pred
    (assert (eql (length stations) 1) () "unexpected number of stations in ~A: ~A" pred stations)
    (with-slots (platforms) (first stations)
      (loop
        for platform in platforms
        append (with-slots (trains) platform
                 trains)))))

(defun read-when-created (source)
  "Read the value of the WhenCreated tag from SOURCE."
  (let ((data (nth-value 1 (klacks:skip source :characters))))
    (setf (slot-value *current-prediction* 'created-ts) data)
    (klacks:skip source :end-element nil "WhenCreated")))

(defmacro define-tag-reader (name-sym element-name obj obj-slot)
  (let ((source-sym (gensym))
        (data-sym (gensym)))
    `(defun ,name-sym (,source-sym)
       (let ((,data-sym (nth-value 1 (klacks:skip ,source-sym :characters))))
         (setf (slot-value ,obj ,obj-slot) ,data-sym)
         (klacks:find-event ,source-sym :end-element)
         (klacks:skip ,source-sym :end-element nil ,element-name)))))

(defmacro attribute-mapping-lambda (apply-to &body lname-map)
  (alexandria:with-gensyms (ns-uri lname qname value defaulted args)
    (let ((lname-map-cond
            (loop
              for (tag-name slot-name &key map) in lname-map
              collect `((string= ,lname ,tag-name)
                        (setf (slot-value ,apply-to ,slot-name)
                              (,(or map 'identity) ,value))))))
      `(lambda (,ns-uri ,lname ,qname ,value ,defaulted &rest ,args)
         (declare (ignore ,ns-uri ,qname ,defaulted ,args))
         (cond
           ,@lname-map-cond
           (t
            ;;(warn "Unknown attribute ~A encountered for ~A" ,lname (quote ,apply-to))
            ))))))

(defun handle-unexpected-start (of-lname source)
  "Consumes elements from SOURCE to deal with an unexpected START-ELEMENT with the given OF-LNAME."
  (let ((closing-name))
    (loop
      while (setf closing-name (nth-value 3 (klacks:find-event source :end-element)))
      do (klacks:consume source)
      while (not (string= closing-name of-lname)))))

(defun discard-disclaimer (source)
  "Read the Disclaimer tag and discard it."
  (handle-unexpected-start "Disclaimer" source))


(defmacro dispatch-subelements (our-element-lname source &body subelement-defs)
  (alexandria:with-gensyms (type arg1 arg2 chars)
    (let ((subelt-defs-expanded
            (loop
              for (tag-name func-name) in subelement-defs
              collect `((string= ,arg2 ,tag-name)
                        (,func-name ,source)))))
      `(loop
         (multiple-value-bind
               (,type ,arg1 ,arg2)
             (klacks:consume ,source)
           (case ,type
             (:start-element
              (cond
                ,@subelt-defs-expanded
                (t
                 (warn "Unexpected START-ELEMENT for ~A while parsing ~A"
                       ,arg2 ,our-element-lname)
                 (handle-unexpected-start ,arg2 ,source))))
             (:end-element
              (assert (string= ,arg2 ,our-element-lname) ()
                      "Unexpected END-ELEMENT for ~A while parsing ~A"
                      ,arg2 ,our-element-lname)
              (return))
             (:characters
              (let ((,chars
                      (string-trim '(#\Return #\Space #\Tab #\Linefeed) ,arg1)))
                (when (> (length ,chars) 0)
                  (warn "Unhandled characters: '~A' while parsing ~A"
                        ,chars ,our-element-lname))))
             (t
              (unless ,type
                (return))
              (warn "Unhandled '~A' event while parsing ~A"
                    ,type ,our-element-lname))))))))

(defun read-train (source)
  "Read a train element from SOURCE."
  (let ((*current-train* (make-instance 'trackernet-train)))
    (flet ((zero-boolean-convert (str) (string= str "1")))
      (klacks:map-attributes
       (attribute-mapping-lambda *current-train*
         ("TrainId" 'train-id)
         ("LCID" 'lcid)
         ("SetNo" 'set-no)
         ("TripNo" 'trip-no)
         ("SecondsTo" 'seconds-to :map parse-integer)
         ("TimeTo" 'time-to)
         ("Location" 'location-desc)
         ("Destination" 'destination-desc)
         ("DestCode" 'dest-code)
         ("Order" 'order)
         ("DepartTime" 'depart-time)
         ("DepartInterval" 'depart-interval :map parse-integer)
         ("Departed" 'departed :map zero-boolean-convert)
         ("Direction" 'direction)
         ("IsStalled" 'is-stalled :map zero-boolean-convert)
         ("TrackCode" 'track-code)
         ("LN" 'line)
         ("InputDest" 'input-dest)
         ("LeadingCarNo" 'leading-car-no))
       source))
    (dispatch-subelements "T" source)
    (push *current-train* (slot-value *current-platform* 'trains))))

(defun read-platform (source)
  "Read a platform element from SOURCE."
  (let ((*current-platform* (make-instance 'trackernet-platform)))
    (flet ((boolean-convert (str) (string= str "true")))
      (klacks:map-attributes
       (attribute-mapping-lambda *current-platform*
         ("N" 'name)
         ("Num" 'num :map parse-integer)
         ("TrackCode" 'track-code)
         ("NextTrain" 'next-train :map boolean-convert))
       source))
    (dispatch-subelements "P" source
      ("T" read-train))
    (push *current-platform* (slot-value *current-station* 'platforms))))

(defun read-station (source)
  "Read a station element from SOURCE."
  (let ((*current-station* (make-instance 'trackernet-station)))
    (klacks:map-attributes
     (attribute-mapping-lambda *current-station*
       ("Code" 'code)
       ("N" 'name)
       ("Mess" 'mess)
       ("CurTime" 'cur-time))
     source)
    (dispatch-subelements "S" source
      ("P" read-platform))
    (push *current-station* (slot-value *current-prediction* 'stations))))

(define-tag-reader read-line-code "Line" *current-prediction* 'line-code)
(define-tag-reader read-line-name "LineName" *current-prediction* 'line-name)

(defun read-root (source)
  "Read and dispatch an XML event from SOURCE."
  (let ((*current-prediction* (make-instance 'trackernet-prediction)))
    (dispatch-subelements "ROOT" source
      ("WhenCreated" read-when-created)
      ("Disclaimer" discard-disclaimer)
      ("Line" read-line-code)
      ("LineName" read-line-name)
      ("S" read-station))
    *current-prediction*))

(defun read-trackernet-prediction (source)
  (when (klacks:find-element source "ROOT")
    (klacks:consume source)
    (read-root source)))

(defun read-trackernet-file (file)
  (with-open-file (stream file
                          :element-type '(unsigned-byte 8))
    (let ((source (cxml:make-source stream)))
      (read-trackernet-prediction source))))

(defun parse-trackernet-datetime (dt)
  "this makes me sad"
  (labels ((int-chomp ()
             (multiple-value-bind (int end)
                 (parse-integer dt :junk-allowed t)
               (setf dt (subseq dt end))
               int))
           (bump ()
             (setf dt (subseq dt 1)))
           (tidy ()
             (loop
               while (and
                      (> (length dt) 0)
                      (or
                       (char= (elt dt 0) #\Space)
                       (char= (elt dt 0) #\:)))
               do (bump)))
           (int-chomp-tidy ()
             (prog1 (int-chomp) (tidy))))
    (let* ((day (int-chomp-tidy))
           (next-space (position #\Space dt))
           (month (prog1
                      (subseq dt 0 next-space)
                    (setf dt (subseq dt next-space))))
           (month-num (1+ (position month +months+
                                    :test #'string=)))
           (year (int-chomp-tidy))
           (hour (int-chomp-tidy))
           (min (int-chomp-tidy))
           (sec (int-chomp-tidy)))
      (local-time:timestamp-to-universal
       (local-time:encode-timestamp
        0 sec min hour day month-num year
        :timezone +europe-london-tz+)))))

(defun parse-trackernet-time (dt)
  "this makes me sad, round 2"
  (labels ((int-chomp ()
             (multiple-value-bind (int end)
                 (parse-integer dt :junk-allowed t)
               (setf dt (subseq dt end))
               int))
           (bump ()
             (setf dt (subseq dt 1)))
           (tidy ()
             (loop
               while (and
                      (> (length dt) 0)
                      (or
                       (char= (elt dt 0) #\Space)
                       (char= (elt dt 0) #\:)))
               do (bump)))
           (int-chomp-tidy ()
             (prog1 (int-chomp) (tidy))))
    (let* ((hour (int-chomp-tidy))
           (min (int-chomp-tidy))
           (sec (int-chomp-tidy)))
      (+ (* hour 3600)
          (* min 60)
          sec))))

(defun fetch-trackernet-prediction (line-code station-code)
  "Perform a Trackernet HTTP call to fetch the given station's predictions."
  (multiple-value-bind (stream status-code)
      (drakma:http-request (format nil "~A/PredictionDetailed/~A/~A"
                                   *trackernet-base-url*
                                   line-code station-code)
                           :user-agent *user-agent*
                           :connection-timeout 3
                           :additional-headers
                           (when *trackernet-app-key*
                             `(("app_key" . ,*trackernet-app-key*)))
                           :want-stream t)
    (unless (eql status-code 200)
      (error "TrackerNet returned code ~A" status-code))
    (let ((source (cxml:make-source
                   (flexi-streams:flexi-stream-stream stream)
                   :buffering nil)))
      (unwind-protect
           (read-trackernet-prediction source)
        (close stream)))))

(defun make-trackernet-filename (pred)
  (with-slots (line-code created-ts stations) pred
    (assert (eql (length stations) 1) () "unexpected number of stations in ~A: ~A" pred stations)
    (let ((universal-ts (parse-trackernet-datetime created-ts)))
      (values
       (format nil "~A-~A-~A.dat" line-code (code (first stations)) universal-ts)
       universal-ts))))

(defun cpk-base64 (object)
  (cpk:with-named-index 'trackernet-cpk-v1
   (qbase64:encode-bytes
    (cpk:encode object))))

(defun cpk-unbase64 (data)
  (when data
    (cpk:with-named-index 'trackernet-cpk-v1
      (cpk:decode (subseq (qbase64:decode-string data) 0)))))

(defun get-all (pattern)
  "Calls RED:SCAN in a loop, returning all the Redis keys matching PATTERN."
  (loop
    with cursor
    with cur
    while (or (not cursor) (not (string= cursor "0")))
    do (setf cur (red:scan (or cursor "0") :match pattern :count 1000))
    do (setf cursor (first cur))
    append (second cur)))

(redis::def-expect-method :multi-bytes
  (let ((n (parse-integer redis::reply)))
    (unless (= n -1)
      (loop :repeat n
            :collect (ecase (peek-char nil (redis::conn-stream redis::*connection*))
                       (#\: (redis::expect :integer))
                       (#\$ (redis::expect :bytes))
                       (#\* (redis::expect :multi-bytes)))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 'red-getdel (find-package 'redis)))
(redis::def-cmd GETDEL (key) :bulk
                "Delete a key and return it.")

(defun redis-raw-get (key)
  (return-from redis-raw-get
    (redis::with-reconnect-restart
      (redis::tell 'get key)
      (prog1
          (redis::expect :bytes)
        (unless redis::*pipelined*
          (clear-input (redis::conn-stream redis::*connection*)))))))

(defun redis-raw-mget (&rest keys)
  (return-from redis-raw-mget
    (redis::with-reconnect-restart
      (apply #'redis::tell 'mget keys)
      (prog1
          (redis::expect :multi-bytes)
        (unless redis::*pipelined*
          (clear-input (redis::conn-stream redis::*connection*)))))))

(defun redis-raw-zrange (key min max &rest args)
  (return-from redis-raw-zrange
    (redis::with-reconnect-restart
      (apply #'redis::tell 'zrange key min max args)
      (prog1
          (redis::expect :multi-bytes)
        (unless redis::*pipelined*
          (clear-input (redis::conn-stream redis::*connection*)))))))

(defun redis-raw-zadd (key score byte-value)
  (let ((score (redis::ensure-string score)))
    (return-from redis-raw-zadd
      (redis::with-reconnect-restart
        (redis::format-redis-number #\* 4) ; array, 4 elements
        (redis::format-redis-number #\$ #.(babel:string-size-in-octets "ZADD" :encoding :UTF-8))
        (redis::format-redis-string "ZADD")
        (redis::format-redis-number #\$ (babel:string-size-in-octets key :encoding :UTF-8))
        (redis::format-redis-string key)
        (redis::format-redis-number #\$ (babel:string-size-in-octets score :encoding :UTF-8))
        (redis::format-redis-string score)
        (redis::format-redis-number #\$ (length byte-value))
        (let* ((conns (redis::conn-stream redis::*connection*))
               (flex (flex:flexi-stream-stream conns)))
          (write-sequence byte-value flex)
          (terpri conns)
          (force-output conns))
        (prog1
            (redis::expect :integer)
        (unless redis::*pipelined*
          (clear-input (redis::conn-stream redis::*connection*))))))))

(defun redis-raw-set (key byte-value)
  (return-from redis-raw-set
    (redis::with-reconnect-restart
      (redis::format-redis-number #\* 3) ; array, 3 elements
      (redis::format-redis-number #\$ #.(babel:string-size-in-octets "SET" :encoding :UTF-8))
      (redis::format-redis-string "SET")
      (redis::format-redis-number #\$ (babel:string-size-in-octets key :encoding :UTF-8))
      (redis::format-redis-string key)
      (redis::format-redis-number #\$ (length byte-value))
      (let* ((conns (redis::conn-stream redis::*connection*))
             (flex (flex:flexi-stream-stream conns)))
        (write-sequence byte-value flex)
        (terpri conns)
        (force-output conns))
      (prog1
          (redis::expect :status)
        (unless redis::*pipelined*
          (clear-input (redis::conn-stream redis::*connection*)))))))

(defun cpk-raw (object)
  (cpk:with-named-index 'trackernet-cpk-v2
    (cpk:encode object)))

(defun cpk-unraw (data)
  (when data
    (cpk:with-named-index 'trackernet-cpk-v2
      (cpk:decode data))))

(defun redis-raw-set-cpk (key object &optional (index 'trackernet-cpk-v2))
  (cpk:with-named-index index
    (redis-raw-set key (cpk:encode object))))

(defun redis-raw-get-cpk (key &optional (index 'trackernet-cpk-v2))
  (let ((data (redis-raw-get key)))
    (when data
      (cpk:with-named-index index
        (cpk:decode data)))))

(defun redis-raw-mget-cpk (&rest keys)
  (when keys
    (let ((data (apply #'redis-raw-mget keys)))
      (mapcar #'cpk-unraw data))))

(defun redis-score-parser (value)
  (cond
    ((eql value :pipelined) :pipelined)
    (value
     (parse-integer (babel:octets-to-string (second value)
                                            :encoding :utf-8)))
    (t nil)))

(defun redis-cpk-parser (value)
  (cond
    ((eql value :pipelined) :pipelined)
    (value
     (cpk-unraw (first value)))
    (t nil)))

(defun redis-last-score (key)
  "Gets the score of the last element of the sorted set with the Redis KEY."
  (redis-score-parser
   (redis-raw-zrange key "+inf" "-inf"
                     "byscore" "rev"
                     "limit" "0" "1"
                     "withscores")))

(defun redis-first-score (key)
  "Gets the score of the first element of the sorted set with the Redis KEY."
  (redis-score-parser
   (redis-raw-zrange key "-inf" "+inf"
                     "byscore"
                     "limit" "0" "1"
                     "withscores")))

(defun redis-first-cpk (key)
  "Gets the CONSPACK-decoded value of the first element of the sorted set with the Redis KEY."
  (redis-cpk-parser
   (redis-raw-zrange key "-inf" "+inf"
                     "byscore"
                     "limit" "0" "1"
                     "withscores")))

(defun redis-last-cpk (key)
  "Gets the CONSPACK-decoded value of the last element of the sorted set with the Redis KEY."
  ;; NOTE: the fact that this also returns the data is relied on
  ;;       inside GET-ALL-TRAINS-EXTENDED
  (redis-cpk-parser
   (redis-raw-zrange key "+inf" "-inf"
                     "byscore" "rev"
                     "limit" "0" "1"
                     "withscores")))

(defun redis-paired-mget (key-names)
  "Given a list of KEY-NAMES, returns an alist of each name paired with its value, or NIL if the value did not exist."
  (when key-names
   (loop
     for key in key-names
     for value in (apply #'red:mget key-names)
     collect (cons key value))))

(defun redis-cpk-sorted-set-all (key)
  "Gets all elements of the sorted set KEY, alongside their scores, CONSPACK-decoding each element."
  (let ((data
          (redis-raw-zrange key "-inf" "+inf" "byscore" "withscores")))
    (loop
      with elt
      with score
      while (setf elt (pop data))
      do (setf score (parse-integer (babel:octets-to-string (pop data)
                                                            :encoding :utf-8)))
      collect (cons score (cpk-unraw elt)))))

(defun redis-sorted-set-all (key)
  "Gets all elements of the sorted set KEY, alongside their scores."
  (let ((data (red:zrangebyscore key "-inf" "+inf" :withscores t)))
    (loop
      with elt
      with score
      while (setf elt (pop data))
      do (setf score (parse-integer (pop data)))
      collect (cons score elt))))

(defun redis-sorted-set-length (key)
  (red:zcount key "-inf" "+inf"))

(defun nilify-zeros (str)
  "If STR is not NIL and contains nothing or only zeroes, return NIL; otherwise, return STR."
  (when (and str
             (> (length (string-trim '(#\0) str)) 0))
    str))

(defun nilify-zeros-parse (str)
  (if (stringp str)
      (when (and str
                 (> (length (string-trim '(#\0) str)) 0))
        (parse-integer str))
      str))

(defun hash-location (location-desc)
  "Produce a good enough short hash of LOCATION-DESC."
  (with-output-to-string (out)
    (loop
      for byte across (ironclad:digest-sequence
                       :sha1
                       (babel:string-to-octets location-desc))
      for i from 1 upto 6
      do (format out "~2,'0X" byte))))


(defun redis-train (universal-ts line-code code-station train)
  (with-slots (train-id set-no trip-no lcid seconds-to track-code location-desc leading-car-no) train
    ;; HACK(eta): The Circle (and other SSR) doesn't provide track codes
    ;; or train identifiers in some situations, so we're going to try
    ;; making up some data as a hack :(
    (when (and
           (or
            (string= line-code "H")
            (string= line-code "M")
            (string= line-code "D"))
           location-desc
           (not lcid)
           (not train-id)
           (not track-code)
           (nilify-zeros set-no)
           (nilify-zeros trip-no))
      (setf line-code "S") ; for "SSR"
      (setf train-id (format nil "fake-~Ax~A" set-no trip-no))
      (setf track-code (format nil "fake-~A" (hash-location location-desc))))
    (when (and (or lcid train-id) seconds-to track-code)
      (let* ((lcid (nilify-zeros lcid))
             (tid (or lcid
                      (when (string= line-code "X") train-id)
                      (when (and
                             (string= line-code "M")
                             (nilify-zeros set-no))
                        (format nil "set~A" set-no))
                      (format nil "tid~A" train-id)))
             (key (or (format nil "~A-train-~A" line-code tid))))
        ;; get a mapping of seconds to -> track code
        (red:zadd (format nil "~A-secs-to-track-code"
                          code-station)
                  seconds-to
                  track-code)
        ;; get a mapping of track code -> description
        (when (and location-desc
                   (not (position #\· track-code))
                   ;; this is highly annoying
                   (not (string= location-desc "At Platform")))
          (red:setnx (format nil "~A-track-desc-~A"
                             line-code track-code)
                     location-desc))
        ;; add this train into the new list of trains for
        ;; the station
        (red:sadd (format nil "~A-trains-new" code-station) key)
        ;; log the new train
        (unless (red:exists key)
          (log:info "~Anew ~A~A (~A): ~A"
                    (bgcol :green) key (colreset) code-station train)
          (statsd-inc "intertube.new")
          ;; make a rescuable thingy
          ;; (see the note inside GET-RESCUABLE-TRAINS for more)
          (red:setex (format nil "~A-rescue-~A"
                             line-code tid)
                     (if (string= line-code "X")
                         30
                         (+ *train-active-expiry-secs*
                             *rescue-grace-period-secs*
                             *train-active-expiry-secs-real*))
                     track-code))
        ;; mark this train as active
        (red:setex (format nil "~A-active-~A"
                           line-code tid)
                   *train-active-expiry-secs*
                   "yep")
        ;; add this observation to the live train subsystem
        (add-live-observation key universal-ts code-station train)
        ;; add the actual train data to the ordered set
        ;; of data about the train
        (redis-raw-zadd
         key
         universal-ts
         ;; include the station the train is from
         ;; for things like secondsto
         ;; also include some random data, to minimize
         ;; the chance that this element is the same as some
         ;; previous element and redis scronches stuff
         (cpk-raw (list (format nil "~A-~A" code-station (random 256))
                        train)))))))

(defun maybe-redis-trackernet-prediction (pred)
  (multiple-value-bind (filename universal-ts)
      (make-trackernet-filename pred)
    (with-slots (line-code created-ts stations) pred
      (when (< universal-ts (- (get-universal-time)
                                *max-trackernet-data-age-secs*))
        (error "TrackerNet data is too old: made at ~A"
               (slot-value pred 'created-ts)))
      (unless (red:exists filename)
        (let ((code-station (format nil "~A-~A"
                                    line-code (code (first stations)))))
          ;; write out the raw prediction object
          (red:setex filename *prediction-expiry-secs* (cpk-base64 pred))
          (dolist (train (get-trains pred))
            (red:publish "fresh-trains" (cpk-base64 (list universal-ts line-code code-station train)))
            (redis-train universal-ts line-code code-station train))
          (if (red:exists (format nil "~A-trains-new" code-station))
              ;; swap out the new list of trains for the station
              ;; for the old list
              (red:rename (format nil "~A-trains-new" code-station)
                          (format nil "~A-trains" code-station))
              ;; no trains; just delete old list
              (red:del (format nil "~A-trains" code-station)))
          (length (get-trains pred)))))))

(defun should-archive-train (train first-score last-score data)
  "Returns true if TRAIN should be archived."
  (declare (ignore train data))
  (let* ((age-cutoff (- (get-universal-time) *max-train-age-secs*))
         (cutoff (- (get-universal-time) *train-active-expiry-secs-real*)))
    (or
     (< first-score age-cutoff)
     (< last-score cutoff))))

(defun should-archive-xr-train (train first-score last-score data)
  "Returns true if TRAIN (a Crossrail train id) should be archived."
  (declare (ignore first-score))
  (let* ((age-cutoff (- (get-universal-time) *xr-train-expiry-secs*))
         (deleted-cutoff (- (get-universal-time) 3))
         (smart-deleted-cutoff (- (get-universal-time) 30))
         (smart-cancel-cutoff (- (get-universal-time) 600)))
      (or
       ;; archive for age
       (< last-score age-cutoff)
       ;; get rid of "SMART cancelled" trains within
       ;; 10m if they're not going anywhere
       ;; (e.g. in depot)
       (and
        (uiop:string-prefix-p "X-XCB" (car data))
        (< last-score smart-cancel-cutoff))
       ;; archive trains being explicitly deleted
       (and
        (uiop:string-prefix-p "X-DEL" (car data))
        ;; don't honour old deletions from rescued
        ;; trains (issue #44)
        (string=
         (subseq train #.(length "X-train-"))
         (train-id (cadr data)))
        ;; don't *immediately* delete trains marked
        ;; as deleted; wait 3 seconds (see above) for
        ;; something to be interposed there
        ;; (or 30 seconds for SMART)
        (if (position #\· (track-code (cadr data)))
            (< last-score smart-deleted-cutoff)
            (< last-score deleted-cutoff))))))

(defun get-all-trains-extended ()
  "Return a list of all trains, represented with a list for each train with its ID, first observed timestamp, last observed timestamp, and last datapoint."
  (let* ((trains (get-all "?-train-*"))
         (first-last-data
           (coerce
            (redis:with-pipelining
              (dolist (train trains)
                ;; HACK(eta): redis-last-cpk currently returns the score as well
                ;; so we can just use that
                (redis-first-score train)
                (redis-last-cpk train)))
            'vector)))
    (loop
      with fs
      with ls
      with data
      for i from 0
      for train in trains
      do (setf fs (redis-score-parser
                   (elt first-last-data (+ (* i 2) 0))))
      do (setf ls (redis-score-parser
                   (elt first-last-data (+ (* i 2) 1))))
      do (setf data (redis-cpk-parser
                     (elt first-last-data (+ (* i 2) 1))))
      when (and fs ls data)
        collect (list train fs ls data))))

(defun get-stale-train-lists ()
  "Returns as two values a set of trains that should be rescued, and a set of trains that should be archived."
  (let ((trains (get-all-trains-extended))
        (archive-list '())
        (rescue-list '())
        (rescue-cutoff (- (get-universal-time) *train-active-expiry-secs*)))
    (loop
      for (train first-score last-score data) in trains
      do (if (uiop:string-prefix-p "X" train)
             (when (should-archive-xr-train train first-score last-score data)
               (push train archive-list))
             (cond
               ((should-archive-train train first-score last-score data)
                (push train archive-list))
               ((< last-score rescue-cutoff)
                (push train rescue-list)))))
    (values rescue-list archive-list)))

(defun get-iso-8601-for-ts (ts)
  "Returns the current date, in %Y-%m-%d format."
  (local-time:format-timestring
   nil
   (local-time:universal-to-timestamp ts)
   :format '((:year 4) #\- (:month 2) #\- (:day 2))))

(defun get-iso-8601-for-ts-london (ts)
  "Returns the current date, in %Y-%m-%d format."
  (local-time:format-timestring
   nil
   (local-time:universal-to-timestamp ts)
   :format '((:year 4) #\- (:month 2) #\- (:day 2))
   :timezone +europe-london-tz+))

(defun get-iso-8601-date ()
  "Returns the current date, in %Y-%m-%d format."
  (local-time:format-timestring
   nil
   (local-time:now)
   :format '((:year 4) #\- (:month 2) #\- (:day 2))))

(defun get-iso-8601-date-yesterday ()
  "Returns yesterday's date, in %Y-%m-%d format."
  (local-time:format-timestring
   nil
   (let ((now (local-time:now)))
     (setf (local-time:day-of now) (1- (local-time:day-of now)))
     now)
   :format '((:year 4) #\- (:month 2) #\- (:day 2))))

(defun get-dates-since-last-month ()
  "Returns a list of the dates of the last 27 days before today, in ISO 8601 format."
  (let* ((now (local-time:now))
         (28-days-ago (- (local-time:day-of now) 28)))
    (loop
      for i from 0 below 28
      do (setf (local-time:day-of now) (+ 28-days-ago i))
      collect (local-time:format-timestring
               nil now
               :format '((:year 4) #\- (:month 2) #\- (:day 2))))))

(defun publish-firehose-v2-update (lt kind data)
  "Publish a firehose v2 update for the live-train LT of kind KIND, with the DATA being an alist of stuff to include."
  (check-type lt live-train)
  (when (actually-live lt)
    (red:publish "firehose-v2"
                 (cpk-base64
                  `((:ltid . ,(unique-id lt))
                    (:line . ,(line-code lt))
                    (:trackernet-id . ,(princ-to-string (trackernet-id lt)))
                    (:kind . ,kind)
                    ,@data)))))

(defun rescue-train (to-be-archived new-train)
  "Copy records from the train with key TO-BE-ARCHIVED into the NEW-TRAIN, deleting the TO-BE-ARCHIVED train."
  (statsd-inc "intertube.rescues")
  (red:setex (format nil "rescue-ptr-~A"
                     to-be-archived)
             *rescue-ptr-expiry-secs*
             new-train)
  (red:del
   (format nil "~A-rescue-~A"
           (subseq new-train 0 1)
           (subseq new-train #.(length "X-train-"))))
  ;; Get the live train data for the old archived train.
  (let ((start-time (get-internal-real-time))
        (lt (get-or-create-live-train to-be-archived (get-universal-time)))
        (new-observations (redis-cpk-sorted-set-all new-train)))
    ;; Add the new train's data to the live train object.
    ;; Note: we might miss observations that come in while we're doing this.
    ;; Oh well.
    (loop
      for (ts . data) in new-observations
      do (destructuring-bind
             (code-station-random train) data
           (if (position #\· (track-code train))
               (add-smart-live-train-observation
                lt
                ts
                (subseq code-station-random 0 #.(length "X-XXX"))
                train)
               (add-live-train-observation
                lt
                ts
                (subseq code-station-random 0 #.(length "X-XXX"))
                train))))
    ;; Persist the changes we made.
    (persist-live-train lt)
    ;; Delete the pointer for the old trackernet ID to the live train.
    (red:del (format nil "ltmap:~A" to-be-archived))
    ;; Add a pointer for the new trackernet ID, ensuring updates from this
    ;; point get added to the live train.
    (let ((stub-lt
            (red:getset (format nil "ltmap:~A" new-train)
                        (format nil "lt:~A" (unique-id lt)))))
      ;; A stub live train was almost certainly created for the new train
      ;; object before we realised it should be used for rescue, so delete
      ;; that.
      (when stub-lt
        (publish-firehose-v2-update lt :rescue
                                    `((:from-ltid . ,stub-lt)))
        (red:publish
         "firehose"
         (cpk-base64 (list (unique-id lt) :rescue-from stub-lt)))
        (red:del stub-lt)))
    (let* ((end-time (get-internal-real-time))
           (end-time-ms (*
                         (/ (- end-time start-time)
                            internal-time-units-per-second)
                         1000)))
      (log:info "Processed rescue for ~A (~A backfilled in ~,1Fms)"
                (unique-id lt) (length new-observations) end-time-ms)))
  (loop
    for (ts . data) in (redis-cpk-sorted-set-all to-be-archived)
    do (redis-raw-zadd new-train ts (cpk-raw data)))
  ;; get rid of the old train
  (red:del to-be-archived))

(defun get-rescuable-trains (line-code)
  "Gets rescuable trains for the given LINE-CODE, i.e. trains that were created recently that could be joined onto the end of stale trains."
  (when (gethash line-code *rescuable-trains*)
    (return-from get-rescuable-trains
      (gethash line-code *rescuable-trains*)))
  (mapcar
   (lambda (x)
     (cons
      (format nil "~A-train-~A" line-code (subseq (car x) #.(length "D-rescue-")))
      (cdr x)))
   (redis-paired-mget
    (delete-if
     (lambda (x)
       ;; This is kind of subtle: to prevent old trains screwing up
       ;; the rescue of new trains, we add a grace period to the TTL
       ;; of the rescue key that's the length of time it takes for
       ;; an active train to become inactive (plus a few seconds).
       ;;
       ;; This means that an old train can't snatch new trains until
       ;; trains that could conceivably have just expired and be
       ;; the most likely candidate have had their go.
       (> (red:ttl x) *train-active-expiry-secs-real*))
     (get-all (format nil "~A-rescue-*" line-code))))))

(defun format-path (rescue-path)
  "Formats a rescue path, putting -> arrows between codes."
  (format nil "~{~A~^ -> ~}" rescue-path))

(defun cartesian-product (a b)
  (loop
    for x in a
    nconc (loop for y in b
                collect (cons x y))))

(defun get-rescue-candidates-for (stale-train)
  (unless (or
           (uiop:string-prefix-p "S-" stale-train)
           (uiop:string-prefix-p "X-" stale-train))
    (when-let* ((line-code (subseq stale-train 0 1))
                (last-score (redis-last-score stale-train))
                (last-observation (cadr (redis-last-cpk stale-train)))
                (last-track-code (track-code last-observation))
                (candidates (get-rescuable-trains line-code)))
      (let ((last-model-points (toposort-model-points line-code last-track-code))
            (last-leading-car (nilify-zeros (leading-car-no last-observation)))
            (returned-candidates '()))
        ;; Log a failure to get model points.
        (unless last-model-points
          (red:sadd "rescuer:missing-model-points"
                    (format nil "~A:~A"
                            line-code last-track-code)))
        ;; Iterate through all possible candidates.
        (dolist (candidate-code candidates)
          (let* ((candidate (car candidate-code))
                 (candidate-track-code (cdr candidate-code))
                 (candidate-model-points (toposort-model-points line-code candidate-track-code))
                 (candidate-first-score (redis-first-score candidate))
                 (time-difference
                   (when candidate-first-score
                     (- candidate-first-score last-score)))
                 (candidate-data (cadr (redis-first-cpk candidate)))
                 (candidate-leading-car
                   (when candidate-data
                     (nilify-zeros (leading-car-no candidate-data)))))
            ;; Conditions for a rescue:
            (when (and
                   ;; The destination must still exist
                   candidate-first-score candidate-data
                   ;; They can't be the same train
                   (not (equal candidate stale-train))
                   ;; If both trains have a leading car number, the
                   ;; leading car numbers can't be different
                   (or
                    (not last-leading-car)
                    (not candidate-leading-car)
                    (equal last-leading-car candidate-leading-car))
                   ;; The dest must be a given number of seconds newer
                   ;; than the source
                   (>= time-difference
                       *rescue-timediff-threshold*))
              ;; If we don't have any model points, take note
              (unless candidate-model-points
                (red:sadd "rescuer:missing-model-points"
                          (format nil "~A:~A"
                                  line-code candidate-track-code)))
              ;; Check if they share the same leading car
              (when (equal (leading-car-no candidate-data) last-leading-car)
                (log:info "rescuer: ~A and ~A share leading car ~A"
                          stale-train candidate last-leading-car)
                (push (list stale-train candidate time-difference -1)
                      returned-candidates))
              ;; Check to see if they just share the same track code
              (when (equal candidate-track-code last-track-code)
                (log:info "rescuer: ~A and ~A share track code ~A"
                          stale-train candidate last-track-code)
                (push (list stale-train candidate time-difference -1)
                      returned-candidates))
              ;; Check all routes between the source model points and
              ;; the destination model points.
              (loop
                with path-between
                for (from . to) in (cartesian-product last-model-points candidate-model-points)
                do (setf path-between (toposort-model-path-between line-code from to))
                do (when (<= 1 (length path-between) 2)
                     (push (list stale-train candidate time-difference (length path-between))
                           returned-candidates)
                     (log:info "rescuer: ~A (~A, ~A) -> ~A (~A, ~A), ~A hops/~As"
                               stale-train from last-track-code
                               (subseq candidate #.(length "X-train-"))
                               (if (equal from to)
                                   "same" to)
                               (if (equal last-track-code candidate-track-code)
                                   "same" candidate-track-code)
                               (length path-between)
                               time-difference))))))
        returned-candidates))))

(defun toposort-based-rescuer (archivable-trains)
  "A rescue algorithm based on the toposort model; it compares archiveable trains to rescue trains based on their respective model points, instead of track codes."
  (let ((rescues-by-destination (make-hash-table :test 'equal))
        (rescued-trains))
    (loop
      ;; Get a list of rescues to be done across all trains that are about
      ;; to be archived.
      for (source dest time-diff hop-diff)
        in (mapcan (function get-rescue-candidates-for)
                   archivable-trains)
      ;; Take the absolute value of the time difference, since it could be
      ;; negative.
      do (setf time-diff (abs time-diff))
      ;; Set up a hash indexed by rescue destination.
      do (alexandria:ensure-gethash
          dest rescues-by-destination
          (list time-diff hop-diff source))
      ;; If the current source is better than the existing one...
      do (let ((current (gethash dest rescues-by-destination)))
           (when (or
                  ;; ...because the time difference is smaller...
                  (< time-diff (first current))
                  ;; ...or it's the same and the hop difference is less...
                  (and
                   (<= time-diff (first current))
                   (< hop-diff (second current))))
             ;; ...update the destination to use this source.
             (setf (gethash dest rescues-by-destination)
                   (list time-diff hop-diff source)))))
    (setf rescued-trains
          (loop
            for dest being the hash-keys of rescues-by-destination
              using (hash-value source-parts)
            collect (destructuring-bind (time-diff hop-diff source) source-parts
                      (rescue-train source dest)
                      (log:info "rescued ~A -> ~A (~As~A)"
                                source dest time-diff
                                (if (< hop-diff 0)
                                    ""
                                    (format nil ", ~A hops" hop-diff)))
                      source)))))

(defun archive-train (key)
  "Archive the train with the KEY to Clickhouse (if not in staging mode). Returns the number of observations the train had, with the last observed train object as a second value."
  (let* ((last-data (cadr (redis-last-cpk key)))
         (last-ts (redis-last-score key))
         (records-archived (redis-sorted-set-length key))
         (line-code (subseq key 0 1))
         (all-records (redis-cpk-sorted-set-all key))
         (unique-track-code-count
           (ignore-errors
            (when (< records-archived 1000)
              (length
               (remove-duplicates
                (mapcar (lambda (trn) (track-code (third trn)))
                        all-records)
                :test #'string=)))))
         (ltid (red:get (format nil "ltmap:~A" key)))
         (ltid-or-key (or ltid key))
         (clickhouse-data (list ltid-or-key last-ts key all-records)))
    (ignore-errors
     (red:incr (format nil "~A-code-archives-~A"
                       line-code (track-code last-data))))
    (statsd-inc "intertube.archived")
    (statsd-counter "intertube.archived-records" records-archived)
    (unless *staging-mode-p*
      (handler-case
          (sb-ext:with-timeout 10
            (clickhouse-insert-live clickhouse-data))
        (serious-condition (e)
          (log:error "clickhouse insert failed (adding to clickhouse-failures): err: ~A" e)
          (red:sadd "clickhouse-failures" (cpk-base64 clickhouse-data))))
      (when (and ltid (string= line-code "X"))
        ;; delete XR live-trains immediately, or else they'll stick around
        ;; forever
        (red:del ltid)))
    (red:del (format nil "ltmap:~A" key))
    (red:del key)
    (values records-archived last-data unique-track-code-count)))

(defun archive-trains (keys)
  "Archive trains from KEYS, storing them in Clickhouse if configured."
  (dolist (key keys)
    (multiple-value-bind (num-recs last-data unique-tracks)
        (archive-train key)
      (when num-recs
        (log:info "archived ~A, ~A recs / ~A uniq-trks, last: ~A"
                  key num-recs unique-tracks last-data)))))

(defun archive-all-trains ()
  "Archive every single currently active train; start afresh with a clean slate."
  (archive-trains
   (get-all "?-train-*")))

(defun clean-up-stale-trains ()
  "Archive all trains that have expired, and rescue trains that can be rescued."
  (let ((*rescuable-trains* (make-hash-table :test 'equal)))
    ;; Optimization: pre-cache the result of GET-RESCUABLE-TRAINS to avoid
    ;;               hitting the database some O(terrible) number of times.
    (dolist (line *lines*)
      (setf (gethash line *rescuable-trains*)
            (get-rescuable-trains line)))
    ;; Get the lists of trains to consider for rescue and archive.
    (multiple-value-bind (trains-to-rescue trains-to-archive)
        (get-stale-train-lists)
      ;; Rescue trains that need rescuing.
      (toposort-based-rescuer trains-to-rescue)
      ;; Archive trains that were not rescued and actually need to be archived.
      (archive-trains trains-to-archive))))

(defun archiver-loop ()
  (set-thread-name "archiver")
  (redis:with-connection ()
    ;; Let some scrapes happen, so we don't archive stuff
    ;; after a restart
    (sleep (* 3 (duration-sec (car *current-scraper-plan*))))
    (loop
      while (not *trackernet-kill-switch*)
      do (clean-up-stale-trains)
      do (sleep 1))))

(defun get-code-archive-counts (line-code)
  (sort
   (mapcar
    (lambda (key)
      (cons (subseq (car key) #.(length "D-code-archives-"))
            (parse-integer (cdr key))))
    (redis-paired-mget
     (get-all (format nil "~A-code-archives-*" line-code))))
   #'>
   :key #'cdr))

(defun make-zstd-decompressing-stream (in)
  (uiop:process-info-output
   (uiop:launch-program
    "zstd -d"
    :input in
    :element-type '(unsigned-byte 8)
    :output :stream)))

(defclass scraper-plan ()
  ((duration-sec
    :reader duration-sec
    :initarg :duration-sec)
   (start-time-int
    :accessor start-time-int
    :initform nil)
   (worker-instructions
    :reader worker-instructions
    :initarg :worker-instructions)))

(conspack:defencoding scraper-plan
  duration-sec worker-instructions start-time-int)

(defmethod print-object ((obj scraper-plan) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (with-slots (duration-sec worker-instructions start-time-int) obj
      (format stream "for ~A workers, lasting ~,1Fsec/cycle, ~A"
              (length worker-instructions) duration-sec
              (if start-time-int
                  (format nil "started ~,1Fsec ago"
                          (/ (- (get-internal-real-time) start-time-int)
                             internal-time-units-per-second))
                  "unstarted")))))

(defun generate-simple-scraper-plan (station-list)
  "Generates a 'scraper plan' that allocates one worker to each station in STATION-LIST."
  ;; We can do *RUDE-REQUESTS-PER-MINUTE* requests in a minute (e.g. 300).
  ;; We have STATIONS = (LENGTH STATION-LIST) things to poll.
  ;; 300 req / min = [300 / 60] req / sec.
  ;; So it takes us (STATIONS / (300 / 60)) seconds to poll all of the
  ;; stations.
  ;; We wait (1 / (300 / 60)) = 60 / 300 sec between each poll, so that
  ;; we do [300 / 60] requests in each second.
  (let* ((requests-per-sec (/ *rude-requests-per-minute* 60))
         (stations (length station-list))
         (time-to-poll-all (/ stations requests-per-sec))
         (time-between-polls (/ 1 requests-per-sec)))
    (make-instance 'scraper-plan
                   :duration-sec time-to-poll-all
                   :worker-instructions
                   (loop
                     for i from 0 below stations
                     for station in station-list
                     collect `(
                               (,(* i time-between-polls) ,station))))))

(defun calculate-next-scrape (plan worker-id)
  "Given a scraper PLAN and a WORKER-ID that says which worker we are, return as multiple values how long to sleep for, what to scrape next, and how many cycles have elapsed since the start of the plan."
  (when (< worker-id (length (worker-instructions plan)))
    (let* ((now (get-internal-real-time))
           (our-instructions (elt (worker-instructions plan) worker-id))
           (poll-all-int (* (duration-sec plan) internal-time-units-per-second))
           (time-since-plan-start (- now (start-time-int plan))))
      (multiple-value-bind (cycles-elapsed time-through-current-cycle)
          (floor time-since-plan-start poll-all-int)
        (loop
          for cycle-delta from 0
          do (dolist (plan-item our-instructions)
               (destructuring-bind (from-cycle-start-sec station) plan-item
                 (let* ((from-cycle-start-int
                          (+
                              (* cycle-delta poll-all-int)
                              (* from-cycle-start-sec internal-time-units-per-second)))
                        (time-from-now (- from-cycle-start-int time-through-current-cycle)))
                   (when (> time-from-now 0)
                     (return-from calculate-next-scrape
                       (values (/ time-from-now internal-time-units-per-second)
                               station
                               (+ cycles-elapsed cycle-delta))))))))))))

(defun scraper-loop (worker-id)
  (set-thread-name (format nil "scraper ~A" worker-id))
  (when *staging-mode-p*
    (log:error "Not scraping: in staging mode.")
    (return-from scraper-loop))
  (redis:with-connection ()
    (let ((backoff-time *backoff-time-secs*)
          (timeout-count 0)
	  (ns-count 0)
          (scraper-plan (car *current-scraper-plan*))
          (last-epoch nil)
          (line-code nil)
          (station-code nil))
     (tagbody
        (go :start)
      :backoff
        (log:warn "(w#~A) sleeping for ~As during backoff" worker-id backoff-time)
        (sleep backoff-time)
        (setf backoff-time (* 2 backoff-time))
        (go :start)
      :start
        (unless (eq scraper-plan (car *current-scraper-plan*))
          (setf scraper-plan (car *current-scraper-plan*))
          (setf last-epoch nil)
          (log:info "(w#~A) new plan: ~A" worker-id scraper-plan))
        (multiple-value-bind (sleep-sec station cycle)
            (calculate-next-scrape scraper-plan worker-id)
          (if sleep-sec
              (progn
                (setf line-code (car station))
                (setf station-code (cadr station))
                (when (and last-epoch (> cycle (1+ last-epoch)))
                  (log:warn "(w#~A) Worker can't keep up! Skipped from epoch ~A to ~A"
                            worker-id last-epoch cycle))
                (setf last-epoch cycle)
                (sleep sleep-sec))
              (progn
                (log:warn "(w#~A) Nothing to scrape, worker stopping!" worker-id)
                (go :end))))
        (when *trackernet-kill-switch*
          (log:warn "(w#~A) Kill switch pulled, worker stopping!" worker-id)
          (go :end))
        (handler-case
            (let ((pred
                    (handler-bind
                        ((sb-ext:timeout (lambda (c)
                                           (declare (ignore c))
                                           (format *error-output* "~&Timeout in scraper, backtrace follows:~%")
                                           (sb-debug:print-backtrace
                                            :count most-positive-fixnum
                                            :stream *error-output*
                                            :emergency-best-effort t))))
                      (sb-ext:with-timeout 7
                        (fetch-trackernet-prediction line-code station-code)))))
              (statsd-inc (format nil "scraper.~A-~A.scraped" line-code station-code))
              (statsd-inc "intertube.scraped-total")
              (setf timeout-count 0)
              (setf ns-count 0)
              (setf backoff-time *backoff-time-secs*)
              (let ((num-trains (maybe-redis-trackernet-prediction pred)))
                (when num-trains
                  (statsd-gauge (format nil "scraper.~A-~A.trains" line-code station-code) num-trains)
                  (statsd-inc (format nil "scraper.~A-~A.new" line-code station-code)))))
          (sb-ext:timeout (e)
            (statsd-inc (format nil "scraper.~A-~A.errors" line-code station-code))
            (when (> (incf timeout-count) 10)
              (log:error "10 consecutive timeouts experienced, aborting!")
              (sb-ext:exit :code 100 :abort t))
            (log:error "timeout occurred for ~A-~A, ~A" line-code station-code e))
	  (sb-bsd-sockets:name-service-error (e)
            (statsd-inc (format nil "scraper.~A-~A.errors" line-code station-code))
            (when (> (incf ns-count) 10)
              (log:error "10 consecutive ns errors experienced, aborting!")
              (sb-ext:exit :code 103 :abort t))
            (log:error "ns error occurred for ~A-~A, ~A" line-code station-code e))
          ;; Stupid USOCKET doesn't make its condition classes subtypes of ERROR,
          ;; so let's catch them separately. Well done.
          (usocket:ns-condition (e)
            (statsd-inc (format nil "scraper.~A-~A.errors" line-code station-code))
            (log:error "resolver failed for ~A-~A, ~A" line-code station-code e)
            (go :backoff))
          (usocket:socket-condition (e)
            (statsd-inc (format nil "scraper.~A-~A.errors" line-code station-code))
            (log:error "socket error for ~A-~A, ~A" line-code station-code e)
            (go :backoff))
          (error (e)
            (statsd-inc (format nil "scraper.~A-~A.errors" line-code station-code))
            (log:error "applevel error for ~A-~A, ~A" line-code station-code e)
            (go :backoff)))
        (go :start)
      :end))))

(defun trains-active-on-line (line-code)
  "Returns a list of all trains active on the given LINE-CODE right now."
  (get-all (format nil "~A-active-*"
                   line-code)))

(defun old-active-keys (line-code)
  (delete-if #'red:get (trains-active-on-line line-code)))

(defun all-trains-with-sizes ()
  (sort
   (mapcar
    (lambda (trn)
      (cons trn (redis-sorted-set-length trn)))
    (get-all "?-train-*"))
   #'>
   :key #'cdr))

(defun statsd-reporter-loop (line-code)
  (set-thread-name "statsd-reporter")
  (redis:with-connection ()
    (loop
      while (not *trackernet-kill-switch*)
      do (let ((active (length (trains-active-on-line line-code))))
           (statsd-gauge (format nil "line.~A.active-trains" line-code)
                         active)
           (red:set (format nil "active-trains-~A" line-code) active)
           (sleep 1)))))

(defun get-all-live-trains ()
  "Returns a list of all currently active live train objects."
  (delete-if #'null
             (apply #'redis-raw-mget-cpk (get-all "lt:*"))))

(defun line-members-loop ()
  "Scans all active live trains and creates sets for each Tube line containing all live trains on that line. Also updates statsd."
  (loop
    while (not *trackernet-kill-switch*)
    do (let ((lts (get-all-live-trains)))
         (dolist (line *lines*)
           (let ((line-trains (remove-if-not
                               (lambda (lt)
                                 (string= (line-code lt) line))
                               lts))
                 (line-set-new (format nil "line-trains-new:~A" line))
                 (line-set (format nil "line-trains:~A" line)))
             (dolist (lt line-trains)
               (red:sadd line-set-new
                         (format nil "lt:~A" (unique-id lt))))
             (if (red:exists line-set-new)
                 (red:rename line-set-new line-set)
                 (red:del line-set))
             (statsd-gauge (format nil "line.~A.active-trains" line)
                           (length line-trains))))
         ;; not technically related but meh
         (statsd-gauge "clickhouse-failures" (red:scard "clickhouse-failures"))
         (sleep 1))))

(defun tph-expiry-loop-cycle ()
  "Scans all trains-per-hour station call lists, expiring elements older than an hour."
  (let ((tphs (mapcar (lambda (key)
                        (cons key
                              (redis-sorted-set-all key)))
                      (get-all "tph:*")))
        (hour-ago (- (get-universal-time) 3600)))
    (dolist (entry tphs)
      (loop
        for (time . ltid) in (cdr entry)
        when (> time hour-ago)
          do (return)
        do (statsd-inc "tph-expired")
        do (red:zrem (car entry) ltid)))))

(defun tph-expiry-loop ()
  (loop
    while (not *trackernet-kill-switch*)
    do (tph-expiry-loop-cycle)
    do (sleep 30)))

(defun tph-register-station-call (line-code station-name platform ltid universal-ts)
  "Register a station call, for the purposes of counting trains per hour."
  (red:zadd (format nil "tph:~A:~A:~A" line-code station-name platform)
            universal-ts (format nil "~A|~A" ltid universal-ts)))

(defun tph-recent-station-calls (line-code station-name platform)
  "Get a list of recent station calls at PLATFORM of STATION-NAME, as a set of (universal-ts . ltid) cons cells."
  (mapcar
   (lambda (ent)
     (cons (car ent)
           (subseq (cdr ent) 0 (position #\| (cdr ent)))))
   (redis-sorted-set-all (format nil "tph:~A:~A:~A" line-code station-name platform))))

(defun tph-for-station (line-code station-name platform)
  "Get trains-per-hour figures for the last hour, 30 minutes, and 15 minutes for the provided location."
  (let* ((all-calls (tph-recent-station-calls line-code station-name platform))
         (now (get-universal-time))
         (hour-ago (- now 3600))
         (30min (- now 1800))
         (15min (- now 900))
         (num-hour-ago 0)
         (num-30min 0)
         (num-15min 0))
    (loop
      for (ts . ltid) in all-calls
      when (>= ts hour-ago)
        do (incf num-hour-ago)
      when (>= ts 30min)
        do (incf num-30min)
      when (>= ts 15min)
        do (incf num-15min))
    (values num-hour-ago (* 2 num-30min) (* 4 num-15min))))

(defun start-statsd-reporter-loop (line-code)
  "Starts a loop to report the number of active trains on a line to statsd."
  (sleep 0.1)
  (sb-thread:make-thread
   #'statsd-reporter-loop
   :name (format nil "statsd reporter loop for ~A" line-code)
   :arguments (list line-code)))

(defun start-archiver-loop ()
  (sb-thread:make-thread
   #'archiver-loop
   :name "archiver loop"))

(defun scraper-plan-update-loop (redis)
  (let ((redis:*connection* redis))
    (red:subscribe "plan-updates"))
  (loop
    for msg = (let ((redis:*connection* redis))
                (redis:expect :anything))
    while msg
    do (handler-case
           (let ((plan (cpk-unbase64 (third msg))))
             (log:info "New scraper plan received via Redis")
             (set-scraper-plan plan))
         (error (e)
           (log:error "Could not parse Redis scraper plan: ~A" e)))))

(defun set-scraper-plan-remote (plan)
  (check-type plan scraper-plan)
  (red:set "scraper-plan" (cpk-base64 plan))
  (red:publish "plan-updates" (cpk-base64 plan)))

(defun set-scraper-plan (plan)
  (log:info "Changing scraper plan to ~A" plan)
  (setf (start-time-int plan) (get-internal-real-time))
  (setf (car *current-scraper-plan*) plan)
  (let* ((workers-required (length (worker-instructions plan)))
         (num-extra (- workers-required *current-worker-count*)))
    (when (> num-extra 0)
      (unless *staging-mode-p*
        (log:info "Starting ~A extra workers for plan switch" num-extra)
        (loop
          for id from *current-worker-count*
            below workers-required
          do (sb-thread:make-thread
              #'scraper-loop
              :name (format nil "scrape worker #~A" id)
              :arguments (list id)))))
    (setf *current-worker-count* workers-required)))

(defun stop-scrapers ()
  (setf *trackernet-kill-switch* t))

(defun forward-scrapes ()
  (log:info "Forwarding scrapes from production")
  (let ((prod-db (make-instance 'redis:redis-connection
                                :host "intertube.i.eta.st")))
    (let ((redis:*connection* prod-db))
      (red:subscribe "fresh-trains"))
    (loop
      for msg = (let ((redis:*connection* prod-db))
                  (redis:expect :anything))
      while msg
      do (red:publish "fresh-trains" (third msg)))))

(defun read-forwarded-scrapes ()
  (let ((conn (make-instance 'redis:redis-connection
                             :host *redis-host* :port *redis-port*)))
    (let ((redis:*connection* conn))
      (red:subscribe "fresh-trains"))
    (loop
      for msg = (let ((redis:*connection* conn))
                  (redis:expect :anything))
      while msg
      do (let* ((msg (trackernet::cpk-unbase64 (third msg))))
           (destructuring-bind (universal-ts line-code code-station train) msg
             (redis-train universal-ts line-code code-station train))))))

(defun start ()
  (unless (or
           *staging-mode-p*
           (and (boundp 'statsd:*client*)
                statsd:*client*))
    (setf statsd:*client* (statsd:make-sync-client :host "metrics.i.eta.st")))
  (unless (red:exists "scraper-plan")
    (log:warn "No scraper plan in Redis; constructing one")
    (set-scraper-plan-remote (generate-simple-scraper-plan *codes-to-scrape*)))
  (set-scraper-plan (cpk-unbase64 (red:get "scraper-plan")))
  (start-archiver-loop)
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "plan updater")
     (redis:with-connection ()
       (scraper-plan-update-loop (make-instance 'redis:redis-connection))))
   :name "scraper plan update loop")
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "line members")
     (redis:with-connection ()
       (line-members-loop)))
   :name "line members loop")
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "tph expiry")
     (redis:with-connection ()
       (tph-expiry-loop)))
   :name "tph expiry loop")
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "elizabeth")
     (redis:with-connection ()
       (elizabeth-line-loop)))
   :name "elizabeth line loop")
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "tfl status")
     (redis:with-connection ()
       (tfl-status-information-loop)))
   :name "tfl status loop")
  (sb-thread:make-thread
   (lambda ()
     (set-thread-name "clickhouse clr")
     (redis:with-connection ()
       (loop
         (handler-case
             (clear-clickhouse-queue)
           (serious-condition (c) (log:error "failed to clear clickhouse queue: ~A" c)))
         (sleep 60))))
   :name "clickhouse queue clearing loop")
  (when *staging-mode-p*
    (sb-thread:make-thread
     (lambda ()
       (redis:with-connection ()
         (handler-case
             (forward-scrapes)
           (serious-condition (e)
             (log:error "Failed to start scrape forwarder: ~A" e)))))
     :name "prod scrape forwarder")
    (sb-thread:make-thread
     (lambda ()
       (redis:with-connection ()
         (read-forwarded-scrapes)))
     :name "prod scrape reader"))
  t)

(defun irccat-put (message)
  (handler-case
      (sb-ext:with-timeout 1
        (drakma:http-request
         "http://metrics.i.eta.st:6001/send"
         :method :post
         :content message))
    (serious-condition (e)
      (log:warn "Failed to inform irccat: ~A" e))))

(defun debugger-hook (condition self)
  (declare (ignore self))
  ;; attempt to inform irccat
  (irccat-put
   (format nil "~A ~A (on thread ~A)"
           (if *staging-mode-p*
               "%ORANGEintertube-staging error%NORMAL:"
               "%REDintertube-prod error%NORMAL (cc eta):")
           (substitute #\Space #\Newline
                       (princ-to-string condition))
           (sb-thread:thread-name sb-thread:*current-thread*)))
  (trivial-backtrace:print-backtrace condition)
  (sb-ext:exit :abort t))

(defun main ()
  (format t "~&=== intertube-scraper version ~A / eta.st ===~%" *code-version*)
  (when (uiop:getenv "INTERTUBE_STAGING")
    (format t "~&*** Running in staging mode ***~%")
    (setf *redis-host* "intertube-staging.i.eta.st")
    (setf *staging-mode-p* t))
  (unless *nrod-data-key*
    (let ((nrod-user (uiop:getenv "NROD_USER"))
          (nrod-password (uiop:getenv "NROD_PASSWORD")))
      (when (and nrod-user nrod-password)
        (format t "~&Loaded NROD credentials from environment!~%")
        (setf *nrod-data-key* (cons nrod-user nrod-password)))))
  (alexandria:when-let ((custom-baseurl (uiop:getenv "TRACKERNET_BASEURL")))
    (setf *trackernet-base-url* custom-baseurl)
    (format t "~&Using Trackernet base URL: ~A~%" *trackernet-base-url*))
  (alexandria:when-let ((custom-rps (uiop:getenv "TRACKERNET_REQUESTS_PER_MIN")))
    (setf *rude-requests-per-minute* (parse-integer custom-rps))
    (format t "~&Will target ~A requests per second~%" *rude-requests-per-minute*))
  (alexandria:when-let ((custom-appkey (uiop:getenv "TRACKERNET_APP_KEY")))
    (setf *trackernet-app-key* custom-appkey)
    (format t "~&Loaded Trackernet app key from environment!~%"))
  (when (and (> (length sb-ext:*posix-argv*) 1)
             (string= (elt sb-ext:*posix-argv* 1) "--repl"))
    (format t "Starting REPL...~%")
    (sb-impl::toplevel-repl nil)
    (return-from main))
  (setf *debugger-hook* #'debugger-hook)
  (format t "Connecting to Redis...")
  (redis:connect)
  (format t "done.~%")
  (when-let ((trackernet-base-url (red:get "config:trackernet-base-url")))
    (format t "note: using trackernet base URL from Redis~%")
    (setf *trackernet-base-url* trackernet-base-url))
  (when-let ((unified-api-base-url (red:get "config:unified-api-base-url")))
    (format t "note: using unified API base URL from Redis~%")
    (setf *unified-api-base-url* unified-api-base-url))
  (when-let ((nrod-stomp-host (red:get "config:nrod-stomp-host")))
    (format t "note: using NROD STOMP host from Redis~%")
    (setf *nrod-stomp-host* nrod-stomp-host))
  (when-let ((nrod-stomp-port (red:get "config:nrod-stomp-port")))
    (format t "note: using NROD STOMP port from Redis~%")
    (setf *nrod-stomp-port* (parse-integer nrod-stomp-port)))
  (start)
  (irccat-put (format nil "intertube-scraper: launched version ~A"
                      *code-version*))
  (loop (sleep 1)))

(defun xr-line-track-descriptions ()
  (let ((known-berths
          (remove-if
           (lambda (berth)
             (member berth *ignored-berths* :test (function string=))
             (uiop:string-suffix-p berth "XXCB"))
           (remove-duplicates
            (append
             (alexandria:hash-table-keys
              *smart-station-descriptions-table*)
             (alexandria:hash-table-keys
              xrcos::*xrcos-descriptions*)
             (mapcar
              (lambda (key)
                (subseq key #.(length "X-track-desc-")))
              (get-all "X-track-desc-*")))
            :test (function string=)))))
    (loop
      for berth in known-berths
      collect (cons berth
                    (or
                     (gethash berth *smart-station-descriptions-table*)
                     (gethash berth xrcos::*xrcos-descriptions*)
                     (format nil "berth ~A" berth))))))

(defun line-track-descriptions (line-code)
  "Returns an alist of all track codes and their textual descriptions for the given LINE-CODE."
  (when (string= line-code "X")
    (return-from line-track-descriptions
      (xr-line-track-descriptions)))
  (mapcar
   (lambda (key)
     (cons (subseq (car key) #.(length "X-track-desc-"))
           (cdr key)))
   (redis-paired-mget
    (remove-if
     (lambda (key)
       (let ((tc (subseq key #.(length "X-track-desc-"))))
         (or
          (member tc *ignored-berths* :test (function string=))
          (and
           (string= line-code "X")
           (uiop:string-suffix-p tc "XXCB")))))
     (get-all (format nil "~A-track-desc-*"
                      line-code))))))

(defvar *track-descriptions-cache* (make-hash-table :test 'equal))

(defun cached-track-descriptions (code)
  (symbol-macrolet
      ((cached-result (gethash code *track-descriptions-cache*)))
    (if cached-result
        cached-result
        (setf cached-result (line-track-descriptions code)))))

(defun reset-track-links-set ()
  (setf *track-links-set* (make-hash-table
                           :test 'equal)))

(defparameter *links-to-inspect* '())

(defun handle-archived-train-inspect (archived-train)
  (destructuring-bind (filename archived-universal-ts train-key history)
      archived-train
    (declare (ignore filename archived-universal-ts train-key))
    (let ((previous nil))
      (dolist (entry history)
        (block nil
          (unless previous
            (setf previous entry)
            (return))
          (let ((track-a (track-code (third previous)))
                (track-b (track-code (third entry))))
            (when (member (list track-a track-b) *links-to-inspect*
                          :test #'equal)
              (format t "*** FOUND ~A~%" archived-train)
              (return-from handle-archived-train-inspect))
            (setf previous entry)))))))

(defun handle-archived-train-redis (archived-train)
  (destructuring-bind (filename archived-universal-ts train-key history)
      archived-train
    (declare (ignore archived-universal-ts))
    (when (red:sismember "model:sources" filename)
      (log:warn "Already processed train ~A, not reprocessing" filename)
      (return-from handle-archived-train-redis))
    (let* ((line-code (subseq train-key 0 1))
           (track-links-ht (format nil "model:links:~A" line-code))
           (previous nil)
           (added 0))
      (redis:with-pipelining
        (dolist (entry history)
          (block nil
            (unless previous
              (setf previous entry)
              (return))
            (incf added)
            (let ((track-a (track-code (third previous)))
                  (track-b (track-code (third entry))))
              (unless (or (string= track-a track-b)
                          ;; Ignore skips back in time, or just
                          ;; observations with the same timestamp.
                          (>= (first previous) (first entry)))
                (red:hincrby track-links-ht
                             (format nil "~A•~A" track-a track-b)
                             1)))
            ;;(log:info "added ~A entries (~A) from ~A~%" added line-code filename)
            (setf previous entry)))))
    (red:sadd "model:sources" filename)))

(defun delete-redis-model-data ()
  (red:del "model:sources")
  (mapcar #'red:del (get-all "model:links:*")))

(defvar *clickhouse-cont* nil)
(defvar *clickhouse-rows* nil)
(defvar *clickhouse-file* nil)

(defun write-clickhouse-tsv (archived-train)
  (destructuring-bind (filename archived-universal-ts train-key history)
      archived-train
    (loop
      for (observation-ts observer train) in history
      do (with-slots
               (train-id lcid set-no trip-no seconds-to time-to
                location-desc destination-desc dest-code order
                depart-time depart-interval departed direction
                is-stalled track-code line input-dest leading-car-no)
             train
           (let* ((stuff
                    (list
                     (- observation-ts 2208988800)
                     (subseq observer 2 #.(length "X-XXX"))
                     (- archived-universal-ts 2208988800)
                     filename
                     *clickhouse-file*
                     train-id
                     (nilify-zeros-parse lcid)
                     (ignore-errors (nilify-zeros-parse set-no))
                     (nilify-zeros-parse trip-no)
                     (nilify-zeros-parse seconds-to)
                     location-desc destination-desc dest-code order
                     (if (and depart-time (> (length depart-time) 0))
                         (parse-trackernet-time depart-time)
                         0)
                     (nilify-zeros-parse depart-interval)
                     (nilify-zeros-parse departed)
                     (nilify-zeros-parse direction)
                     (nilify-zeros-parse is-stalled)
                     track-code
                     (or line (subseq train-key 0 1))
                     input-dest
                     (nilify-zeros-parse leading-car-no)))
                  (line
                    (with-output-to-string (*clickhouse-out*)
                      (loop
                        for thing in stuff
                        for i from 1
                        do (if (null thing)
                               (princ "0" *clickhouse-out*)
                               (princ thing *clickhouse-out*))
                        unless (eql i (length stuff))
                          do (princ #\Tab *clickhouse-out*)
                        finally (terpri *clickhouse-out*)))))
             (incf *clickhouse-rows*)
             (funcall *clickhouse-cont* line t))))))

(defun clickhouse-insert-live (archived-train)
  (let ((start-time (get-internal-real-time))
        (*clickhouse-cont*
          (drakma:http-request "http://clickhouse.i.eta.st:8123/"
                               :method :post
                               :parameters '(("query" . "INSERT INTO trains FORMAT TabSeparated")
                                             ("password" . "intertube"))
                               :form-data nil
                               :external-format-out :utf-8
                               :content :continuation))
        (*clickhouse-file* "live")
        (*clickhouse-rows* 0))
    (write-clickhouse-tsv archived-train)
    (multiple-value-bind
          (body status-code headers uri stream must-close)
        (funcall *clickhouse-cont* "" nil)
      (declare (ignore headers uri))
      (unless (eql status-code 200)
        (statsd-inc "intertube.clickhouse-fail")
        (error "clickhouse returned ~A: ~A"
               status-code body))
      ;; Of course you have to do this. Of course it isn't documented.
      ;; Of course you'll just leak sockets and get weird errors elsewhere
      ;; until you do.
      (when must-close
        (close stream))
      (let* ((end-time (get-internal-real-time))
             (end-time-ms (*
                           (/ (- end-time start-time)
                              internal-time-units-per-second)
                           1000)))
        (log:debug "Inserted ~A rows into clickhouse in ~,1Fms"
                   *clickhouse-rows* end-time-ms)))))

(defun add-manual-track-link (src dest)
  (setf (gethash (list src dest) *replaced-track-links-set*)
        (gethash (list src dest) *track-links-set*))
  (setf (gethash (list src dest) *track-links-set*)
        +manual-count+))

(defun remove-manual-track-links ()
  (loop
    for k being the hash-keys of *replaced-track-links-set*
    do (setf (gethash k *track-links-set*)
             (gethash k *replaced-track-links-set*)))
  (clrhash *replaced-track-links-set*))

(defun calculate-exponential-base (max top)
  (expt top
        (/ 1 max)))

(defun track-links-frequencies (&key (cutoff 0))
  (sort (loop
          for v being the hash-values of *track-links-set*
          unless (< v cutoff)
            collect v)
        #'<))

(defun track-links-quartiles (&key (cutoff 0))
  (let ((data (track-links-frequencies :cutoff cutoff)))
    (values
      (elt data
           (* 1
              (floor (length data) 4)))
      (elt data
           (* 3
              (floor (length data) 4))))))

(defun squish-link-length (in max top &optional (base (calculate-exponential-base max top)))
  (cond
   ((> in max) top)
   (t (expt base in))))

(defun dump-track-links-to-file (out)
  (with-open-file (stream out
                   :direction :output
                   :element-type '(unsigned-byte 8))
    (cpk:encode (alexandria:hash-table-alist *track-links-set*)
                :stream stream)))

(defun dump-normalized-track-links-to-file (out)
  (with-open-file (stream out
                          :direction :output
                          :element-type '(unsigned-byte 8))
    (cpk:encode (alexandria:hash-table-alist *normalized-track-links*)
                :stream stream)))

(defun dump-track-links-to-file-as-normalized (out)
  (with-open-file (stream out
                          :direction :output
                          :element-type '(unsigned-byte 8))
    (let ((table (make-hash-table :test #'equal)))
      (loop
        for (a b) being the hash-keys of *track-links-set*
        do (symbol-macrolet
               ((normalized-links (gethash a table)))
             (unless normalized-links
               (setf normalized-links nil))
             (push (list b 1) normalized-links)))
      (cpk:encode (alexandria:hash-table-alist table)
                  :stream stream))))

(defun regenerate-network-model (&optional (revision 1))
  (dolist (line *lines*)
    (let* ((date (get-iso-8601-date))
           (track-links-filename (format nil "./~A-~A-~A.dot"
                                         date line revision))
           (normalized-filename (format nil "./~A-~A-~A.cpk"
                                        date line revision)))
      (log:info "Regenerating model for line ~A" line)
      (load-redis-track-links line)
      (write-track-links-graphviz-to-file track-links-filename line)
      (log:info "Wrote track links graph to ~A" track-links-filename)
      (dump-normalized-track-links-to-file normalized-filename)
      (log:info "Wrote normalized graph data to ~A" normalized-filename))))

(defun write-mini-graphviz (around-codes line-code &key (out *standard-output*) (neato nil))
  "Write out a mini graph containing the nodes around AROUND-CODE in graphviz format."
  (let ((*normalized-track-links*
          (or (gethash line-code *per-line-track-graph*)
              *normalized-track-links*)))
    (princ "digraph {" out)
    (terpri out)
    (princ "node [shape=box margin=\"0.1,0.1\"];" out)
    (terpri out)
    (if neato
        (progn
          (princ "layout = \"neato\";" out)
          (terpri out))
        (progn
          (princ "rankdir = LR;" out)
          (terpri out)))
    (let ((nodes around-codes)
          (nodes-1 '())
          (links '()))
      (loop
        for src being the hash-keys of *normalized-track-links*
          using (hash-value dests)
        when (member src around-codes :test #'string=)
          do (dolist (dest dests)
               (pushnew (car dest) nodes
                         :test #'string=)
               (push (list src (car dest) (cadr dest))
                     links))
        unless (member src around-codes :test #'string=)
          do (loop
               for (dest dist) in dests
               when (member dest around-codes :test #'string=)
                 do (pushnew src nodes
                             :test #'string=)
               when (member dest around-codes :test #'string=)
                 do (push
                     (list src dest dist)
                     links)))
      (setf nodes-1 (copy-seq nodes))
      (loop
        for src being the hash-keys of *normalized-track-links*
          using (hash-value dests)
        when (and (member src nodes-1 :test #'string=)
                  (not (member src around-codes :test #'string=)))
          do (dolist (dest dests)
               (unless (member (car dest) around-codes :test #'string=)
                 (pushnew (car dest) nodes
                           :test #'string=)
                 (push (list src (car dest) (cadr dest))
                       links))))
      (loop
        for node in nodes
        for (nil . value) in (redis-paired-mget
                              (mapcar
                               (lambda (node)
                                 (format nil "~A-track-desc-~A"
                                         line-code node))
                               nodes))
        do (format out "\"~A\" [fillcolor=~A style=filled label=<<B>~A</B><BR /><I><FONT POINT-SIZE=\"10\">~{~A~^<BR ALIGN=\"LEFT\"/>~}</FONT></I>>]~%"
                   node
                   (if (member node around-codes :test #'string=)
                       "lightblue1"
                       "white")
                   node
                   (if (eql (length value) 0)
                       '("???")
                       (bobbin:wrap (list value) 20))))
      (loop
        for (src dest dist) in links
        do (format out "\"~A\" -> \"~A\" [len=~F];~%"
                   src dest dist))
      (princ "}" out)
      (terpri out))))

(defun load-redis-track-links (line-code)
  (clrhash *track-links-set*)
  (let* ((clickhoused t)
         (raw (red:hgetall (format nil "model:links-ch:~A" line-code)))
         (raw (if raw raw
                  (progn
                    (log:warn "No Clickhouse links for ~A; using regular links" line-code)
                    (setf clickhoused nil)
                    (red:hgetall (format nil "model:links:~A" line-code)))))
         (prev nil))
    (format *debug-io* "<b>Loaded ~A track links for ~A from ~A</b>~%"
            (length raw) line-code
            (if clickhoused "ClickHouse" "Redis"))
    (dolist (entry raw)
      (block nil
        (unless prev
          (setf prev entry)
          (return))
        (let* ((sep (position #\• prev))
               (track-a (subseq prev 0 sep))
               (track-b (subseq prev (1+ sep))))
          (setf (gethash (list track-a track-b)
                         *track-links-set*)
                (parse-integer entry)))
        (setf prev nil)))))

(defun write-track-links-graphviz (&key (out *standard-output*) (cutoff 10) line)
  "Write the track links out in graphviz format."
  (princ "digraph {" out)
  (format out "node [shape=box margin=\"0.1,0.1\"];
layout = \"neato\";")
  (terpri out)
  (format *debug-io* "*** Writing track descriptions~%")
  (loop
    for (key . value) in (redis-paired-mget (get-all (format nil
                                                             "~A-track-desc-*"
                                                             line)))
    for actual = (subseq key 13)
    do (format out "\"~A\" [label=<<B>~A</B><BR /><I><FONT POINT-SIZE=\"10\">~{~A~^<BR ALIGN=\"LEFT\"/>~}</FONT></I>>]~%"
               actual
               actual
               (if (eql (length value) 0)
                   '("???")
                   (bobbin:wrap (list (substitute #\+ #\& value)) 20))))
  (format *debug-io* "*** Calculating quartiles~%")
  (multiple-value-bind (q1 q3) (track-links-quartiles :cutoff cutoff)
    (let* ((base (calculate-exponential-base q3 3))
           (terminators nil)
           (sources nil)
           (nlinks 0)
           (per-dest-links (make-hash-table :test 'equal))
           (per-src-links (make-hash-table :test 'equal)))
      (format *debug-io* "*** Q1 ~A / Q3 ~A~%" q1 q3)
      (format *debug-io* "*** Calculating per-destination links (~A links)~%" (hash-table-size *track-links-set*))
      (loop
        for (src dest) being the hash-keys of *track-links-set*
          using (hash-value v)
        unless (or
                (string= src dest)
                (< v q1))
          do (symbol-macrolet
                 ((dest-links (gethash dest per-dest-links)))
               (unless dest-links
                 (setf dest-links nil))
               (push (cons src v) dest-links)))
      (format *debug-io* "*** Calculating per-source links (~A dests)~%" (hash-table-size per-dest-links))
      (loop
        for b being the hash-keys of per-dest-links
          using (hash-value srcs)
        do (let* ((num-manuals (count-if (lambda (srcval)
                                           (eql (cdr srcval)
                                                +manual-count+))
                                         srcs))
                  (new-len (length srcs)))
             (declare (ignore num-manuals)) ; no idea what this was for
             (setf (gethash b per-dest-links)
                   (subseq (sort (copy-seq srcs) #'> :key #'cdr) 0 new-len)))
           (loop
             for (a . v) in (gethash b per-dest-links)
             for i from 0
             do (symbol-macrolet
                          ((src-links (gethash a per-src-links)))
                  (unless src-links
                    (setf src-links nil))
                  (incf nlinks)
                  (push (cons b v) src-links))))
      (format *debug-io* "*** Calculating sources & terminators~%")
      (loop
        for b being the hash-keys of per-dest-links
        using (hash-value srcs)
        do (loop
             for (a . v) in srcs
             when (null (gethash a per-dest-links))
               do (pushnew a sources :test #'string=))
           (when (null (gethash b per-src-links))
             (pushnew b terminators :test #'string=)))
      (format *debug-io* "*** ~A sources / ~A terminators~%" (length sources) (length terminators))
      (format *debug-io* "*** Writing ~A links~%" nlinks)
      (clrhash *normalized-track-links*)
      (loop
        for b being the hash-keys of per-dest-links
          using (hash-value srcs)
        do (loop
             for (a . v) in srcs
             for i from 0
             do (let* ((score-2 (1- (squish-link-length v q3 3 base)))
                       (score-perc (floor (* 50 score-2)))
                       (src-count (length (gethash a per-dest-links)))
                       (redness 0.0)
                       (distance (+ (- 3.5 score-2)
                                     (if (eql v +manual-count+)
                                         0
                                         (* 0.5 (1- (length srcs)))))))
                  (when (and (eql src-count 1)
                             (eql (length srcs) 1))
                    (setf redness 1.0)
                    (setf distance 1.5))
                  (symbol-macrolet
                      ((normalized-links (gethash a *normalized-track-links*)))
                    (unless normalized-links
                      (setf normalized-links nil))
                    (push (list b distance) normalized-links))
                  (format out "\"~A\" -> \"~A\" [weight=~A len=~F label=\"~A (~A/~A ~A%)\" color=\"0.0 ~F ~F\" fontcolor=\"0.0 ~F ~F\" style=\"~A\"];~%"
                          a
                          b
                          score-perc
                          distance
                          v
                          (1+ i)
                          (length srcs)
                          score-perc
                          redness
                          (/ (- 100 score-perc) 100)
                          redness
                          (+ 0.2 (/ (- 100 score-perc) 120))
                          (cond
                            ((> score-perc 80) "bold")
                            ((< score-perc 20) "dotted")
                            (t "solid"))))))
      (princ "}" out)
      (format t "*** Sources:~%")
      (loop
        for code in sources
        do (format t "   ~A: ~A~%" code (red:get (format nil "D-track-desc-~A" code))))
      (format t "*** Terminators:~%")
      (loop
        for code in terminators
        do (format t "   ~A: ~A~%" code (red:get (format nil "D-track-desc-~A" code))))
      "}")))

(defun write-track-links-graphviz-clustered (&optional (out *standard-output*))
  "Write the track links out in graphviz format."
  (princ "digraph {" out)
  (terpri out)
  (format out "  overlap = false;
  splines = polyline;
")
  (let ((descs-ht (make-hash-table :test 'equal)))
    (loop
      for key in (get-all "D-track-desc-*")
      for actual = (subseq key #.(length "D-track-desc-"))
      for value = (red:get key)
      do (when (and
                (> (length value) 0)
                (not (uiop:string-prefix-p "Approaching" value))
                (not (uiop:string-prefix-p "Left" value))
                (not (string= value "At Platform")))
           (unless (gethash value descs-ht)
             (setf (gethash value descs-ht) nil))
           (push actual (gethash value descs-ht))))
    (loop
      for description being the hash-keys of descs-ht
        using (hash-value track-codes)
      for i from 0
      do (format out "subgraph cluster_~A {
label=\"~A\";
style=\"filled\";
color=lightblue;
~{\"~A\";~^~%~}
}
"
                 i
                 description
                 track-codes)))
  (loop
    for (a b) being the hash-keys of *track-links-set*
      using (hash-value v)
    unless (or (< v 5)
               (string= a b))
      do (format out "\"~A\" -> \"~A\" [weight=~A label=\"~A\"];~%" a b v v))
  (princ "}" out))

;; Stolen from https://stackoverflow.com/questions/47875185/lisp-split-list-in-n-lists
;; Note: is a bit dodgy
(defun partition-list (list parts &key (last-part-longer nil))
  ;; Partition LIST into PARTS parts.  They will all be the same
  ;; length except the last one which will be shorter or, if
  ;; LAST-PART-LONGER is true, longer.  Doesn't deal with the case
  ;; where there are less than PARTS elements in LIST at all (it does
  ;; something, but it may not be sensible).
  (loop with size = (if last-part-longer
                        (floor (length list) parts)
                        (ceiling (length list) parts))
        and tail = list
        for part upfrom 1
        while tail
        collect (loop for pt on tail
                      for i upfrom 0
                      while (or (and last-part-longer (= part parts))
                                (< i size))
                      collect (first pt)
                      finally (setf tail pt))))

(defparameter *redis-port* 6379)
(defparameter *redis-host* "localhost")

(defun populate-track-links-from-pattern (pat &key match)
  (populate-track-links
   (mapcar #'princ-to-string (directory pat))
   :match match))

(defun write-track-links-graphviz-to-file (outfile line)
  (with-open-file (out outfile
                       :direction :output)
    (write-track-links-graphviz :out out :line line)))

(defclass train-history-entry ()
  ((universal-ts
    :initarg :universal-ts
    :reader universal-ts)))

(defclass train-identity-change (train-history-entry)
  ((wtt-id
    :initarg :wtt-id
    :reader wtt-id)
   (wtt-trip
    :initarg :wtt-trip
    :reader wtt-trip)))

(conspack:defencoding train-identity-change
  universal-ts wtt-id wtt-trip)

(defclass train-car-change (train-history-entry)
  ((new-car-no
    :initarg :new-car-no
    :reader new-car-no)))

(conspack:defencoding train-car-change
  universal-ts new-car-no)

(defmethod print-object ((obj train-car-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts new-car-no) obj
      (format stream "at ~A: new leading car ~A"
              (universal-time-railway universal-ts)
              new-car-no))))

;; FIXME(eta): lol
(defun universal-time-nonrailway-colon (time)
  (local-time:with-decoded-timestamp
      (:minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D:~2,'0D" h m)))

;; FIXME(eta): lol
(defun universal-time-nonrailway (time)
  (local-time:with-decoded-timestamp
      (:minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D~2,'0D" h m)))

(defun universal-time-railway (time)
  (local-time:with-decoded-timestamp
      (:sec s :minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D~2,'0D~A" h m
            (cond
              ((>= s 45) "¾")
              ((>= s 30) "½")
              ((>= s 15) "¼")
              (t "")))))

(defmethod print-object ((obj train-identity-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts wtt-id wtt-trip) obj
      (format stream "at ~A: WTT ~A×~A"
              (universal-time-railway universal-ts)
              (or wtt-id "?")
              (or wtt-trip "?")))))

(defclass train-destination-platform-change (train-history-entry)
  ((platform
    :initarg :platform
    :reader platform)))

(conspack:defencoding train-destination-platform-change
  universal-ts platform)

(defmethod print-object ((obj train-destination-platform-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts platform) obj
      (format stream "at ~A: destination platform ~A"
              (universal-time-railway universal-ts)
              platform))))

(defclass train-input-dest-change (train-history-entry)
  ((input-dest
    :initarg :input-dest
    :reader input-dest)))

(conspack:defencoding train-input-dest-change
  universal-ts input-dest)

(defmethod print-object ((obj train-input-dest-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts input-dest) obj
      (format stream "at ~A: input-dest ~A"
              (universal-time-railway universal-ts)
              input-dest))))

(defclass train-branch-change (train-history-entry)
  ((branch
    :initarg :branch
    :reader branch)))

(conspack:defencoding train-branch-change
  universal-ts branch)

(defmethod print-object ((obj train-branch-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts branch) obj
      (format stream "at ~A: branch ~A"
              (universal-time-railway universal-ts)
              branch))))

(defclass train-nr-schedule-change (train-history-entry)
  ((uid
    :initarg :uid
    :reader uid)
   (stp-indicator
    :initarg :stp-indicator
    :reader stp-indicator)
   (start-date
    :initarg :start-date
    :reader start-date)))

(conspack:defencoding train-nr-schedule-change
  universal-ts uid stp-indicator start-date)

(defmethod print-object ((obj train-nr-schedule-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts uid stp-indicator start-date) obj
      (format stream "at ~A: ~A (~A), starts ~A"
              (universal-time-railway universal-ts)
              uid
              stp-indicator
              start-date))))

(defclass train-trust-identity-change (train-history-entry)
  ((trust-id
    :initarg :trust-id
    :reader trust-id)
   (activation-data
    :initarg :activation-data
    :reader activation-data)))

(conspack:defencoding train-trust-identity-change
  universal-ts trust-id activation-data)

(defmethod print-object ((obj train-trust-identity-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts trust-id) obj
      (format stream "at ~A: TRUST ID ~A"
              (universal-time-railway universal-ts)
              trust-id))))

(defclass train-td-change (train-history-entry)
  ((td-area
    :initarg :td-area
    :reader td-area)))

(conspack:defencoding train-td-change
  universal-ts td-area)

(defmethod print-object ((obj train-td-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts td-area) obj
      (format stream "at ~A: TD area ~A"
              (universal-time-railway universal-ts)
              td-area))))

(defclass train-destination-change (train-history-entry)
  ((destination
    :initarg :destination
    :reader destination)))

(conspack:defencoding train-destination-change
  universal-ts destination)

(defmethod print-object ((obj train-destination-change) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts destination) obj
      (format stream "at ~A: destination ~A"
              (universal-time-railway universal-ts)
              destination))))

(defclass train-rescue (train-history-entry)
  ((new-key
    :initarg :new-key
    :reader new-key)))

(conspack:defencoding train-rescue
  universal-ts new-key)

(defmethod print-object ((obj train-rescue) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts new-key) obj
      (format stream "at ~A: new tid ~A"
              (universal-time-railway universal-ts)
              new-key))))

(defclass train-transit (train-history-entry)
  ((end-ts
    :initarg :end-ts
    :accessor end-ts)
   (track-codes
    :initarg :track-codes
    :initform '()
    :accessor track-codes)
   (description
    :initarg :description
    :initform nil
    :accessor description)))

(conspack:defencoding train-transit
  end-ts track-codes universal-ts description)

(defmethod print-object ((obj train-transit) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts end-ts track-codes description) obj
      (format stream "from ~A to ~A: ~A~@[ (\"~A\")~]"
              (universal-time-railway universal-ts)
              (universal-time-railway end-ts)
              (format-path (reverse track-codes))
              description))))

(defclass train-station-stop (train-transit)
  ((station-name
    :initarg :station-name
    :reader station-name)
   (platform
    :initarg :platform
    :reader platform)
   (ready-to-depart
    :initform nil
    :accessor ready-to-depart)))

(conspack:defencoding train-station-stop
  station-name platform end-ts track-codes universal-ts
  ready-to-depart)

(defmethod print-object ((obj train-station-stop) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (universal-ts end-ts track-codes station-name platform
                 ready-to-depart)
        obj
      (format stream "from ~A to ~A: ~As at ~A (plat ~A)~A, ~A"
              (universal-time-railway universal-ts)
              (universal-time-railway end-ts)
              (- end-ts universal-ts)
              station-name
              platform
              (if ready-to-depart
                  " RTD"
                  "")
              (format-path track-codes)))))

(defun extract-station-information (description)
  ;; XR SMART format
  (when (uiop:string-prefix-p "Arrived at " description)
    (cl-ppcre:register-groups-bind (station-name platform)
        ("Arrived at (.+) platform ([^ ]+)( .*)?" description)
      (return-from extract-station-information
        (values station-name platform)))
    (cl-ppcre:register-groups-bind (station-name extra)
        ("Arrived at (.+) \\((.+)\\)" description)
      (return-from extract-station-information
        (values station-name extra)))
    (cl-ppcre:register-groups-bind (station-name)
        ("Arrived at (.+)( (.+))?" description)
      (return-from extract-station-information
        (values station-name nil))))
  (when (uiop:string-prefix-p "At " description)
    (cl-ppcre:register-groups-bind (station-name platform)
        ("At (.+) Plat?form ([^ ]+)( .*)?" description)
      (return-from extract-station-information
        (values station-name platform)))
    ;; componentizer /A format
    (cl-ppcre:register-groups-bind (station-name pfm)
        ("At (.+) (/[A-Z])" description)
      (return-from extract-station-information
        (values station-name (format nil "T~A" pfm))))
    ;; componentizer single track code format
    (cl-ppcre:register-groups-bind (station-name pfm)
        ("At (.+) - ([A-Z0-9]+)" description)
      (return-from extract-station-information
        (values station-name pfm)))
    (values (subseq description #.(length "At ")) nil)))

(defun extract-inputdest-information (line-code input-dest)
  (cl-ppcre:register-groups-bind (code pfm branch)
      ;; SSR-style:
      ;; HMS3 (HMS3/OO/HMS3)
      ("^([A-Z]{3})(\\d+) \\([A-Z\\d]+\\/([A-Z\\-]+)\\/" input-dest)
    (let ((translated-code
            (red:get (format nil "code:~A:~A" line-code code)))
          (translated-branch
            (cond
              ;; Circle/H&C branches:
              ((and
                (string= line-code "H")
                (string= branch "OO"))
               "Circle")
              ((and
                (string= line-code "H")
                (string= branch "ALL"))
               "H&C")
              ((and
                (string= line-code "H")
                (string= branch "DO"))
               "Shuttle")
              ;; Met branches:
              ((and
                (string= line-code "M")
                (string= branch "ALL"))
               "All stations")
              ((and
                (string= line-code "M")
                (string= branch "M-W-NP-NPNN-M"))
               "All stations")
              ;; Semi-fast trains skip Northwick Park etc.
              ;; Fast trains skip Northwood etc.
              ((and
                (string= line-code "M")
                (string= branch "M-W-XX-NPNN-M"))
               "Semi-fast")
              ((and
                (string= line-code "M")
                (string= branch "M-W-XX-XXXX-M"))
               "Fast")
              ;; On the southbound, the fast types can also
              ;; skip Wembley Park.
              ((and
                (string= line-code "M")
                (string= branch "M-X-XX-NPNN-M"))
               "Semi-fast except Wembley")
              ((and
                (string= line-code "M")
                (string= branch "M-X-XX-XXXX-M"))
               "Fast except Wembley")
              ;; These ones end -X, indicating not via Moor Park.
              ((and
                (string= line-code "M")
                (string= branch "M-W-XX-XXXX-X"))
               "Fast")
              ((and
                (string= line-code "M")
                (string= branch "M-X-XX-XXXX-X"))
               "Fast except Wembley")
              ((and
                (string= line-code "M")
                (string= branch "M-W-NP-XXXX-X"))
               "All stations")
              (t nil))))
      (when (and translated-code translated-branch)
        (return-from extract-inputdest-information
          ;; I don't understand why the WHEN is needed but sure
          (values translated-code (when pfm (parse-integer pfm)) translated-branch)))))
  (cl-ppcre:register-groups-bind (code pfm branch)
      ("^([A-Z]{3})(\\d+)?(-[A-Z])?" input-dest)
    (let ((translated-code
            (red:get (format nil "code:~A:~A" line-code code)))
          (translated-branch
            (cond
              ((null branch) nil)
              ((string= branch "-X") "Charing Cross")
              ((string= branch "-Y") "Bank")
              (t (subseq branch 1)))))
      (when translated-code
        (values translated-code (when pfm (parse-integer pfm)) translated-branch)))))

(defclass majority-ballot ()
  ((inner
    :initform (make-hash-table :test 'equal)
    :accessor inner)))

(conspack:defencoding majority-ballot
  inner)

(defun add-majority-vote (ballot vote-key value)
  "Register a vote in BALLOT for VALUE, under the given VOTE-KEY. If a vote already exists with that VOTE-KEY, it is updated."
  (setf (gethash vote-key (inner ballot)) value))

(defun get-majority (ballot &key (sort-function #'string<))
  "Retrieves the value stored in BALLOT with the most votes. If there's a tie, uses the SORT-FUNCTION to choose a value among the list of values with the most votes."
  (let ((votes (make-hash-table :test #'equal)))
    (loop
      for value being the hash-values of (inner ballot)
      do (setf (gethash value votes)
               (1+ (or (gethash value votes) 0))))
    (let* ((max-vote-count
             (loop
               for vote-cnt being the hash-values of votes
               maximizing vote-cnt))
           (winners
             (loop
               for value being the hash-keys of votes
                 using (hash-value vote-cnt)
               when (eql vote-cnt max-vote-count)
                 collect value)))
      (first (sort winners sort-function)))))

(defvar *train-id-sequence-number* 0)

(defun generate-train-id ()
  (concatenate
   'string
   "train_"
   (string-right-trim
    '(#\=)
    (qbase64:encode-bytes
     (uuid:uuid-to-byte-array
      (uuid:make-v4-uuid))
     :scheme :uri))))

(defclass live-train ()
  ((unique-id
    :initform (generate-train-id)
    :initarg :unique-id
    :reader unique-id)
   (train-identity
    :initform nil
    :accessor train-identity)
   (leading-car-no
    :initform nil
    :accessor leading-car-no)
   (first-updated
    :initform nil
    :accessor first-updated)
   (last-updated
    :initform nil
    :accessor last-updated)
   (destination
    :initform nil
    :accessor destination)
   (input-dest
    :initform nil
    :accessor input-dest)
   (destination-platform
    :initform nil
    :accessor destination-platform)
   (branch
    :initform nil
    :accessor branch)
   (current-entry
    :initform nil
    :accessor current-entry)
   (trackernet-id
    :initarg :trackernet-id
    :accessor trackernet-id)
   (line-code
    :initarg :line-code
    :accessor line-code)
   (model-point
    :initform nil
    :accessor model-point)
   (history
    :initform nil
    :accessor history)
   (actually-live
    :initform t
    :initarg :actually-live
    :reader actually-live)
   (td-area
    :initform nil
    :accessor td-area)
   (trust-data
    :initform nil
    :accessor trust-data)
   (nr-schedule-data
    :initform nil
    :accessor nr-schedule-data)
   (proposed-destination
    :initform nil
    :accessor proposed-destination)))

(conspack:defencoding live-train
  unique-id train-identity first-updated last-updated destination current-entry
  trackernet-id line-code model-point history leading-car-no
  input-dest destination-platform branch td-area trust-data
  nr-schedule-data actually-live proposed-destination)

(defmethod print-object ((obj live-train) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (unique-id line-code first-updated last-updated destination current-entry trackernet-id history model-point leading-car-no) obj
      (format stream "~A… [~A/~A/~A] ~A to ~A (age ~As, ~A recs)~A state ~A"
              (subseq unique-id 0 #.(length "train_XXXX"))
              line-code
              trackernet-id
              leading-car-no
              (when first-updated (universal-time-railway first-updated))
              destination
              (when last-updated (- (get-universal-time) last-updated))
              (length history)
              (if model-point
                  " MODELPOINT"
                  "")
              current-entry))))

(defun persist-live-train (live-train)
  (setf (last-updated live-train) (get-universal-time))
  (log:debug "updated: ~A" live-train)
  (let ((ltid (format nil "lt:~A" (unique-id live-train))))
    (redis-raw-set-cpk ltid live-train)
    (red:expire ltid (if (string= (line-code live-train) "X")
                         *xr-live-train-expiry-secs*
                         *live-train-expiry-secs*))))

(defgeneric add-live-train-entry (live-train entry)
  (:documentation "Add the ENTRY to the list of entries for LIVE-TRAIN."))

(defmethod add-live-train-entry ((lt live-train) entry)
  (push entry (history lt)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-destination-change))
  (setf (destination lt) (destination entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-rescue))
  (setf (trackernet-id lt) (new-key entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-identity-change))
  (setf (train-identity lt) (list (wtt-id entry) (wtt-trip entry))))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-car-change))
  (setf (leading-car-no lt) (new-car-no entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-destination-platform-change))
  (setf (destination-platform lt) (platform entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-branch-change))
  (setf (branch lt) (branch entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-td-change))
  (setf (td-area lt) (td-area entry)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-trust-identity-change))
  (if (trust-id entry)
      (setf (trust-data lt) (list (trust-id entry)
                                  (activation-data entry)))
      (setf (trust-data lt) nil)))

(defmethod add-live-train-entry :after ((lt live-train) (entry train-nr-schedule-change))
  (if (uid entry)
      (setf (nr-schedule-data lt) (list (uid entry)
                                        (stp-indicator entry)
                                        (start-date entry)))
      (setf (nr-schedule-data lt) nil)))

(defmethod add-live-train-entry :after ((lt live-train) entry)
  (when (actually-live lt)
    (publish-firehose-v2-update lt :add
                                `((:event . ,entry)))
    (red:publish
     "firehose"
     (cpk-base64 (list (unique-id lt) :add entry)))
    (red:publish
     (format nil "lt-updates:~A" (unique-id lt))
     (cpk-base64 (cons :add entry)))))

(defgeneric exchange-current-entry (live-train new-entry)
  (:documentation "Set the current entry for LIVE-TRAIN to be NEW-ENTRY."))

(defmethod exchange-current-entry ((lt live-train) entry)
  (with-slots (current-entry) lt
    (unless (eql current-entry nil)
      (setf (end-ts current-entry) (universal-ts entry))
      (add-live-train-entry lt current-entry))
    (unless (first-updated lt)
      (setf (first-updated lt) (universal-ts entry)))
    (let ((model-point
            (toposort-model-point (line-code lt) (first (track-codes entry)))))
      (when (or model-point
                (string= (line-code lt) "X"))
        (setf (model-point lt) model-point)))
    (setf current-entry entry)))

(defmethod exchange-current-entry :after ((lt live-train) entry)
  (when (actually-live lt)
    (publish-firehose-v2-update lt :update
                                `((:event . ,entry)
                                  (:reason . "exchanged")
                                  (:model-point . ,(model-point lt))))
    (red:publish
     "firehose"
     (cpk-base64 (list (unique-id lt) :update entry)))
    (red:publish
     (format nil "lt-updates:~A" (unique-id lt))
     (cpk-base64 (cons :update entry)))))

(defgeneric update-current-entry (live-train visited-code universal-ts)
  (:documentation "Add the new track code VISITED-CODE to the list of codes inside the current entry, at the given UNIVERSAL-TS."))

(defmethod update-current-entry ((lt live-train) visited-code universal-ts)
  (with-slots (current-entry) lt
    (let ((model-point
            (toposort-model-point (line-code lt) visited-code)))
      (when (or model-point
                (string= (line-code lt) "X"))
        (setf (model-point lt) model-point)))
    (pushnew visited-code (track-codes current-entry)
             :test #'string=)
    (setf (end-ts current-entry) universal-ts)))

(defmethod update-current-entry :after ((lt live-train) visited-code universal-ts)
  (when (actually-live lt)
    (publish-firehose-v2-update lt :update
                                `((:event . ,(current-entry lt))
                                  (:reason . "move")
                                  (:model-point . ,(model-point lt))
                                  (:move-code . ,visited-code)
                                  (:move-ts . ,universal-ts)))
    (red:publish
     "firehose"
     (cpk-base64 (list (unique-id lt) :update (current-entry lt))))
    (red:publish
     (format nil "lt-updates:~A" (unique-id lt))
     (cpk-base64 (cons :update (current-entry lt))))))

(defgeneric set-rtdi (live-train readyp)
  (:documentation "Indicate that the provided LIVE-TRAIN is ready to depart (or not) from its current location."))

(defmethod set-rtdi ((lt live-train) readyp)
  (with-slots (current-entry) lt
    (when (typep current-entry 'train-station-stop)
      (setf (ready-to-depart current-entry) readyp))))

(defmethod set-rtdi :after ((lt live-train) readyp)
  (declare (ignore readyp))
  (when (actually-live lt)
    (publish-firehose-v2-update lt :update
                                `((:event . ,(current-entry lt))
                                  (:reason . "ready-to-depart")))
    (red:publish
     "firehose"
     (cpk-base64 (list (unique-id lt) :update (current-entry lt))))
    (red:publish
     (format nil "lt-updates:~A" (unique-id lt))
     (cpk-base64 (cons :update (current-entry lt))))))

(defun maybe-update-trust-data (live-train headcode universal-ts)
  "Possibly update LIVE-TRAIN with the TRUST activation data that might exist for HEADCODE."
  (let* ((trust-data (cpk-unbase64 (red:get (format nil "trust-used:~A" headcode))))
         (trust-id (cdr (assoc :trust-id trust-data)))
         (uid (cdr (assoc :schedule-id trust-data)))
         (stp-indicator (cdr (assoc :schedule-type trust-data)))
         (start-date (cdr (assoc :schedule-start-date trust-data))))
    (unless (equal (car (trust-data live-train)) trust-id)
      (add-live-train-entry
       live-train
       (make-instance 'train-trust-identity-change
                      :trust-id trust-id
                      :universal-ts universal-ts
                      :activation-data trust-data)))
    (unless (equal (car (nr-schedule-data live-train)) uid)
      (add-live-train-entry
       live-train
       (make-instance 'train-nr-schedule-change
                      :uid uid
                      :stp-indicator stp-indicator
                      :universal-ts universal-ts
                      :start-date start-date)))))

(defun add-live-train-observation (live-train universal-ts observer data)
  "Update LIVE-TRAIN with the DATA (a TRACKERNET-TRAIN)."
  (check-type live-train live-train)
  (check-type universal-ts integer)
  (check-type observer string)

  (with-slots (line-code current-entry input-dest history proposed-destination) live-train
    (when (and current-entry
               (>= (universal-ts current-entry) universal-ts))
      (log:debug "Rejecting skip back in time (~As): ~A"
                 (- universal-ts (universal-ts current-entry)) data)
      (return-from add-live-train-observation))
    (let* ((identity (list (nilify-zeros (set-no data))
                           (nilify-zeros (trip-no data))))
           (lcid (nilify-zeros (lcid data)))
           (tid (or lcid
                    (when (and
                           (string= line-code "M")
                           (nilify-zeros (set-no data)))
                      (format nil "set~A" (set-no data)))
                    (when (string= line-code "X") (train-id data))
                    (format nil "tid~A" (train-id data))))
           (descr (or
                   ;; Speed up historical train processing massively
                   ;; by not requiring a bajillion roundtrips
                   (when t ; (actually-live live-train) FIXME(eta): causes regression
                     (cdr (assoc (track-code data)
                                 (cached-track-descriptions line-code)
                                 :test (function equal))))
                   (when t ; (actually-live live-train) FIXME(eta): see above
                     (red:get (format nil "~A-track-desc-~A"
                                      line-code (track-code data))))
                   (location-desc data)))
           (destination (or (destination-desc data) nil))
           (car-no (nilify-zeros (leading-car-no data)))
           (should-disqualify-destination nil))
      (when (string= line-code "X")
        (unless (or
                 (equal (td-area live-train) "Q0")
                 (typep (first history) 'train-td-change))
          (add-live-train-entry
           live-train
           (make-instance 'train-td-change
                          :td-area "Q0"
                          :universal-ts universal-ts))))
      (unless (equal tid (trackernet-id live-train))
        (add-live-train-entry
         live-train
         (make-instance 'train-rescue
                        :new-key tid
                        :universal-ts universal-ts))
        (when (and (string= line-code "X")
                   (actually-live live-train))
          (maybe-update-trust-data live-train tid universal-ts)))
      (unless (equal identity (train-identity live-train))
        (add-live-train-entry
         live-train
         (make-instance 'train-identity-change
                        :wtt-id (first identity)
                        :wtt-trip (second identity)
                        :universal-ts universal-ts)))
      (unless (equal car-no (leading-car-no live-train))
        (add-live-train-entry
         live-train
         (make-instance 'train-car-change
                        :new-car-no car-no
                        :universal-ts universal-ts)))
      ;; input-dest field stores (input-dest parsed-successfully-p)
      (unless (equal (first input-dest) (input-dest data))
        (multiple-value-bind (station pfm branch)
            (extract-inputdest-information line-code (input-dest data))
          ;; FIXME(eta): this should ideally use the event mechanism
          (setf input-dest (list (input-dest data) (when station t)))
          (add-live-train-entry
           live-train
           (make-instance 'train-input-dest-change
                          :input-dest (input-dest data)
                          :universal-ts universal-ts))
          (when (and
                 station
                 (not (equal station (destination live-train))))
            (add-live-train-entry
             live-train
             (make-instance 'train-destination-change
                            :destination station
                            :universal-ts universal-ts)))
          (unless (equal pfm (destination-platform live-train))
            (add-live-train-entry
             live-train
             (make-instance 'train-destination-platform-change
                            :platform pfm
                            :universal-ts universal-ts)))
          (unless (equal branch (branch live-train))
            (add-live-train-entry
             live-train
             (make-instance 'train-branch-change
                            :branch branch
                            :universal-ts universal-ts)))))
      ;; If the destinations don't match, and we're not using input-dest,
      ;; we use the 'proposed-destination' to help prevent flapping.
      ;; When something wants to change the destination, we require another
      ;; observer to confirm the change first.
      (when (and (not (second input-dest))
                 (not (equal destination (destination live-train))))
        (cond
          ((equal line-code "X")
           ;; allow the destination to change immediately
           )
          ;; There isn't a proposal, or it's different from ours.
          ((or
            (not proposed-destination)
            (not (equal (car proposed-destination) destination)))
           (setf proposed-destination (list destination observer))
           (setf should-disqualify-destination t))
          ;; There is a proposal, and it's confirmed by someone else;
          ;; use it!
          ((and
            (equal (car proposed-destination) destination)
            (not (equal (cadr proposed-destination) observer)))
           ;; success case (this logic is bad isn't it)
           )
          ;; Any other case is a fail.
          (t
           (setf should-disqualify-destination t))))
      ;; Crap destination fallback:
      (unless (or
               ;; We've already updated the destination with an input-dest
               (second input-dest)
               ;; We just did a destination change, and another one would be spammy.
               (typep (first history) 'train-destination-change)
               ;; We agreed to not honour this destination change above
               should-disqualify-destination
               ;; The destination was already updated
               (equal destination (destination live-train)))
        (add-live-train-entry
         live-train
         (make-instance 'train-destination-change
                        :destination destination
                        :universal-ts universal-ts))
        (when (destination-platform live-train)
          (add-live-train-entry
           live-train
           (make-instance 'train-destination-platform-change
                          :platform nil
                          :universal-ts universal-ts)))
        (when (branch live-train)
          (add-live-train-entry
           live-train
           (make-instance 'train-branch-change
                          :branch nil
                          :universal-ts universal-ts))))
      (multiple-value-bind (station-name platform)
          (extract-station-information descr)
        (when (and platform
                   (string= line-code "X"))
          ;; strip off (E) or (W) suffix
          (setf platform (subseq platform 0 1)))
        (unless (and current-entry
                     (eql (when station-name t)
                          (typep current-entry 'train-station-stop))
                     (if (and station-name
                              (typep current-entry 'train-station-stop))
                         (string= (station-name current-entry)
                                  station-name)
                         t))
          (when (and station-name (actually-live live-train))
            (tph-register-station-call line-code station-name platform
                                       (unique-id live-train) universal-ts))
          (exchange-current-entry
           live-train
           (if station-name
               (make-instance 'train-station-stop
                              :station-name station-name
                              :platform platform
                              :universal-ts universal-ts
                              :end-ts universal-ts)
               (make-instance 'train-transit
                              :universal-ts universal-ts
                              :end-ts universal-ts))))
        (update-current-entry live-train (track-code data) universal-ts)))))

(defun track-descriptions-by-description (line-code)
  (let ((ret (make-hash-table :test 'equal))
        (descs (line-track-descriptions line-code)))
    (dolist (pair descs)
      (unless (gethash (cdr pair) ret)
        (setf (gethash (cdr pair) ret) nil))
      (pushnew (car pair) (gethash (cdr pair) ret)
               :test #'equal))
    ret))

(defun get-link-distance (n1 n2 graph-ht)
  (dolist (other (gethash n1 graph-ht))
    (when (equal (car other) n2)
      (return-from get-link-distance (cadr other))))
  nil)

(defun track-graphs-by-description (line-code)
  (let ((descs (track-descriptions-by-description line-code))
        (links (gethash line-code *per-line-track-graph*)))
    (loop
      for (desc-name . codes) in (alexandria:hash-table-alist descs)
      collect (cons
               desc-name
               (mapcar
                (lambda (code)
                  (cons code
                        (mapcar
                         #'car
                         (remove-if-not
                          (lambda (other)
                            (member (car other) codes :test #'string=))
                          (gethash code links)))))
                codes)))))

(defun track-graphs-by-description-2 (line-code &key (cutoff 100))
  (format *debug-io* "<b>Using link score cutoff ~A</b>~%" cutoff)
  (let ((descs (track-descriptions-by-description line-code))
        (links (mapcar #'car
                       (remove-if-not
                        (lambda (link-score)
                          (> (cdr link-score) cutoff))
                        (alexandria:hash-table-alist *track-links-set*)))))
    (loop
      for (desc-name . codes) in (alexandria:hash-table-alist descs)
      collect (cons
               ;; HACK(eta): fixes curly quote in Earl's Court
               (substitute #\' #\right_single_quotation_mark
                           desc-name)
               (mapcar
                (lambda (code)
                  (cons code
                        (mapcar #'cadr
                                (remove-if-not
                                 (lambda (link)
                                   (and
                                    (string= (car link) code)
                                    (member (cadr link) codes :test #'string=)))
                                 links))))
                codes)))))

(defun alist-flatten (alist)
  "Flatten an alist of (a . b) (a . c) (d . e) into (a c b) (d e)."
  (let ((ret (make-hash-table :test 'equal)))
    (loop
      for (k . v) in alist
      do (alexandria:ensure-gethash k ret)
      do (pushnew v (gethash k ret)
                  :test #'equal))
    ret))

(defun toposort-kahn (graph)
  (let* ((non-start-nodes
           (alexandria:mappend #'cdr graph))
         (incoming-edges
           (alist-flatten
            (alexandria:mappend
             (lambda (ent)
               (mapcar
                (lambda (node) (cons node (car ent)))
                (cdr ent)))
             graph)))
         (start-nodes
           (mapcar #'car
                   (remove-if
                    (lambda (ent)
                      (member (car ent) non-start-nodes
                              :test #'equal))
                    graph)))
         (ret '())
         (edges (alexandria:alist-hash-table graph
                                             :test 'equal)))
    ;;(format t "nsn ~A~%ie ~A~%sn ~A~%graph ~A~%"
    ;;        non-start-nodes
    ;;        (alexandria:hash-table-alist incoming-edges)
    ;;        start-nodes
    ;;        graph)
    (tagbody
     :start
       (when (endp start-nodes)
         (go :end))
       (let ((cur-node (car start-nodes)))
         (push cur-node ret)
         (dolist (other-node (gethash cur-node edges))
           ;; (format t "~A -> ~A; ~A~%" cur-node other-node (gethash other-node incoming-edges))
           (when (endp
                  (setf (gethash other-node incoming-edges)
                        (remove cur-node
                                (gethash other-node incoming-edges)
                                :test #'equal)))
             ;;(format t "rplacd~%")
             (rplacd (last start-nodes) (cons other-node nil))))
         (remhash cur-node edges)
         (setf start-nodes (cdr start-nodes))
         (go :start))
     :end)
    (if (find-if-not (lambda (x) (endp (cdr x)))
                     (alexandria:hash-table-alist incoming-edges))
        (progn
          (with-simple-restart (continue "Continue anyway.")
            (error "Graph ~A contains cycles:~%   ~A" graph
                   (alexandria:hash-table-alist incoming-edges)))
          (reverse ret))
        (reverse ret))))

(defun any-graph-cycle (graph)
  "If GRAPH contains any cycles, returns all nodes in an arbitrarily chosen cycle of GRAPH."
  (let ((sccs (tarjan graph)))
    (find-if
     (lambda (component)
       (> (length component) 1))
     sccs)))

(defun zero-to-most-positive (x)
  (if (eql x 0)
      most-positive-fixnum
      x))

(defun breaking-toposort-kahn (graph links-hashtable &key name)
  (loop
    (alexandria:when-let ((ret
                           (ignore-errors
                            (toposort-kahn graph))))
      (return-from breaking-toposort-kahn ret))
    (let* ((sccs (tarjan (alexandria:alist-hash-table graph
                                                      :test 'equal)))
           (cycle-members
             (alexandria:mappend
              (lambda (scc)
                (when (> (length scc) 1)
                  scc))
              sccs))
           (minimum-distance
             (loop
               for (node . others) in graph
               minimizing
               (zero-to-most-positive
                (loop
                  for other in others
                  when (member other cycle-members :test #'equal)
                    minimizing (gethash (list node other) links-hashtable)))))
           (already-removed nil))
      (when name
        (format *debug-io* "<b>While toposorting '<i>~A</i>':</b>~%" name))
      (format *debug-io* "tarjan returned cycle members: ~A~%" cycle-members)
      (format *debug-io* "removing links with dist ~A~%" minimum-distance)
      (setf graph
            (mapcar
             (lambda (entry)
               (cons
                (car entry)
                (remove-if
                 (lambda (other)
                   (let ((ret
                           (and
                            (member other cycle-members :test #'equal)
                            (not already-removed)
                            (eql (gethash (list (car entry) other) links-hashtable)
                                 minimum-distance))))
                     (when ret
                       (format *debug-io* "removing ~A -> ~A~%"
                               (car entry) other)
                       (setf already-removed t)
                       ret)))
                 (cdr entry))))
             graph))
      (unless already-removed
        (error "Didn't remove any cycles where we expected to")))))

(defun toposort-graphs (graphs)
  (mapcar
   (lambda (codes)
     (cons (car codes)
           (or
            (ignore-errors (toposort-kahn (cdr codes)))
            (progn
              (warn "failed to toposort ~A" codes)
              (sort (mapcar #'car (cdr codes)) #'string<)))))
   graphs))

(defun breaking-toposort-graphs (graphs links-ht)
  (format *debug-io* "<center><b>breaking-toposort-graphs</b></center>~%")
  (mapcar
   (lambda (codes)
     (cons (car codes)
           (breaking-toposort-kahn (cdr codes) links-ht
                                   :name (car codes))))
   graphs))

(defun track-toposorts-by-description (line-code)
  (toposort-graphs
   (track-graphs-by-description line-code)))

(defun track-description-links (line-code)
  (let ((ret (make-hash-table :test 'equal))
        (descrs (line-track-descriptions line-code))
        (links (gethash line-code *per-line-track-graph*))
        (descrs-by-descr (track-descriptions-by-description line-code)))
    (loop
      for descr being the hash-keys of descrs-by-descr
        using (hash-value codes)
      do (alexandria:ensure-gethash descr ret)
      do (dolist (code codes)
           (dolist (other (gethash code links))
             (let ((other-descr
                     (cdr (assoc (car other) descrs
                                 :test #'equal))))
               (when (and other-descr
                          (not (equal other-descr descr)))
                 (pushnew
                  other-descr
                  (gethash descr ret)
                  :test #'equal))))))
    ret))

(defun bidirectionalize (graph)
  (alist-flatten
   (alexandria:mappend
    (lambda (entry)
      (alexandria:mappend
       (lambda (other-node)
         (list
          (cons (car entry) other-node)
          (cons other-node (car entry))))
       entry))
    graph)))

(defun hash-table-arbitrary-key (ht)
  (catch 'result
    (maphash (lambda (k v)
               (declare (ignore v))
               (throw 'result k))
             ht)))

(defun discover-one-component (bidi-graph)
  (let ((visited-set (make-hash-table :test 'equal))
        (queue (list (hash-table-arbitrary-key bidi-graph))))
    (loop
      for head = (pop queue)
      while head
      do (alexandria:ensure-gethash head visited-set t)
      do (dolist (neighbour (gethash head bidi-graph))
           (unless (gethash neighbour visited-set)
             (push neighbour queue))))
    (alexandria:hash-table-keys visited-set)))

(defun separate-components (graph)
  (loop
    while graph
    for component-keys = (discover-one-component
                          (bidirectionalize graph))
    collect (remove-if-not
             (lambda (ent)
               (member (car ent) component-keys :test #'equal))
             graph)
    do (setf graph
             (remove-if
              (lambda (ent)
                (member (car ent) component-keys :test #'equal))
              graph))))

(defun split-graphs-into-components (named-graphs)
  (alexandria:mappend
   (lambda (ent)
     (let* ((components (separate-components (cdr ent)))
            (num-components (length components))
            (letter (1- (char-code #\A))))
       (mapcar
        (lambda (component)
          (cons (cond
                  ((eql num-components 1)
                   (car ent))
                  ((eql (length component) 1)
                   (format nil "~A - ~A"
                           (car ent)
                           (caar component)))
                  (t
                   (format nil "~A /~A"
                           (car ent)
                           (code-char (incf letter)))))
                component))
        components)))
   named-graphs))

(defun inter-graph-links (named-graphs line-code)
  (let ((ret (make-hash-table :test 'equal))
        (descrs
          (alist-flatten
           (alexandria:mappend
            (lambda (named-graph)
              (mapcar
               (lambda (graph-ent)
                 (cons (car graph-ent) (car named-graph)))
               (cdr named-graph)))
            named-graphs)))
        (links (gethash line-code *per-line-track-graph*)))
    (loop
      for (name . graph) in named-graphs
      do (alexandria:ensure-gethash name ret)
      do (dolist (code (apply #'append graph))
           ;; (format t "~A ~A -> ~A~%" name code (gethash code links))
           (dolist (other (gethash code links))
             (let* ((other (car other))
                    (other-descr
                      (car (gethash other descrs))))
               (when (and other-descr
                          (not (equal other-descr name)))
                 (pushnew
                  other-descr
                  (gethash name ret)
                  :test #'equal))))))
    ret))

(defun inter-toposorts-links (toposorts &key (cutoff 100))
  (let ((ret (make-hash-table :test 'equal))
        (descrs
          (alist-flatten
           (alexandria:mappend
            (lambda (toposort)
              (mapcar
               (lambda (code)
                 (cons code (car toposort)))
               (cdr toposort)))
            toposorts)))
        (links (mapcar #'car
                       (remove-if-not
                        (lambda (link-score)
                          (> (cdr link-score) cutoff))
                        (alexandria:hash-table-alist *track-links-set*)))))
    (loop
      for (name . codes) in toposorts
      do (alexandria:ensure-gethash name ret)
      when (and (gethash name descrs)
                (not (string= (gethash name descrs) name)))
        do (error "~A is ~A in descrs!" name (gethash name descrs))
      do (dolist (code codes)
           ;; (format t "~A ~A -> ~A~%" name code (gethash code links))
           (dolist (other
                    (mapcar #'cadr
                            (remove-if-not
                             (lambda (link)
                               (string= (car link) code))
                             links)))
             (let* ((other-descr
                      (car (gethash other descrs))))
               (when (and other-descr
                          (not (equal other-descr name)))
                 ;;(format *debug-io* "\"~A\" (~A) -> \"~A\" (~A) with score ~A~%"
                 ;;        name code other-descr other
                 ;;        (gethash (list code other) *track-links-set*))
                 (pushnew
                  other-descr
                  (gethash name ret)
                  :test #'equal
                  ))))))
    ret))

(defun sum-links-between (set-1 set-2)
  "Returns the sum of all links between SET-1 and SET-2."
  (let ((ret 0))
    (dolist (a set-1)
      (dolist (b set-2)
        (incf ret (or (gethash (list a b) *track-links-set*) 0))))
    ret))

(defun hex-sxhash (obj)
  "Returns the value of (SXHASH OBJ), formatted as a hexadecimal string."
  (write-to-string (sxhash obj)
                   :base 16))

(defun default-links-graphviz-labeller (text)
  "Labels nodes by wrapping and left-aligning them."
  (format nil "[label=<<FONT POINT-SIZE=\"10\">~{~A~^<BR ALIGN=\"LEFT\"/>~}</FONT>>]"
          (bobbin:wrap (list (substitute #\+ #\& text)) 30)))

(defun links-graphviz (links-alist &optional (out *standard-output*) (labeller #'default-links-graphviz-labeller))
  (princ "digraph {" out)
  (terpri out)
  (format out "node [shape=box margin=\"0.1,0.1\"];
layout = \"neato\";")
  (terpri out)
  (loop
    for head in (remove-duplicates (apply #'append links-alist) :test #'equal)
    when (find-if (lambda (ent)
                    (or
                     (and
                      (string= (car ent) head)
                      (cdr ent))
                     (and
                      (cdr ent)
                      (position head ent :test #'string=))))
                    links-alist)
    do (format out "\"~A\" ~A;~%"
               (hex-sxhash head)
               (funcall labeller head)))
  (loop
    for (head . rest) in links-alist
    do (dolist (other rest)
         (format out "\"~A\" -> \"~A\";~%"
                 (hex-sxhash head)
                 (hex-sxhash other))))
  (princ "}" out)
  (terpri out))

(defun write-links-graphviz-to-file (links-alist outfile)
  (with-open-file (out outfile
                       :direction :output)
    (links-graphviz links-alist out)))

(define-condition new-scc ()
  ((scc
    :initarg :scc
    :accessor scc)))

(defstruct tarjan-data
  metadata ; plist containing node metadata (:index / :lowlink / :on-stack)
  index ; current index
  stack ; current recursion stack
  links) ; graph

(defun tarjan-strongconnect (vertex data)
  (macrolet
      ((prop (vertex prop)
         `(getf (gethash ,vertex (tarjan-data-metadata data)) ,prop)))
    (setf (prop vertex :index) (tarjan-data-index data))
    (setf (prop vertex :lowlink) (tarjan-data-index data))
    (incf (tarjan-data-index data))
    (push vertex (tarjan-data-stack data))
    (setf (prop vertex :on-stack) t)
    (dolist (other (gethash vertex (tarjan-data-links data)))
      (cond
        ((not (prop other :index))
         (tarjan-strongconnect other data)
         (setf (prop vertex :lowlink)
               (min
                (prop vertex :lowlink)
                (prop other :lowlink))))
        ((prop other :on-stack)
         (setf (prop vertex :lowlink)
               (min
                (prop vertex :lowlink)
                (prop other :index))))))
    (when (eql
           (prop vertex :lowlink)
           (prop vertex :index))
      (signal 'new-scc
              :scc
              (loop
                for w = (pop (tarjan-data-stack data))
                do (setf (prop w :on-stack) nil)
                collect w
                while (not (equal w vertex)))))))

(defun tarjan (graph)
  (let ((ret '())
        (data (make-tarjan-data
               :metadata (make-hash-table :test 'equal)
               :index 0
               :stack '()
               :links graph)))
    (handler-bind
        ((new-scc
           (lambda (c)
             (push (scc c) ret))))
      (dolist (vertex (alexandria:hash-table-keys graph))
        (unless (getf (gethash vertex (tarjan-data-metadata data)) :index)
          (tarjan-strongconnect vertex data))))
    ret))

(defun generate-componentized-toposorts-and-links (track-code)
  (let* ((graphs (track-graphs-by-description track-code))
         (componentized (split-graphs-into-components graphs))
         (toposorted (breaking-toposort-graphs
                      componentized
                      (gethash track-code *per-line-track-graph*)))
         (linked (inter-toposorts-links toposorted)))
    (values
     (alexandria:alist-hash-table toposorted :test 'equal)
     linked)))

;;(red:publish "new-observations"
;;             (cpk-base64
;;              (list key universal-ts code-station train)))

(defun assert-is-train (x)
  ;; HACK(eta): whoopsie daisy
  (when (and x (not (slot-boundp x 'actually-live)))
    (log:warn "fixing up unbound actually-live slot!")
    (setf (slot-value x 'actually-live) t))
  (when x
    (return-from assert-is-train x))
  (error "No live train found!"))

(defun get-or-create-live-train (trackernet-id latest-record)
  "Returns the live train with the given TRACKERNET-ID; if one does not yet exist, creates it, backfilling all data before LATEST-RECORD."
  (let ((maybe-lt (red:get (format nil "ltmap:~A" trackernet-id))))
    (handler-case
        (when maybe-lt
          (return-from get-or-create-live-train
            (assert-is-train (redis-raw-get-cpk maybe-lt))))
      (serious-condition (c)
        (log:warn "failed to get live train for ~A (map ~A); recreating! err ~A"
                  trackernet-id maybe-lt c)))
    (let ((new-lt (make-instance 'live-train
                                 :line-code (subseq trackernet-id 0 1)
                                 :trackernet-id trackernet-id))
          (start-time (get-internal-real-time))
          (trn-data (redis-cpk-sorted-set-all trackernet-id)))
      (dolist (entry trn-data)
        (destructuring-bind (universal-ts observer train) entry
          (when (< universal-ts latest-record)
            (add-live-train-observation new-lt universal-ts observer train))))
      (persist-live-train new-lt)
      (red:set (format nil "ltmap:~A" trackernet-id)
               (format nil "lt:~A" (unique-id new-lt)))
      (statsd-inc "intertube.new-live")
      (let* ((end-time (get-internal-real-time))
             (end-time-ms (*
                           (/ (- end-time start-time)
                              internal-time-units-per-second)
                           1000)))
        (red:publish
         "firehose"
         (cpk-base64 (list (unique-id new-lt) :create)))
        (publish-firehose-v2-update new-lt :create
                                    `((:object . ,new-lt)))
        (log:info "Made new live train ~A for ~A (~A backfilled in ~,1Fms)"
                  (unique-id new-lt) trackernet-id (length trn-data) end-time-ms))
      new-lt)))

(defun add-live-observation (trackernet-id universal-ts observer train)
  (let ((lt
          (get-or-create-live-train trackernet-id universal-ts)))
    ;; HACK(eta): This is truly horrible as a way of setting these apart ;w;
    (if (position #\· (track-code train))
        (add-smart-live-train-observation lt universal-ts observer train)
        (add-live-train-observation lt universal-ts observer train))
    (persist-live-train lt)
    (statsd-inc "intertube.updated-live")))

(defun flush-live-trains ()
  (mapc #'red:del (get-all "lt:*")))

(defun incoming-node-with-smallest-weight (node edges weights)
  "Find the node incoming to NODE with the smallest weight in the provided graph, using the provided WEIGHTS hash table to judge the weight of an edge."
  (let ((current-weight most-negative-double-float)
        (current-node))
    (dolist (edge edges)
      (when (and
             (equal (cdr edge) node)
             (> (gethash edge weights) current-weight))
        (setf current-weight (gethash edge weights))
        (setf current-node (car edge))))
    current-node))

(defun generate-smallest-weights-list (edges weights)
  (let ((ret (make-hash-table :test #'equal)))
    (dolist (edge edges)
      (destructuring-bind (u . v) edge
        (symbol-macrolet
            ((entry (gethash v ret)))
          (when (or (not entry)
                    (> (gethash edge weights) (cdr entry)))
            (setf entry (cons u (gethash edge weights)))))))
    (loop
      for k being the hash-keys of ret
      do (setf (gethash k ret) (car (gethash k ret))))
    ret))

(defun edmonds-cycle-generate-edge (edge cycle weights new-node swl)
  "Generate a new edge for EDGE that contracts the nodes in CYCLE into NEW-NODE, using the provided WEIGHTS.
Returns (a) the new edge, (b) the weight of the new edge."
  (destructuring-bind (u . v) edge
    (let ((u-member (member u cycle :test #'equal))
          (v-member (member v cycle :test #'equal)))
      (cond
        ((and (not u-member) v-member)
         ;; an edge coming into the cycle
         (values
          (cons u new-node) ; contract into (source, new-node)
          (- (gethash edge weights)
              (gethash
               (cons (gethash v swl)
                     v)
               weights))))
        ((and u-member (not v-member))
         ;; an edge going away from the cycle
         (values
          (cons new-node v) ; contract into (new-node, dest)
          (gethash edge weights)))
        ((and (not u-member) (not v-member))
         ;; an edge unrelated to the cycle
         (values
          edge
          (gethash edge weights)))
        (t
         ;; an edge that's part of the cycle, discard
         nil)))))

(defun edmonds-cycle-contract (nodes edges cycle weights swl)
  "Contract the digraph with NODES and EDGES such that the nodes making up the CYCLE form one new node (in line with Edmonds' algorithm).
Return four values: a set of nodes, edges, weights, and a new->old edge mapping."
  (format t "  contracting~%")
  (let* ((new-node-name (symbol-name (gensym "contract")))
         (new-weights (make-hash-table :test 'equal))
         (new-nodes
           (remove-if
            (lambda (node)
              (member node cycle :test #'equal))
            (cons new-node-name nodes)))
         (new-edges '())
         (edge-mappings (make-hash-table :test 'equal)))
    (dolist (edge edges)
      (multiple-value-bind (new-edge new-weight)
          (edmonds-cycle-generate-edge edge cycle weights new-node-name swl)
        (when new-edge
          (push new-edge new-edges)
          (setf (gethash new-edge edge-mappings) edge)
          (setf (gethash new-edge new-weights) new-weight))))
      (values
       new-nodes
       new-edges
       new-weights
       edge-mappings
       new-node-name)))

(defun edmonds (nodes edges root weights)
  "Find a spanning arborescence of minimum weight in the digraph with NODES and EDGES, with the provided ROOT."
  (format *debug-io* "* edmonds with ~A nodes / ~A edges~%" (length nodes) (length edges))
  (let* ((edges ; (without root)
          (remove-if
           (lambda (edge)
             (equal (cdr edge) root))
           edges))
         (swl (generate-smallest-weights-list edges weights))
         ;; Greedily attempt to build a graph P where each node's edge
         ;; is determined by INCOMING-NODE-WITH-SMALLEST-WEIGHT
         ;; (discarding nodes with no incoming node).
         (p-edges
           (delete-if
            (lambda (edge)
              (not (car edge)))
            (mapcar
             (lambda (node)
               (cons
                (gethash node swl)
                node))
             nodes)))
         ;; Check whether the graph P contains a cycle (if not, it's an
         ;; arborescence!)
         (first-p-graph-cycle
           (any-graph-cycle
            (alist-flatten p-edges)))
         ;; If there is a cycle, find all the edges in it.
         (cycle-edges
           (when first-p-graph-cycle
             (remove-if-not
              (lambda (edge)
                (and (member (car edge) first-p-graph-cycle
                             :test #'equal)
                     (member (cdr edge) first-p-graph-cycle
                             :test #'equal)))
              p-edges))))
    (if first-p-graph-cycle
        ;; We got a cycle. We now "contract" the original graph by replacing
        ;; all nodes in the cycle with one new imaginary node, find an
        ;; arborescence in that new graph, and then "expand" the nodes we
        ;; removed back into the arborescence we found.
        (multiple-value-bind
              (new-nodes new-edges new-weights edge-mappings new-node-name)
            (edmonds-cycle-contract nodes edges first-p-graph-cycle weights swl)
          ;; Now that the cycle is removed, find an arborescence for this
          ;; new graph (recursive call!).
          (let* ((arborescence (edmonds new-nodes new-edges root new-weights))
                 ;; Find the unique incoming edge to the new node that the
                 ;; cycle was contracted down to...
                 (new-node-edge (find-if
                                 (lambda (node)
                                   (equal (cdr node) new-node-name))
                                 arborescence))
                 ;; ...and find which edge that corresponds to in the original
                 ;; graph.
                 (corresponding-edge (gethash new-node-edge edge-mappings))
                 ;; This is the clever bit!
                 ;; In the original graph, CORRESPONDING-EDGE (which is in
                 ;; that graph, and not graph P) points at a given node X.
                 (node-x (cdr corresponding-edge))
                 ;; This node X is part of the CYCLE (in graph P) that we're
                 ;; trying to "expand" into our ARBORESCENCE by including
                 ;; all its nodes -- but we can't introduce a cycle into
                 ;; the arborescence! We must break one edge to break the
                 ;; cycle.
                 ;;
                 ;; So, we choose the edge in the CYCLE that points at node
                 ;; X (which isn't CORRESPONDING-EDGE) to break, and then
                 ;; include all of the edges & nodes in that cycle in the
                 ;; arborescence.
                 (cycle-edges-without-node-x
                   (remove-if
                    (lambda (edge)
                      (equal (cdr edge) node-x))
                    cycle-edges))
                 ;; We translate each arborescence edge into its original
                 ;; edge before the contraction...
                 (arborescence-expanded
                   (mapcar
                    (lambda (edge)
                      (gethash edge edge-mappings))
                    arborescence)))
            ;; ...and now, we can combine the ARBORESCENCE-EXPANDED with our
            ;; CYCLE-EDGES-WITHOUT-NODE-X to get an arborescence in the
            ;; original graph!
            (append arborescence-expanded cycle-edges-without-node-x)))
        ;; No cycle, return the arborescence.
        p-edges)))

(defun nodes-edges-weights (simple-edge-descriptions)
  "Translates SIMPLE-EDGE-DESCRIPTIONS, a list of lists of the form (NODE-A NODE-B WEIGHT) into a list of NODES, EDGES, and a hashmap of WEIGHTS for each edge, returned as multiple values."
  (let ((nodes
          (delete-duplicates
           (alexandria:mappend
            (lambda (descr)
              (list (first descr) (second descr)))
            simple-edge-descriptions)
           :test #'equal))
        (edges '())
        (weights (make-hash-table :test #'equal)))
    (dolist (descr simple-edge-descriptions)
      (destructuring-bind (u v weight) descr
        (push (cons u v) edges)
        (setf (gethash (cons u v) weights) weight)))
    (values nodes edges weights)))

(defun nodes-edges-weights-2 (track-graph)
  (let ((nodes)
        (edges)
        (weights (make-hash-table :test #'equal)))
    (dolist (node-data track-graph)
      (push (car node-data) nodes)
      (dolist (link-data (cdr node-data))
        (let ((link (cons (car node-data) (car link-data))))
          (push link edges)
          (setf (gethash link weights) (second link-data)))))
    (values nodes edges weights)))

(defun nodes-edges-weights-3 (track-graph)
  (let ((nodes)
        (edges)
        (weights (make-hash-table :test #'equal)))
    (dolist (node-data track-graph)
      (pushnew (caar node-data) nodes :test #'string=)
      (let ((link (cons (caar node-data) (cadar node-data))))
        (push link edges)
        (setf (gethash link weights) (cdr node-data))))
    (values nodes edges weights)))

(defun graphviz-with-arborescence (simple-edge-descriptions root &optional (out *standard-output*))
  (multiple-value-bind (nodes edges weights)
      (nodes-edges-weights simple-edge-descriptions)
    (let ((arborescence (edmonds nodes edges root weights)))
      (princ "digraph {" out)
      (terpri out)
      (format out "node [shape=box margin=\"0.1,0.1\"];")
      (terpri out)
      (dolist (edge edges)
        (format out "\"~A\" -> \"~A\" [label=\"~A\" color=~A];~%"
                (car edge)
                (cdr edge)
                (gethash edge weights)
                (if (member edge arborescence :test #'equal)
                    "green"
                    "black")))
      (princ "}" out)
      (terpri out))))

(defun line-arborescence-graphviz (line-code root &optional (out *standard-output*))
  (let ((line-data
          (alexandria:hash-table-alist
           (gethash line-code *per-line-track-graph*)))
        (descriptions
          (line-track-descriptions line-code)))
  (multiple-value-bind (nodes edges weights)
      (nodes-edges-weights-2 line-data)
    (let ((arborescence (edmonds nodes edges root weights)))
      (princ "digraph {" out)
      (terpri out)
      (format out "node [shape=box margin=\"0.1,0.1\"];")
      (terpri out)
      (loop
        for key in nodes
        for value = (cdr (assoc key descriptions :test #'equal))
        do (format out "\"~A\" [label=<<B>~A</B><BR /><I><FONT POINT-SIZE=\"10\">~{~A~^<BR ALIGN=\"LEFT\"/>~}</FONT></I>>];~%"
                   key
                   key
                   (if (or (not value) (eql (length value) 0))
                       '("???")
                       (bobbin:wrap (list (substitute #\+ #\& value)) 20))))
      (dolist (edge edges)
        (format out "\"~A\" -> \"~A\" [label=\"~,2F\" color=~A];~%"
                (car edge)
                (cdr edge)
                (gethash edge weights)
                (if (member edge arborescence :test #'equal)
                    "green"
                    "lightgray")
                ))
      (princ "}" out)
      (terpri out)))))

(defun line-arborescence-graphviz-2 (line-code data root &optional (out *standard-output*))
  (let ((line-data data)
        (descriptions
          (line-track-descriptions line-code)))
  (multiple-value-bind (nodes edges weights)
      (nodes-edges-weights-3 line-data)
    (let ((arborescence (edmonds nodes edges root weights)))
      (princ "digraph {" out)
      (terpri out)
      (format out "node [shape=box margin=\"0.1,0.1\"];")
      (terpri out)
      (loop
        for key in nodes
        for value = (cdr (assoc key descriptions :test #'equal))
        do (format out "\"~A\" [label=<<B>~A</B><BR /><I><FONT POINT-SIZE=\"10\">~{~A~^<BR ALIGN=\"LEFT\"/>~}</FONT></I>>];~%"
                   key
                   key
                   (if (or (not value) (eql (length value) 0))
                       '("???")
                       (bobbin:wrap (list (substitute #\+ #\& value)) 20))))
      (dolist (edge arborescence)
        (format out "\"~A\" -> \"~A\" [label=\"~,2F\" color=~A];~%"
                (car edge)
                (cdr edge)
                (gethash edge weights)
                (if (member edge arborescence :test #'equal)
                    "green"
                    "lightgray")
                ))
      (princ "}" out)
      (terpri out)))))

(defvar *visited-stations* (make-hash-table :test 'equal))
(defparameter *stack-limit* 20)
(defvar *intermediate-counter* 0)

(defun station-description-p (description line-code)
  (and (or
        ;; fuckssake Trackernet
        (uiop:string-prefix-p "East Finchley Platform" description)
        (uiop:string-prefix-p "Finchley Central Platform" description)
        (extract-station-information description))
       (or
        (not (string= line-code "M"))
        (and
         (not (search "Neasden" description))
         (not (search "Dollis Hill" description))
         (not (search "Willesden Green" description))
         (not (search "Kilburn" description))
         (not (search "West Hampstead" description))))
       ;; XR junctions
       (not (search "London Liverpool Street" description))
       (not (search "London Paddington" description))
       (not (search "Portobello Jn" description))
       (not (search "Stockley Junction" description))
       (not (search "Manor Park Junction" description))
       ;; some are "At Leaving Stanmore", sigh
       (not (search "Leaving" description))))

(defun recursive-station-terminating-dfs (stack toposorts-hash line-code)
  (when (< (length stack) *stack-limit*)
    (let ((neighbours (gethash (car stack) toposorts-hash)))
      (if (and
           (> (length neighbours) 0)
           (or (eql (length stack) 1)
               (not (station-description-p (car stack) line-code))))
          (loop
            for neighbour in neighbours
            append (recursive-station-terminating-dfs
                    (cons neighbour stack)
                    toposorts-hash
                    line-code))
          (list stack)))))

(defvar *inter-node-collision-hash*
  (make-hash-table :test 'equal))
(defparameter *inter-node-hash-length* 7)

(defun inter-node-name (from to line)
  "Generate an 'interXXX' node for the node between FROM and TO on LINE (containing a 7-character SHA1 hash)"
  (let* ((input (format nil "~A|~A|~A" line from to))
         (ret (subseq
               (ironclad:byte-array-to-hex-string
                (ironclad:digest-sequence
                 :sha1
                 (babel:string-to-octets input)))
               0
               *inter-node-hash-length*))
         (other-input (gethash ret *inter-node-collision-hash*)))
    (when (and other-input
               (not (equal other-input input)))
      (error "Hash collision detected: increment hash length!~%~A = ~A~%~A = ~A"
             ret input ret other-input))
    (setf (gethash ret *inter-node-collision-hash*) input)
    ret))

(defun inter-station-toposorter (links-hash track-codes-hash line-code)
  (format *debug-io* "<center><b>Running inter-station toposorter</b></center>~%")
  (let* ((specialised-station-description-p
          (lambda (x) (station-description-p x line-code)))
        (stations
          (remove-if-not
           specialised-station-description-p
           (alexandria:hash-table-keys links-hash)))
        (ret-links-hash (make-hash-table :test 'equal))
        (ret-track-codes-hash (make-hash-table :test 'equal)))
    (dolist (station stations)
      ;; Do a DFS from the current station to find all paths that point
      ;; to other stations.
      (let* ((unfiltered-paths
               (mapcar #'reverse
                       (recursive-station-terminating-dfs
                        (cons station nil) links-hash line-code)))
             (paths
               (remove-if
                (lambda (path)
                  (< (length path) 2))
                unfiltered-paths))
             ;; Find all unique destination stations (for which there may
             ;; be multiple paths to reach those destinations in PATHS!)
             (uniq-dests (remove-duplicates
                          (mapcar (alexandria:compose #'car #'last)
                                  paths)
                          :test #'string=)))
        (format *debug-io* "<b>\"~A\"</b> -> ~{\"~A\"~^, ~} (~A paths)~%"
                station uniq-dests (length paths))
        (when (gethash station links-hash)
          (format *debug-io* "immediate-paths ~A~%" (gethash station links-hash))
          (format *debug-io* "paths ~A~%" paths))
        ;; Fixup: is there exactly one unique destination that is
        ;; a station, with all of the other destinations not being one?
        ;; If so, insert the station at the end of all of the other paths,
        ;; because there are probably just missing links between
        ;; the other destinations and the station.
        (when (eql (count-if specialised-station-description-p uniq-dests) 1)
          (let ((station-name (find-if specialised-station-description-p uniq-dests)))
            (setf paths (mapcar (lambda (path)
                                  (if (string= (car (last path)) station-name)
                                      path
                                      (append path (list station-name))))
                                paths))
            (setf uniq-dests (list station-name))))
        ;; For each unique destination:
        (dolist (dest uniq-dests)
          (unless (equal station dest)
          (format *debug-io* "\"~A\" <b>-> \"~A\"</b> (subpath)~%" station dest)
          ;; Generate a name for a new dummy node that represents all
          ;; paths from STATION to DEST.
          (let* ((inter-name (inter-node-name station dest line-code))
                 ;; Get all nodes mentioned in all paths that go from
                 ;; STATION to DEST.
                 (mentioned-nodes
                   (remove-if
                    ;; NOTE: we need to remove these from the toposorter
                    ;; and add them back later, otherwise the toposorter
                    ;; could sort them incorrectly and put something else
                    ;; first/last!
                    (lambda (node)
                      (or
                       (string= node station)
                       (string= node dest)))
                    (remove-duplicates
                     (apply #'append
                            (remove-if-not
                             (lambda (path)
                               (and
                                (string= (first path) station)
                                (string= (car (last path)) dest)))
                             paths))
                     :test #'string=)))
                 ;; Collect the links of mentioned nodes.
                 (mentioned-links
                   (mapcar
                    (lambda (node)
                      (cons node
                            (remove-if-not
                             (lambda (other)
                               (member other mentioned-nodes
                                       :test #'equal))
                             (gethash node links-hash))))
                    mentioned-nodes))
                 ;; For cycle breaking purposes: assign each mentioned link
                 ;; a score based off the number of track codes in its
                 ;; first member.
                 (mentioned-link-scores
                   (alexandria:alist-hash-table
                    (alexandria:mappend
                     (lambda (links)
                       (mapcar
                        (lambda (other)
                          (cons (list (car links) other)
                                (sum-links-between
                                 (gethash (car links) track-codes-hash)
                                 (gethash other track-codes-hash))))
                        (cdr links)))
                     mentioned-links)
                    :test 'equal))
                 ;; Topologically sort all mentioned nodes, using the
                 ;; MENTIONED-LINKS graph.
                 (inter-nodes
                   (append
                    `(,station)
                    (when mentioned-links
                      (breaking-toposort-kahn
                       mentioned-links
                       mentioned-link-scores))
                    `(,dest))))
            (format *debug-io* "mentioned-nodes ~A~%" mentioned-nodes)
            (format *debug-io* "mentioned-links ~A~%" mentioned-links)
            (format *debug-io* "scores ~A~%"
                    (alexandria:hash-table-alist mentioned-link-scores))
            (format *debug-io* "inter-nodes ~A~%" inter-nodes)
            ;; Copy over the track codes for start and destination.
            (setf (gethash station ret-track-codes-hash)
                  (gethash station track-codes-hash))
            (setf (gethash dest ret-track-codes-hash)
                  (gethash dest track-codes-hash))
            ;; Special case: is there nothing in between STATION and
            ;; DEST?
            ;; For this to apply, there must be only 2 inter-nodes,
            (if (and
                 (eql (length inter-nodes) 2)
                 (eql (length paths) 1))
                (progn
                  (format *debug-io* "<i>Special case: simply linking</i>~%")
                  ;; If so, just link them together.
                  (setf (gethash station ret-links-hash)
                        (cons dest (gethash station ret-links-hash))))
                ;; Otherwise, make the inter node:
                (progn
                  ;; Make the track codes for the intermediate be the
                  ;; concatenation of all items in INTER-NODES.
                  (setf (gethash inter-name ret-track-codes-hash)
                        (alexandria:mappend
                         (lambda (name)
                           (gethash name track-codes-hash))
                         (subseq inter-nodes 1 (1- (length inter-nodes)))))
                  ;; Insert the STATION -> inter link.
                  (alexandria:ensure-gethash station ret-links-hash)
                  (setf (gethash station ret-links-hash)
                        (cons inter-name (gethash station ret-links-hash)))
                  ;; Insert the inter -> dest link.
                  (setf (gethash inter-name ret-links-hash)
                        (list dest)))))))))
    (values ret-links-hash ret-track-codes-hash)))

;; FIXME(eta): Move me somewhere less dumb!
(defmethod print-object ((obj toposort-model) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (line generated-ts codes code-to-point point-to-station) obj
      (format stream "for ~A, generated ~A ~A, with ~A points, ~A track codes, ~A stations"
              line
              (get-iso-8601-for-ts generated-ts)
              (universal-time-railway generated-ts)
              (hash-table-count codes)
              (hash-table-count code-to-point)
              (hash-table-count point-to-station)))))

(defun stop-before-toposort-model (line-code)
  (load-redis-track-links line-code)
  (let* ((toposorts (breaking-toposort-graphs
                     (split-graphs-into-components
                      (track-graphs-by-description-2 line-code))
                     *track-links-set*))
         (links (inter-toposorts-links toposorts)))
    links))

(defun make-toposort-model (line-code &key cutoff)
  (load-redis-track-links line-code)
  (let* ((split (if (string= line-code "X")
                    (track-graphs-by-description-2 line-code
                                                   :cutoff cutoff)
                    (split-graphs-into-components
                     (track-graphs-by-description-2 line-code
                                                    :cutoff cutoff))))
         (toposorts (breaking-toposort-graphs
                     split
                     *track-links-set*))
         (toposorts-ht (alexandria:alist-hash-table toposorts
                                                    :test 'equal))
         (links (inter-toposorts-links toposorts :cutoff cutoff)))
    (inter-station-toposorter links toposorts-ht line-code)))

(defun spawn-graphviz-x11 ()
  (uiop:process-info-input
   (uiop:launch-program
    "dot -Tx11"
    :input :stream
    :element-type :character)))

(defun preview-toposort-model (line-code outfile)
  (multiple-value-bind (links-hash track-codes-hash)
      (make-toposort-model line-code)
    (declare (ignore track-codes-hash))
    (log:info "Writing toposort model graphviz to ~A..." outfile)
    (write-links-graphviz-to-file
     (alexandria:hash-table-alist links-hash)
     outfile)))

(defun preview-toposort-model-interactively (line-code)
  (multiple-value-bind (links-hash track-codes-hash)
      (make-toposort-model line-code)
    (declare (ignore track-codes-hash))
    (let ((spawned (spawn-graphviz-x11)))
      (links-graphviz
       (alexandria:hash-table-alist links-hash)
       spawned)
      ;; needed to get it to actually render
      (close spawned))))

(defun model-graphviz-labeller (model-point stations-ht)
  (let ((station (gethash model-point stations-ht)))
    (if station
        (format nil "[label=<<B>~A</B><BR/>~@[<FONT POINT-SIZE=\"10\">pfm <B>~A</B></FONT>~]>]"
                (cl-who:escape-string (first station)) (second station))
        (format nil "[label=<<FONT POINT-SIZE=\"10\"><I>~A</I></FONT>>]"
                (cl-who:escape-string model-point)))))

(defun dot-json-toposort-model-inner (model &optional (out *standard-output*))
  (let* ((unfucker (lambda (list)
                     (cons (symbol-name (car list)) (cdr list))))
         (links (mapcar unfucker
                        (cdr (assoc :|links| model))))
         (stations
           (alexandria:alist-hash-table
            (mapcar unfucker
                    (cdr (assoc :|stations| model)))
            :test #'equal))
         (labeller
           (lambda (thing)
             (model-graphviz-labeller thing stations))))
    (links-graphviz
     links
     out
     labeller)))

(defun dot-json-toposort-model (file &optional (out *standard-output*))
  (with-open-file (stream file)
    (let* ((cl-json:*json-identifier-name-to-lisp* #'identity)
           (model (cl-json:decode-json stream)))
      (dot-json-toposort-model-inner model out))))

(defun view-json-toposort-model (file)
  (let ((spawned (spawn-graphviz-x11)))
    (dot-json-toposort-model file spawned)
    (close spawned)))

(defun write-json-toposort-model (line-code stream &key cutoff)
  (multiple-value-bind (links-hash track-codes-hash)
      (make-toposort-model line-code :cutoff cutoff)
    (let ((each-station-metadata
            (delete-if
             (lambda (pair)
               (null (cadr pair)))
             (mapcar (lambda (station)
                       (cons
                        station
                        (multiple-value-list (extract-station-information station))))
                     (alexandria:hash-table-keys links-hash)))))
      (cl-json:encode-json
       `((:metadata .
                    ((:line-code . ,line-code)
                     (:cutoff . ,cutoff)
                     (:generated-ts . ,(get-universal-time))))
         (:links . ,links-hash)
         (:stations . ,(alexandria:alist-hash-table each-station-metadata))
         (:codes . ,track-codes-hash))
       stream))))

(defun load-track-description-overrides (path)
  "Load a JSON file at PATH containing track description overrides, into the database."
  (with-open-file (file path)
    ;; HACK(eta): disable CL-JSON lispification of symbols, because
    ;;            we need the raw track codes!
    (let* ((cl-json:*json-identifier-name-to-lisp* #'identity)
           (json (cl-json:decode-json file)))
      (dolist (line-data json)
        (when (cdr line-data)
          (apply
           #'red:mset
           (loop
             with count = 0
             for (code-sym . descr) in (cdr line-data)
             append (list (format nil "~A-track-desc-~A"
                                  (car line-data) (symbol-name code-sym))
                          descr)
             do (incf count)
             finally (format *debug-io* "~A new descriptions for ~A~%"
                             count (car line-data)))))))))

(defun clickhouse-format-parameter (param)
  (typecase param
    ;; FIXME(eta): this serialization code is seriously awful.
    (list (with-output-to-string (s)
            (princ "[" s)
            (loop
              for i from 1
              for elem in param
              when (typep elem 'string)
                do (princ "'" s)
              do (princ (clickhouse-format-parameter elem) s)
              when (typep elem 'string)
                do (princ "'" s)
              unless (eql i (length param))
                do (princ "," s))
            (princ "]" s)))
    (t (princ-to-string param))))

(defun clickhouse-query (query &rest parameters)
  (let ((parameters
          (mapcar (lambda (cell)
                    (cons (format nil "param_~A"
                                  (string-downcase (symbol-name (car cell))))
                          (clickhouse-format-parameter (cdr cell))))
                  (alexandria:plist-alist parameters)))
        (query (format nil "~A FORMAT JSONCompact" query)))
    (multiple-value-bind
          (strm status-code headers uri stream must-close)
        (drakma:http-request "http://clickhouse.i.eta.st:8123/"
                             :want-stream t
                             :external-format-out :utf-8
                             :parameters `(("query" . ,query)
                                           ,@parameters
                                           ("password" . "intertube")))
      (declare (ignore headers uri))
      (unless (eql status-code 200)
        (error "clickhouse query failed (code ~A): ~A"
               status-code (read-line strm)))
      (setf (flexi-streams:flexi-stream-external-format strm) :utf-8)
      (let ((ret (cl-json:decode-json strm)))
        (when must-close
          (close stream))
        (values (cdr (assoc :data ret))
                (cdr (assoc :meta ret))
                (cdr (assoc :statistics ret)))))))

(defclass clickhouse-trackernet-train ()
  ((universal-ts
    :initarg :universal-ts
    :reader universal-ts)
   (observer
    :initarg :observer
    :reader observer)
   (set-no
    :initarg :set-no
    :reader set-no)
   (trip-no
    :initarg :trip-no
    :reader trip-no)
   (lcid
    :initarg :lcid
    :reader lcid)
   (train-id
    :initarg :train-id
    :reader train-id)
   (track-code
    :initarg :track-code
    :reader track-code)
   (destination-desc
    :initarg :destination-desc
    :reader destination-desc)
   (location-desc
    :initarg :location-desc
    :reader location-desc)
   (leading-car-no
    :initarg :leading-car-no
    :reader leading-car-no)
   (line
    :initarg :line
    :reader line)
   (filename
    :initarg :filename
    :reader filename)
   (input-dest
    :initarg :input-dest
    :reader input-dest)))

;; HACK(eta): This exists because we don't deserialize all the fields properly, so
;;            CHANGE-CLASS doesn't quite work. Hack around this for now...
(defmethod update-instance-for-different-class :after ((previous clickhouse-trackernet-train)
                                                       (current trackernet-train)
                                                       &rest initargs)
  (declare (ignore initargs previous))
  ;; We might actually use some of these soon!!
  (setf (slot-value current 'is-stalled) nil)
  (setf (slot-value current 'direction) nil)
  (setf (slot-value current 'departed) nil)
  (setf (slot-value current 'depart-interval) nil)
  (setf (slot-value current 'depart-time) nil)
  (setf (slot-value current 'order) nil)
  (setf (slot-value current 'dest-code) nil)
  (setf (slot-value current 'time-to) nil)
  (setf (slot-value current 'seconds-to) 0))

;; NOTE(eta): This is basically the TRACKERNET-TRAIN impl, apart from filename.
(defmethod print-object ((obj clickhouse-trackernet-train) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (train-id lcid set-no trip-no track-code location-desc destination-desc leading-car-no filename) obj
      (format
       stream
       "~A… ~A (l=~A/s=~A/t=~A/n=~A) ~A (~A) to ~A"
       (subseq filename 0 #.(length "lt:train_XXXX"))
       (or train-id "???")
       (or lcid "???")
       set-no
       trip-no
       (or leading-car-no "?")
       (or location-desc "<somewhere>")
       (or track-code "???")
       (or destination-desc "<somewhere>")))))

(defun fixup-clickhouse-track-code (track-code)
  (if (position #\LATIN_CAPITAL_LETTER_A_WITH_CIRCUMFLEX track-code)
      (format nil "~A·~A" (subseq track-code 0 2) (subseq track-code 4))
      track-code))

(defun clickhouse-query-trains (where-clause &rest params)
  (loop
    for (universal-ts observer set-no trip-no lcid train-id track-code
                      destination-desc location-desc leading-car-no line
                      intertube-filename input-dest)
      in (apply #'clickhouse-query
                (format nil "SELECT toUnixTimestamp(observation_ts) AS ts, observer, set_no, trip_no, lcid, train_id, track_code, destination_desc, location_desc, leading_car_no, line, intertube_filename, input_dest FROM trains WHERE ~A" where-clause)
                params)
    collect (make-instance 'clickhouse-trackernet-train
                           :universal-ts (+ universal-ts 2208988800)
                           :observer observer
                           :set-no (princ-to-string set-no)
                           :trip-no (princ-to-string trip-no)
                           :lcid (princ-to-string lcid)
                           :train-id (princ-to-string train-id)
                           :track-code (fixup-clickhouse-track-code track-code)
                           :destination-desc destination-desc
                           :location-desc location-desc
                           :leading-car-no (princ-to-string leading-car-no)
                           :line line
                           :filename intertube-filename
                           :input-dest input-dest)))

(defun fetch-clickhouse-observations (intertube-filename)
  (clickhouse-query-trains "intertube_filename = {filename:String} ORDER BY ts ASC" :filename intertube-filename))

(defun fetch-clickhouse-live-train (intertube-filename)
  (let* ((records (fetch-clickhouse-observations intertube-filename)))
    (when records
      (let* ((line-code (line (first records)))
             (last-updated (universal-ts (car (last records))))
             (new-lt (make-instance 'live-train
                                    :unique-id (subseq intertube-filename #.(length "lt:"))
                                    :line-code line-code
                                    :trackernet-id intertube-filename
                                    :actually-live nil)))
        (dolist (rec records)
          (if (position #\· (track-code rec))
              ;; HACK(eta): Maybe encode this data properly?
              (add-smart-live-train-observation new-lt (universal-ts rec)
                                          (format nil "X-~A-X" (observer rec))
                                          rec)
              (add-live-train-observation new-lt (universal-ts rec)
                                          (format nil "X-~A-X" (observer rec))
                                          rec)))
        (setf (last-updated new-lt) last-updated)
        new-lt))))

(defun dodgy-clickhouse-query (query &rest parameters)
  (let ((parameters
          (mapcar (lambda (cell)
                    (cons (format nil "param_~A"
                                  (string-downcase (symbol-name (car cell))))
                          (princ-to-string (cdr cell))))
                  (alexandria:plist-alist parameters)))
        (query (format nil "~A FORMAT TabSeparated" query)))
    (multiple-value-bind
          (strm status-code headers uri stream must-close)
        (drakma:http-request "http://clickhouse.i.eta.st:8123/"
                             :want-stream t
                             :parameters `(("query" . ,query)
                                           ,@parameters
                                           ("password" . "intertube")))
      (declare (ignore headers uri stream))
      (unless (eql status-code 200)
        (error "clickhouse query failed (code ~A): ~A"
               status-code (read-line strm)))
      (setf (flexi-streams:flexi-stream-external-format strm) :utf-8)
      (values strm must-close))))

(defun fetch-track-code-history (line start-ts interval)
  (dodgy-clickhouse-query
   ;; NOTE(eta): This is NOT SQL injection compliant! However, it's only
   ;;            called manually.
   (format nil "SELECT intertube_filename, toUnixTimestamp(observation_ts) AS ts, track_code FROM trains WHERE observation_ts > (FROM_UNIXTIME({ts:Integer}) - INTERVAL ~A) AND line = {line:String} AND destination_desc != 'Chiltern Trains' ORDER BY intertube_filename, observation_ts ASC" interval)
   :ts (- start-ts 2208988800)
   :line line))

(defun count-track-code-history (line start-ts interval)
  (parse-integer
   (caar
    (clickhouse-query
     (format nil "SELECT COUNT(*) FROM trains WHERE observation_ts > (FROM_UNIXTIME({ts:Integer}) - INTERVAL ~A) AND line = {line:String}" interval)
     :ts (- start-ts 2208988800)
     :line line))))

(defun populate-track-links-from-clickhouse (line &key (pipeline-size 10000) (interval "4 week"))
  (let* ((start-ts (get-universal-time))
         (track-links-ht (format nil "model:links-ch:~A" line))
         (total (count-track-code-history line start-ts interval))
         (previous nil)
         (lastline nil)
         (entry nil)
         (history nil)
         (processed 0)
         (num-files 0)
         (enter-start nil))
    (tagbody
     :start
       (setf history (fetch-track-code-history line start-ts interval))
       (red:del track-links-ht)
     :repeat
       (redis:with-pipelining
         (setf enter-start (get-internal-real-time))
         (block outer-loop
           (loop
             (setf lastline (read-line history nil nil))
             (unless lastline
               (return-from outer-loop))
             (setf entry (split-sequence:split-sequence #\Tab lastline))
             (setf (second entry) (parse-integer (second entry)))
             (incf processed)
             (block nil
               (when (or
                      ;; No previous entry.
                      (not previous)
                      ;; The previous entry is a different filename.
                      (not (string= (first previous) (first entry))))
                 (incf num-files)
                 (setf previous entry)
                 (return))
               (let ((track-a (third previous))
                     (track-b (third entry)))
                 (unless (or (string= track-a track-b)
                             ;; Ignore skips back in time, or just
                             ;; observations with the same timestamp.
                             (>= (second previous) (second entry)))
                   (red:hincrby track-links-ht
                                (format nil "~A•~A" track-a track-b)
                                1)))
               (setf previous entry))
             (when (eql (rem processed pipeline-size) 0)
               (return-from outer-loop)))))
       (format t "[~A] done ~A (~Af) of ~A (~,1F%) -- ~,2Fus/entry~%"
               line processed num-files total
               (/ (* processed 100) total)
               (/
                (*
                 (/ (- (get-internal-real-time) enter-start)
                    internal-time-units-per-second)
                 1000000)
                pipeline-size))
       (when lastline
         (go :repeat)))))

(defun refresh-track-links-clickhouse ()
  (delete-redis-ch-model-data)
  (loop
    for line in '("D" "J" "B" "C" "P" "H" "M" "N" "V")
    do (populate-track-links-from-clickhouse line)))

(defun delete-redis-ch-model-data ()
  (mapcar #'red:del (get-all "model:links-ch:*")))

(defun find-lts-with-no-ttl ()
  (loop
    for ltid in (get-all "lt:*")
    when (eql (red:ttl ltid) -1)
      collect ltid))

(defun clear-clickhouse-queue ()
  "Clear the queue of clickhouse failures."
  ;; make sure we have a connection first
  (assert (string= "PONG" (red:ping)))
  (loop
    for train = (red:spop "clickhouse-failures")
    while train
    do (handler-bind
           ((serious-condition
              (lambda (c)
                (declare (ignore c))
                (log:warn "reinserting train into failures due to condition")
                (red:sadd "clickhouse-failures" train))))
         (clickhouse-insert-live (cpk-unbase64 train)))))

(defun get-track-codes-for-station (line-code station-name)
  "Return a list of all track codes associated with all platforms a given station has."
  (let ((model (line-model-for line-code)))
    (alexandria:mappend
     (lambda (x)
       (gethash (cdr x) (codes model)))
     (remove-if-not
      (lambda (y)
        (string= (caar y) station-name))
      (alexandria:hash-table-alist (station-to-point model))))))

(defun get-historical-trains-for-station (line-code station-name start-ts end-ts)
  "Fetch historical trains passing through a given station in the given time ranges (in Universal Time)."
  (let* ((track-codes (get-track-codes-for-station line-code station-name))
         (start-ts-unix (- start-ts 2208988800))
         (end-ts-unix (- end-ts 2208988800))
         (relevant-train-entries
           (clickhouse-query-trains "line = {line:String} AND observation_ts BETWEEN FROM_UNIXTIME({start:Integer}) AND FROM_UNIXTIME({end:Integer}) AND has({codes:Array(String)}, track_code) ORDER BY ts ASC"
                             :start start-ts-unix
                             :end end-ts-unix
                             :codes track-codes
                             :line line-code))
         (ret (make-hash-table :test 'equal)))
    (dolist (train relevant-train-entries)
      (alexandria:ensure-gethash (filename train) ret (list (cons train (universal-ts train))))
      ;; If this train is newer than the current train for that filename
      ;; by <60 seconds, replace that train (probably part of the same
      ;; station call).
      ;; However, if it's greater than that, it's probably a separate
      ;; station call.
      (symbol-macrolet ((hash-entry (gethash (filename train) ret)))
        (let ((time-delta-sec (- (universal-ts train)
                                  (universal-ts (caar (last hash-entry))))))
          (if (> time-delta-sec 60)
              (push (cons train (universal-ts train)) (cdr (last hash-entry)))
              (setf (caar (last hash-entry)) train)))))
    (alexandria:hash-table-alist ret)))

(defun get-current-train-ids-by-identifier (line-code start-ts end-ts identifier)
  (let ((lts (live-trains-on-line line-code)))
    (loop
      for lt in lts
      append (let ((event-start)
                   (event-end)
                   (ret))
               (dolist (entry (append (reverse (history lt)) (list (current-entry lt))))
                 (when (>= end-ts (universal-ts entry) start-ts)
                   (alexandria:when-let
                       ((new-identifier
                         (or
                          (when (and (string= line-code "X")
                                     (typep entry 'train-rescue))
                            (new-key entry))
                          (when (typep entry 'train-identity-change)
                            (wtt-id entry)))))
                     (when (and (not event-start)
                                (equal new-identifier identifier))
                       (setf event-start (universal-ts entry)))
                     (when (and event-start
                                (not (equal new-identifier identifier)))
                       (setf event-end (universal-ts entry))
                       (push (cons event-start event-end) ret)
                       (setf event-start nil)
                       (setf event-end nil)))))
               (when event-start
                 (push (cons event-start (get-universal-time)) ret))
               (mapcar
                (lambda (x)
                  (list (unique-id lt) :live nil (car x) (cdr x)))
                ret)))))

(defun get-train-ids-by-identifier (line-code start-ts end-ts identifier)
  (sort
   (append
    (get-current-train-ids-by-identifier line-code start-ts end-ts identifier)
    (mapcar
     (lambda (x)
       (list (car x)
             (sort (second x) (function <))
             (third x)
             (+ (fourth x) 2208988800)
             (+ (fifth x) 2208988800)))
     (clickhouse-query
      (format nil "SELECT intertube_filename, groupArray(DISTINCT trip_no), groupArray(DISTINCT destination_desc), toUnixTimestamp(min(observation_ts)) AS start, toUnixTimestamp(max(observation_ts)) AS end FROM trains WHERE line = {line:String} AND ~A = {identifier:~A} AND observation_ts BETWEEN FROM_UNIXTIME({start:Integer}) AND FROM_UNIXTIME({end:Integer}) GROUP BY intertube_filename ORDER BY start ASC"
              (if (string= line-code "X") "train_id" "set_no")
              (if (string= line-code "X") "String" "UInt16"))
      :line line-code
      :start (- start-ts 2208988800)
      :end (- end-ts 2208988800)
      :identifier identifier)))
   (lambda (x y)
     (< (fourth x) (fourth y)))))

(defun live-trains-on-line (code)
  (let ((keys (red:smembers (format nil "line-trains:~A" code))))
    (when keys
      (delete-if-not
       (lambda (lt)
         (and lt (string= (line-code lt) code)))
       (apply #'redis-raw-mget-cpk keys)))))

(defun live-trains-by-platform-on-line (code)
  (let ((ret (make-hash-table :test 'equal)))
    (dolist (lt (live-trains-on-line code))
      (when (typep (current-entry lt) 'train-station-stop)
        (setf (gethash (list (station-name (current-entry lt))
                             (platform (current-entry lt)))
                       ret)
              lt)))
    ret))

;; FIXME(eta): This function is kind of dumb, because it's basically "undoing"
;;             ADD-LIVE-TRAIN-OBSERVATION.
(defun get-lt-state-at-time (live-train universal-ts)
  "Return the train identity, leading car number, destination, and trackernet-id of LIVE-TRAIN at UNIVERSAL-TS."
  (let ((identity)
        (leading-car-no)
        (destination)
        (trackernet-id))
    (loop
      for entry in (append (reverse (history live-train)) (list (current-entry live-train)))
      while (<= (universal-ts entry) universal-ts)
      do (typecase entry
           (train-rescue
            (setf trackernet-id (new-key entry)))
           (train-destination-change
            (setf destination (destination entry)))
           (train-identity-change
            (setf identity (list (wtt-id entry) (wtt-trip entry))))
           (train-car-change
            (setf leading-car-no (new-car-no entry)))))
    (list identity leading-car-no destination trackernet-id)))

(defun get-current-lts-for-station (line-code station-name start-ts end-ts)
  "Fetch currently active LIVE-TRAIN objects passing through a given station in the given time ranges (in Universal Time)."
  (let ((lts (live-trains-on-line line-code)))
    (remove-if
     (alexandria:compose #'null #'cdr)
     (loop
       for lt in lts
       collect (cons lt
                     (loop
                       for entry in (append (history lt) (list (current-entry lt)))
                       when (and (typep entry 'train-station-stop)
                                 (>= end-ts (universal-ts entry) start-ts)
                                 (string= (station-name entry) station-name))
                         collect entry))))))

(defun fetch-unified-api-arrivals (line &optional sp)
  "Fetch a set of arrivals from the Unified API for the provided LINE."
  (multiple-value-bind (body status-code)
      (drakma:http-request (format nil "~A/Line/~A/Arrivals/~A?cachebust=~A"
                                   *unified-api-base-url*
                                   line
                                   (or sp "")
                                   (random 4096))
                           :user-agent "intertube/0.1 (intertube@eta.st)")
    (unless (eql status-code 200)
      (error "Unified API returned code ~A: ~A" status-code body))
    (sort
     (mapcar (lambda (entry)
               (list
                (cdr (assoc :naptan-id entry))
                (cdr (assoc :station-name entry))
                (cdr (assoc :platform-name entry))
                (cdr (assoc :destination-naptan-id entry))
                (cdr (assoc :time-to-station entry))))
;;                (local-time:timestamp-to-universal
;;                 (local-time:parse-rfc3339-timestring
;;                  (cdr (assoc :expected-arrival entry))))))
             (cl-json:decode-json-from-string (babel:octets-to-string body)))
             (lambda (a1 a2)
               (< (fifth a1) (fifth a2))))))

(defun unified-arrivals-by-station (data)
  (let ((ret (make-hash-table :test 'equal)))
    (loop
      for (nid name pfm dest time) in data
      do (alexandria:ensure-gethash (list nid name pfm) ret)
      do (push (cons time dest) (gethash (list nid name pfm) ret)))
    (loop
      for k being the hash-keys of ret
      do (setf (gethash k ret)
               (sort (gethash k ret)
                     (lambda (a1 a2)
                       (< (car a1) (car a2))))))
    (alexandria:hash-table-alist ret)))

(defun listify-pairs (cons-pairs)
  (let ((ret (make-hash-table :test 'equal)))
    (loop
      for (a . b) in cons-pairs
      do (alexandria:ensure-gethash a ret)
      do (setf (gethash a ret) (append (gethash a ret) (list b))))
    (alexandria:hash-table-alist ret)))

;; stolen from the interwebs
(defun loop-n-window (n list function)
  (loop for x on list
        while (nthcdr (1- n) x)
        do (apply function (subseq x 0 n))))

(defun shifted-paired-subtract (alis blis shift)
  (loop
    for item in alis
    for i from 0
    collect (if (> (length blis) (+ shift i) -1)
                (- item (elt blis (+ shift i)))
                nil)))

(defun stupid-offset-finder (data line-of-stations destination naptan)
  (let ((each-station-arrivals
          (sort
           (listify-pairs
            (loop
              for nid-los in line-of-stations
              append
              (loop
                for (nid name pfm dest time) in data
                when (and (string= nid (format nil "~A~A" naptan nid-los))
                          (or
                           (string= dest (format nil "~A~A" naptan destination))
                           (string= nid (format nil "~A~A" naptan destination))))
                  collect (cons nid-los time))))
           (lambda (a1 a2)
             (< (position (car a1) line-of-stations :test #'string=)
                (position (car a2) line-of-stations :test #'string=)))))
        (ret nil))
    (loop-n-window
     2 each-station-arrivals
     (lambda (s1 s2)
       (format t "~A ~A~%  → ~A ~A:~%" (car s1) (cdr s1) (car s2) (cdr s2))
       (let ((this-ret))
       (loop
         for pshift from 0 upto 1
         unless this-ret
           do
              (loop
                for shift in (if (eql pshift 0)
                                 (list 0)
                                 (list pshift (- 0 pshift)))
                unless this-ret
                  do (let ((results (shifted-paired-subtract (cdr s2) (cdr s1) shift)))
                       (format t "  shift ~A: ~A~%"
                               shift results)
                       (loop
                         with admissible = 0
                         with failed
                         with value
                         for n in results
                         when (and n (>= 300 n 0))
                           do (incf admissible)
                              (unless value
                                (setf value n))
                              (when (> (abs (- value n)) 5)
                                (setf failed t))
                         finally
                            (unless (or failed (< admissible 1))
                              (setf this-ret value))))))
         (terpri)
         (setf ret (append ret (list this-ret))))))
    ret))

(defvar *stomp-td* nil)

(defun enqueue-td-deletion (headcode berth ts)
  "If a train with HEADCODE exists, get rid of it by scheduling a hacky deletion record."
  (when (red:exists (format nil "X-train-~A" headcode))
    (let* ((train (format nil "X-train-~A" headcode))
           (data (redis-last-cpk train))
           (ts (or ts
                   (1+ (redis-last-score train))))
           (fake-train (cadr data))
           (last-key (format nil "berth-last:Q0:~A" (track-code fake-train))))
      (setf (slot-value fake-train 'track-code) berth)
      (setf (slot-value fake-train 'location-desc) (format nil "cancellation in berth ~A" berth))
      (when (position #\· (track-code fake-train))
        (setf last-key (format nil "berth-last:~A:~A"
                               (subseq (track-code fake-train) 0 2)
                               (subseq (track-code fake-train) 3))))
      (log:info "Deleting train with headcode ~A" headcode)
      (red:setex last-key
                 30
                 (subseq train #.(length "X-train-")))
      (redis-train ts "X" "X-DEL" fake-train))))

;; Some berths are duplicated between signalboxes. We need to hide the
;; duplicates, choosing carefully to ensure we exclude the dupe that is
;; not actually part of the area.
(defparameter *ignored-berths*
  '(;; overlap between Q2→Q1 (westbound)
    "Q1·0478" "Q1·0474" "Q1·0470" "Q2·0464" "Q2·0460"
    "Q2·0456" "Q2·0452" "Q2·0448" "Q2·0446" "Q2·0442"
    ;; overlap between Q1→Q2 (eastbound)
    "Q1·R447" "Q1·0447" "Q1·0445" "Q1·0441" "Q2·0433"
    "Q2·0431" "Q2·0427" "Q2·0423" "Q2·0421" "Q2·0417"
    "Q2·0415"
    ;; weird rear berth
    "Q2·R447"
    ;; GDP sidings
    "Q2·0468"
    ;; overlap between XR→D3 (westbound)
    "D3·X009" "0091"
    ;; overlap between D3→DA (westbound)
    "D3·0175"
    ;; overlap between D4→D6 (westbound)
    "D6·0477"
    ;; overlap between D6→D1 (westbound)
    "D1·1611"
    ;; overlap between D1→D6 (eastbound)
    "D6·1612"
    ;; overlap between D6→D4 (eastbound)
    "D4·0472"
  ))

;; Some berths are "a train turning around in a platform", in which case
;; we should treat an interpose in the first berth as a rescue from the
;; second berth.
(defparameter *turnaround-berths*
  '(("Q2·0516" . "Q2·0495") ;; SNF platform 5 west . east
    ("Q2·0495" . "Q2·0516") ;; SNF platform 5 east . west
    ("Q2·B526" . "Q2·R526") ;; SNF platform 6 B . last-arrival
    ("D4·B352" . "D4·R352") ;; Heathrow T4 last-arrival 1
    ("D4·B348" . "D4·R348") ;; Heathrow T4 last-arrival 2
    ("D4·B364" . "D4·R364") ;; Heathrow T5 pfm 4 B . last-arrival
    ("D4·B362" . "D4·R362") ;; Heathrow T5 pfm 3 B . last-arrival
    ("D1·1698" . "D1·1723") ;; RDG platform 15 east . west
    ("D1·1723" . "D1·1698") ;; RDG platform 15 west . east
    ("D1·1696" . "D1·1721") ;; RDG platform 14 east . west
    ("D1·1721" . "D1·1696") ;; RDG platform 14 west . east
    ("D1·1694" . "D1·1719") ;; RDG platform 13 east . west
    ("D1·1719" . "D1·1694") ;; RDG platform 13 west . east
    ("D1·1692" . "D1·1717") ;; RDG platform 12 east . west
    ("D1·1717" . "D1·1692") ;; RDG platform 12 west . east
    ("D6·0574" . "D6·0579") ;; MAI platform 4 east . west
    ("D6·0579" . "D6·0574") ;; MAI platform 4 west . east
    ("D6·0590" . "D6·R590") ;; MAI carriage sidings
    ("D6·0588" . "D6·R588") ;; MAI carriage sidings
    ("D6·0586" . "D6·R586") ;; MAI carriage sidings
    ("D6·0584" . "D6·R584") ;; MAI carriage sidings
    ("D6·0582" . "D6·R582") ;; MAI carriage sidings
    ("D6·0580" . "D6·R580") ;; MAI carriage sidings
    ("DA·0214" . "DA·0223") ;; WEA platform 4 east . west
    ("DA·0223" . "DA·0214") ;; WEA platform 4 west . east
    ("Q1·0468" . "Q1·R468") ;; GDP carriage sidings
    ("Q1·0988" . "Q1·0987") ;; GDP carriage sidings
    ("Q1·0987" . "Q1·0988") ;; GDP carriage sidings
    ("Q1·0986" . "Q1·0985") ;; GDP carriage sidings
    ("Q1·0985" . "Q1·0986") ;; GDP carriage sidings
    ("Q1·0984" . "Q1·0983") ;; GDP carriage sidings
    ("Q1·0983" . "Q1·0984") ;; GDP carriage sidings
    ("Q1·0982" . "Q1·0981") ;; GDP carriage sidings
    ("Q1·0981" . "Q1·0982") ;; GDP carriage sidings
    ("D4·B282" . "D4·R282") ;; HAY bay platform
    ("D3·C031" . "D3·R031") ;; PAD pfm 14 C . last-arrival
    ("D3·C029" . "D3·R029") ;; PAD pfm 12 C . last-arrival
    ("D3·C025" . "D3·R025") ;; PAD pfm 11 C . last-arrival
    ("D3·C019" . "D3·R019") ;; PAD pfm 10 C . last-arrival
    ("Q4·FC51" . "Q4·R051") ;; LST pfm 17 C . last-arrival
    ("Q4·FC49" . "Q4·R049") ;; LST pfm 16 C . last-arrival
    ("Q4·FC45" . "Q4·R045") ;; LST pfm 15 C . last-arrival
    ("Q1·0268" . "Q1·0279") ;; SRA platform 5 west . east
    ("Q1·0279" . "Q1·0268") ;; SRA platform 5 east . west
    ("Q1·0266" . "Q1·0277") ;; SRA platform 8 west . east
    ("Q1·0277" . "Q1·0266") ;; SRA platform 8 east . west
    ))

(defun headcode-is-crossrail (headcode)
  (and
   (uiop:string-prefix-p "9" headcode)
   ;; filter out c2c on XR East
   (not (uiop:string-prefix-p "9B" headcode))))

(defun extract-destination-from-xr-headcode (headcode)
  (cond
    ;; this is a bit meh
    ((uiop:string-prefix-p "5" headcode) "Not in Service")
    ((uiop:string-prefix-p "3" headcode) "Not in Service")
    ((uiop:string-prefix-p "6" headcode) "Engineering Train")
    ((uiop:string-prefix-p "2W" headcode) "XR East")
    ((uiop:string-prefix-p "9A" headcode) "Core Shuttle")
    ((uiop:string-prefix-p "9B" headcode) "Core Shuttle")
    ((uiop:string-prefix-p "9C" headcode) "Gidea Park")
    ((uiop:string-prefix-p "9D" headcode) "Ilford")
    ((uiop:string-prefix-p "9E" headcode) "West Ealing")
    ((uiop:string-prefix-p "9G" headcode) "Special")
    ((uiop:string-prefix-p "9H" headcode) "Heathrow Terminal 4")
    ((uiop:string-prefix-p "9N" headcode) "Maidenhead")
    ((uiop:string-prefix-p "9P" headcode) "Paddington HL")
    ((uiop:string-prefix-p "2Y" headcode) "Paddington HL")
    ((uiop:string-prefix-p "9E" headcode) "Hayes & Harlington")
    ((uiop:string-prefix-p "9R" headcode) "Reading")
    ((uiop:string-prefix-p "9T" headcode) "Heathrow Terminal 5")
    ((uiop:string-prefix-p "9U" headcode) "Abbey Wood")
    ((uiop:string-prefix-p "9W" headcode) "Shenfield")
    ((uiop:string-prefix-p "9Y" headcode) "Paddington")
    ((uiop:string-prefix-p "9Z" headcode) "Special")))

(defun xr-smart-make-fake-train (headcode area berth)
  "Make a fake TRACKERNET-TRAIN to be used for XR SMART TD steps."
  (let ((fake-train (make-instance 'trackernet-train)))
    (loop
      for thing in '(lcid dest-code order direction
                     depart-time depart-interval departed
                     time-to is-stalled)
      do (setf (slot-value fake-train thing) ""))
    (setf (slot-value fake-train 'train-id) headcode)
    (setf (slot-value fake-train 'seconds-to) 0)
    (setf (slot-value fake-train 'location-desc)
          (format nil "berth ~A·~A" area berth))
    ;; HACK(eta): The presence of a &middot; here means it'll use
    ;; ADD-SMART-LIVE-TRAIN-OBSERVATION.
    (setf (slot-value fake-train 'track-code)
          (format nil "~A·~A" area berth))
    (setf (slot-value fake-train 'destination-desc)
          (or (extract-destination-from-xr-headcode headcode)
              ""))
    (setf (slot-value fake-train 'line) "X")
    (setf (slot-value fake-train 'set-no) "000")
    (setf (slot-value fake-train 'trip-no) "0")
    fake-train))

(defun check-trust-activation-for (headcode ts)
  "Checks whether a TRUST activation exists, and whether it makes sense for this to happen at TS."
  (let ((activation (cpk-unbase64 (red:get (format nil "trust:~A" headcode)))))
    (when activation
      (> ts (- (cdr (assoc :depart-ts activation))
                ;; Allow 30 minutes of slack
                1800)))))

(defun stomp-td-message-handler-smart (message-type body)
  "Process a TD message from NROD."
  (unless (member message-type '(:+CA-MSG+ :+CB-MSG+ :+CC-MSG+ :+CT-MSG+))
    (return-from stomp-td-message-handler-smart))
  (let* ((headcode (cdr (assoc :descr body)))
         (area (cdr (assoc :area--id body)))
         (berth (cdr (assoc :to body)))
         (area-and-berth (format nil "~A·~A" area berth))
         (old-berth (cdr (assoc :from body)))
         (ts
           (+ (/ (parse-integer (cdr (assoc :time body))) 1000)
               2208988800)))
    (when (and headcode
               (member area *xr-smart-areas* :test #'string=)
               ;; Conditions where we'll use a message:
               (or
                ;; Train already exists in system
                (red:exists (format nil "X-train-~A" headcode))
                ;; Headcode activated by TRUST
                (check-trust-activation-for headcode ts)
                ;; FIXME(eta): starts with a 9 (;w;)
                (headcode-is-crossrail headcode)
                ;; Interposed on top of an existing berth
                (and
                 (eql message-type :+CC-MSG+)
                 (red:exists (format nil "berth:~A:~A" area berth)))
                ;; Would match against a train in a corresponding
                ;; turnaround berth
                (let ((turnaround-from
                        (cdr (assoc area-and-berth *turnaround-berths*
                                    :test #'string=))))
                  (when turnaround-from
                    (red:exists
                     (format nil "berth:~A:~A"
                             (subseq turnaround-from 0 2)
                             (subseq turnaround-from 3)))))))
      ;; Don't accept headcodes with asterisks in them
      ;; (they break the archiver!!)
      ;; ...or - in them (they're not real headcodes)
      (when (or
             (position #\* headcode)
             (position #\- headcode))
        (return-from stomp-td-message-handler-smart))

      ;; Process cancellation messages:
      (when old-berth
        (let ((old-headcode (red:getdel
                             (format nil "berth:~A:~A" area old-berth))))
          (when (and old-headcode (eql message-type :+CB-MSG+))
            (statsd-inc "intertube.xr-smart-berth-cancel")
            (log:info "in berth ~A (SMART area ~A), ~A cancelled"
                      old-berth area old-headcode)
            ;; Has the train moved on? (e.g. is this cancel of a berth
            ;; the train is no longer in?)
            (let ((last-entry
                    (cadr (redis-last-cpk
                           (format nil "X-train-~A" old-headcode)))))
              (when (and
                     last-entry
                     (not (string= (track-code last-entry)
                                   (format nil "~A·~A" area old-berth))))
                (log:info "...but current position is ~A, which is different from ~A·~A"
                          (track-code last-entry) area old-berth)
                (return-from stomp-td-message-handler-smart)))
            ;; Is there a SMART record for this cancel?
            ;; NOTE(eta): Since the interpose-treated-as-cancel change,
            ;;            this should NOT be generated for inter-area steps.
            (let ((smart-entry
                    (gethash (list area old-berth nil "C")
                             *xr-smart-data*)))
              (when smart-entry
                ;; HACK(eta): Seriously, National Rail?
                (log:info "...but this cancel has a SMART record")
                (let ((fake-train
                        (xr-smart-make-fake-train old-headcode area "XXCB")))
                  (redis-train ts "X" "X-XCB" fake-train)
                  (return-from stomp-td-message-handler-smart))))
            ;; None of the above, delete the train
            (enqueue-td-deletion old-headcode
                                 (format nil "~A·~A" area old-berth)
                                 ts))))
      ;; Process normal movements:
      (when (member message-type '(:+CA-MSG+ :+CC-MSG+))
        (let* ((old-headcode
                 (red:getset (format nil "berth:~A:~A" area berth)
                             headcode))
               (last-headcode
                 (red:get (format nil "berth-last:~A:~A" area berth)))
               (rescue-headcode (or old-headcode last-headcode))
               (last-entry (redis-last-cpk (format nil "X-train-~A" headcode)))
               (last-score (redis-last-score (format nil "X-train-~A" headcode)))
               (fake-train (xr-smart-make-fake-train headcode area berth)))
          ;; don't have berths stick around for longer than the trains in them
          (red:expire (format nil "berth:~A:~A" area berth)
                      *xr-train-expiry-secs*)
          (when (member (format nil "~A·~A" area berth) *ignored-berths*
                        :test #'string=)
            (log:info "Ignoring berth step (type ~A) into ~A·~A as requested"
                      message-type area berth)
            (return-from stomp-td-message-handler-smart))
          (when (and
                 last-entry
                 (eql message-type :+CC-MSG+)
                 (string= (subseq (track-code (cadr last-entry)) 3) "XXCB"))
            (log:warn "train ~A: removing previous SMART cancellation record after interpose into berth ~A (SMART area ~A)"
                      headcode berth area)
            (red:zremrangebyscore (format nil "X-train-~A" headcode) last-score last-score))
          ;; check jump (FIXME copypasta)
          (when (and last-entry old-berth
                     (string= (subseq (track-code (cadr last-entry)) 2) area)
                     (not (string= (subseq (track-code (cadr last-entry)) 3) old-berth)))
            (log:warn "train ~A (SMART area ~A) jumped from berth ~A to ~A (expected from ~A)!"
                      headcode area (track-code (cadr last-entry))
                      berth old-berth)
            (statsd-inc "intertube.xr-smart-berth-jump")
            (when (string= (red:get (format nil "berth:~A:~A" area old-berth))
                           headcode)
              (red:del (format nil "berth:~A:~A" area old-berth))))
          ;; FIXME: used to check clobber
          ;; If there's a TRUST activation, rename it for the live train code
          ;; to find (and so we don't use it twice):
          (when (red:exists (format nil "trust:~A" headcode))
            (log:info "train ~A has corresponding TRUST ID ~A, using"
                      headcode
                      (cdr (assoc :trust-id
                                  (cpk-unbase64 (red:get (format nil "trust:~A" headcode))))))
            (red:rename (format nil "trust:~A" headcode)
                        (format nil "trust-used:~A" headcode))
            (red:expire (format nil "trust-used:~A" headcode)
                        300))
          ;; Add the fake train observation:
          (redis-train ts "X" (if (eql message-type :+CA-MSG+)
                                  "X-XCA"
                                  "X-XCC")
                       fake-train)
          ;; Check turnaround:
          (let ((turnaround-from
                  (cdr (assoc area-and-berth
                              *turnaround-berths*
                              :test #'string=))))
            (when (and
                   (eql message-type :+CC-MSG+)
                   (not rescue-headcode)
                   turnaround-from)
              (let* ((turn-area (subseq turnaround-from 0 2))
                     (turn-berth (subseq turnaround-from 3))
                     (turnaround-headcode (red:getdel (format nil "berth:~A:~A" turn-area turn-berth))))
                (when turnaround-headcode
                  (log:info "interpose in berth ~A·~A grabs ~A from turnaround berth ~A"
                            area berth turnaround-headcode turnaround-from)
                  (setf rescue-headcode turnaround-headcode)))))
          (when (and
                 (eql message-type :+CC-MSG+)
                 rescue-headcode
                 ;; FIXME(eta): why not??
                 (not (equal rescue-headcode headcode))
                 (red:exists (format nil "X-train-~A" rescue-headcode)))
            (log:info "interpose in berth ~A (SMART area ~A) links ~A to ~A, rescuing"
                      berth area rescue-headcode headcode)
            (when last-entry
              (log:warn "...but destination train ~A already existed, aborting!" headcode)
              (return-from stomp-td-message-handler-smart))
            (statsd-inc "intertube.xr-rescue")
            (rescue-train (format nil "X-train-~A" rescue-headcode)
                          (format nil "X-train-~A" headcode))))))))

(defun make-sclass-delta (message-type address data)
  (if (eql message-type :+SF-MSG+)
      ;; Easy: just shift the data bytes by the address
      (values
       (ash (parse-integer data :radix 16)
            (* address 8))
       (ash #xFF (* address 8)))
      ;; More complicated: these have 4 bytes and we need to shift
      ;; them individually
      (let ((b1 (parse-integer data :start 0 :end 2 :radix 16))
            (b2 (parse-integer data :start 2 :end 4 :radix 16))
            (b3 (parse-integer data :start 4 :end 6 :radix 16))
            (b4 (parse-integer data :start 6 :end 8 :radix 16))
            (address (* address 8)))
        (values
         (logior
          (ash b1 address)
          (ash b2 (+ address 8))
          (ash b3 (+ address 16))
          (ash b4 (+ address 24)))
         (ash #xFFFFFFFF address)))))

(defun apply-sclass-delta (original message-type address data)
  (multiple-value-bind (delta nandmask)
      (make-sclass-delta message-type address data)
    (logior
     (logand original (lognot nandmask))
     delta)))

(defun stomp-td-message-handler-sclass (message-type body)
  "Handles a STOMP S-class message."
  (unless (member message-type '(:+SF-MSG+ :+SG-MSG+ :SH-MSG+))
    (return-from stomp-td-message-handler-sclass))
  (let* ((address (parse-integer
                   (cdr (assoc :address body))
                   :radix 16))
         (area (cdr (assoc :area--id body)))
         (data (cdr (assoc :data body))))
    (unless (or
             (string= area "Q0")
             (member area *xr-smart-areas* :test #'string=))
      (return-from stomp-td-message-handler-sclass))
    (let* ((existing-key (red:get (format nil "sclass:~A" area)))
           (existing (if existing-key
                         (parse-integer existing-key :radix 16)
                         0))
           (new (apply-sclass-delta existing message-type address data))
           (changed-bits (logxor existing new))
           (mentioned-signals (xrcos::get-mentioned-signals area changed-bits)))
      ;; In order to make this bit less horrendously bad (i.e. fetching a
      ;; full set of all trains for *every* message!), we only bother doing
      ;; the inefficient thing if any of the signals that changed are ones
      ;; we care about.
      (when (position-if (lambda (sigbit)
                           (xrcos::get-starter-for-signal area (car sigbit)))
                         mentioned-signals)
        ;; FIXME(eta): This is quite inefficient.
        (let ((live-trains (live-trains-by-platform-on-line "X")))
          (loop
            for (sig . bit) in mentioned-signals
            do (alexandria:when-let*
                   ((platform-starter (xrcos::get-starter-for-signal area sig
                                                                     :strip-direction t))
                    (lt (gethash platform-starter live-trains)))
                 (log:debug "~A ready to depart at ~A (~A pfm ~A)"
                            (trackernet-id lt) sig
                            (station-name (current-entry lt))
                            (platform (current-entry lt)))
		 (when (not (slot-boundp lt 'actually-live))
		   (log:warn "argh unbound again")
		   (setf (slot-value lt 'actually-live) t))
                 (set-rtdi lt (logbitp bit new))
                 (persist-live-train lt)))))

      (red:set (format nil "sclass:~A" area)
               (write-to-string new :base 16))
      (when (eql message-type :+SH-MSG+)
        (log:info "Signalling refresh finished for area ~A" area)))))

(defun stomp-td-message-handler (message-type body)
  "Handles a STOMP message."
  (when (member message-type '(:+SF-MSG+ :+SG-MSG+ :SH-MSG+))
    (return-from stomp-td-message-handler
      (stomp-td-message-handler-sclass message-type body)))
  (unless (member message-type '(:+CA-MSG+ :+CB-MSG+ :+CC-MSG+ :+CT-MSG+))
    (return-from stomp-td-message-handler))
  (when (member (cdr (assoc :area--id body))
                *xr-smart-areas*
                :test #'string=)
    (statsd-inc "intertube.xr-stomp-message")
    (return-from stomp-td-message-handler
      (stomp-td-message-handler-smart message-type body)))
  (unless (string= (cdr (assoc :area--id body)) "Q0")
    (return-from stomp-td-message-handler))
  (statsd-inc "intertube.xr-stomp-message")
  (when (and (cdr (assoc :descr body))
             (position #\* (cdr (assoc :descr body))))
    ;; FIXME(eta): hotpatch to prevent asterisks from getting in
    (return-from stomp-td-message-handler))
  (let ((old-berth (cdr (assoc :from body))))
    (when old-berth
      (let ((old-headcode (red:getdel
                           (format nil "berth:Q0:~A" old-berth)))
            (ts (+ (/ (parse-integer (cdr (assoc :time body))) 1000)
                   2208988800)))
        ;; FIXME(eta): We should check to see if the train exists elsewhere, too.
        (when (and old-headcode (eql message-type :+CB-MSG+))
          (statsd-inc "intertube.xr-berth-cancel")
          (log:info "in berth ~A, ~A cancelled" old-berth old-headcode)
          (enqueue-td-deletion old-headcode old-berth ts)))))
  ;; this is so ridiculously stupid and yet also excellent
  (when (member message-type '(:+CA-MSG+ :+CC-MSG+))
    (let* ((berth (cdr (assoc :to body)))
           (old-berth (cdr (assoc :from body))) ; note can be NIL
           (descr (red:get (format nil "X-track-desc-~A" berth)))
           (mpt (toposort-model-point "X" berth))
           (neighbour (when mpt
                        (or
                         (toposort-model-neighbouring-station "X" mpt :forward)
                         (toposort-model-neighbouring-station "X" mpt :backward))))
           (dir-descr (or (second neighbour) descr))
           (headcode (cdr (assoc :descr body)))
           (old-headcode
             (red:getset (format nil "berth:Q0:~A" berth)
                         headcode))
           (last-headcode
             (red:get (format nil "berth-last:Q0:~A" berth)))
           (rescue-headcode (or old-headcode last-headcode))
           (last-entry (redis-last-cpk (format nil "X-train-~A" headcode)))
           (ts (+ (/ (parse-integer (cdr (assoc :time body))) 1000)
                   2208988800))
           (fake-train (make-instance 'trackernet-train)))
      ;; don't have berths stick around for longer than the trains in them
      (red:expire (format nil "berth:Q0:~A" berth)
                  *xr-train-expiry-secs*)
      (statsd-gauge "intertube.xr-lag" (- (get-universal-time) ts))
      (when (member berth *ignored-berths*
                    :test #'string=)
        (log:info "Ignoring berth step (type ~A) into ~A as requested"
                  message-type berth)
        (return-from stomp-td-message-handler))
      ;; HACK(eta): clobber any existing entry for this timestamp, in order
      ;;            to deal with really fast berth stepping
      (red:zremrangebyscore (format nil "X-train-~A" headcode) ts ts)
      (when (and last-entry old-berth
                 (not (string= (track-code (cadr last-entry)) old-berth)))
        (log:warn "train ~A jumped from berth ~A to ~A (expected from ~A)!"
                  headcode (track-code (cadr last-entry))
                  berth old-berth)
        (statsd-inc "intertube.xr-berth-jump")
        (when (string= (red:get (format nil "berth:Q0:~A" old-berth))
                                headcode)
          (red:del (format nil "berth:Q0:~A" old-berth))))
      (when old-headcode
        (statsd-inc "intertube.xr-berth-clobber")
        (log:warn "in berth ~A, clobbered ~A with ~A" berth old-headcode headcode))
      ;;(format t "xr (~A): ~A~%" ts body)
      ;; If there's a TRUST activation, rename it for the live train code
      ;; to find (and so we don't use it twice):
      ;; FIXME(eta): copypasta
      (when (red:exists (format nil "trust:~A" headcode))
        (log:info "train ~A has corresponding TRUST ID ~A, using"
                  headcode
                  (cdr (assoc :trust-id
                              (cpk-unbase64 (red:get (format nil "trust:~A" headcode))))))
        (red:rename (format nil "trust:~A" headcode)
                    (format nil "trust-used:~A" headcode))
        (red:expire (format nil "trust-used:~A" headcode)
                    300))
      (loop
        for thing in '(lcid dest-code order direction
                       depart-time depart-interval departed
                       time-to is-stalled)
        do (setf (slot-value fake-train thing) ""))
      (setf (slot-value fake-train 'train-id) headcode)
      (setf (slot-value fake-train 'seconds-to) 0)
      (setf (slot-value fake-train 'location-desc)
            (or descr
                (format nil "berth ~A" berth)))
      (setf (slot-value fake-train 'destination-desc)
            (or (extract-destination-from-xr-headcode headcode)
                (cond
                  ((or (search "eastbound" dir-descr)
                       (search "(E)" dir-descr))
                   "Eastbound (core)")
                  ((or (search "westbound" dir-descr)
                       (search "(W)" dir-descr))
                   "Westbound (core)"))))
      (setf (slot-value fake-train 'track-code) berth)
      (setf (slot-value fake-train 'line) "X")
      (setf (slot-value fake-train 'set-no) "000")
      (setf (slot-value fake-train 'trip-no) "0")
      (redis-train ts "X" (if (eql message-type :+CA-MSG+)
                              "X-XCA"
                              "X-XCC")
                   fake-train)
      (when (and
             (eql message-type :+CC-MSG+)
             rescue-headcode
             ;; FIXME(eta): why not??
             (not (equal rescue-headcode headcode))
             (red:exists (format nil "X-train-~A" rescue-headcode)))
        (log:info "interpose in berth ~A links ~A to ~A, rescuing"
                  berth rescue-headcode headcode)
        (when last-entry
          (log:warn "...but destination train ~A already existed, aborting!" headcode)
          (return-from stomp-td-message-handler))
        (statsd-inc "intertube.xr-rescue")
        (rescue-train (format nil "X-train-~A" rescue-headcode)
                      (format nil "X-train-~A" headcode))))))

(defvar *current-stomp-td-backoff* 1)

;; NOTE(eta): So cl-stomp just doesn't conform to the specification at all.
;;            Here's a function that actually does the thing.
(defun stomp-ack-if-necessary (frame)
  (let ((ack-id (stomp::get-header frame "ack"))
        (message-id (stomp::get-header frame "message-id")))
    ;; NOTE(eta): Sometimes we get malformed messages; acking these will
    ;;            result in an error. Sigh.
    (when (and ack-id *stomp-td*
               (not (equal message-id "null")))
      (stomp::sending-frame (*stomp-td* frame "ACK"
                                        "id" ack-id)))))

;; "All timestamp values are in milliseconds since the UNIX epoch.
;; But it appears that in certain fields this is incorrect during British
;; Summer Time (UK DST) and is actually one hour in the future. You will
;; need to subtract one hour in order for the times to be valid."
(defun trust-ts-to-universal-ts (ts &key (fuckedp nil))
  "Translate a TRUST timestamp to universal time."
  (let* ((ts (+ (/ (parse-integer ts) 1000)
                 2208988800)) ; UNIX time difference
         (daylight-p (nth-value 7 (decode-universal-time ts))))
    (if (and daylight-p fuckedp)
        (- ts 3600)
        ts)))

;; We use TRUST only for getting Train Activation messages, so we can tell
;; if a headcode is Crossrail-related (and, if so, what schedule it's for).
(defun stomp-trust-message-handler (msg)
  "Handles a TRUST message from NROD."
  (let ((msg-type (cdr (assoc :msg--type
                              (cdr (assoc :header msg))))))
    ;; FIXME(eta): We should probably also do Train Cancellation.
    (unless (equal msg-type "0001") ; Train Activation
      (return-from stomp-trust-message-handler)))
  (let* ((body (cdr (assoc :body msg)))
         (trust-id (cdr (assoc :train--id body)))
         (schedule-id (cdr (assoc :train--uid body)))
         (schedule-type (cdr (assoc :schedule--type body)))
         ;; "Note: There is a bug that causes this field to be populated
         ;; incorrectly. The value O should be P and P should be O."
         (real-schedule-type
           (cond
             ((string= schedule-type "P") "O")
             ((string= schedule-type "O") "P")
             (t schedule-type)))
         (toc-id (cdr (assoc :toc--id body)))
         (depart-ts
           (trust-ts-to-universal-ts
            (cdr (assoc :origin--dep--timestamp body))))
         (headcode (cdr (assoc :schedule--wtt--id body)))
         (headcode-4 (subseq headcode 0 4)))
    (unless (equal toc-id "33") ; XR
      (log:warn "TRUST activation received for non-XR TOC ~A" toc-id)
      (return-from stomp-trust-message-handler))
    (log:info "TRUST activation for ~A as ~A, schedule ~A/~A" headcode trust-id schedule-id real-schedule-type)
    (when (red:exists (format nil "trust:~A" headcode-4))
      (when (string= schedule-type "C")
        ;; This can happen when you've got a planned cancellation overridden
        ;; by a VSTP'd reinstatement.
        (log:warn "attempted to clobber ~A with a cancellation schedule, let's not" headcode-4)
        (return-from stomp-trust-message-handler))
      (log:warn "~A already activated, clobbering!" headcode-4))
    (red:setex (format nil "trust:~A" headcode-4)
               ;; max(30 mins, 1 hour after the train's scheduled departure)
               (max 1800
                    (+ (- depart-ts (get-universal-time)) 3600))
               (cpk-base64
                `((:trust-id . ,trust-id)
                  (:creation-ts
                   . ,(trust-ts-to-universal-ts
                       (cdr (assoc :creation--timestamp body))))
                  (:depart-ts . ,depart-ts)
                  (:schedule-id . ,schedule-id)
                  (:schedule-type . ,real-schedule-type)
                  (:schedule-origin-stanox
                   . ,(cdr (assoc :sched--origin--stanox body)))
                  (:schedule-start-date
                   . ,(cdr (assoc :schedule--start--date body))))))))

(defvar *last-stomp-td-frame* 0)

(defun stomp-td-frame-handler (frame)
  "Handles a STOMP frame from NROD."
  (let* ((destination (cadr
                       (assoc "destination"
                              (stomp:frame-headers frame)
                              :test #'string=)))
         (body (stomp:frame-body frame))
         (parsed))
    (unless (or
             (string= destination "TD_ALL_SIG_AREA")
             (string= destination "TRAIN_MVT_EX_TOC"))
      (log:warn "Unknown STOMP message on topic ~A: ~A" destination body)
      (return-from stomp-td-frame-handler))
    (when (> *current-stomp-td-backoff* 0)
      (irccat-put (format nil "intertube-scraper: connected to NROD"))
      (log:info "Received first NROD message; clearing backoff.")
      (setf *current-stomp-td-backoff* 0))
    (setf *last-stomp-td-frame* (get-universal-time))
    (handler-case
        (setf parsed (cl-json:decode-json-from-string body))
      (error (e)
        (log:warn "Failed to parse TD message: ~A~%body: ~A" e body)
        (stomp-ack-if-necessary frame)
        (return-from stomp-td-frame-handler)))
    (cond
      ((string= destination "TD_ALL_SIG_AREA")
       ;; Make sure we don't get ourselves into wobbles processing things
       ;; in the wrong order
       (setf parsed (stable-sort parsed
                                 (lambda (a b)
                                   (< (parse-integer
                                       (cdr (assoc :time (cdar a))))
                                      (parse-integer
                                       (cdr (assoc :time (cdar b))))))))
       (loop
         for msg in parsed
         do (stomp-td-message-handler (caar msg) (cdar msg))))
      ((string= destination "TRAIN_MVT_EX_TOC")
       (loop
         for msg in parsed
         do (stomp-trust-message-handler msg))))
    (stomp-ack-if-necessary frame)))

(defun stomp-td-error-handler (err)
  (incf *current-stomp-td-backoff*)
  (statsd-inc "intertube.xr-error")
  (irccat-put
   (format nil "~A ~A"
           (if *staging-mode-p*
               "%ORANGEintertube-staging STOMP error frame%NORMAL:"
               "%REDintertube-prod STOMP error frame%NORMAL:")
           (stomp:get-header err "message")))
  (log:error "Got an error frame from NROD: ~A" err)
  (stomp-td-stop))

(defun stomp-td-stop ()
  "Stop the currently active STOMP connection."
  (when *stomp-td*
    (stomp:stop *stomp-td*)
    (setf *stomp-td* nil)))

(defvar *stomp-subscription-id* 0)

;; Start a durable subscription, using the machine hostname as the
;; subscription name
(defmethod stomp::send :around (conn (frame stomp:frame))
  (when (string= (stomp:frame-name frame) "CONNECT")
    (stomp:set-header frame "client-id"
                      (format nil "~A-~A-~A"
                              (car *nrod-data-key*)
                              (machine-instance)
                              (red:get "xr:stomp-client-id"))))
  (when (string= (stomp:frame-name frame) "SUBSCRIBE")
    (let* ((destination (stomp:get-header frame "destination"))
           (suffix
             (cond
               ((string= destination "TD_ALL_SIG_AREA")
                "")
               ((string= destination "TRAIN_MVT_EX_TOC")
                "-trust-liz")
               (t
                destination)))
           (subscription-name (format nil "~A~A" (machine-instance) suffix)))
      ;; the new platform requires this, and so does the spec actually
      (stomp:set-header frame "id" (princ-to-string (incf *stomp-subscription-id*)))
      (stomp:set-header frame "activemq.subscriptionName" subscription-name)))
  (call-next-method))

(defvar *last-stomp-td-connection-start* 0)

(defun stomp-td-connect ()
  "Connect to the National Rail TD feeds over STOMP."
  (unless *nrod-data-key*
    (log:warn "No NROD keys available, not starting TD feed"))
  (when *nrod-data-key*
    (setf *stomp-td* (stomp:make-connection
                      *nrod-stomp-host*
                      *nrod-stomp-port*))
    (stomp:register *stomp-td* #'stomp-td-frame-handler "TD_ALL_SIG_AREA"
                    :client-ack? t)
    (stomp:register *stomp-td* #'stomp-td-frame-handler "TRAIN_MVT_EX_TOC"
                    :client-ack? t)
    (stomp:register-error *stomp-td* #'stomp-td-error-handler)
    (setf *last-stomp-td-connection-start* (get-universal-time))
    (sb-thread:make-thread
     (lambda ()
       (set-thread-name "stomp td")
       (redis:with-connection (:host *redis-host* :port *redis-port*)
         (handler-case
             (stomp:start *stomp-td*
                          :username (car *nrod-data-key*)
                          :passcode (cdr *nrod-data-key*)
                          :timeout 5)
           (serious-condition (e)
             (irccat-put
              (format nil "~A ~A"
                      (if *staging-mode-p*
                          "%ORANGEintertube-staging STOMP error%NORMAL:"
                          "%REDintertube-prod STOMP error%NORMAL:")
                      (substitute #\Space #\Newline
                                  (princ-to-string e))))
             (setf *stomp-td* nil)
             (log:warn "Failed to connect to NROD: ~A" e))))))))

(defun elizabeth-line-loop ()
  "Periodically checks the connection to Network Rail and restarts it if necessary."
  (loop
    (when (and *stomp-td* (slot-value *stomp-td* 'stomp::stream)
               (> *current-stomp-td-backoff* 0)
               (> (- (get-universal-time) *last-stomp-td-connection-start*)
                  7))
      (irccat-put (format nil "intertube-scraper: NROD connection timeout!"))
      (log:warn "Failed to connect to NROD within 7sec, retrying...")
      (stomp-td-stop))
    (when (and *stomp-td* (slot-value *stomp-td* 'stomp::stream)
               (eql *current-stomp-td-backoff* 0)
               (> (- (get-universal-time) *last-stomp-td-frame*)
                  90))
      (irccat-put (format nil "intertube-scraper: NROD 90 second timeout!"))
      (log:warn "Haven't heard from NROD in 90 seconds, reconnecting...")
      (stomp-td-stop))
    (unless (and *stomp-td* (slot-value *stomp-td* 'stomp::stream))
      (when *current-stomp-td-backoff*
        (let ((sleep-time (expt 2 *current-stomp-td-backoff*)))
          (log:warn "Sleeping ~As due to NROD backoff" sleep-time)
          (sleep sleep-time)))
      (irccat-put (format nil "intertube-scraper: reconnecting to NROD"))
      (log:info "Reconnecting to NROD...")
      (incf *current-stomp-td-backoff*)
      (stomp-td-connect))
    (sleep 10)))

(defun read-line-station-information (line-code cxml-source)
  (loop
    with event-type
    while (setf event-type (multiple-value-list (klacks:consume cxml-source)))
    while (first event-type)
    when (and (eql (first event-type) :start-element)
              (string= (third event-type) "S"))
      do (let ((name)
               (code))
           (klacks:map-attributes
            (lambda (uri lname qname value dtd-default)
              (declare (ignore uri qname dtd-default))
              (when (string= lname "Code")
                (setf code value))
              (when (string= lname "N")
                (setf name (subseq value 0 (1- (length value))))))
            cxml-source)
           (when (and name code)
             (red:set (format nil "code:~A:~A" line-code code) name)
             (format t "~A = ~A~%" code name)))))

(defun get-line-station-information (line-code)
  "Gets information on how 3-letter station codes map to names from the PredictionSummary endpoint for LINE-CODE."
  (multiple-value-bind (stream status-code)
      (drakma:http-request (format nil "~A/PredictionSummary/~A"
                                   *trackernet-base-url*
                                   line-code)
                           :user-agent "intertube/0.1 (intertube@eta.st)"
                           :want-stream t)
    (unless (eql status-code 200)
      (error "TrackerNet returned code ~A" status-code))
    (let ((source (cxml:make-source
                   (flexi-streams:flexi-stream-stream stream)
                   :buffering nil)))
      (unwind-protect
           (read-line-station-information line-code source)
        (close stream)))))

(defun get-all-line-station-information ()
  (dolist (code *lines*)
    (unless (string= code "X")
      (get-line-station-information code))))

(defun present-model-linearly (line start-point callback)
  (let ((visited (make-hash-table :test 'equal)))
    (labels ((recurse (point depth branch)
               (let ((visited-children)
                     (unvisited-children))
                 (setf (gethash point visited) t)
                 (loop
                   for child in
                             (gethash point
                                      (links (line-model-for line)))
                   if (gethash child visited)
                     do (push child visited-children)
                   else
                     do (push child unvisited-children))
                 (funcall callback depth branch point visited-children)
                 (if (eql (length unvisited-children) 1)
                     (recurse (first unvisited-children) depth branch)
                     (loop
                       for child in unvisited-children
                       for i from 0
                       do (recurse child (1+ depth) i))))))
      (recurse start-point 0 0))))

(defun add-smart-live-train-observation (live-train universal-ts observer data)
  "Update LIVE-TRAIN with the DATA (a fake TRACKERNET-TRAIN that actually comes from Network Rail). The Network Rail SMART berth offset database will be consulted to translate the movement."
  (with-slots (current-entry line-code smart-entry-history history) live-train
    (assert (equal line-code "X") () "Attempted to use SMART on a non-XR train!")
    (unless current-entry
      (exchange-current-entry
       live-train
       (make-instance 'train-transit
                      :universal-ts universal-ts
                      :end-ts universal-ts
                      :track-codes (list))))
    (unless (or
             (equal (train-id data) (trackernet-id live-train)))
      (add-live-train-entry
       live-train
       (make-instance 'train-rescue
                      :new-key (train-id data)
                      :universal-ts universal-ts))
      (when (actually-live live-train)
        (maybe-update-trust-data live-train (train-id data) universal-ts)))
    (unless (equal (destination-desc data) (destination live-train))
      (add-live-train-entry
       live-train
       (make-instance 'train-destination-change
                      :destination (destination-desc data)
                      :universal-ts universal-ts)))
    (let* ((previous-berths
             (mapcar (lambda (x)
                       (subseq x 3))
                     (track-codes current-entry)))
           ;; HACK(eta): Provide previous area information so that we can
           ;;            treat an interpose in a different area as a cancel,
           ;;            and therefore register it correctly with SMART.
           (previous-area
             (when (track-codes current-entry)
               (subseq (first (track-codes current-entry)) 0 2)))
           (step-type (subseq observer 3 5)) ; CB/CA/CC
           ;; berth of the form Q1·BERTH
           (area (subseq (track-code data) 0 2))
           (to-berth (subseq (track-code data) 3))
           (ops (smart-operations-for-step area step-type to-berth previous-berths
                                           :previous-area previous-area)))
      (unless (or
               (equal (td-area live-train) area)
               (typep (first history) 'train-td-change))
        (add-live-train-entry
         live-train
         (make-instance 'train-td-change
                        :td-area area
                        :universal-ts universal-ts)))
      (if ops
          ;; We got a SMART result, so we have to make a new entry for this
          ;; regardless!
          (dolist (op ops)
            (destructuring-bind
                (event station-name pfm offset description) op
              (let ((station-stop-p
                      (typep current-entry 'train-station-stop))
                    (offset-ts (+ universal-ts offset)))
                ;; NOTE(eta): hackily add SMART-based description here to bodge
                ;;            the model into hopefully working
                (when (actually-live live-train)
                  (red:set (format nil "~A-track-desc-~A"
                                   line-code (track-code data))
                           description)
                  (when (and station-name (eql event :arrive))
                    (tph-register-station-call "X" station-name pfm
                                               (unique-id live-train) universal-ts)))
                (exchange-current-entry
                 live-train
                 (if (eql event :arrive)
                     (make-instance 'train-station-stop
                                    :station-name station-name
                                    :platform pfm
                                    :universal-ts offset-ts
                                    :end-ts offset-ts
                                    :description description
                                    :track-codes (list (track-code data)))
                     (make-instance 'train-transit
                                    :universal-ts offset-ts
                                    :end-ts offset-ts
                                    :description description
                                    :track-codes (list (track-code data)))))
                (unless (or
                         station-stop-p
                         (eql event :arrive))
                  (add-live-train-entry
                   live-train
                   (make-instance 'train-station-stop
                                  :station-name station-name
                                  :platform pfm
                                  :universal-ts offset-ts
                                  :end-ts offset-ts
                                  :track-codes (list)))))))
          ;; We didn't get a SMART result; add the track code, but don't
          ;; add any timing information (!)
          (progn
            ;; NOTE(eta): Setting this based on the current description is
            ;;            brittle and prone to errors being carried through.
            ;;            Just set it to something nondescript and let the
            ;;            toposorter figure it out.
            (when (actually-live live-train)
              (red:set (format nil "~A-track-desc-~A"
                               line-code (track-code data))
                       (format nil "berth ~A" (track-code data))))
            (update-current-entry live-train (track-code data) (end-ts current-entry)))))))

(defparameter *refdata-patterns*
  '("*-track-desc-*" ; track descriptions
    "code:*" ; LU inputdest codes
    )
  "Patterns identifying 'reference data' intertube needs to run.")

(defun dump-refdata (redis-host redis-port to-file)
  "Dump reference data to TO-FILE, from the provided database."
  (redis:with-connection (:host redis-host :port redis-port)
    (let ((keys (mapcan #'get-all *refdata-patterns*)))
      (log:info "Dumping ~A keys of reference data..." (length keys))
      (let ((ret
              (loop
                for key in keys
                for i from 0
                when (eql (rem i 1000) 0)
                  do (format t "~A/~A (~,2F%)~%"
                             i (length keys) (/ i (length keys)))
                collect (cons key (red:dump key)))))
        (with-open-file (out to-file
                             :direction :output)
          (format t "Writing to ~A..." to-file)
          (cl-json:encode-json ret out))))))

(defun load-refdata (redis-host redis-port from-file)
  "Load reference data from FROM-FILE and insert it into the provided database."
  (redis:with-connection (:host redis-host :port redis-port)
    (with-open-file (in from-file)
      (length
       (redis:with-pipelining
         ;; HACK(eta): disable CL-JSON lispification of symbols, because
         ;;            we need the raw keys!
         (let* ((cl-json:*json-identifier-name-to-lisp* #'identity)
                (data (cl-json:decode-json in)))
           (loop
             for (k . v) in data
             do (red:restore k 0 v))))))))

(defun map-unified-api-to-trackernet (unified-api-id)
  (cond
    ((equal unified-api-id "bakerloo") "B")
    ((equal unified-api-id "central") "C")
    ((equal unified-api-id "circle") "H")
    ((equal unified-api-id "district") "D")
    ((equal unified-api-id "elizabeth") "X")
    ((equal unified-api-id "hammersmith-city") "A")
    ((equal unified-api-id "jubilee") "J")
    ((equal unified-api-id "metropolitan") "M")
    ((equal unified-api-id "northern") "N")
    ((equal unified-api-id "piccadilly") "P")
    ((equal unified-api-id "victoria") "V")
    ((equal unified-api-id "waterloo-city") "W")
    (t nil)))

(defun fetch-tfl-status-information ()
  (multiple-value-bind (stream status-code)
      (drakma:http-request *tfl-status-url*
                           :user-agent "intertube/0.1 (intertube@eta.st)"
                           :external-format-out :utf-8
                           :want-stream t)
    (unwind-protect
         (let ((ret (make-hash-table :test 'equal)))
           (unless (eql status-code 200)
             (error "TfL Unified API returned ~A for status: ~A"
                    status-code
                    (read-line stream)))
           (dolist (line (cl-json:decode-json stream))
             (alexandria:when-let
                 ((line-code (map-unified-api-to-trackernet
                              (cdr (assoc :id line))))
                  (highest-severity 100))
               (dolist (status (cdr (assoc :line-statuses line)))
                 (let ((severity (cdr (assoc :status-severity status)))
                       (description (cdr (assoc :status-severity-description status)))
                       (reason (cdr (assoc :reason status))))
                   (when (and (not (equal description "Good Service"))
                              (< severity highest-severity))
                     (setf highest-severity severity)
                     (setf (gethash line-code ret)
                           (list severity description reason)))))))
           ret)
      (close stream))))

(defun tfl-status-information-loop ()
  (redis:with-connection ()
    (loop
      (handler-case
          (let ((info (fetch-tfl-status-information)))
            (redis-raw-set-cpk "line-status" info)
            (red:expire "line-status" 120)
            (sleep 60))
        (serious-condition (c)
          (log:error "Failed to update line status: ~A~%" c)
          (sleep 15))))))

(defun tfl-line-status-for (line-code)
  (let ((status-ht (or
                    (ignore-errors (redis-raw-get-cpk "line-status"))
                    (make-hash-table))))
    (values-list (gethash line-code status-ht))))

;; NOTE(eta): Backwards compat for when SMART records were a thing; remove in a few days :p
(defmethod conspack:decode-object ((class (eql 'smart-record)) alist &key &allow-other-keys))
