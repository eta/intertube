(in-package :intertube-web)

(setf (cl-who:html-mode) :html5)

(defparameter *title* "intertube")
(defparameter *base-url* "https://intertube.eta.st")
(defparameter *code-version*
  (or
   (ignore-errors
    (string-right-trim
     '(#\Newline #\Tab #\Space)
     (uiop:run-program
      "git describe --always --dirty"
      :output :string)))
   "unknown"))
(defvar *phase-banner* nil)

(defun with-rendering-context (func &key title meta-props)
  (let ((whole-title
          (if title
              (format nil "~A · ~A"
                      title *title*)
              *title*)))
    (who:with-html-output-to-string (*standard-output* nil
                                     :prologue t)
      (:html
       :lang "en"
       (:head
        (:meta
         :charset "utf-8")
        (:title
         (who:esc whole-title))
        (:meta
         :name "viewport"
         :content "width=device-width, initial-scale=1, viewport-fit=cover")
        (:meta
         :name "theme-color"
         :content *accent-colour*)
        (:meta
         :name "twitter:card"
         :content "summary")
        (:link
         :rel "apple-touch-icon"
         :sizes "180x180"
         :href "/apple-touch-icon.png")
        (:link
         :rel "icon"
         :type "image/png"
         :sizes "32x32"
         :href "/favicon-32x32.png")
        (:link
         :rel "icon"
         :type "image/png"
         :sizes "16x16"
         :href "/favicon-16x16.png")
        (:link
         :rel "manifest"
         :href
         (concatenate
          'string
          "/site.webmanifest?v="
          (if trackernet::*staging-mode-p*
              (format nil "dev-~A" (get-universal-time))
              *code-version*)))
        (:meta
         :name "apple-mobile-web-app-title"
         :content "intertube")
        (:meta
         :name "application-name"
         :content "intertube")
        (:meta
         :name "msapplication-TileColor"
         :content "#da532c")
        (unless (assoc "og:title" meta-props :test #'string=)
          (who:htm
           (:meta
            :name "og:title"
            :content (or title "intertube"))))
        (unless (assoc "og:url" meta-props :test #'string=)
          (who:htm
           (:meta
            :name "og:url"
            :content (format nil "~A~A" *base-url*
                             (hunchentoot:request-uri*)))))
        (unless (assoc "og:site_name" meta-props :test #'string=)
          (who:htm
           (:meta
            :name "og:site_name"
            :content "intertube")))
        (unless (assoc "og:type" meta-props :test #'string=)
          (who:htm
           (:meta
            :name "og:type"
            :content "website")))
        (loop
          for (k . v) in meta-props
          do (who:htm
              (:meta
               :name k
               :content v)))
        (:link
         :rel "stylesheet"
         :href
         (concatenate
          'string
          "/styles.css?v="
          (if trackernet::*staging-mode-p*
              (format nil "dev-~A" (get-universal-time))
              *code-version*))))
       (:body
        :class "govuk-template__body"
        (:a
         :href "#main-content"
         :class "govuk-skip-link"
         "Skip to main content")
        (:header
         :class "govuk-header"
         :role "banner"
         (:div
          :class "govuk-header__container govuk-width-container"
          :style (format nil "border-bottom: 10px solid ~A;"
                         *accent-colour*)
          (:div
           :class "govuk-header__logo"
           (:a
            :href "/"
            :class "govuk-header__link govuk-header__link--homepage"
            (:img
             :src "/logo.svg"
             :class "govuk-header__logotype-crown-fallback-image"
             :width "36"
             :height "32")
            (:span
             :class "govuk-header__logotype-text"
             "intertube"
             (when trackernet::*staging-mode-p*
               (who:fmt " (dev)")))))))
        (:div
         :class "govuk-width-container"
         (:div
          :class "govuk-phase-banner freshness-indicator"
          (:p
           :class "govuk-phase-banner__content"
           (:span
            :class "govuk-phase-banner__text"
            (:span
             :id "freshness-circle"
             "●")
            (:span
             :id "freshness-text"
             "Updated: "
             (who:str (universal-time-hms (get-universal-time))))
            (:a
             :class "govuk-link"
             :id "freshness-refresh"
             :href "javascript:location.reload()"
             "Refresh"))))
        (when *phase-banner*
          (who:htm
           (:div
            :class "govuk-phase-banner"
            (:p
             :class "govuk-phase-banner__content"
             (:span
              :class "govuk-phase-banner__text"
              (who:str *phase-banner*))))))
         (:main
          :class "govuk-main-wrapper"
          :id "main-content"
          :role "main"
          (alexandria:when-let
              ((site-notice
                (ignore-errors (red:get "site-notice"))))
            (who:htm
             (:div
              :class "govuk-error-summary"
              (:div
               :role "alert"
               (:div
                :class "govuk-error-summary__body"
                (:p
                 :class "govuk-body !-no-bottom-margin"
                 (who:str site-notice)))))))
          (who:str (funcall func))))
        (:footer
         :class "govuk-footer"
         :role "contentinfo"
         (:div
          :class "govuk-width-container"
          (:div
           :class "govuk-footer__meta"
           (:div
            :class "govuk-footer__meta-item govuk-footer__meta-item--grow"
            (:span
             :class "govuk-footer__licence-description"
             "Powered by "
             (:a
              :class "govuk-footer__link"
              :href "https://tfl.gov.uk/info-for/open-data-users/"
              "open data from Transport for London")
             "; track layout information generated internally. "
             "Elizabeth line information from Network Rail "
             (:a
              :class "govuk-footer__link"
              :href "https://www.networkrail.co.uk/data-feeds/terms-and-conditions"
              "under license")
             ". "
             "Site design using the "
             (:a
              :class "govuk-footer__link"
              :href "https://github.com/alphagov/govuk-frontend"
              "GOV.UK Design System")
             ", under the terms of the "
             (:a
              :class "govuk-footer__link"
              :href "https://github.com/alphagov/govuk-frontend/blob/master/LICENSE.txt"
              "MIT License")
             ". "
             "Some icons by "
             (:a
              :class "govuk-footer__link"
              :href "https://fontawesome.com/"
              "Font Awesome")
             " and "
             (:a
              :class "govuk-footer__link"
              :href "https://twemoji.twitter.com/"
              "Twemoji")
             ", used under "
             (:a
              :class "govuk-footer__link"
              :href "https://creativecommons.org/licenses/by/4.0/deed.en"
              "CC BY 4.0")
             ". "
             "Font is "
             (:a
              :class "govuk-footer__link"
              :href "https://rsms.me/inter/"
              "Inter")
             ". "
             "Logo design by "
             (:a
              :class "govuk-footer__link"
              :href "https://arturs.co.uk/"
              "Arturs Dobrecovs")
             ". "
             "Please don't scrape or re-use information provided here "
             "without express consent. All rights reserved. "
             "Served by "
             (:a
              :class "govuk-footer__link"
              :href "https://git.eta.st/eta/intertube"
              "intertube-web")
             " version "
             (:span
              :class "!-monospace"
              (who:esc *code-version*))
             "."
             (:span
              :class "!-float-right"
              (:a
               :class "govuk-footer__link"
               :href "https://eta.st/"
               "an eta project"))))))))))))

(defmacro render ((&key title meta-props) &body body)
  `(with-rendering-context
     (lambda ()
       (who:with-html-output-to-string (*standard-output* nil
                                        :prologue nil)
         ,@body))
     :title ,title
     :meta-props ,meta-props))

(defun relative-redirect (target &key (code hunchentoot:+http-moved-temporarily+))
  (check-type code (integer 300 399))
  (setf (hunchentoot:header-out :location) target
        (hunchentoot:return-code*) code)
  (hunchentoot:abort-request-handler))

(defvar *server-timing-events* '())

(defun server-timing-start (event-name)
  (push (list event-name (get-internal-real-time) nil)
        *server-timing-events*))

(defun server-timing-done (event-name)
  (push (list event-name (get-internal-real-time) t)
        *server-timing-events*))

(defmacro with-server-timing ((event-name) &body body)
  `(progn
     (server-timing-start ,event-name)
     (unwind-protect
          (progn ,@body)
       (server-timing-done ,event-name))))

(defun server-timing-generate ()
  (loop
    for (event-name time endp) in (reverse *server-timing-events*)
    unless endp
      append (alexandria:when-let
                 ((end-time
                   (second
                    (car
                     (member-if
                      (lambda (ent)
                        (and (string= (car ent) event-name)
                             (third ent)))
                      *server-timing-events*)))))
               (list (list event-name
                           (*
                            (/ (- end-time time)
                               internal-time-units-per-second)
                            1000))))))

(defun server-timing-formatted ()
  (format nil "~{~A~^, ~}" (mapcar
                            (lambda (ent)
                              (format nil "~A;dur=~F"
                                      (first ent) (second ent)))
                            (server-timing-generate))))

(hunchentoot:define-easy-handler (css :uri "/styles.css") ()
  (serve-embedded-file *website-css* "text/css"))

(hunchentoot:define-easy-handler (robots :uri "/robots.txt") ()
  (serve-embedded-file *robots-txt* "text/plain"))

(defun html-rgb-colour (r g b)
  (format nil "#~2,'0x~2,'0x~2,'0x" r g b))

(defun uri-for-lt (lt &key nojourney)
  (easy-routes:genurl 'route-live-train
                      :tid (subseq (trackernet::unique-id lt)
                                   #.(length "train_"))))

(defun uri-for-lt-journey (lt)
  (easy-routes:genurl 'route-live-train-journey
                      :tid (subseq (trackernet::unique-id lt)
                                   #.(length "train_"))))

(defun uri-for-station (line-code station-name)
  (let* ((line-code (if (equal line-code "A") "H" line-code))
         (slug (trackernet::station-slug-for line-code station-name)))
    (when slug
      (easy-routes:genurl 'route-station
                          :line line-code
                          :station slug))))

(defun universal-time-hhmm (time)
  (local-time:with-decoded-timestamp
      (:minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D~2,'0D" h m)))

(defun uri-for-station-timed (line-code station-name around-ts)
  (let ((slug (trackernet::station-slug-for line-code station-name))
        (date (trackernet::get-iso-8601-for-ts-london around-ts))
        (time (universal-time-hhmm around-ts)))
    (when slug
      (easy-routes:genurl 'route-station-timed
                          :line line-code
                          :station slug
                          :date date
                          :time time))))

(defun uri-for-station-event (line-code ltid event)
  (let* ((station-name (trackernet::station-name event))
         (around-ts (trackernet::universal-ts event))
         (base-uri (uri-for-station-timed line-code station-name around-ts)))
    (when base-uri
      (if ltid
          (format nil "~A#~A-~A" base-uri (subseq ltid
                                                  #.(length "train_"))
                  (universal-time-hhmm around-ts))
          base-uri))))

(defparameter *tube-line-data*
  '(("Bakerloo" "B" (178 99 0) t t)
    ("Central" "C" (220 36 31) t t)
    ("Circle" "H" (255 211 41) t t)
    ("District" "D" (0 125 50) t t)
    ("Elizabeth" "X" (105 80 161) nil t)
    ;; This doesn't actually exist, but we pretend it does to make things
    ;; present properly.
    ("Hammersmith & City" "A" (244 169 190) t t)
    ("Jubilee" "J" (161 165 167) t t)
    ("Metropolitan" "M" (155 0 88) t t)
    ("Northern" "N" (0 0 0) t t)
    ("Piccadilly" "P" (0 25 168) t t)
    ("Victoria" "V" (0 152 216) t t)
    ("Waterloo & City" "W" (147 206 186) t t)
    ("Subsurface" "S" (255 79 0) t nil))
  "A list of Tube line data, in the format (NAME TRACKERNET-CODE RGB-COLOUR)")

(defun tube-line-by-code (code)
  (find-if (lambda (d) (string= (second d) code))
           *tube-line-data*))

(defun line-model-data-for (code)
  (cdr
   (find-if (lambda (d) (string= (first d) code))
            *line-model-data*)))

(defun available-model-directions (code)
  (mapcar (lambda (item)
            (cons (first item) (second item)))
          (remove-if
           (lambda (item)
             (eql (third item) :header))
           (line-model-data-for code))))

(defun default-model-direction (code)
  (if (string= code "D")
      "upminster-ealing"
      (caar (available-model-directions code))))

(defun model-data-by-code-and-direction (code direction)
  (cdr
   (find-if (lambda (md) (string= (first md) direction))
            (cdr
             (find-if (lambda (d) (string= (first d) code))
                      *line-model-data*)))))

(defun all-model-line-stops (code)
  (remove-duplicates
   (sort
    (loop
      for (name descr . stops) in (line-model-data-for code)
      append (mapcar
              (lambda (stop)
                (list (car stop) (cdr stop)
                      name
                      (cdr (assoc name
                                  (cdr (assoc code *dropdown-direction-map*
                                              :test #'string=))
                                  :test #'string=))))
              (remove-if
               (lambda (item)
                 (eql (car item) :html))
               stops)))
    (lambda (i1 i2)
      ;; FIXME: this is very hacky :P
      (string< (princ-to-string i1) (princ-to-string i2))))
   :test (lambda (i1 i2)
           (equal (subseq i1 0 2) (subseq i2 0 2)))))

(defparameter *model-line-stops*
  (mapcar (lambda (entry)
            (cons (car entry)
                  (all-model-line-stops (car entry))))
          *line-model-data*))

(defun live-trains-on-line (code)
  (trackernet::live-trains-on-line code))

(defun live-train-wtt-id (lt &aux (ident (trackernet::train-identity lt)))
  (when (and ident (first ident))
    (first ident)))

(defun group-lts-by-destination (train-list)
  (let ((dests (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (let ((dest (or (trackernet::destination lt)
                         "???")))
           (unless (gethash dest dests)
             (setf (gethash dest dests) nil))
           (push lt (gethash dest dests))))
    (sort
     (alexandria:hash-table-alist dests)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun group-lts-by-model-point (train-list)
  (let ((ret (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (alexandria:when-let
             ((model-point (trackernet::model-point lt)))
           (unless (gethash model-point ret)
             (setf (gethash model-point ret) nil))
           (push lt (gethash model-point ret))))
    ret))

(defun group-lts-by-model-station (train-list)
  (let ((dests (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (let ((dest (trackernet::toposort-model-previous-station
                      (trackernet::line-code lt)
                      (trackernet::model-point lt)))
               (cur (trackernet::current-entry lt)))
           ;; FIXME: also very hacky
           (when (and dest
                      (eql (third dest) 0)
                      (not (typep cur 'trackernet::train-station-stop)))
             (setf (third dest) 1))
           ;; FIXME: this is quite hacky; the model and the
           ;; live train thing shouldn't disagree like this! (but they do)
           (when (and
                  (or (not dest)
                      (not (eql (third dest) 0)))
                  (typep cur 'trackernet::train-station-stop)
                  (trackernet::platform cur))
             (setf dest
                   (list
                    (trackernet::station-name cur)
                    (trackernet::platform cur)
                    0)))
           ;; FIXME: a tad hacky
           (when (and dest (> (third dest) 1))
             (setf (third dest) 1))
           ;; FIXME: more hacks for XR
           (when (or (search "(E)" (second dest))
                     (search "(W)" (second dest)))
             (setf (second dest) (subseq (second dest) 0 1)))
           (unless (gethash dest dests)
             (setf (gethash dest dests) nil))
           (push lt (gethash dest dests))))
    dests))

(defun train-objects-on-line (code)
  (let ((active (trackernet::trains-active-on-line code)))
    (mapcan (lambda (active-key)
              (let* ((train-id (subseq active-key #.(length "X-active-")))
                     (whole-key (format nil "~A-train-~A" code train-id))
                     (data (trackernet::redis-last-cpk whole-key)))
                (when data
                  (list (cons whole-key data)))))
            active)))

(defun group-lts-by-headcode (train-list)
  (let ((wtts (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (alexandria:when-let
             ((wtt (trackernet::trackernet-id lt)))
           (setf (gethash wtt wtts)
                 (cons
                  lt
                  (gethash wtt wtts)))))
    (sort
     (alexandria:hash-table-alist wtts)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun group-lts-by-leading-car (train-list)
  (let ((wtts (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (alexandria:when-let
             ((wtt (trackernet::leading-car-no lt)))
           (setf (gethash wtt wtts)
                 (cons
                  lt
                  (gethash wtt wtts)))))
    (sort
     (alexandria:hash-table-alist wtts)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun group-lts-by-wtt (train-list)
  (let ((wtts (make-hash-table :test 'equal)))
    (loop
      for lt in train-list
      do (alexandria:when-let
             ((wtt (first (trackernet::train-identity lt))))
           (setf (gethash wtt wtts)
                 (cons
                  lt
                  (gethash wtt wtts)))))
    (sort
     (alexandria:hash-table-alist wtts)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun group-trains-by-wtt (train-list)
  (let ((wtts (make-hash-table :test 'equal)))
    (loop
      for (key . (observer train-obj)) in train-list
      do (alexandria:when-let
             ((wtt (trackernet::set-no train-obj)))
           (setf (gethash wtt wtts)
                 (cons
                  (list key train-obj)
                  (gethash wtt wtts)))))
    (sort
     (alexandria:hash-table-alist wtts)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun group-trains-by-destination (train-list)
  (let ((dests (make-hash-table :test 'equal)))
    (loop
      for (key . (observer train-obj)) in train-list
      do (let ((dest (or (trackernet::destination-desc train-obj)
                         "???")))
           (unless (gethash dest dests)
             (setf (gethash dest dests) nil))
           (push (list key observer train-obj) (gethash dest dests))))
    (sort
     (alexandria:hash-table-alist dests)
     (lambda (a b)
       (string< (car a) (car b))))))

(defun string-base64 (str)
  (qbase64:encode-bytes
   (flexi-streams:string-to-octets
    str)))

(defun universal-time-hms (time)
  (local-time:with-decoded-timestamp
      (:sec s :minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
      "~2,'0D:~2,'0D:~2,'0D" h m s)))

(defun mini-graphviz-stream (around-codes line-code)
  (let* ((proc
           (uiop:launch-program
            '("dot" "-Tpng")
            :input :stream
            :output :stream
            :element-type '(unsigned-byte 8)))
         (viz-in
           (flexi-streams:make-flexi-stream
            (uiop:process-info-input proc)
            :external-format :utf-8)))
    (unwind-protect
         (trackernet::write-mini-graphviz around-codes line-code :out viz-in)
      (close viz-in))
    (uiop:process-info-output proc)))

(defparameter *hmac-key* (crypto:random-data 32))

(defun hmac-sha256 (key data)
  "Computes the HMAC-SHA256 message authentication code for DATA, using the secret KEY."
  (let ((mac (crypto:make-mac :hmac key :sha256)))
    (crypto:update-mac mac data)
    (crypto:produce-mac mac)))

(defun trusted-serialize (data)
  "Given some DATA, serializes it into CONSPACK-encoded base64 and returns the data, plus an HMAC used to authenticate the data."
  (let* ((cpk-data (cpk:encode data))
         (hmac (hmac-sha256 *hmac-key* cpk-data)))
    (values
     (qbase64:encode-bytes cpk-data)
     (qbase64:encode-bytes hmac))))

(defun trusted-deserialize (data hmac)
  "Given two base64-encoded DATA and HMAC strings, deserializes and returns the data iff the HMAC validates. If it does not validate, returns NIL."
  (let* ((undata (subseq (qbase64:decode-string data) 0))
         (unhmac (qbase64:decode-string hmac))
         (should-be (hmac-sha256 *hmac-key* undata)))
    (when (equalp should-be unhmac)
      (cpk:decode undata))))

(defun format-duration (secs)
  (if (>= secs 60)
      (format nil "~Am ~As" (floor secs 60) (rem secs 60))
      (format nil "~As" secs)))

(defun format-duration-small (secs)
  (if (>= secs 60)
      (format nil "~Am" (floor secs 60))
      (format nil "~As" secs)))

(defun universal-time-train2 (time)
  (local-time:with-decoded-timestamp
      (:sec s :minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (format nil
            "~2,'0D~2,'0D~A" h m
            (cond
              ((>= s 45) "<small>¾</small>")
              ((>= s 30) "<small>½</small>")
              ((>= s 15) "<small>¼</small>")
              (t "")))))

(defun track-codes-from-history-entries (ents)
  (dolist (ent ents)
    (when (typep ent 'trackernet::train-transit)
      (return-from track-codes-from-history-entries
        (trackernet::track-codes ent))))
  nil)

(defun display-fraction-as-perc (frac)
  (format nil "~02D" (truncate (* frac 100))))

(defun display-branch (branch line-code)
  (who:with-html-output-to-string (*standard-output* nil
                                   :prologue nil)
    (:span
     :class
     (cond
       ;; Northern
       ((string= branch "Bank") "govuk-tag govuk-tag--turquoise")
       ((string= branch "Charing Cross") "govuk-tag govuk-tag--pink")
       ;; Circle
       ((string= branch "Circle") "govuk-tag govuk-tag--yellow")
       ((string= branch "H&C") "govuk-tag govuk-tag--pink")
       ((string= branch "Shuttle") "govuk-tag govuk-tag--orange")
       ;; Met
       ((string= branch "All stations") "govuk-tag govuk-tag--green")
       ((string= branch "Fast") "govuk-tag govuk-tag--pink")
       ((string= branch "Fast except Wembley") "govuk-tag govuk-tag--pink")
       ((string= branch "Semi-fast") "govuk-tag govuk-tag--purple")
       ((string= branch "Semi-fast except Wembley") "govuk-tag govuk-tag--purple")
       (t "govuk-tag govuk-tag--green"))
     (if (equal line-code "N")
         (who:fmt "~A branch" branch)
         (who:esc branch)))))

(defun describe-train-entry-location (ent line-code)
  "Get a description for the most recent track code in ENT, a TRAIN-TRANSIT entry."
  (or
   (trackernet::description ent)
   (cdr (assoc (first (trackernet::track-codes ent))
               (cached-track-suggestions line-code)
               :test #'string=))
   (format nil "Location unknown (~A)"
           (first (trackernet::track-codes ent)))))

(defun describe-train-history-entry (ent line-code)
  "Output a simple description of a train history event (or NIL), for use in a description for example."
  (cl-who:escape-string
   (typecase ent
     (trackernet::train-station-stop
      (format nil "at ~A~A since ~A"
              (trackernet::station-name ent)
              (when (trackernet::platform ent)
                (format nil " platform ~A" (trackernet::platform ent)))
              (trackernet::universal-time-railway
               (trackernet::universal-ts ent))))
     (trackernet::train-transit
      (format nil "in transit since ~A (\"~A\")"
              (trackernet::universal-time-railway
               (trackernet::universal-ts ent))
              (describe-train-entry-location ent line-code))))))

(defun display-train-history-entry (ent line-code &key ltid lastp fadein)
  (who:with-html-output-to-string (*standard-output* nil
                                   :prologue nil)
    (:li
     :class (concatenate 'string
                         (if lastp "train-history-last " "")
                         (if fadein "fadein " "")
                         "train-history-entry")
     :id (format nil "ent-~A" (trackernet::universal-ts ent))
     :title (who:escape-string (princ-to-string ent))
     (when (typep ent 'trackernet::train-station-stop)
       (who:htm
        (:span
         :class "history-entry-time"
         (who:str (universal-time-train2
                   (trackernet::universal-ts ent))))))
     (:span
      :class (concatenate 'string
                          "history-content"
                          (cond
                            ;; If it's a station stop, don't apply any
                            ;; special styling at all, so it uses the rest
                            ;; of the container alongside the time.
                            ((typep ent 'trackernet::train-station-stop)
                             "")
                            ;; If it's at the top and *not* a station stop
                            ;; (otherwise it'd be caught above), give it
                            ;; the whole width of the current status bit.
                            (lastp
                             " history-content-full-width")
                            ;; If we get here, it's a small grey italicised
                            ;; history line item.
                            (t
                             " history-content-small")))
      (typecase ent
        ;; FIXME(eta): this might be dodgy
        (trackernet::train-input-dest-change
         (return-from display-train-history-entry ""))
        (trackernet::train-td-change
         (who:htm
          (:span
           :class "history-td-change"
           (who:esc
            (cond
              ((string= (trackernet::td-area ent) "Q0")
               "Crossrail Core")
              ((string= (trackernet::td-area ent) "Q1")
               "Stratford IECC")
              ((string= (trackernet::td-area ent) "Q2")
               "Shenfield IECC")
              ((string= (trackernet::td-area ent) "Q4")
               "Liverpool Street IECC")
              ((string= (trackernet::td-area ent) "D1")
               "Reading IECC A")
              ((string= (trackernet::td-area ent) "D3")
               "Paddington Area IECC")
              ((string= (trackernet::td-area ent) "D4")
               "Hayes Area IECC")
              ((string= (trackernet::td-area ent) "D6")
               "Maidenhead Area IECC")
              ((string= (trackernet::td-area ent) "DA")
               "Acton Area IECC")
              (t (trackernet::td-area ent))))
           " is now signalling this train")))
        (trackernet::train-destination-platform-change
         (return-from display-train-history-entry ""))
        (trackernet::train-branch-change
         (if (trackernet::branch ent)
             (who:htm
              (:span
               :class "history-branch-change"
               (who:str (display-branch (trackernet::branch ent)
                                        line-code))
               " is now this train's branch"))
             (who:htm
              (:span
               :class "history-branch-lost"
               "lost train branch information"))))
        (trackernet::train-destination-change
         (who:htm
          (:span
           :class "history-dest-change"
           (:strong
            :class "history-dest"
            (who:str (trackernet::destination ent)))
           " is now this train's destination")))
        (trackernet::train-station-stop
         (who:htm
          (:span
           :class "history-station-stop"
           (:strong
            :class "history-station"
            (when lastp
              (who:str *govuk-go-arrow-standalone-svg*)
              (who:str " ")
              (who:htm
               (:span
                :class "atward"
                (if (and lastp (trackernet::ready-to-depart ent))
                    (who:str "leaving ")
                    (who:str "at ")))))
            (:a
             :href (uri-for-station-event line-code ltid ent)
             :class (format nil "govuk-link ~A"
                            (if (and
                                   (eql (trackernet::universal-ts ent)
                                        (trackernet::end-ts ent))
                                   (not lastp))
                              "station-calls-link-intermediate"
                              "station-calls-link"))
             (who:str (trackernet::station-name ent))))
           (:span
            :class "history-station-platform"
            (if (trackernet::platform ent)
              (who:htm
               (:span
                :class "!-no-phones"
                (when lastp
                  (who:str
                   (if (trackernet::ready-to-depart ent)
                       " ready to depart "
                       " now standing at ")))
                (if (or
                     (uiop:string-prefix-p "down"
                                           (trackernet::platform ent))
                     (uiop:string-prefix-p "up"
                                           (trackernet::platform ent)))
                    (who:esc (trackernet::platform ent))
                    (who:htm
                     (who:str " plat. ")
                     (:strong
                      :class "history-platform"
                      (who:str (trackernet::platform ent)))))
                " "))
              (when lastp
                (who:htm
                 (:span
                  :class "!-no-phones"
                  " here "))))
            (when (and
                   (not lastp)
                   (< (trackernet::end-ts ent)
                      (trackernet::universal-ts ent)))
              (who:htm
               (:span
                :class "history-station-nonstop"
                "nonstop")))
            (unless (or lastp
                        (<= (trackernet::end-ts ent)
                            (trackernet::universal-ts ent)))
              (who:str " for ")
              (who:htm
               (:span
                :class (concatenate
                        'string
                        "history-station-dwell "
                        (if (> (- (trackernet::end-ts ent)
                                   (trackernet::universal-ts ent))
                               (if (string= line-code "X") 180 90))
                            "history-dwell-tardy"
                            ""))
                (who:esc (format-duration
                          (- (trackernet::end-ts ent)
                              (trackernet::universal-ts ent)))))))))))
        (trackernet::train-transit
         (let ((progress (trackernet::toposort-model-station-progress line-code (first (trackernet::track-codes ent)))))
           (if
            (and progress lastp)
            (who:htm
             (:span
              :class "history-next-station-stop"
              ;; FIXME(eta): this is awful!
              (:span
               :class "history-entry-time"
               "next")
              (:span
               :class "history-station"
               (:a
                :href (uri-for-station-timed line-code (first (third progress)) (trackernet::universal-ts ent))
                :class "govuk-link station-calls-link"
                (who:esc (first (third progress)))))
              (when (second (third progress))
                (who:htm
                 (:span
                  :class "history-station-platform !-no-phones"
                  "estimated plat. "
                  (:strong
                   (who:esc (second (third progress))))))))
             (:span
              :class "history-progress-bar"
              (:span
               :class "history-progress-bar-elapsed"
               "T+"
               (if (string= line-code "X")
                   (who:htm
                    (:abbr
                     :title "Transit time not available for the Elizabeth line"
                     "???"))
                   (who:htm
                    (:span
                     :class "history-progress-bar-elapsed-seconds"
                     (who:esc (format-duration
                               (- (trackernet::end-ts ent)
                                   (trackernet::universal-ts ent))))))))
              (:progress
               :max "100"
               :value (format nil "~D" (floor (* (second progress) 100))))))
            (who:htm
             (:span
              :class "history-transit"
              (when lastp
                (who:htm
                 (:span
                  :class "history-transit-last"
                  (when lastp
                    (who:str *govuk-go-arrow-standalone-svg*)
                    (who:str " "))
                  (who:str (describe-train-entry-location ent line-code))
                  (who:str " "))))
              (unless (and lastp (trackernet::description ent))
                (who:htm
                 (:span
                  "in transit for "
                  (who:esc (format-duration
                            (- (trackernet::end-ts ent)
                                (trackernet::universal-ts ent))))))))))))
        (trackernet::train-trust-identity-change
         (return-from display-train-history-entry ""))
        (trackernet::train-rescue
         (who:htm
          (:span
           :class "history-rescue"
           "train identifier changed to "
           (:span
            :class "!-monospace"
            (who:esc (trackernet::new-key ent))))))
        (trackernet::train-car-change
         (if (trackernet::new-car-no ent)
             (who:htm
              (:span
               :class "history-car-change"
               "leading car identifier changed to "
               (:span
                :class "!-monospace"
                (who:esc (trackernet::new-car-no ent)))))
             (who:htm
              (:span
               :class "history-car-change"
               "lost leading car identifier"))))
        (trackernet::train-nr-schedule-change
         (if (trackernet::uid ent)
             (who:htm
              (:span
               :class "history-nr-schedule-change"
               "Network Rail timetable UID is now "
               (:a
                :href (format nil "https://www.realtimetrains.co.uk/service/gb-nr:~A/~A/detailed"
                              (string-left-trim '(#\Space)
                                                (trackernet::uid ent))
                              (trackernet::get-iso-8601-for-ts-london
                               (trackernet::universal-ts ent)))
                :class "govuk-link"
                (:strong
                 :class "govuk-tag govuk-tag--purple"
                 (who:esc (trackernet::uid ent))))))
             (who:htm
              (:span
               :class "history-nr-schedule-change"
               "lost Network Rail timetable information"))))
        (trackernet::train-identity-change
         (if (trackernet::wtt-id ent)
             (let* ((class (if (trackernet::wtt-trip ent)
                               "govuk-tag govuk-tag--purple"
                               "govuk-tag govuk-tag--green")))
              (who:htm
               (:span
                :class "history-ident-change"
                "assigned timetable ID "
                   (:strong
                    :class class
                    (who:esc (trackernet::wtt-id ent))
                    (when (trackernet::wtt-trip ent)
                      (who:str " ×")
                      (who:esc (trackernet::wtt-trip ent)))))))
             (who:htm
              (:span
               :class "history-ident-lost"
               "lost timetable assignment")))))))))

(defun universal-time-as-wtt-time (time)
  (local-time:with-decoded-timestamp
      (:minute m :hour h
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (list h m)))

(defun universal-time-as-wtt-runs-when (time)
  (local-time:with-decoded-timestamp
      (:day-of-week dow
       :timezone trackernet::+europe-london-tz+)
      (local-time:universal-to-timestamp time)
    (case dow
      (0 :sundays)
      (6 :saturdays)
      (t :weekdays))))

(defun maybe-display-wtt-data (line-code wtt-id trip-no last-ts)
  (unless (string= line-code "D")
    (return-from maybe-display-wtt-data))
  (alexandria:when-let*
      ((wtt-id (princ-to-string (parse-integer wtt-id)))
       (runs-when (universal-time-as-wtt-runs-when last-ts))
       (trip-no (ignore-errors (parse-integer trip-no)))
       (timetable
        (or
         (when trip-no
           (wtt::find-wtt-for wtt-id trip-no runs-when))
         (wtt::find-wtt-for-time
          wtt-id
          (universal-time-as-wtt-time last-ts)
          runs-when))))
    (who:with-html-output-to-string (*standard-output* nil
                                     :prologue nil)
      (:details
       :class "govuk-details"
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "View timetable "
         (who:fmt "~A (run ~A)"
                  (wtt::train-no timetable)
                  (wtt::trip-no timetable))))
       (:div
        :class "govuk-details__text"
        (:ul
         :class "govuk-list"
         (dolist (cpt (reverse (wtt::calling-points timetable)))
           (destructuring-bind
             (place-kwd h m s offset) cpt
            (who:htm
             (:li
              :class "train-history-entry"
              (:span
               :class "history-entry-time"
               (who:fmt "~2,'0D~2,'0D~A"
                        h m
                        (if (eql s 30)
                            "<small>½</small>"
                            "")))
              (:span
               :class "history-station-stop"
               (:span
                :class "history-station"
                (who:fmt "~A" (wtt::print-wtt-keyword place-kwd)))
               (when (> offset 0)
                 (who:htm
                  (:span
                   :class "history-station-platform"
                   (who:fmt "arrives ~A earlier"
                            (format-duration offset)))))))))))
        (if (wtt::to-form timetable)
            (who:htm
             (:p
              :class "govuk-body"
              "This train's next service begins at "
              (:strong
               (who:fmt "~2,'0D~2,'0D"
                        (first (wtt::to-form timetable))
                        (second (wtt::to-form timetable))))
              "."))
            (who:htm
             (:p
              :class "govuk-body"
              "This is the last timetable for this train.")))
        (:p
         :class "govuk-body"
         "This information was sourced from page "
         (:strong
          (who:fmt "~D" (wtt::from-page timetable)))
         " of the official Working Timetable."))))))

(defun display-trackernet-train (train line-code &key code ts reporter distance show-dest maybe-hide-codes)
  (who:with-html-output-to-string (*standard-output* nil
                                   :prologue nil)
    (:tr
     :class "train-desc govuk-table__row"
     (cond
       (ts
         (who:htm
          (:td
           :class "govuk-table__cell ts-field"
           (who:esc (universal-time-hms ts)))))
       (distance
        (who:htm
         (:td
          :class "govuk-table__cell distance-field !-monospace !-no-phones"
          (who:fmt "~,2F" distance))))
       (t
        (who:htm
         (:td ; Train ID
          :class "govuk-table__cell !-monospace !-no-phones"
          (who:esc (trackernet::train-id train))))))
     (unless (string= line-code "X")
       (who:htm
        (:td ; Set/trip number
         :class (if ts
                    "govuk-table__cell !-no-phones"
                    "govuk-table__cell")
         (let* ((trip-no (trackernet::trip-no train))
                (set-no (trackernet::set-no train))
                (class (if (string= trip-no "0")
                           "govuk-tag govuk-tag--green"
                           "govuk-tag govuk-tag--pink")))
           (unless (string= set-no "000")
             (who:htm
              (:strong
               :class class
               (who:esc set-no)
               (unless (string= trip-no "0")
                 (who:str " ×")
                 (who:esc trip-no)))))))
        (:td ; LCID
         :class "govuk-table__cell !-monospace !-no-phones"
         (unless (string= (trackernet::lcid train) "0")
           (who:esc (trackernet::lcid train))))))
     (:td ; Location / destination
      :class (format nil "govuk-table__cell ~A"
                     (if (string= line-code "X") "" "govuk-!-width-two-thirds"))
      (:span
       (who:esc (or
                 (when (and
                        (string= line-code "X")
                        (position #\· (trackernet::track-code train)))
                   (format nil "berth ~A"
                           (subseq (trackernet::track-code train) 3)))
                 (cdr (assoc (trackernet::track-code train)
                             (cached-track-suggestions line-code)
                             :test #'string=))
                 (trackernet::location-desc train)
                 "<somewhere>"))
       (who:str " ")
       (when show-dest
         (who:htm
          (:span
           :style "color: darkgray;"
           (who:str *govuk-go-arrow-small-svg*)
           " "
           (who:esc (or
                     (trackernet::destination-desc train)
                     "<somewhere>")))
          (who:str " ")))
       (when code
         (who:htm
          (:a
           :class "govuk-link"
           :href (easy-routes:genurl 'route-underlying-train
                                     :tid code)
           "[more]")
          (who:str " ")))
       (when reporter
         (who:htm
          (:strong
           :class "govuk-tag govuk-tag--yellow train-reporter !-no-phones"
           (if (string= line-code "X")
               (who:str
                (cond
                  ((uiop:string-prefix-p "X-XCA" reporter) "step")
                  ((uiop:string-prefix-p "X-XCC" reporter) "interpose")
                  ((uiop:string-prefix-p "X-XCB" reporter) "cancel")
                  ((uiop:string-prefix-p "X-DEL" reporter) "delete")
                  (t reporter)))
               (who:esc (subseq reporter
                                (if (string= line-code "S") 0 2)
                                #.(length "X-XXX")))))
          (who:str " ")))
       (when (and code
                  (< (trackernet::redis-last-score code)
                     (- (get-universal-time) 30)))
         (who:htm
          (:strong
           :class "govuk-tag govuk-tag--red train-stale"
           "Stale")))
       (:a
        :class (if maybe-hide-codes
                   "govuk-tag govuk-tag--grey !-float-right !-no-phones"
                   "govuk-tag govuk-tag--grey !-float-right")
        (who:esc
         (cond
           ((string= line-code "S") "×")
           ((and
             (string= line-code "X")
             (position #\· (trackernet::track-code train)))
            (format nil "area ~A"
                    (subseq (trackernet::track-code train) 0 2)))
           (t (trackernet::track-code train))))))))))

(defparameter *ws-magic-string* "258EAFA5-E914-47DA-95CA-C5AB0DC85B11")

(defun generate-ws-accept (sec-key)
  "Given a Sec-Websocket-Key value in SEC-KEY, return the correct Sec-Websocket-Accept value for it."
  (let* ((concatenated (concatenate 'string
                                    sec-key
                                    *ws-magic-string*))
         (hashed (ironclad:digest-sequence
                  :sha1
                  (babel:string-to-octets concatenated))))
    (qbase64:encode-bytes hashed)))

(defclass websocket ()
  ((inner
    :initarg :inner
    :accessor inner)
   (addr
    :initarg :addr
    :accessor addr)
   (closed
    :initform nil
    :accessor closed)
   (ws
    :accessor ws)
   (parser
    :accessor parser)))

(defun notice-websocket-closed (websocket)
  (unless (closed websocket)
    (setf (closed websocket) t)
    (log:info "WebSocket from ~A closed." (addr websocket))))

(define-condition websocket-closed (error) ()
  (:report (lambda (c s)
             (declare (ignore c))
             (format s "Attempted to write to closed WebSocket"))))

(defun websocket-write (websocket payload &key type)
  (when (closed websocket)
    (error 'websocket-closed))
  (handler-bind
      ;; Transform any errors while writing to WEBSOCKET if
      ;; the websocket turns out to be closed when they happen.
      ((error (lambda (e)
                (declare (ignore e))
                (when (closed websocket)
                  (error 'websocket-closed)))))
    (let ((frame
            (fast-websocket:compose-frame payload :type type)))
      (write-sequence frame (inner websocket)))
    (force-output (inner websocket))))

(defun websocket-message (websocket message)
  (log:info "[ws ~A] unexpected message: ~A" (addr websocket) message))

(defun websocket-ping (websocket payload)
  (websocket-write websocket payload :type :pong))

(defun read-websocket-frame (websocket)
  (let ((stream (inner websocket))
        (buf (make-array 2 :element-type '(unsigned-byte 8)))
        (extended-buf (make-array 8 :element-type '(unsigned-byte 8))))
    (block nil
      (tagbody retry
         (let ((read-bytes (read-sequence buf stream)))
           (when (= read-bytes 0)
             (return nil))

           (let ((maskp (plusp (ldb (byte 1 7) (aref buf 1))))
                 (data-length (ldb (byte 7 0) (aref buf 1))))
             (cond
               ((<= 0 data-length 125))
               (t
                (let ((end (if (= data-length 126) 2 8)))
                  (read-sequence extended-buf stream :end end)
                  (incf read-bytes end)
                  (setf data-length
                        (loop with length = 0
                              for i from 0 below end
                              do (setf length (+ (ash length 8) (aref extended-buf i)))
                              finally (return length))))))
             (when maskp
               (incf data-length 4))
             (let ((data (make-array (+ read-bytes data-length) :element-type '(unsigned-byte 8))))
               (replace data buf :end2 2)
               (unless (= read-bytes 2)
                 (replace data extended-buf :start1 2 :end2 (- read-bytes 2)))
               (read-sequence data stream :start read-bytes)
               (return data))))))))

(defun websocket-read-loop (websocket)
  (setf (sb-thread:thread-name sb-thread:*current-thread*)
        (format nil "ws-read-loop-~A" (addr websocket)))
  (loop
    (block retry
      (let ((frame (handler-case
                       (read-websocket-frame websocket)
                     (sb-sys:io-timeout ()
                       (return-from retry))))
            (parser (parser websocket)))
        (unless frame
          (notice-websocket-closed websocket)
          (return-from websocket-read-loop))
        (funcall parser frame)))))

(defmethod initialize-instance :after ((instance websocket) &rest args)
  (declare (ignore args))
  (setf (ws instance) (fast-websocket:make-ws))
  (setf (parser instance)
        (fast-websocket:make-parser
         (ws instance)
         :require-masking t
         :close-callback
         (lambda (&rest args)
           (declare (ignore args)))
         :ping-callback
         (lambda (payload)
           (websocket-ping instance payload))
         :message-callback
         (lambda (message)
           (websocket-message instance message))))
  (bt:make-thread
   (lambda ()
     (handler-case
         (websocket-read-loop instance)
       (websocket-closed (e)
         (declare (ignore e)))
       (serious-condition (c)
         (unless (closed instance)
           (setf (closed instance) t)
           (log:warn "[ws ~A] read loop failed: ~A" (addr instance) c)))))))

(defun live-train-update-loop (redis lt on-update)
  (let ((redis:*connection* redis))
    (red:subscribe (format nil "lt-updates:~A" (trackernet::unique-id lt))))
  (let ((fresh-lt
          (trackernet::redis-raw-get-cpk (format nil "lt:~A"
                                                 (trackernet::unique-id lt)))))
    (funcall
     on-update
     :backfill
     (format nil "~{~A~}"
             (mapcar
              (lambda (ent)
                (display-train-history-entry
                 ent
                 (trackernet::line-code fresh-lt)
                 :lastp nil
                 :ltid (trackernet::unique-id fresh-lt)))
              (trackernet::history fresh-lt))))
    (funcall
     on-update
     :update
     (display-train-history-entry
      (trackernet::current-entry fresh-lt)
      (trackernet::line-code lt)
      :ltid (trackernet::unique-id fresh-lt)
      :lastp t))
    (loop
      for msg = (let ((redis:*connection* redis))
                  (sb-ext:with-timeout 300
                    (redis:expect :anything)))
      while msg
      do (let* ((msg (trackernet::cpk-unbase64 (third msg)))
                (type (car msg))
                (event (cdr msg)))
           (funcall
            on-update
            type
            (display-train-history-entry
             event
             (trackernet::line-code lt)
             :fadein (not (eql type :update))
             :ltid (trackernet::unique-id fresh-lt)
             :lastp (eql type :update)))))))

(defun firehose-update-loop (redis on-update &key (key "firehose"))
  (let ((redis:*connection* redis))
    (red:subscribe key))
  (loop
    for msg = (let ((redis:*connection* redis))
                (redis:expect :anything))
    while msg
    do (let* ((msg (trackernet::cpk-unbase64 (third msg))))
         (trackernet::statsd-inc "intertube.firehose-read")
         (funcall on-update msg))))

(defun firehose-fresh-trains-loop (redis on-update)
  (let ((redis:*connection* redis))
    (red:subscribe "fresh-trains"))
  (loop
    for msg = (let ((redis:*connection* redis))
                (redis:expect :anything))
    while msg
    do (let* ((msg (trackernet::cpk-unbase64 (third msg))))
         (funcall on-update msg))))

(defun websocket-handshake ()
  (let ((stream hunchentoot::*hunchentoot-stream*)
        (key (hunchentoot:header-in* :sec-websocket-key)))
    (unless (and
             (string= (hunchentoot:header-in* :upgrade) "websocket")
             (string= (hunchentoot:header-in* :sec-websocket-version) "13")
             key)
      (setf (hunchentoot:return-code*) 400)
      (hunchentoot:abort-request-handler "invalid websocket handshake"))
    (setf hunchentoot::*headers-sent* t)
    (hunchentoot::send-response hunchentoot:*acceptor*
                                stream
                                101
                                :headers
                                `(("Upgrade" . "websocket")
                                  ("Connection" . "Upgrade")
                                  ("Sec-Websocket-Accept" . ,(generate-ws-accept key))))
    (force-output stream)
    (hunchentoot::detach-socket hunchentoot:*acceptor*)
    ;; HACK(eta): hunchentoot is dumb and will still try and do things
    ;;            with the stream even after detaching it (sigh!).
    ;;            Put an empty broadcast stream in there instead.
    (setf hunchentoot::*hunchentoot-stream* (make-broadcast-stream))
    (make-instance 'websocket
                   :inner stream
                   :addr (hunchentoot:real-remote-addr))))

(hunchentoot:define-easy-handler (train-ws :uri "/live-ws") (id)
  (let ((lt (with-server-timing ("fetch-lt")
              (ignore-errors
               (trackernet::redis-raw-get-cpk (format nil "lt:~A" id)))))
        (stream hunchentoot::*hunchentoot-stream*)
        (addr (hunchentoot:real-remote-addr)))
    (unless lt
      (let ((socket (websocket-handshake)))
        (log:info "Accepting new (defunct) WebSocket connection for ~A from ~A"
                  id addr)
        (websocket-write socket (cl-json:encode-json-to-string (list :closed)))
        (close stream)
        (return-from train-ws)))
    (let* ((redis (make-instance 'redis:redis-connection
                                 :host trackernet::*redis-host* :port trackernet::*redis-port*))
           (socket (websocket-handshake)))
      (log:info "Accepting new WebSocket connection for ~A from ~A"
                id addr)
      (setf (sb-thread:thread-name sb-thread:*current-thread*)
            (format nil "ws-~A-~A" addr id))
      (unwind-protect
           (handler-case
               (live-train-update-loop
                redis lt
                (lambda (update-type html)
                  (websocket-write
                   socket
                   (cl-json:encode-json-to-string
                    (list update-type html))
                   :type :text)))
             (sb-ext:timeout ()
               (log:info "WebSocket from ~A timed out." addr))
             (websocket-closed ()
               (log:debug "main thread noticed WebSocket from ~A closed" addr)))
        (ignore-errors (close stream))
        (ignore-errors (redis:close-connection redis))))))

(hunchentoot:define-easy-handler (api-firehose-ws :uri "/api/v1/firehose") (token ver)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((stream hunchentoot::*hunchentoot-stream*)
        (addr (hunchentoot:real-remote-addr)))
    (let* ((redis (make-instance 'redis:redis-connection
                                 :host trackernet::*redis-host* :port trackernet::*redis-port*))
           (socket (websocket-handshake)))
      (log:info "Accepting new WebSocket firehose connection from ~A"
                addr)
      (unwind-protect
           (handler-case
               (firehose-update-loop
                redis
                (lambda (msg)
                  (websocket-write
                   socket
                   (cl-json:encode-json-to-string msg)
                   :type :text))
                :key (if (equal ver "2") "firehose-v2" "firehose"))
             (websocket-closed ()
               (log:info "WebSocket from ~A closed." addr)))
        (ignore-errors (close stream))
        (ignore-errors (redis:close-connection redis))))))

(hunchentoot:define-easy-handler (api-train-firehose-ws :uri "/api/v1/train-firehose") (token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((stream hunchentoot::*hunchentoot-stream*)
        (addr (hunchentoot:real-remote-addr)))
    (let* ((redis (make-instance 'redis:redis-connection
                                 :host trackernet::*redis-host* :port trackernet::*redis-port*))
           (socket (websocket-handshake)))
      (log:info "Accepting new WebSocket train firehose connection from ~A"
                addr)
      (unwind-protect
           (handler-case
               (firehose-fresh-trains-loop
                redis
                (lambda (msg)
                  (websocket-write
                   socket
                   (cl-json:encode-json-to-string msg)
                   :type :text)))
             (websocket-closed ()
               (log:info "WebSocket from ~A closed." addr)))
        (ignore-errors (close stream))
        (ignore-errors (redis:close-connection redis))))))

(defparameter *max-train-lines* 100)

(hunchentoot:define-easy-handler (api-list-trains :uri "/api/v1/list-trains") (history token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((trains (mapcar
                 (lambda (lt)
                   (unless history
                     (setf (trackernet::history lt) nil))
                   lt)
                 (trackernet::get-all-live-trains))))
    (setf (hunchentoot:content-type*) "application/json")
    (cl-json:encode-json-to-string trains)))

(hunchentoot:define-easy-handler (api-list-trains-on-line :uri "/api/v1/list-trains-on-line") (line history token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((trains (mapcar
                 (lambda (lt)
                   (unless history
                     (setf (trackernet::history lt) nil))
                   (cons (first (trackernet::train-identity lt))
                         lt))
                 (live-trains-on-line line))))
    (setf (hunchentoot:content-type*) "application/json")
    (cl-json:encode-json-to-string
     (alexandria:alist-hash-table trains))))

(hunchentoot:define-easy-handler (api-list-threads :uri "/api/v1/debug/list-threads") (token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (cl-json:encode-json-to-string
   (mapcar #'sb-thread:thread-name (sb-thread:list-all-threads))))

(hunchentoot:define-easy-handler (api-get-by-id :uri "/api/v1/train-by-wtt-id") (wtt line token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((train (find-if
                 (lambda (lt)
                   (string= (first (trackernet::train-identity lt))
                            wtt))
                 (live-trains-on-line line))))
    (unless train
      (setf (hunchentoot:return-code*) 404)
      (hunchentoot:abort-request-handler "sorry mate, haven't got a clue"))
    (setf (hunchentoot:content-type*) "application/json")
    (cl-json:encode-json-to-string train)))

(hunchentoot:define-easy-handler (api-live-train :uri "/api/v1/train") (id token)
  (unless (and token (red:sismember "api-tokens" token))
    (setf (hunchentoot:return-code*) 401)
    (hunchentoot:abort-request-handler "no or invalid API token"))
  (let ((lt (with-server-timing ("fetch-lt")
              (ignore-errors
               (trackernet::redis-raw-get-cpk (format nil "lt:~A" id))))))
    (unless lt
      (setf (hunchentoot:return-code*) 404)
      (hunchentoot:abort-request-handler "not found lol"))
    (setf (hunchentoot:content-type*) "application/json")
    (cl-json:encode-json-to-string lt)))

(defun return-rate-limit (seconds)
  (setf (hunchentoot:return-code*) 420)
  (hunchentoot:abort-request-handler
   (render (:title "Rate limit exceeded")
     (:h1
      :class "govuk-heading-l"
      "Rate limit exceeded")
     (:p
      :class "govuk-body"
      "Sorry, we're a bit busy right now. Try waiting about "
      (:strong
       (who:fmt "~A" seconds))
      " seconds, and pushing the retry button below.")
     (:a
      :class "govuk-button govuk-button--start"
      :id "refresh-button"
      :|onClick| "window.location.reload()"
      "Try again"))))

(defun rate-limit (requests expiry-sec name &optional identifier)
  "Rate limit the current route to a maximum of REQUESTS per EXPIRY-SEC seconds. If an IDENTIFIER is provided, rate limit based on that.
Returns the number of requests counted, including this one."
  ;; cribbed from https://redis.io/commands/incr/
  (let* ((key (format nil "ratelimit:~A:~A" name (or identifier "global")))
         (current (red:llen key)))
    (if (>= current requests)
        (progn
          (format *error-output* "Rate limit hit: ~A" key)
          (trackernet::statsd-inc "intertube.rate-limit")
          (return-rate-limit expiry-sec))
        (if (red:exists key)
            (red:rpushx key "x")
            (first
             (redis:with-pipelining
               (red:rpush key "x")
               (red:expire key expiry-sec)))))))

(defun return-bad-request (&optional text)
  (setf (hunchentoot:return-code*) 400)
  (hunchentoot:abort-request-handler
   (render (:title "Bad request")
     (:h1
      :class "govuk-heading-l"
      "Bad request")
     (:p
      :class "govuk-body"
      (who:str
       (or text
           "Hmm, that didn't look right. Sorry if you got here by following a link from somewhere else. If you didn't, well, you know what you did.")))
     (:p
      :class "govuk-body"
      "You can report this problem by messaging eta "
      (:a
       :href "https://eta.st/#contact"
       :class "govuk-link"
       "through one of the links here")
      "."))))

(defun return-not-found (&optional text message-eta-p)
  (setf (hunchentoot:return-code*) 404)
  (hunchentoot:abort-request-handler
   (render (:title "Page not found")
     (:h1
      :class "govuk-heading-l"
      "Page not found")
     (:p
      :class "govuk-body"
      (who:esc
       (or text
           "Sorry, we can't find what you're looking for.")))
     (if message-eta-p
         (who:htm
          (:p
           :class "govuk-body"
           "You can report this problem by messaging eta "
           (:a
            :href "https://eta.st/#contact"
            :class "govuk-link"
            "through one of the links here")
           "."))
         (who:htm
          (:p
           :class "govuk-body"
           (:a
            :href "javascript:history.back()"
            :class "govuk-link"
            "Go one step back")
           ", or try "
           (:a
            :href "/"
            :class "govuk-link"
            "returning to the home page")
           ".")
          (:p
           :class "govuk-body"
           "(Not what you expected? "
           (:a
            :href "https://eta.st/#contact"
            :class "govuk-link"
            "Get in touch")
           ".)"))))))

(hunchentoot:define-easy-handler (choose-live :uri "/choose-live") (line ids)
  (let* ((line-data (tube-line-by-code line))
         (ltids (split-sequence:split-sequence #\, ids))
         (lts (mapcar (lambda (trn)
                        (ignore-errors
                         (trackernet::redis-raw-get-cpk (format nil "lt:~A" trn))))
                      ltids)))
    (unless line-data
      (return-bad-request))
    (let* ((*accent-colour* (apply #'html-rgb-colour (third line-data))))
      (render (:title (format nil "Choose a train · ~A line"
                              (first line-data)))
        (:a
         :class "govuk-back-link"
         :href (easy-routes:genurl 'route-line :code line)
         (who:fmt "~A line"
                  (first line-data)))
        (:h1
         :class "govuk-heading-xl"
         "Choose a train")
        (:ul
         :class "govuk-list"
         (dolist (lt lts)
           (when (and lt
                      (typep (trackernet::current-entry lt)
                             'trackernet::train-station-stop))
             (let ((ent (trackernet::current-entry lt)))
               (who:htm
               (:li
                (:h3
                 :class "govuk-body-l !-clearfix"
                 "at "
                 (:strong
                  (who:esc (trackernet::station-name ent)))
                 (when (trackernet::platform ent)
                   (who:str " platform ")
                   (who:htm
                    (:strong
                     (who:esc (trackernet::platform ent)))))
                 (who:htm
                  (:a
                   :href (uri-for-lt lt)
                   :class "choose-line-train-link govuk-link"
                   (:span
                    :class "!-underline"
                    (if (first (trackernet::train-identity lt))
                        (who:fmt "view #~A"
                                 (first (trackernet::train-identity lt)))
                        (who:str "view"))
                    " " ;; Inter-specific right arrow glyph
                   ))))
                 ))))))))))

(defun model-kitchen-render-svg (model)
  (let* ((proc
           (uiop:launch-program
            '("dot" "-Tsvg")
            :input :stream
            :output :stream
            :element-type '(unsigned-byte 8)))
         (viz-in
           (flexi-streams:make-flexi-stream
            (uiop:process-info-input proc)
            :external-format :utf-8)))
    (unwind-protect
         (trackernet::dot-json-toposort-model-inner model viz-in)
      (close viz-in))
    (uiop:process-info-output proc)))

;; FIXME(eta): MUST be locked down to staging only (!)
(hunchentoot:define-easy-handler (model-kitchen-renderer :uri "/model-kitchen/render") (model)
  (unless trackernet::*staging-mode-p*
    (return-not-found))
  (let ((model
          (let ((cl-json:*json-identifier-name-to-lisp* #'identity))
            (cl-json:decode-json-from-string model))))
    (setf (hunchentoot:content-type*) "image/svg+xml")
    (let ((out (hunchentoot:send-headers))
          (in (model-kitchen-render-svg model)))
      (unwind-protect
           (loop
             for byte = (read-byte in nil nil)
             while byte
             do (write-byte byte out))
        (close in)))))

(hunchentoot:define-easy-handler (model-kitchen-api :uri "/model-kitchen/generate") (line cutoff)
  (unless trackernet::*staging-mode-p*
    (return-not-found))
  (let ((line-data (tube-line-by-code line))
        (cutoff (ignore-errors (parse-integer cutoff))))
    (unless line-data
      (return-not-found))
    (unless cutoff
      (return-bad-request))
    (let ((debug-log (make-array 0
                                 :element-type 'character
                                 :adjustable t
                                 :fill-pointer 0))
          (model-json (make-array 0
                                  :element-type 'character
                                  :adjustable t
                                  :fill-pointer 0)))
      (with-output-to-string (*debug-io* debug-log)
        (with-output-to-string (json-out model-json)
          (trackernet::write-json-toposort-model line json-out
                                                 :cutoff cutoff)))
      (setf (hunchentoot:content-type*) "application/json")
      (cl-json:encode-json-to-string (list debug-log model-json)))))

(hunchentoot:define-easy-handler (model-kitchen :uri "/model-kitchen") (line)
  (unless trackernet::*staging-mode-p*
    (return-not-found
     "Nice try, but this path is only available in staging mode."))
  (let ((line-data (tube-line-by-code line)))
    (unless line-data
      (return-not-found
       "Please provide a line parameter."))
    (let* ((*accent-colour* (apply #'html-rgb-colour (third line-data))))
      (render (:title (format nil "Model kitchen · ~A line"
                              (first line-data)))
        (:span
         :class "govuk-caption-xl"
         (who:fmt "~A line" (first line-data)))
        (:h1
         :class "govuk-heading-xl"
         "Model kitchen 👩‍🍳")
        (:script
         :type "text/javascript"
         (who:str *model-kitchen-js*))
        (:p
         :class "govuk-body"
         (:details
          :class "govuk-details"
          (:summary
           :class "govuk-details__summary"
           (:span
            :class "govuk-details__summary-text"
            "Model generation log ("
            (:span
             :id "model-log-lines"
             "0")
            " lines)"))
          (:div
           :class "govuk-details__text"
           (:p
            :class "govuk-body"
            :id "model-log"
            "Nothing here yet!")))
         (:details
          :class "govuk-details"
          (:summary
           :class "govuk-details__summary"
           (:span
            :class "govuk-details__summary-text"
            "Model generation parameters"))
          (:div
           :class "govuk-details__text"
           (:fieldset
            :class "govuk-fieldset"
            (:div
             :class "govuk-form-group"
             (:label
              :class "govuk-label"
              :for "cutoff"
              "Link score cutoff")
             (:input
              :class "govuk-input govuk-input--width-10"
              :type "text"
              :id "cutoff"
              :name "cutoff"
              :value "100")))))
         (:details
          :class "govuk-details"
          (:summary
           :class "govuk-details__summary"
           (:span
            :class "govuk-details__summary-text"
            "Find a node"))
          (:div
           :class "govuk-details__text"
            (:div
             :class "govuk-form-group"
             (:select
              :class "govuk-select"
              :id "station-select"
              :name "station-select"
              (:option
               :value ""
               :selected "true"
               :disabled "true"
               "(no model yet)")
              ))
            (:p
             :class "govuk-body"
             "Track codes: "
             (:span
              :style "display: block; white-space: pre-wrap; overflow-wrap: anywhere;"
              :id "track-code-list"
              :class "!-monospace"
              "(none)"))))
         (:div
          :class "govuk-button-group"
          (:button
           :class "govuk-button"
           :id "cook"
           "Generate"
           (who:str *govuk-go-arrow-svg*))
          (:button
           :class "govuk-button govuk-button--secondary"
           :id "upload-model"
           "Upload")
          (:button
           :class "govuk-button govuk-button--secondary"
           :id "download"
           :disabled "t"
           "Download")
          ))
        (:div
         :id "model-container"
         :style "height: 500px; border: 1px solid black; overflow: scroll; cursor: grab; margin-bottom: 20px;")))))

(defun terrible-timestamp-decoder (date time)
  "this is truly awful"
  (when (or
         (< (length date) #.(length "XXXX-XX-XX"))
         (< (length time) #.(length "XX:XX")))
    (error "invalid length date or time"))
  (let ((yyyy (parse-integer date :start 0 :end 4))
        (mm (parse-integer date :start 5 :end 7))
        (dd (parse-integer date :start 8 :end 10))
        (hh (parse-integer time :start 0 :end 2))
        (min (parse-integer time :start 3 :end 5)))
    (local-time:encode-timestamp
     0 0 min hh dd mm yyyy
     :timezone trackernet::+europe-london-tz+)))

(defun terrible-date-decoder (date)
  "this is truly awful"
  (when (< (length date) #.(length "XXXX-XX-XX"))
    (error "invalid length date"))
  (let ((yyyy (parse-integer date :start 0 :end 4))
        (mm (parse-integer date :start 5 :end 7))
        (dd (parse-integer date :start 8 :end 10)))
    (local-time:encode-timestamp
     0 0 0 0 dd mm yyyy
     :timezone trackernet::+europe-london-tz+)))

(defun terrible-timestamp-decoder-hhmm (date time)
  "this is truly awful"
  (when (or
         (< (length date) #.(length "XXXX-XX-XX"))
         (< (length time) #.(length "XXXX")))
    (error "invalid length date or time"))
  (let ((yyyy (parse-integer date :start 0 :end 4))
        (mm (parse-integer date :start 5 :end 7))
        (dd (parse-integer date :start 8 :end 10))
        (hh (parse-integer time :start 0 :end 2))
        (min (parse-integer time :start 2 :end 4)))
    (local-time:encode-timestamp
     0 0 min hh dd mm yyyy
     :timezone trackernet::+europe-london-tz+)))

(defun start-and-end-of-date-ts (timestamp)
  (let ((lt (local-time:universal-to-timestamp timestamp)))
    (cons (local-time:timestamp-to-universal
           (local-time:timestamp-minimize-part
            lt :hour
            :timezone trackernet::+europe-london-tz+))
          (local-time:timestamp-to-universal
           (local-time:timestamp-maximize-part
            lt :hour
            :timezone trackernet::+europe-london-tz+)))))

(defun render/identifier-search (line around-ts identifier)
  (rate-limit 3 1 "identifier-search")
  (let* ((line-data (tube-line-by-code line))
         (around-ts (or around-ts (get-universal-time)))
         (day-before (trackernet::get-iso-8601-for-ts-london
                      (- around-ts (* 24 3600))))
         (day-after (trackernet::get-iso-8601-for-ts-london
                      (+ around-ts (* 24 3600))))
         (start-and-end (start-and-end-of-date-ts around-ts))
         (*accent-colour* (apply #'html-rgb-colour (third line-data)))
         (trains
           (when identifier
             (trackernet::get-train-ids-by-identifier
              line
              (car start-and-end)
              (cdr start-and-end)
              (if (string= line "X")
                  identifier
                  (or
                   (ignore-errors (parse-integer identifier))
                   (return-bad-request)))))))
    (render (:title (format nil "Historical search · ~A line"
                            (first line-data)))
      (:a
       :class "govuk-back-link"
       :href (easy-routes:genurl 'route-line :code line)
       "Back to line overview")
      (:span
       :class "govuk-caption-xl"
       (who:fmt "~A line"
                (first line-data)))
      (:h1
       :class "govuk-heading-xl"
       "Find a train by "
       (who:str
        (if (string= line "X")
            "headcode"
            "Working Timetable number")))
      (when identifier
        (who:htm
         (:p
          :class "govuk-body-l"
          "Showing all runnings of train "
          (:strong
           (who:esc identifier))
          " on "
          (:strong
           (who:str (trackernet::get-iso-8601-for-ts-london around-ts)))
          ". ")))
      (:details
       :class "govuk-details"
       :open (not trains)
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "Change search query"))
       (:div
        :class "govuk-details__text"
      (:form
       :action "/form/identifier-search"
       :method "get"
       (:input
        :name "line"
        :type "hidden"
        :value line)
       (:div
        :class "govuk-form-group"
        (:label
         :class "govuk-label"
         :for "date-input"
         "Date")
        (:input
         :type "date"
         :required t
         :class "govuk-input govuk-!-width-one-third"
         :max (trackernet::get-iso-8601-for-ts-london (get-universal-time))
         :id "date-input"
         :name "date"
         :value (trackernet::get-iso-8601-for-ts-london around-ts)))
       (:div
        :class "govuk-form-group"
        (:label
         :class "govuk-label"
         :for "identifier-input"
         (who:str
          (if (string= line "X")
              "Headcode"
              "Working Timetable number")))
        (:div
         :class "govuk-hint"
         (who:str
          (if (string= line "X")
              "e.g. 9Y60 or 2W81"
              "e.g. 026 or 372")))
        (:input
         :class (format nil "govuk-input govuk-input--width-~A"
                        (if (string= line "X") "4" "3"))
         :type "text"
         :required t
         :id "identifier-input"
         :name "identifier"
         :value identifier))
       (:button
        :type "submit"
        :class "govuk-button !-no-bottom-margin"
        "Find trains"
        (who:str *govuk-go-arrow-svg*)))))
      (:ul
       :class "govuk-list station-calls-list"
       (when identifier
         (who:htm
          (:li
           (:span
            :class "time-jump-link"
            (:a
             :href (easy-routes:genurl 'route-id-search-populated
                                       :code line
                                       :date day-before
                                       :identifier identifier)
             :class "govuk-link"
             "← "
             (who:esc day-before))))))
       (dolist (entry trains)
         (destructuring-bind (id trips dests from-ts to-ts) entry
           (who:htm
            (:li
             :class "station-call"
             (:span
              :class (format nil "station-call-time station-call-time-double ~@A"
                             (when (eql trips :live) "station-call-time-live"))
              (who:esc (trackernet::universal-time-nonrailway from-ts))
              "&minus;"
              (who:esc (trackernet::universal-time-nonrailway to-ts)))
             (:span
              :class "station-call-station"
              (if (eql trips :live)
                  (who:htm
                   (:em "train still live"))
                  (who:esc
                   (if (string= line "X")
                       (format nil "to ~{~A~^, ~}" dests)
                       (format nil "trips ×~{~A~^, ~}" trips)))))
             (:span
              :class "station-call-links"
              (:a
               :href (format nil "~A#ent-~A"
                             (easy-routes:genurl 'route-live-train
                                                 :tid id)
                             from-ts)
               :class "station-call-link govuk-link"
               (:span
                :class "!-underline"
                "view")
               " " ;; Inter-specific right arrow glyph
               ))))))
       (unless (or trains (not identifier))
         (who:htm
          (:li
           (:span
            :class "govuk-body-l no-data-available"
            "No matching trains found. Try a different query."))))
       (when (and identifier
                  (not (> (+ around-ts (* 24 3600))
                          (get-universal-time))))
         (who:htm
          (:li
           (:span
            :class "time-jump-link"
            (:a
             :href (easy-routes:genurl 'route-id-search-populated
                                       :code line
                                       :date day-after
                                       :identifier identifier)
             :class "govuk-link"
             (who:esc day-after)
             " →"
             )))))))))

(defun render/station-calls (line station around-ts)
  (rate-limit 3 1 "station-calls")
  (let* ((line-data (tube-line-by-code line))
         (*accent-colour* (apply #'html-rgb-colour (third line-data)))
         (around-ts-provided (when around-ts t))
         (around-ts (or around-ts (get-universal-time)))
         (max-ts (+ around-ts 3600))
         (min-ts (- around-ts 3600))
         (lts (trackernet::get-current-lts-for-station line station min-ts max-ts))
         (clicktrains (trackernet::get-historical-trains-for-station line station min-ts max-ts))
         ;; Format: (ts platform identity destination link-to-more livep at-station-p)
         (combined)
         (wtts))
    (loop
      for (lt . entries) in lts
      do (dolist (entry entries)
           (destructuring-bind (identity lcid destination trackernet-id)
               (trackernet::get-lt-state-at-time lt (trackernet::universal-ts entry))
             (declare (ignore lcid))
             (push (list
                    (trackernet::universal-ts entry)
                    (trackernet::platform entry)
                    identity
                    destination
                    (subseq
                     (trackernet::unique-id lt)
                     #.(length "train_"))
                    t
                    (eql (trackernet::universal-ts (trackernet::current-entry lt))
                         (trackernet::universal-ts entry))
                    trackernet-id)
                   combined))))
    (loop
      for (filename . entries) in clicktrains
      do (dolist (entry entries)
           (destructuring-bind (entry . universal-ts) entry
             (let ((platform
                     (or (nth-value
                          1 (trackernet::extract-station-information
                             (cdr (assoc (trackernet::track-code entry)
                                         (cached-track-suggestions line)
                                         :test #'string=))))
                         (nth-value
                          1 (trackernet::extract-station-information
                             (trackernet::location-desc entry))))))
               (push (list
                      universal-ts
                      platform
                      (list (trackernet::nilify-zeros (trackernet::set-no entry))
                            (trackernet::nilify-zeros (trackernet::trip-no entry)))
                      (trackernet::destination-desc entry)
                      (if (uiop:string-prefix-p "lt:" filename)
                          (subseq
                           filename
                           #.(length "lt:train_"))
                          filename)
                      nil
                      nil
                      (trackernet::train-id entry))
                     combined)))))
    (setf combined (sort combined (lambda (a1 a2)
                                    (< (car a1) (car a2)))))
    (setf wtts
          (loop
            for entry in combined
            when (third entry)
              collect (cons
                       (format nil
                               "~A-~A"
                               (fifth entry)
                               (universal-time-hhmm (first entry)))
                       (format nil "~3,'0D~A"
                               ;; FIXME(eta): this is hacky
                               (typecase (first (third entry))
                                 (string (or ; HACK(eta): bodge around chiltern trains with headcodes in this field
                                          (ignore-errors (parse-integer (first (third entry))))
                                          0))
                                 (integer (first (third entry))))
                               (if (second (third entry))
                                   (format nil " ×~A" (second (third entry)))
                                   "")))))
    (setf wtts (sort wtts (lambda (a1 a2)
                            (string< (cdr a1) (cdr a2)))))
    (render (:title (format nil "~A station · ~A line"
                            station (first line-data)))
      (:a
       :class "govuk-back-link"
       :href (easy-routes:genurl 'route-line :code line)
       (who:fmt "~A line"
                (first line-data)))
      (:span
       :class "govuk-caption-xl"
       (who:str
        (if around-ts-provided
            "Historical station overview"
            "Station overview")))
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "~A station" station))
      (when around-ts-provided
        (who:htm
         (:p
          :class "govuk-body-l"
          "Viewing station at around "
          (:strong
           (who:str (trackernet::universal-time-railway around-ts)))
          " on "
          (:strong
           (who:str (trackernet::get-iso-8601-for-ts-london around-ts)))
          ". "
          (:a
           :class "govuk-link"
           :href (uri-for-station line station)
           "View the station right now instead."))))
      (:details
       :class "govuk-details"
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "Change date and time"))
       (:div
        :class "govuk-details__text"
        (:form
         :action "/station-calls"
         :method "get"
         (:input
          :name "line"
          :type "hidden"
          :value line)
         (:input
          :name "station"
          :type "hidden"
          :value (who:escape-string station))
         (:div
          :class "govuk-form-group"
          (:label
           :class "govuk-label"
           :for "date-input"
           "Date")
          (:input
           :type "date"
           :class "govuk-input govuk-!-width-one-third"
           :max (trackernet::get-iso-8601-date)
           :id "date-input"
           :name "date"
           :value (trackernet::get-iso-8601-for-ts-london around-ts)))
         (:div
          :class "govuk-form-group"
          (:label
           :class "govuk-label"
           :for "time-input"
           "Time")
          (:input
           :type "time"
           :class "govuk-input govuk-!-width-one-third"
           :id "time-input"
           :name "time"
           :value (trackernet::universal-time-nonrailway-colon around-ts)))
         (:button
          :type "submit"
          :class "govuk-button !-no-bottom-margin"
          "View trains"))))
      (unless (string= line "X")
        (who:htm
         (:details
          :class "govuk-details"
          (:summary
           :class "govuk-details__summary"
           (:span
            :class "govuk-details__summary-text"
            "Jump to a call with a Working Timetable number"))
          (:div
           :class "govuk-details__text"
           (:p
            :class "govuk-body"
            "Jump to: "
            (loop
              for (link . display) in wtts
              for i from 1
              with len = (length wtts)
              do (who:htm
                  (:a
                   :class "govuk-link !-monospace"
                   :href (format nil "#~A" link)
                   (who:esc display)))
              unless (eql len i)
                do (who:str " &middot; ")))))))
      (:ul
       :class "govuk-list station-calls-list"
       (:li
        (:span
         :class "time-jump-link"
         (:a
          :href (uri-for-station-timed line station (- around-ts (* 2 3600)))
          :class "govuk-link"
          "← 1 hour earlier")))
       (dolist (entry combined)
         (destructuring-bind (ts platform identity destination link livep at-station-p trackernet-id) entry
           (declare (ignore identity))
           (who:htm
            (:li
             :class "station-call"
             :id (format nil
                         "~A-~A"
                         link
                         (universal-time-hhmm ts))
             (:span
              :class (format nil "station-call-time ~A"
                             (if livep "station-call-time-live" ""))
              (who:esc (trackernet::universal-time-nonrailway ts)))
             (:span
              :class (format nil "station-call-station ~A"
                             (if at-station-p "station-call-station-live" ""))
              (:span
               :class "atward"
               "to ")
              (if (or
                   (string-equal destination "out of service")
                   (string-equal destination "not in service"))
                  (who:htm
                   (:span
                    :class "station-call-station-oos"
                    "not in service"))
                  (who:esc destination)))
             (:span
              :class "station-call-links"
              (when (string= line "X")
                (who:htm
                 (:span
                  :class "!-monospace !-no-phones"
                  (who:esc trackernet-id))
                 (who:str " &middot; ")))
              (when platform
                (who:htm
                 (who:str "p")
                 (:span
                  :class "!-no-phones"
                  "lat. ")
                 (:strong
                  (who:esc platform))
                 (who:str " &middot; ")))
              (:a
               :href (format nil "~A#ent-~A"
                             (easy-routes:genurl 'route-live-train
                                                 :tid link)
                             ts)
               :class "station-call-link govuk-link"
               (:span
                :class "!-underline"
                "view")
               " " ;; Inter-specific right arrow glyph
               ))))))
       (unless combined
         (who:htm
          (:li
           (:span
            :class "govuk-body-l no-data-available"
            "No trains found."))))
       (unless (>= (+ around-ts 3600) (get-universal-time))
         (who:htm
          (:li
           (:span
            :class "time-jump-link"
            (:a
             :href (uri-for-station-timed line station (+ around-ts (* 2 3600)))
             :class "govuk-link"
             "→ 1 hour later")))))))))

(defun get-journeys-for-lt (lt)
  (let ((split-points '()) ; identity start-ts end-ts
        (history (reverse (cons (trackernet::current-entry lt)
                                (trackernet::history lt)))))
    (loop
      for entry in history
      when (if (string= (trackernet::line-code lt) "X")
               (typep entry 'trackernet::train-rescue)
               (typep entry 'trackernet::train-identity-change))
        do (setf split-points
                 (append split-points
                         (list (list entry (trackernet::universal-ts entry) nil)))))
    ;; populate the end-ts'es
    (loop
      for i from 1
      for point in (cdr split-points)
      do (setf (third (elt split-points (1- i))) (second point)))
    (loop
      for (identity start-ts end-ts) in split-points
      for i from 1
      collect (cons identity
                    (mapcar
                     (lambda (entry)
                       (let ((event-start (trackernet::universal-ts entry))
                             (event-end (trackernet::end-ts entry)))
                         ;; If this entry also appeared in the previous
                         ;; journey, change it to only show its departure
                         ;; time.
                         (when (and end-ts (>= start-ts event-start)
                                    (not (eql i 1)))
                           (setf event-start event-end)
                           (setf event-end nil))
                         ;; If an event will appear in a previous journey,
                         ;; change it to not show its departure time.
                         (when (and end-ts event-end
                                    (> event-end end-ts))
                           (setf event-end nil))
                         (make-instance 'journey-station
                                        :start-ts event-start
                                        :end-ts event-end
                                        :station-name (trackernet::station-name entry)
                                        :platform (trackernet::platform entry)
                                        :uri (uri-for-station-event
                                              (trackernet::line-code lt)
                                              (trackernet::unique-id lt)
                                              entry)
                                        :era :historical)))
                     (remove-if-not
                      (lambda (event)
                        (and
                         (typep event 'trackernet::train-station-stop)
                         (if end-ts
                             (>= end-ts (trackernet::universal-ts event) start-ts)
                             (or
                              (>= (trackernet::universal-ts event) start-ts)
                              (>= (trackernet::end-ts event) start-ts)))))
                      history))))))

(defun render/historical-train-journey (lt)
  (let* ((line-code (trackernet::line-code lt))
         (line-data (tube-line-by-code line-code))
         (overall-identity (trackernet::train-identity lt))
         (identity-desc
           (or
            (when (first overall-identity)
              (format nil "#~A" (first overall-identity)))
            "Train"))
         (run-date (trackernet::get-iso-8601-for-ts-london
                    (trackernet::first-updated lt)))
         (journeys (get-journeys-for-lt lt))
         (*accent-colour* (apply #'html-rgb-colour (third line-data))))
    (render (:title (format nil "~A on ~A · ~A line"
                            identity-desc run-date (first line-data)))
      (:span
       :class "govuk-caption-xl"
       "Historical train journeys")
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "~A on ~A"
                identity-desc run-date))
      (:details
       :class "govuk-details"
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "More about this train"))
       (:div
        :class "govuk-details__text"
        (:p
         :class "govuk-body"
         "Use the "
         (:a
          :class "govuk-link"
          :href (format nil "~A?nojourney" (uri-for-lt lt))
          "old historical view")
         " for extra information, like leading car numbers."
         (:br)
         "You can also view "
         (:a
          :class "govuk-link"
          :href (easy-routes:genurl 'route-live-train-underlying
                                    :tid (subseq (trackernet::unique-id lt)
                                                 #.(length "train_")))
          (who:str
           (if (string= line-code "X")
               "the raw Network Rail data"
               "the raw Trackernet data")))
         " as it was received.")))
      (loop
        for (identity . journey) in journeys
        do (when journey
             (who:htm
              (:h2
               :class "govuk-heading-m"
               (who:str
                (cond
                  ((and (typep identity 'trackernet::train-identity-change)
                        (trackernet::wtt-id identity)
                        (trackernet::wtt-trip identity)
                        (equal (trackernet::wtt-id identity)
                               (first overall-identity)))
                   (format nil "Trip ~A"
                           (trackernet::wtt-trip identity)))
                  ((and (typep identity 'trackernet::train-identity-change)
                        (trackernet::wtt-id identity)
                        (trackernet::wtt-trip identity))
                   (format nil "Train ~,'03D, trip ~A"
                           (trackernet::wtt-id identity)
                           (trackernet::wtt-trip identity)))
                  ((and (typep identity 'trackernet::train-identity-change)
                        (trackernet::wtt-id identity)
                   (format nil "Train ~,'03D (trip unknown)"
                           (trackernet::wtt-id identity))))
                  ((typep identity 'trackernet::train-identity-change)
                   "Journey without a Working Timetable number")
                  ((typep identity 'trackernet::train-rescue)
                   (format nil "Headcode <span class=\"id-field\">~A</span>"
                           (trackernet::new-key identity)))))))
             (who:str (render-journey line-code journey)))))))

(defun render/historical-train (lt)
  (let* ((line-code (trackernet::line-code lt))
         (line-data (tube-line-by-code line-code))
         (identity (trackernet::train-identity lt))
         (branch (trackernet::branch lt))
         (identity-desc
           (or
            (when (first identity)
              (format nil "#~A" (first identity)))
            "Train"))
         (destination (or (trackernet::destination lt) "nowhere"))
         (run-date (trackernet::get-iso-8601-for-ts-london
                    (trackernet::first-updated lt)))
         (*accent-colour* (apply #'html-rgb-colour (third line-data))))
    (render (:title (format nil "~A to ~A · ~A line · ~A"
                            identity-desc destination (first line-data)
                            run-date))
      ;;(:a
      ;; :class "govuk-back-link"
      ;; :href (format nil "/~A-line?code=~A"
      ;;               (if (eql (fifth line-data) t) "model" "live")
      ;;               line-code)
      ;; (who:fmt "~A line"
      ;;          (first line-data)))
      (:span
       :class "govuk-caption-xl"
       (who:fmt "Historical running from ~A" run-date))
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "~A to ~A"
                identity-desc
                destination)
       (when branch
         (who:htm
          " "
          (:span
           :class "govuk-tag govuk-tag--turquoise"
           (who:fmt "~A branch" branch)))))
      (:details
       :class "govuk-details"
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "Details for nerds"))
       (:div
        :class "govuk-details__text"
        (:p
         :class "govuk-body"
         "View "
         (:a
          :class "govuk-link"
          :href (easy-routes:genurl 'route-live-train-underlying
                                    :tid (subseq (trackernet::unique-id lt)
                                                 #.(length "train_")))
          (who:str
           (if (string= line-code "X")
               "the raw Network Rail data"
               "the raw Trackernet data")))
         " for more granular information.")))
      (:h3
       :class "govuk-heading-m train-history-heading"
       "Train history")
      (:ul
       :class "govuk-list"
       :id "live-history"
       (who:str (display-train-history-entry
                 (trackernet::current-entry lt)
                 line-code :lastp nil :ltid (trackernet::unique-id lt)))
       (dolist (ent (trackernet::history lt))
         (who:str (display-train-history-entry ent line-code
                                               :lastp nil
                                               :ltid (trackernet::unique-id lt))))))))

(defun get-journey-so-far (lt)
  (nreverse
   (loop
     for entry in (trackernet::history lt)
     when (and (string= (trackernet::line-code lt) "X")
               (typep entry 'trackernet::train-rescue))
       do (loop-finish)
     when (typep entry 'trackernet::train-identity-change)
       do (loop-finish)
     when (typep entry 'trackernet::train-station-stop)
       collect entry)))

(defun render/train-journey (lt)
  (let* ((line-code (trackernet::line-code lt))
         (branch (trackernet::branch lt))
         (presentation-code
           (if (equal branch "H&C") "A" line-code))
         (line-data
           (tube-line-by-code presentation-code))
         (identity (trackernet::train-identity lt))
         (trackernet-id (trackernet::trackernet-id lt))
         (identity-desc-nullable
           (or
            (when (string= line-code "X")
              trackernet-id)
            (when (first identity)
              (format nil "#~A" (first identity)))))
         (identity-desc
           (or identity-desc-nullable
               "Train"))
         (progress
           (trackernet::toposort-model-station-progress
            line-code
            (first (trackernet::track-codes (trackernet::current-entry lt)))))
         (progress-perc
           (when progress
             (format nil "~D" (floor (* (second progress) 100)))))
         (destination (trackernet::destination lt))
         (destination-platform (trackernet::destination-platform lt))
         (model-point (trackernet::model-point lt))
         (destination-model-point
           (or
            (trackernet::toposort-model-point-for-station line-code destination (princ-to-string destination-platform))
            (trackernet::toposort-model-points-for-station line-code destination)))
         (journey-so-far (get-journey-so-far lt))
         (journey-to-go)
         (overall-journey)
         (current-entry (trackernet::current-entry lt))
         (*phase-banner* (or *phase-banner* (phase-banner-for-line line-code)))
         (*accent-colour* (apply #'html-rgb-colour (third line-data))))

    (unless destination-model-point
      (return-not-found
       (format nil "Sorry, we can't compute the journey for that train right now (failed to resolve destination '~A', platform ~A into a model point)"
               destination destination-platform)))
    (unless (setf journey-to-go (trackernet::toposort-model-path-between line-code model-point destination-model-point))
      (return-not-found
       (format nil "Sorry, we can't compute the journey for that train right now (pathfind failed, start = ~A, end = ~A)"
               model-point destination-model-point)))
    (setf overall-journey
          (append
           (loop
             for entry in journey-so-far
             for i from 0
             collect (make-instance 'journey-station
                                    :start-ts (trackernet::universal-ts entry)
                                    :end-ts (trackernet::end-ts entry)
                                    :station-name (trackernet::station-name entry)
                                    :platform (trackernet::platform entry)
                                    :uri (uri-for-station-event
                                          line-code
                                          (trackernet::unique-id lt)
                                          entry)
                                    :era :past))
           (if (typep current-entry 'trackernet::train-station-stop)
               (list
                (make-instance 'journey-station
                               :start-ts (trackernet::universal-ts current-entry)
                               :end-ts (trackernet::end-ts current-entry)
                               :station-name (trackernet::station-name current-entry)
                               :platform (trackernet::platform current-entry)
                               :era :current
                               :trainp t))
               (list
                (make-instance 'journey-train
                               :start-ts (trackernet::universal-ts current-entry)
                               :end-ts (trackernet::end-ts current-entry)
                               :progress progress-perc
                               :text (describe-train-entry-location (trackernet::current-entry lt) line-code)
                               :timing-remark (when (equal line-code "X")
                                                (format nil "~,'02D%" progress-perc))
                               :era :current)))
           (loop
             for mpt in (cdr journey-to-go)
             for i from 0
             append (multiple-value-bind (station pfm)
                        (trackernet::toposort-model-station-information line-code mpt)
                      (when station
                        (list
                         (make-instance 'journey-station
                                        :station-name station
                                        :platform
                                        (if (or (uiop:string-suffix-p pfm "(E)")
                                                (uiop:string-suffix-p pfm "(W)"))
                                            (subseq pfm 0 (- (length pfm) 3))
                                            pfm)
                                        :era :future)))))))
    (render (:title (format nil "~A to ~A · ~A line"
                            identity-desc destination (first line-data)))
      (:a
       :class "govuk-back-link"
       :href (uri-for-lt lt)
       "View full history")
      (:span
       :class "govuk-caption-xl"
       "Live train journey")
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "~A to ~A"
                identity-desc
                destination)
       (when (and branch
                  (or (equal branch "Shuttle")
                      (not (equal line-code "H"))))
         (who:htm
          " "
          (who:str (display-branch branch line-code)))))
      (who:str (render-journey line-code overall-journey
                               :past-context 2)))))

(defun render/live-train (lt)
  (let* ((line-code (trackernet::line-code lt))
         (branch (trackernet::branch lt))
         (presentation-code
           (if (equal branch "H&C") "A" line-code))
         (line-data
           (tube-line-by-code presentation-code))
         (identity (trackernet::train-identity lt))
         (secs-new (- (get-universal-time) (trackernet::first-updated lt)))
         (secs-old (- (get-universal-time) (trackernet::last-updated lt)))
         (trackernet-id (trackernet::trackernet-id lt))
         (trust-data-meh (trackernet::trust-data lt))
         ;; FIXME(eta): can be removed in like a day
         (trust-data (if (null (cddr trust-data-meh))
                         (cadr trust-data-meh)
                         (cdr trust-data-meh)))
         (schedule-uid (car (trackernet::nr-schedule-data lt)))
         (identity-desc-nullable
           (or
            (when (string= line-code "X")
              trackernet-id)
            (when (first identity)
              (format nil "#~A" (first identity)))))
         (identity-desc
           (or identity-desc-nullable
               "Train"))
         (identity-desc-share
           (or
            (when identity-desc-nullable
              (format nil "~A line train ~A"
                      (first line-data) identity-desc-nullable))
            (format nil "a ~A line train" (first line-data))))
         (destination (or (trackernet::destination lt) "nowhere"))
         (subsurface-lt
           (when (and
                  (member line-code '("H" "D" "M") :test #'string=)
                  (first identity))
             (let ((grouped
                     (group-lts-by-wtt
                      (live-trains-on-line "S"))))
               (cadr (assoc (first identity)
                            grouped
                            :test #'string=)))))
         (*phase-banner* (or *phase-banner* (phase-banner-for-line line-code)))
         (*accent-colour* (apply #'html-rgb-colour (third line-data))))
    (render (:title (format nil "~A to ~A · ~A line"
                            identity-desc destination (first line-data))
             :meta-props (list
                          (cons "og:description"
                                (who:escape-string
                                 (format nil "~A line train~A to ~A~A, ~A; first seen at ~A."
                                         (first line-data)
                                         (if identity-desc-nullable
                                             (format nil " ~A" identity-desc)
                                             "")
                                         destination
                                         (if (and branch
                                                  (not (equal line-code "C"))
                                                  (not (equal line-code "A")))
                                             (format nil " (~A branch)" branch)
                                             "")
                                         (describe-train-history-entry
                                          (trackernet::current-entry lt)
                                          line-code)
                                         (trackernet::universal-time-railway
                                          (trackernet::first-updated lt)))))))
      (:a
       :class "govuk-back-link"
       :href (easy-routes:genurl 'route-line :code presentation-code)
       (who:fmt "~A line"
                (first line-data)))
      (:span
       :class "govuk-caption-xl"
       "Live train tracker")
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "~A to ~A"
                identity-desc
                destination)
       (when (and branch
                  (or (equal branch "Shuttle")
                      (not (equal line-code "H"))))
         (who:htm
          " "
          (who:str (display-branch branch line-code)))))
      (who:htm
       (:div
        :class "govuk-warning-text"
        :id "reconnection-warning"
        :style "display=none;"
        (:span
         :class "govuk-warning-text__icon"
         "!")
        (:strong
         :class "govuk-warning-text__text"
         :id "reconnection-warning-text"
         "Attempting to connect. Until this completes, data will not update in real-time.")))
      (when (< secs-new 45)
        (who:htm
         (:div
          :class "govuk-warning-text"
          (:span
           :class "govuk-warning-text__icon"
           "!")
          (:strong
           :class "govuk-warning-text__text"
           "This train might be missing data. Please wait a few moments while we attempt to find more history."))))
      (unless (string= line-code "X")
        (cond
          ((> secs-old 120)
           (who:htm
            (:div
             :class "govuk-warning-text"
             (:span
              :class "govuk-warning-text__icon"
              "!")
             (:strong
              :class "govuk-warning-text__text"
              "We seem to have lost track of this train, due to gaps in our open data sources."))))
          ((> secs-old 30)
           (who:htm
            (:div
             :class "govuk-warning-text"
             (:span
              :class "govuk-warning-text__icon"
              "!")
             (:strong
              :class "govuk-warning-text__text"
              "Live updates for this train are currently delayed."))))))
      (:p
       :class "govuk-body"
       (:a
        :class "govuk-link share-train-link"
        :id "share-train"
        :style "display: none;"
        :href "#"
        "Share this train with others"))
      (:details
       :class "govuk-details"
       (:summary
        :class "govuk-details__summary"
        (:span
         :class "govuk-details__summary-text"
         "Details for nerds"))
       (:div
        :class "govuk-details__text"
        (:p
         :class "govuk-body"
         "View "
         (:a
          :class "govuk-link"
          :href (easy-routes:genurl 'route-live-train-underlying
                                    :tid (subseq (trackernet::unique-id lt)
                                                 #.(length "train_")))
          (who:str
           (if (string= line-code "X")
               "the raw Network Rail data"
               "the raw Trackernet data")))
         " for more granular information.")
        (when (and (string= line-code "X")
                   schedule-uid)
          (who:htm
           (:p
            :class "govuk-body"
            "View "
            (:a
             :class "govuk-link"
             :href (format nil "https://www.realtimetrains.co.uk/service/gb-nr:~A/~A/detailed"
                           (string-left-trim '(#\Space) schedule-uid)
                           (trackernet::get-iso-8601-date))
             "this train's schedule on Realtime Trains")
            " ("
            (who:esc (string-left-trim '(#\Space) schedule-uid))
            "/"
            (who:esc (second (trackernet::nr-schedule-data lt)))
            ").")))
        (if (and (string= line-code "X")
                 trust-data)
            (who:htm
             (:p
              :class "govuk-body"
              "Train created in TRUST at "
              (who:str (trackernet::get-iso-8601-for-ts-london (cdr (assoc :creation-ts trust-data))))
              " "
              (who:str (trackernet::universal-time-railway (cdr (assoc :creation-ts trust-data))))
              " with ID "
              (:span
               :class "!-monospace"
               (who:esc (cdr (assoc :trust-id trust-data))))
              "."))
            (when (string= line-code "X")
              (who:htm
               (:p
                :class "govuk-body"
                "No TRUST or NR scheduling data available."))))
        (who:htm
         (:p
          :class "govuk-body"
          (:a
           :class "govuk-link"
           :href (uri-for-lt-journey lt)
           "Try the experimental journey view")
          " (very beta!)"))
        (:p
         :class "govuk-body"
         "Internal debugging information (does not update live):"
         (:br)
         (:span
          :class "!-monospace"
          "model-point&nbsp;= "
          (who:esc (trackernet::model-point lt))
          (:br)
          "input-dest&nbsp;&nbsp;= "
          (who:esc (first (trackernet::input-dest lt)))
          " ")
         (if (second (trackernet::input-dest lt))
             (who:htm
              (:span
               :class "govuk-tag govuk-tag--green"
               "parsed"))
             (who:htm
              (:span
               :class "govuk-tag govuk-tag--red"
               "parse failed"))))
        (when subsurface-lt
          (who:htm
           (:p
            :class "govuk-body"
            (:a
             :class "govuk-link"
             :href (uri-for-lt subsurface-lt)
             "View this train on line S instead")
            " (for debugging only!).")))))
      (:h3
       :class "govuk-heading-m train-history-heading"
       "Current status")
      (:ul
       :class "govuk-list"
       :id "live-current"
       (who:str (display-train-history-entry
                 (trackernet::current-entry lt)
                 line-code
                 :lastp t
                 :ltid (trackernet::unique-id lt))))
      (:h3
       :class "govuk-heading-m train-history-heading"
       "Train history")
      (:ul
       :class "govuk-list"
       :id "live-history"
       (dolist (ent (trackernet::history lt))
         (who:str (display-train-history-entry ent line-code
                                               :lastp nil
                                               :ltid (trackernet::unique-id lt)))))
      (:span
       :id "live-ws-link"
       :style "display: none;"
       (who:esc (format nil "/live-ws?id=~A" (trackernet::unique-id lt))))
      (:span
       :id "share-heading"
       :style "display: none;"
       (who:esc (format nil "Hello from ~A to ~A" identity-desc-share destination)))
      (:script
       (who:str *live-js*)))))

(defun render/raw-train (train limit &key orig-ltid)
  (when limit
    (unless (setf limit (ignore-errors (parse-integer limit)))
      (return-bad-request)))
  (when (< (length train) #.(length "X-train-Y"))
    (return-bad-request))
  (unless (red:exists train)
    (alexandria:when-let ((new-train (red:get (format nil "rescue-ptr-~A" train))))
      ;; train got rescued into another train
      (relative-redirect
       (easy-routes:genurl 'route-underlying-train :tid train :limit limit)))
    (return-not-found))
  (let ((train-data (with-server-timing ("fetch-sorted-set-all")
                      (trackernet::redis-cpk-sorted-set-all train))))
    (render/raw-train-data (subseq train 0 1) train-data limit
                           :live-name train
                           :orig-ltid orig-ltid)))

(defun render/raw-clickhouse-train (historical-records orig-ltid)
  (let ((line-code (trackernet::line (first historical-records)))
        (train-data (mapcar (lambda (rec)
                              (list (trackernet::universal-ts rec)
                                    (trackernet::observer rec)
                                    rec))
                            historical-records)))
    (render/raw-train-data line-code train-data 99999999
                           :orig-ltid orig-ltid)))

(defun render/raw-train-data (line-code train-data limit &key orig-ltid live-name)
  (let* ((line-data (tube-line-by-code line-code))
         (train-data-len (length train-data))
         (*accent-colour* (apply #'html-rgb-colour (third line-data)))
         (start-ts (universal-time-hms (first (first train-data))))
         (last-train-obj (third (car (last train-data))))
         (secs-old (when live-name
                     (- (get-universal-time) (trackernet::redis-last-score live-name))))
         (max-to-show (min train-data-len
                           (or limit *max-train-lines*)))
         (lcids '())
         (dests '())
         (wtts '()))
    (setf train-data
          (sort train-data #'>
                :key (lambda (trn)
                       (first trn))))
    (loop
      for (ts reporter train) in train-data
      do (pushnew (trackernet::set-no train) wtts
                  :test #'string=)
         (when (trackernet::destination-desc train)
           (pushnew (trackernet::destination-desc train) dests
                    :test #'string=))
         (pushnew (trackernet::lcid train) lcids
                  :test #'string=))
    (render ()
      (if orig-ltid
          (who:htm
           (:a
            :class "govuk-back-link"
            :href (easy-routes:genurl 'route-live-train
                                      :tid orig-ltid)
            (who:fmt "Back to ~A" (if live-name
                                      "live train tracker"
                                      "historical train"))))
          (who:htm
           (:a
            :class "govuk-back-link"
            :href (easy-routes:genurl 'route-line
                                      :code line-code)
            (who:fmt "~A line"
                     (first line-data)))))
      (:span
       :class "govuk-caption-xl"
       (who:fmt
        "~A raw data for ~A"
        (if (string= line-code "X") "Network Rail" "Trackernet")
        (or live-name "historical train")))
      (:h1
       :class "govuk-heading-xl"
       (who:fmt "Train to ~A "
                (or
                 (trackernet::destination-desc
                  last-train-obj)
                 "nowhere")))
      (when live-name
        (who:htm
         (:p
          :class "govuk-body-l"
          (unless (or
                   (string= line-code "X"))
            (cond
              ((> secs-old 90)
               (who:htm
                (:strong
                 :class "govuk-tag govuk-tag--red"
                 (who:fmt "Very Stale"))))
              ((> secs-old 30)
               (who:htm
                (:strong
                 :class "govuk-tag govuk-tag--red"
                 (who:fmt "Stale"))))
              ((> secs-old 15)
               (who:htm
                (:strong
                 :class "govuk-tag govuk-tag--orange"
                 "Intermittent")))
              (t
               (who:htm
                (:strong
                 :class "govuk-tag govuk-tag--turquoise"
                 "Active")))))
          " Showing "
          (:strong
           (who:fmt "~A" max-to-show))
          " of "
          (:strong
           (who:fmt "~A" train-data-len))
          " "
          (who:str
           (if (string= line-code "X")
               "TD berth steps"
               "observations"))
          "  for this train; first observed "
          (:strong
           (who:esc start-ts))
          ".")))
      (unless (string= line-code "X")
        (who:htm
         (:p
          :class "govuk-body"
          (who:fmt "Leading car IDs: ~{~A~^, ~} &middot; WTT IDs: ~{~A~^, ~} &middot; Destinations: ~{~A~^, ~}"
                   (nreverse lcids)
                   (nreverse wtts)
                   (nreverse dests)))))
      (:table
       :class "govuk-table"
       (:thead
        :class "govuk-table__head"
        (:tr
         :class "govuk-table__row"
         (:th
          :class "govuk-table__header"
          "Time")
         (unless (string= line-code "X")
           (who:htm
            (:th
             :class "govuk-table__header !-no-phones"
             (:abbr
              :title "Working Timetable train number"
              "WTT"))
            (:th
             :class "govuk-table__header !-no-phones"
             (:abbr
              :title "Leading Car ID (LCID)"
              "LCID"))))
         (:th
          :class (format nil "govuk-table__header ~A"
                         (if (string= line-code "X") "" "govuk-!-width-two-thirds"))
          "Location")))
       (:tbody
        :class "govuk-table__body"
        (loop
          with last = nil
          with last-dest = nil
          with last-ts = nil
          with smart-data = nil
          for (ts reporter train) in train-data
          for i from 0
          unless (or
                  (and
                   (equal last (trackernet::track-code train))
                   (not (string= line-code "X"))
                   (not (string= line-code "S")))
                  (>= i max-to-show))
            do (when (and
                      (equal line-code "X")
                      last
                      last-ts
                      (position #\· (trackernet::track-code train))
                      (position #\· last)
                      (setf smart-data
                            (trackernet::smart-operations-for-step
                             (subseq (trackernet::track-code train) 0 2)
                             (subseq reporter 3 5)
                             (subseq last 3)
                             (list
                              (subseq (trackernet::track-code train) 3))
                             :previous-area (subseq last 0 2))))
                 (loop
                   for (arr-dep station-name platform offset description)
                   in (reverse smart-data)
                   do
                      (who:htm
                       (:tr
                        :class "train-desc govuk-table__row"
                        (:td
                         :class "govuk-table__cell ts-field !-secondary-colour"
                         (who:esc (universal-time-hms (+ last-ts offset))))
                        (:td
                         :class "govuk-table__cell !-secondary-colour"
                         (who:esc description)
                         (:span
                          :class "govuk-tag govuk-tag--pink train-reporter"
                          "SMART"))))))
               (who:str (display-trackernet-train train
                                                  line-code
                                                  :ts ts
                                                  :show-dest (not (string= (trackernet::destination-desc train) last-dest))
                                                  :reporter
                                                  (if (typep train 'trackernet::clickhouse-trackernet-train)
                                                      (format nil "X-~A-X" reporter)
                                                      reporter)
                                                  ))
               (setf last-dest (trackernet::destination-desc train))
               (setf last-ts ts)
               (setf last (trackernet::track-code train))))
       (unless (>= max-to-show train-data-len)
         (who:htm
          (:p
           :class "govuk-body"
           (:a
            :class "govuk-link"
            :href (if orig-ltid
                      (easy-routes:genurl 'route-live-train-underlying
                                          :tid orig-ltid
                                          :limit (+ max-to-show 500))
                      (easy-routes:genurl 'route-underlying-train
                                          :tid live-name
                                          :limit (+ max-to-show 500)))
            "Show up to 500 more observations"))))))))


(defvar *track-suggestions-cache* (make-hash-table :test 'equal))

(defun cached-track-suggestions (code)
  (symbol-macrolet
      ((cached-result (gethash code *track-suggestions-cache*)))
    (if cached-result
        cached-result
        (setf cached-result (with-server-timing ("line-track-descriptions")
                              (trackernet::line-track-descriptions code))))))

(defun now-london-time-str ()
  (local-time:format-timestring
   nil
   (local-time:now)
   :format
   '((:year 4) #\- (:month 2) #\- (:day 2) #\Space
     (:hour 2) #\: (:min 2) #\: (:sec 2))
   :timezone
   trackernet::+europe-london-tz+))

(defun filter-old-lts (live-trains)
  (remove-if (lambda (lt)
               (> (- (get-universal-time) 30)
                  (trackernet::last-updated lt)))
             live-trains))

(defun maybe-filter-lts (code live-trains)
  (cond
    ((string= code "B")
      (remove-if (lambda (lt)
                   (uiop:string-prefix-p "Overground to"
                                         (trackernet::destination lt)))
                 (filter-old-lts live-trains)))
    ((string= code "X") live-trains)
    (t (filter-old-lts live-trains))))


(hunchentoot:define-easy-handler (platform-jump :uri "/platform-jump") (pfm)
  (relative-redirect (babel:octets-to-string
                      (qbase64:decode-string pfm))))

(defun tagged-phase-banner (line-or-colour tag message)
  (let* ((line-data (tube-line-by-code line-or-colour))
         (tag-colour
           (if line-data
               (apply #'html-rgb-colour (third line-data))
               line-or-colour)))
    (format
     nil
     "<strong class=\"govuk-tag govuk-phase-banner__content__tag\" style=\"background-color: ~A;\">~A</strong>~A"
     tag-colour
     tag
     (who:escape-string message))))

(defun phase-banner-for-line (code)
  (cond
    ((string= code "D")
     (tagged-phase-banner
      "D"
      "BETA"
      "Trains may be missing or appear stuck."
      ))
    ((string= code "B")
     (tagged-phase-banner
      "B"
      "Note"
      "Trains north of Stonebridge Park will display inaccurately due to data issues."
      ))
    ((string= code "M")
     (tagged-phase-banner
      "M"
      "BETA"
      "Poor quality. Use with caution!"))
    (t nil)))

(defun render/line-status (code)
  (multiple-value-bind (severity title body)
      (trackernet::tfl-line-status-for code)
    (cond
      ((and severity (<= severity 6))
       (who:with-html-output-to-string (*standard-output* nil
                                        :prologue nil)
         (:div
          :class "govuk-error-summary"
          (:div
           :role "alert"
           (:h2
            :class "govuk-error-summary__title"
            (who:esc title))
           (:div
            :class "govuk-error-summary__body"
            (:p
             :class "govuk-body !-no-bottom-margin"
             (who:esc (string-right-trim '(#\Space #\Newline #\Tab) body))))))))
      (severity
       (who:with-html-output-to-string (*standard-output* nil
                                        :prologue nil)
         (:div
          :class "govuk-notification-banner"
          (:div
           :class "govuk-notification-banner__header"
           (:h2
            :class "govuk-notification-banner__title"
            (who:esc title)))
          (:div
           :class "govuk-notification-banner__content"
           (:p
            :class "govuk-body !-no-bottom-margin"
            (who:esc (string-right-trim '(#\Space #\Newline #\Tab) body)))))))
      (t ""))))

(defun render/model-line (code direction)
  (let* ((line-data (tube-line-by-code code))
         (provided-code code)
         (code (if (equal code "A") "H" code))
         (cookie-name (format nil "model-line-dir-~A" code))
         (cookie-value (hunchentoot:cookie-in cookie-name))
         ;; Get a provided cookie to provide a default direction, but
         ;; check that it works first, so you can't poison yourself.
         (direction (or direction
                        (when (and cookie-value
                                   (model-data-by-code-and-direction provided-code cookie-value))
                          cookie-value)
                        (default-model-direction provided-code)))
         (model-data (model-data-by-code-and-direction provided-code direction)))
    (unless (and line-data model-data)
      (return-not-found))
    (let* ((*accent-colour* (apply #'html-rgb-colour (third line-data)))
           (*phase-banner* (or *phase-banner* (phase-banner-for-line code)))
           (active-lts (with-server-timing ("get-live-trains")
                         (maybe-filter-lts code (live-trains-on-line code))))
           (wtt-grouped (if (string= code "X")
                            (group-lts-by-headcode active-lts)
                            (group-lts-by-wtt active-lts)))
           (lc-grouped (unless (string= code "X")
                         (group-lts-by-leading-car active-lts)))
           (displayed 0))
      (hunchentoot:set-cookie cookie-name
                              :value direction)
      (render (:title (format nil "~A line" (first line-data)))
        (:a
         :class "govuk-back-link"
         :href "/"
         "All Tube lines")
        (:span
         :class "govuk-caption-xl"
         "Line overview")
        (:h1
         :class "govuk-heading-xl"
         (who:fmt "~A line"
                  (first line-data)))
        (who:str (render/line-status provided-code))
        (when (zerop (length active-lts))
          (who:htm
           (:div
            :class "govuk-warning-text"
            (:span
             :class "govuk-warning-text__icon"
             "!")
            (:strong
             :class "govuk-warning-text__text"
             "We can't find any trains on this line. This may be due to technical issues."))))
        (:form
         :action "/model-line"
         :method "get"
         :id "model-line-form"
         (:input
          :name "code"
          :id "linecode"
          :type "hidden"
          :value provided-code)
         (:div
          :class "govuk-form-group"
          (:fieldset
           :class "govuk-fieldset"
           (:div
            :class "govuk-radios"
            (cond
              ((string= code "N")
               ;; Special-case collapse the Northern line into
               ;; "all northbound directions" + "all southbound directions",
               ;; since the directions get chosen via inline button switchers
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "edgware-bank"
                  :name "direction"
                  :type "radio"
                  :value "edgware-bank"
                  :checked (when (uiop:string-prefix-p "Northbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "edgware-bank"
                  "Northbound "
                  (:i
                   "towards Edgware or High Barnet")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "morden-edgware-bank"
                  :name "direction"
                  :type "radio"
                  :value "morden-edgware-bank"
                  :checked (when (uiop:string-prefix-p "Southbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "morden-edgware-bank"
                  "Southbound "
                  (:i
                   "towards Kennington, Morden or Battersea")))))
              ((string= code "P")
               ;; Another special-case, this time for the Picc
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "eastbound-uxbridge"
                  :name "direction"
                  :type "radio"
                  :value "eastbound-uxbridge"
                  :checked (when (uiop:string-prefix-p "Eastbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "eastbound-uxbridge"
                  "Eastbound "
                  (:i
                   "towards Cockfosters")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "westbound-uxbridge"
                  :name "direction"
                  :type "radio"
                  :value "westbound-uxbridge"
                  :checked (when (uiop:string-prefix-p "Westbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "westbound-uxbridge"
                  "Westbound "
                  (:i
                   "towards Uxbridge or Heathrow Airport ✈")))))
              ((string= code "D")
               ;; ...and the District
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :name "direction"
                  :id "edgware-wimbledon"
                  :type "radio"
                  :value "edgware-wimbledon"
                  :checked (when (uiop:string-prefix-p "Eastbound <i>towards Edgware Road"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "edgware-wimbledon"
                  "Northbound "
                  (:i
                   "towards Edgware Road")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :name "direction"
                  :id "upminster-ealing"
                  :type "radio"
                  :value "upminster-ealing"
                  :checked (when (uiop:string-prefix-p "Eastbound <i>towards Upminster"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "upminster-ealing"
                  "Eastbound "
                  (:i
                   "towards Upminster")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :name "direction"
                  :id "wimbledon-edgware"
                  :type "radio"
                  :value "wimbledon-edgware"
                  :checked (when (uiop:string-prefix-p "Westbound <i>towards Wimbledon"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "wimbledon-edgware"
                  "Southbound "
                  (:i
                   "towards Wimbledon")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :name "direction"
                  :id "ealing-upminster"
                  :type "radio"
                  :value "ealing-upminster"
                  :checked (when (and
                                  (uiop:string-prefix-p "Westbound"
                                                        (car model-data))
                                  (not
                                   (uiop:string-prefix-p "Westbound <i>towards Wimbledon"
                                                         (car model-data))))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "ealing-upminster"
                  "Westbound "
                  (:i
                   "towards Ealing Broadway or Richmond")))))
              ((string= code "X")
               ;; ...and XR
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "abbey-wood-reading"
                  :name "direction"
                  :type "radio"
                  :value "abbey-wood-reading"
                  :checked (when (uiop:string-prefix-p "Eastbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "abbey-wood-reading"
                  "Eastbound "
                  (:i
                   "towards Abbey Wood or Shenfield")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "reading-abbey-wood"
                  :name "direction"
                  :type "radio"
                  :value "reading-abbey-wood"
                  :checked (when (uiop:string-prefix-p "Westbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "reading-abbey-wood"
                  "Westbound "
                  (:i
                   "towards Reading or Heathrow Airport ✈")))))
              ((string= code "C")
               ;; ...and the Central
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "hainault-newbury-ealing"
                  :name "direction"
                  :type "radio"
                  :value "hainault-newbury-ealing"
                  :checked (when (uiop:string-prefix-p "Eastbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "hainault-newbury-ealing"
                  "Eastbound "
                  (:i
                   "towards Hainault or Epping")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "ruislip-newbury-hainault"
                  :name "direction"
                  :type "radio"
                  :value "ruislip-newbury-hainault"
                  :checked (when (uiop:string-prefix-p "Westbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "ruislip-newbury-hainault"
                  "Westbound "
                  (:i
                   "towards Ealing Broadway or West Ruislip")))))
              ((string= code "M")
               ;; ...and the Met
               (who:htm
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "westbound-amersham"
                  :name "direction"
                  :type "radio"
                  :value "westbound-amersham"
                  :checked (when (uiop:string-prefix-p "Westbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "westbound-amersham"
                  "Westbound "
                  (:i
                   "towards Amersham, Watford or Uxbridge")))
                (:div
                 :class "govuk-radios__item"
                 (:input
                  :class "govuk-radios__input direction-radio-button"
                  :id "eastbound-amersham"
                  :name "direction"
                  :type "radio"
                  :value "eastbound-amersham"
                  :checked (when (uiop:string-prefix-p "Eastbound"
                                                       (car model-data))
                             "checked"))
                 (:label
                  :class "govuk-label govuk-radios__label"
                  :for "eastbound-amersham"
                  "Eastbound "
                  (:i
                   "towards Aldgate")))))
              (t
               (loop
                 for (dir descr . data) in (line-model-data-for provided-code)
                 do (who:htm
                     (:div
                      :class "govuk-radios__item"
                      (:input
                       :class "govuk-radios__input direction-radio-button"
                       :id dir
                       :name "direction"
                       :type "radio"
                       :value dir
                       :checked (when (string= dir direction)
                                  "checked"))
                      (:label
                       :class "govuk-label govuk-radios__label"
                       :for dir
                       (who:str descr))))))))))
         (:button
          :type "submit"
          :id "change-direction-button"
          :class "govuk-button"
          "Change direction"))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Find a station platform quickly"))
         (:div
          :class "govuk-details__text"
          :style "margin-bottom: 5px;"
          (:form
           :action "/platform-jump"
           :method "get"
           :id "platform-jump-form"
           (:div
            :class "govuk-form-group"
            (:select
             :class "govuk-select"
             :id "pfm"
             :name "pfm"
             (:option
              :value ""
              :selected "true"
              :disabled "true"
              :hidden "true"
              "(select an option to jump)")
             (loop
               for (name platforms jump-dir descr)
                 in (cdr (assoc provided-code *model-line-stops* :test #'string=))
               do (who:htm
                   (:option
                    :value (string-base64
                            (format nil "~A#~A"
                                    (easy-routes:genurl 'route-line
                                                        :code provided-code
                                                        :route jump-dir)
                                    (string-base64
                                     (format nil "~A ~{~A~^ ~}"
                                             name platforms))))
                    (who:fmt "~A plat. ~{~A~^, ~} → ~A"
                             name platforms descr))))))
           (:button
            :type "submit"
            :id "platform-jump-button"
            :class "govuk-button"
            "Jump to location"))))
        (:script
         :type "text/javascript"
         (who:str *direction-js*))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           (who:str
            (if (string= code "X")
                "Find a train by headcode"
                "Find a train by Working Timetable (WTT) or leading car number"))))
         (:div
          :class "govuk-details__text"
          (unless (string= code "X")
            (who:htm
             (:p
              :class "govuk-body"
              (:strong
               "Working Timetable (WTT) numbers"))))
          (:p
           :class "govuk-body"
           (loop
             with len = (length wtt-grouped)
             for (wtt . trains) in wtt-grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link !-monospace"
                  :href (uri-for-lt (first trains))
                  (who:esc wtt)))
             unless (eql len i)
               do (who:str " &middot; ")))
          (:p
           :class "govuk-body"
           "Can't find what you're looking for? Try "
           (:a
            :class "govuk-link"
            :href (easy-routes:genurl 'route-id-search
                                      :code code)
            "searching the archives by "
            (who:str
             (if (string= code "X")
                 "headcode"
                 "Working Timetable number")))
           ".")
          (unless (string= code "X")
            (who:htm
             (:p
              :class "govuk-body"
              (:strong
               "Leading car numbers"))
             (:p
              :class "govuk-body"
              (loop
                with len = (length lc-grouped)
                for (wtt . trains) in lc-grouped
                for i from 1
                do (who:htm
                    (:a
                     :class "govuk-link !-monospace"
                     :href (uri-for-lt (first trains))
                     (who:esc wtt)))
                unless (eql len i)
                  do (who:str " &middot; ")))))))
        (:ul
         :class "govuk-list live-line-list"
         (loop
           with trains-here and trains-after
           and model-points-here and model-points-after
           and next-station
           for (station-name . platforms) in (cdr model-data)
           for i from 0
           ;; Allow injecting arbitrary HTML
           if (eql station-name :html)
             do (who:htm
                 (:li
                  :class "arbitrary-html-li"
                  :style "background: white; margin-bottom: 0px;"
                  (who:str (car platforms))))
           else
           do (setf trains-here nil)
              (setf trains-after nil)
              (setf model-points-here nil)
              (setf model-points-after nil)
              (setf next-station
                    (flet
                        ;; FIXME(eta): this really sucks
                        ((station-at-point (x)
                           (unless (>= x (length (cdr model-data)))
                             (let ((ret (elt (cdr model-data) x)))
                               (unless (eql (car ret) :html)
                                 ret)))))
                      (or (station-at-point (1+ i))
                          (station-at-point (+ 2 i)))))

              ;; FIXME(eta): I don't really love this O(n^2) thing which
              ;;             could also totally just be precomputed, but eh.
              (flet ((crossrailize (list-of-platforms)
                       (append list-of-platforms
                               (alexandria:mappend
                                (lambda (pfm)
                                  (list
                                   (format nil "~A(E)" pfm)
                                   (format nil "~A(W)" pfm)))
                                list-of-platforms))))
                ;; Crossrail platforms might have (E) or (W) appended in
                ;; bi-di sections of the COS. Take this into account when
                ;; searching for platforms.
                (let* ((our-pfms-pre
                         (mapcar #'princ-to-string platforms))
                       (our-pfms
                         (if (equal code "X")
                             (crossrailize our-pfms-pre)
                             our-pfms-pre)))
                  (dolist (our-pfm our-pfms)
                    ;; Find a model point for the current station platform.
                    (alexandria:when-let
                        ((model-point
                          (trackernet::toposort-model-point-for-station
                           code station-name (princ-to-string our-pfm))))
                      (push model-point model-points-here))
                    (when next-station
                      (let* ((their-pfms-pre
                               (mapcar #'princ-to-string (cdr next-station)))
                             (their-pfms
                               (if (equal code "X")
                                   (crossrailize their-pfms-pre)
                                   their-pfms-pre)))
                        ;; Find one for the bit of track between this and the next.
                        (dolist (their-pfm their-pfms)
                          (alexandria:when-let
                              ((model-point
                                (trackernet::toposort-model-point-between-stations
                                 code station-name (princ-to-string our-pfm)
                                 (car next-station) (princ-to-string their-pfm))))
                            (push model-point model-points-after))))))))

              ;; A train is here if it's in one of the model points.
              (setf trains-here
                    (remove-if-not
                     (lambda (lt)
                       (or (member (trackernet::model-point lt)
                                   model-points-here
                                   :test #'equal)
                           ;; HACK(eta): or if it has a matching entry.
                           (and
                            (typep (trackernet::current-entry lt)
                                   'trackernet::train-station-stop)
                            (string= (trackernet::station-name
                                      (trackernet::current-entry lt))
                                     station-name)
                            (member (trackernet::platform
                                     (trackernet::current-entry lt))
                                    (mapcar #'princ-to-string
                                            platforms)
                                    :test #'equal))))
                     active-lts))

              ;; A train is after if it's in one of the model points.
              ;; We sort them by their progress.
              (setf trains-after
                    (sort
                     (remove-if-not
                      (lambda (lt)
                        (and (member (trackernet::model-point lt)
                                     model-points-after
                                     :test #'equal)
                             ;; HACK(eta): this makes me sad
                             (not
                              (typep
                               (trackernet::current-entry lt)
                               'trackernet::train-station-stop))))
                      active-lts)
                     (lambda (t1 t2)
                       (let ((p1 (second
                                  (trackernet::toposort-model-station-progress
                                   code
                                   (first (trackernet::track-codes
                                           (trackernet::current-entry t1))))))
                             (p2 (second
                                  (trackernet::toposort-model-station-progress
                                   code
                                   (first (trackernet::track-codes
                                           (trackernet::current-entry t2)))))))
                         (and p1 p2
                              (< p1 p2))))))
              (who:htm
               (:li
                :class "live-line-station"
                (:span
                 :class "live-line-bsicon"
                 :title (format nil "~A platforms ~{~A~^, ~}&#010;here:~{~A~^, ~}&#010;after:~{~A~^, ~}"
                                station-name platforms
                                model-points-here model-points-after)
                 :style (format nil "color: ~A;" *accent-colour*)
                 (cond
                   ((or (eql i 0)
                        ;; FIXME: hacky
                        (and (eql i 1)
                             (eql (caar (cdr model-data)) :html)))
                    (who:str *bsicons-bhf-start-svg*))
                   ((eql (1+ i) (length (cdr model-data)))
                    (who:str *bsicons-bhf-end-svg*))
                   ((uiop:string-suffix-p station-name "Sidings")
                    (who:str *bsicons-kmw-svg*))
                   (t
                    (who:str *bsicons-bhf-svg*)))
                 (when trains-here
                   (who:htm
                    (:span
                     :class "live-line-bsicon-train"
                     (who:str *metro-svg*)))))
                (:span
                 :class "govuk-heading-m live-line-station-name"
                 :id (string-base64
                      (format nil "~A ~{~A~^ ~}"
                              station-name platforms))
                 (:a
                  :href (uri-for-station code station-name)
                  :class "station-calls-link"
                  (who:esc station-name))
                 (:span
                  :class "!-secondary-colour"
                  (who:fmt "~A ~{~A~^, ~}"
                           (if (uiop:string-suffix-p station-name "Sidings") ""
                               " plat.")
                           platforms))
                 (unless model-points-here
                   (who:htm
                    (:small
                     :class "!-error-colour"
                     :title "No model information available for this station"
                     " ⚠"))))
                (unless (null trains-here)
                  (incf displayed (length trains-here))
                  (who:htm
                   (:a
                    :href (if (> (length trains-here) 1)
                              (format nil "/choose-live?line=~A&ids=~{~A~^,~}"
                                      code
                                      (mapcar #'trackernet::unique-id trains-here))
                              (uri-for-lt (first trains-here)))
                    :class "live-line-station-link govuk-link"
                    (:span
                     :class "!-underline"
                     (who:fmt "~A train~A" (length trains-here)
                              (if (> (length trains-here) 1)
                                  "s"
                                  "")))
                    " " ;; Inter-specific right-arrow glyph
                    )))
                 (when (null trains-here)
                   (alexandria:when-let
                       ((wait-mins (when (eql (length platforms) 1)
                                     (tph-in-mins code station-name (first platforms)))))
                     (who:htm
                      (:span
                       :class "live-line-station-tph"
                       (who:esc wait-mins) " wait"))))))
              (when (and
                     (null trains-after)
                     (not (eql i (1- (length (cdr model-data))))))
                (who:htm
                 (:li
                  :class "line-platform-intermediate-blank"
                  (:span
                   :class "live-line-bsicon-intermediate"
                   :style (format nil "color: ~A;" *accent-colour*)
                   (who:str *bsicons-str-svg*)))))
              (unless (eql i (1- (length (cdr model-data))))
                (unless (null trains-after)
                  (who:htm
                   (:li
                    :class "line-platform-intermediate-filler"
                    (:span
                     :class "live-line-bsicon-intermediate"
                     :style (format nil "color: ~A;" *accent-colour*)
                     (who:str *bsicons-str-svg*)))))
                (dolist (lt trains-after)
                  (incf displayed)
                  (who:htm
                   (:li
                    :class "line-platform-intermediate"
                    (:span
                     :class "live-line-bsicon"
                     :style (format nil "color: ~A;" *accent-colour*)
                     (who:str *bsicons-str-svg*)
                     (who:htm
                      (:span
                       :class "live-line-bsicon-train"
                       (who:str *metro-svg*))))
                    (:span
                     :class "line-intermediate-descr"
                     (when (> (length trains-after) 1)
                       (let ((progress
                               (second
                                (trackernet::toposort-model-station-progress
                                 (trackernet::line-code lt)
                                 (first
                                  (trackernet::track-codes
                                   (trackernet::current-entry lt)))))))
                         (when progress
                           (who:str (format nil "(~D%) " (floor (* progress 100)))))))
                     (who:esc (or
                               (trackernet::description (trackernet::current-entry lt))
                               (cdr (assoc (first (trackernet::track-codes
                                                   (trackernet::current-entry lt)))
                                           (cached-track-suggestions code)
                                           :test #'string=))
                               (format nil "unknown location ~A"
                                       (first (trackernet::track-codes
                                               (trackernet::current-entry lt)))))))
                    (:a
                     :href (uri-for-lt lt)
                     :class "line-intermediate-train govuk-link"
                     (:span
                      :class "!-underline"
                      "view")
                     " " ;; Inter-specific right-arrow glyph
                     ))))
                (unless (null trains-after)
                  (who:htm
                   (:li
                    :class "line-platform-intermediate-filler"
                    (:span
                     :class "live-line-bsicon-intermediate"
                     :style (format nil "color: ~A;" *accent-colour*)
                     (who:str *bsicons-str-svg*)))))
                )))
        (:p
         :class "govuk-body"
         "Showing "
         (:strong
          (who:fmt "~A" displayed))
         " &quot;"
         (who:str (car model-data))
         "&quot; trains, out of "
         (who:str (length active-lts))
         " total trains on this line. "
         (:a
          :href "#model-line-form"
          :class "govuk-link"
          "Change direction")
         " to view other trains.")
        (:p
         :class "govuk-body"
         (:span
          :class "!-error-colour"
          "⚠")
         " Information might be missing or wrong in some areas. These are indicated with a red exclamation point as shown (if we know about it).")
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Can't find a train?"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           "Some trains may be missing due to model deficiencies. "
           (:a
            :class "govuk-link"
            :href (easy-routes:genurl 'route-line
                                      :code code
                                      :fallback "live")
            "Use old layout")
           " to view these trains.")))))))

(defun tph-in-mins (line-code station-name platform)
  (multiple-value-bind (tph-60 tph-30 tph-15)
      (trackernet::tph-for-station line-code station-name platform)
    (when (and tph-15 (> tph-15 0))
      (let* ((min-tph (min tph-60 tph-30 tph-15))
             (max-tph (max tph-60 tph-30 tph-15))
             (min-wait (round (/ 60 max-tph)))
             (max-wait (round (/ 60 min-tph))))
        (if (< (- max-wait min-wait) 2)
            (format nil "~Am" max-wait)
            (format nil "~A–~Am" min-wait max-wait))))))

(defun render/live-line (code)
  (let ((line-data (tube-line-by-code code)))
    (unless line-data
      (return-not-found))
    (let* ((*accent-colour* (apply #'html-rgb-colour (third line-data)))
           (*phase-banner* (or *phase-banner* (phase-banner-for-line code)))
           (active-lts (with-server-timing ("get-live-trains")
                         (live-trains-on-line code)))
           (grouped (group-lts-by-destination active-lts))
           (lc-grouped (group-lts-by-leading-car active-lts))
           (wtt-grouped (group-lts-by-wtt active-lts)))
      (render (:title (format nil "~A line" (first line-data)))
        (:a
         :class "govuk-back-link"
         :href "/"
         "All Tube lines")
        (:span
         :class "govuk-caption-xl"
         "Line overview")
        (:h1
         :class "govuk-heading-xl"
         (who:fmt "~A line"
                  (first line-data)))
        (who:str (render/line-status code))
        (when (string= code "S")
          (who:htm
           (:div
            :class "govuk-warning-text"
            (:span
             :class "govuk-warning-text__icon"
             "!")
            (:strong
             :class "govuk-warning-text__text"
             "How did you get here? This is an internal testing \"line\" which is actually the crappy data from the District, Circle, H&C, and Met mashed together. Don't use this for anything!"))))
        (when (eql (fifth line-data) t)
          (who:htm
           (:div
            :class "govuk-warning-text"
            (:span
             :class "govuk-warning-text__icon"
             "!")
            (:strong
             :class "govuk-warning-text__text"
             "You're using the old line overview. "
             (:a
              :class "govuk-link"
              :href (easy-routes:genurl 'route-line
                                        :code code)
              "Use the new one here.")))))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Find a train by end destination"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           "Jump to: "
           (loop
             with len = (length grouped)
             for (group . trains) in grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link"
                  :href (format nil "#~A"
                                (string-base64 group))
                  (who:esc group)))
             unless (eql len i)
               do (who:str " &middot; ")))))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Find a train by Working Timetable (WTT) or leading car number"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           (:strong
            "Working Timetable (WTT) numbers"))
          (:p
           :class "govuk-body"
           (loop
             with len = (length wtt-grouped)
             for (wtt . trains) in wtt-grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link !-monospace"
                  :href (uri-for-lt (first trains))
                  (who:esc wtt)))
             unless (eql len i)
               do (who:str " &middot; ")))
          (:p
           :class "govuk-body"
           (:strong
            "Leading car numbers"))
          (:p
           :class "govuk-body"
           (loop
             with len = (length lc-grouped)
             for (wtt . trains) in lc-grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link !-monospace"
                  :href (uri-for-lt (first trains))
                  (who:esc wtt)))
             unless (eql len i)
               do (who:str " &middot; ")))))
        (loop
          for (group . trains) in grouped
          do (who:htm
              (:h2
               :class "govuk-heading-l"
               :id (string-base64 group)
               (:span
                :class "!-secondary-colour"
                "Trains to ")
               (who:esc group))
              (:ul
               :class "govuk-list"
               (dolist (lt (sort trains
                                 (lambda (a b)
                                   (string<
                                    (live-train-wtt-id a)
                                    (live-train-wtt-id b)))))
                 (let ((ent (trackernet::current-entry lt)))
                   (who:htm
                    (:li
                     (:h3
                      :class "govuk-body-l !-clearfix"
                      (if (typep ent 'trackernet::train-station-stop)
                          (who:htm
                           "at "
                           (:strong
                            (who:esc (trackernet::station-name ent)))
                           (when (trackernet::platform ent)
                             (who:str " platform ")
                             (who:htm
                              (:strong
                               (who:esc (trackernet::platform ent))))))
                          (who:str
                           (describe-train-entry-location ent code)))
                      (:a
                       :href (uri-for-lt lt)
                       :class "choose-line-train-link govuk-link"
                       (:span
                        :class "!-underline"
                        (if (first (trackernet::train-identity lt))
                            (who:fmt "view #~A"
                                     (first (trackernet::train-identity lt)))
                            (who:str "view")))
                       " " ;; Inter-specific right arrow glyph
                       )))))))))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Use ancient layout"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           "There's an even worse older line overview. "
           (:a
            :class "govuk-link"
            :href (easy-routes:genurl 'route-line
                                      :code code
                                      :fallback "raw")
            "Use ancient layout")
           " if you really, really want to (and maybe "
           (:a
            :class "govuk-link"
            :href "https://eta.st/#contact"
            "let me know")
           " why you're using it?).")))))))

(defun render/raw-line (code)
  (let ((line-data (tube-line-by-code code)))
    (unless line-data
      (return-not-found))
    (let* ((*accent-colour* (apply #'html-rgb-colour (third line-data)))
           (*phase-banner* (or *phase-banner* (phase-banner-for-line code)))
           (active-trains (train-objects-on-line code))
           (grouped (group-trains-by-destination active-trains))
           (wtt-grouped (group-trains-by-wtt active-trains)))
      (render ()
        (:a
         :class "govuk-back-link"
         :href "/"
         "All Tube lines")
        (:span
         :class "govuk-caption-xl"
         "Line overview")
        (:h1
         :class "govuk-heading-xl"
         (who:fmt "~A line"
                  (first line-data)))
        (who:str (render/line-status code))
        (who:htm
         (:div
          :class "govuk-warning-text"
          (:span
           :class "govuk-warning-text__icon"
           "!")
          (:strong
           :class "govuk-warning-text__text"
           "You're using the ancient line overview. "
           (:a
            :class "govuk-link"
            :href (easy-routes:genurl 'route-line
                                      :code code)
            "Use the new one here."))))
        (:p
         :class "govuk-body-l"
         (:strong
          (who:str (length active-trains)))
         " trains on the line as of "
         (:span
          :class "!-monospace"
          (who:esc (now-london-time-str)))
         ".")
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Find a train by end destination"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           "Jump to: "
           (loop
             with len = (length grouped)
             for (group . trains) in grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link"
                  :href (format nil "#~A"
                                (string-base64 group))
                  (who:esc group)))
             unless (eql len i)
               do (who:str " &middot; ")))))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "Find a train by Working Timetable (WTT) number"))
         (:div
          :class "govuk-details__text"
          (:p
           :class "govuk-body"
           "Jump to: "
           (loop
             with len = (length wtt-grouped)
             for (wtt . trains) in wtt-grouped
             for i from 1
             do (who:htm
                 (:a
                  :class "govuk-link !-monospace"
                  :href (easy-routes:genurl 'route-underlying-train
                                            :tid (first (first trains)))
                  (who:esc wtt)))
             unless (eql len i)
               do (who:str " &middot; ")))))
        (:details
         :class "govuk-details"
         (:summary
          :class "govuk-details__summary"
          (:span
           :class "govuk-details__summary-text"
           "What do the columns mean?"))
         (unless (string= code "X")
           (who:htm
            (:div
             :class "govuk-details__text"
             (:ul
              (:li
               (:p
                :class "govuk-body"
                (:strong "TrainID")
                (who:esc
                 " is a unique identifier given in the open data feeds. It's not entirely clear what, if anything, it correlates to in real life.")))
              (:li
               (:p
                :class "govuk-body"
                (:strong "LCID")
                (who:esc
                 " (Leading Car ID) ostensibly identifies the leading car of the train in some way. We use this as our primary means of tracking trains.")))
              (:li
               (:p
                :class "govuk-body"
                (:strong "Track codes")
                " "
                (:strong
                 :class "govuk-tag govuk-tag--grey"
                 "ABCDEF")
                (who:esc
                 " are arbitrary pieces of text describing a piece of track on the line. ")))
              (:li
               (:p
                :class "govuk-body"
                (:strong "WTT")
                (who:esc
                 " gives the Train Number as found in the TfL ")
                (:a
                 :href "https://tfl.gov.uk/corporate/publications-and-reports/working-timetables"
                 "Working Timetables (WTT)")
                (who:esc
                 ". These are keyed in by train operators at the start of their journey, and can be moved around by Line Controllers if necessary.")
                (:ul
                 (:li
                  (:p
                   :class "govuk-body"
                   (:strong
                    :class "govuk-tag govuk-tag--pink"
                    "NNN ×R")
                   (who:esc
                    " denotes a train with WTT number NNN, on its Rth trip of the day.")))
                 (:li
                  (:p
                   :class "govuk-body"
                   (:strong
                    :class "govuk-tag govuk-tag--green"
                    "NNN")
                   (who:esc
                    " denotes a train with WTT number NNN, on its first run of the day (or where no trip number info is available).")))))))))))
        (loop
          for (group . trains) in grouped
          do (who:htm
              (:h2
               :class "govuk-heading-l"
               :id (string-base64 group)
               (:span
                :class "!-secondary-colour"
                "to ")
               (who:esc group))
              (:table
               :class "tube-line-list govuk-table"
               (:thead
                :class "govuk-table__head"
                (:tr
                 :class "govuk-table__row"
                 (:th
                  :class "govuk-table__header !-no-phones"
                  (:abbr
                   :title "TrackerNet unique identifier"
                   "TrainID"))
                 (unless (string= code "X")
                   (who:htm
                    (:th
                     :class "govuk-table__header"
                     (:abbr
                      :title "Working Timetable train number"
                      "WTT"))
                    (:th
                     :class "govuk-table__header !-no-phones"
                     (:abbr
                      :title "Leading Car ID (LCID)"
                      "LCID"))))
                 (:th
                  :class "govuk-table__header govuk-!-width-two-thirds"
                  "Current location")))
               (:tbody
                :class "govuk-table__body"
                (loop
                  for (key reporter train) in (sort trains #'string<
                                                    :key (lambda (trn)
                                                           (trackernet::train-id (third trn))))
                  do (who:str (display-trackernet-train train code :code key)))))))))))

(hunchentoot:define-easy-handler (rate-limit-test :uri "/rate-limit") ()
  (rate-limit 5 30 "rl-test")
  "This accepts 5 requests every 30 seconds.")

(hunchentoot:define-easy-handler (ise :uri "/ise") ()
  (error "Requested ISE"))

(hunchentoot:define-easy-handler (timeout :uri "/timeout") ()
  (when trackernet::*staging-mode-p*
    (sleep 11)))

(hunchentoot:define-easy-handler (root :uri "/") ()
  (render (:meta-props
           '(("og:title" . "intertube: explore the London Underground")
             ("og:description" . "Get real-time, second-by-second updates on the movements of Tube trains.")))
    (:h1
     :class "govuk-heading-xl"
     "Explore the London Underground")
    (:p
     :class "govuk-body-l"
     "Get real-time, second-by-second updates on the movements of Tube trains. ")
    (:table
     :class "tube-line-list govuk-table"
     (:tbody
      :class "govuk-table__body"
      (loop
        for (name code rgb something model) in *tube-line-data*
        unless (string= code "S")
        do (let* ((count-code (if (equal code "A") "H" code))
                  (status (multiple-value-list
                           (trackernet::tfl-line-status-for code)))
                  (ntrains
                    (with-server-timing ((format nil "red-scard-~A" code))
                      (red:scard (format nil "line-trains:~A" count-code)))))
             (who:htm
              (:tr
               :class "tube-line govuk-table__row"
               (:td
                :class "tube-line-name govuk-table__cell"
                (who:esc name)
                (when (string= code "M")
                  (who:htm
                   (:span
                    :class "!-secondary-colour"
                    " (beta)")))
                (when (string= code "D")
                  (who:htm
                   (:span
                    :class "!-secondary-colour"
                    " (beta)")))
                (when (string= code "A")
                  (who:htm
                   (:span
                    :class "!-secondary-colour"
                    " (new!)")))
                (:span
                 :class "govuk-body !-secondary-colour"
                 :style "display: block; margin-bottom: 0px; margin-top: 5px;"
                 (cond
                   ((and status (<= (first status) 6))
                    (who:htm
                     (:span
                      :class "!-error-colour"
                      "⚠ "
                      (who:esc (second status)))))
                   (status
                    (who:htm
                     (:span
                      :class "!-blue-colour"
                      " " ; Inter-specific (!) glyph
                      (who:esc (second status)))))
                   ((eql ntrains 0)
                    (who:htm
                     (:span
                      :class "!-error-colour"
                      "Line suspended or data unavailable"))))))
               (:td
                :class "govuk-table__cell"
                :style "text-align: right;"
                (:a
                 :class "govuk-button govuk-button__start !-no-bottom-margin"
                 :style (format nil "background: ~A;"
                                (apply #'html-rgb-colour rgb))
                 :href (easy-routes:genurl 'route-line :code code)
                 (who:str *govuk-go-arrow-svg*)))))))))))

(defmethod hunchentoot:acceptor-status-message ((acceptor hunchentoot:easy-acceptor) status-code &rest properties &key &allow-other-keys)
  (declare (ignore properties))
  (catch 'hunchentoot::handler-done
    (when (eql status-code 404)
      (return-not-found))
    (when (eql status-code 400)
      (return-bad-request))
    (let ((reason (or (hunchentoot:reason-phrase status-code)
                      (format nil "Code ~A" status-code))))
      (hunchentoot:abort-request-handler
       (render (:title reason)
         (:h1
          :class "govuk-heading-l"
          (who:esc reason))
         (:p
          :class "govuk-body"
          "Sorry, something seems to have gone wrong.")
         (:p
          :class "govuk-body"
          "You can report this problem by messaging eta "
          (:a
           :href "https://eta.st/#contact"
           :class "govuk-link"
           "through one of the links here")
          "."))))))

(define-condition request-timeout (error) ()
  (:report (lambda (c s)
             (declare (ignore c))
             (format s "Request took too long!"))))

(defmethod hunchentoot:acceptor-dispatch-request :around (acceptor request)
  (let ((global-timer (sb-ext:make-timer
                       (lambda ()
                         (trackernet::statsd-inc "intertube.request-timeout")
                         (error 'request-timeout))))
        (*server-timing-events* '()))
    (unless (or
             (uiop:string-prefix-p "/api/v1/firehose" (hunchentoot:request-uri request))
             (uiop:string-prefix-p "/api/v1/train-firehose" (hunchentoot:request-uri request))
             (uiop:string-prefix-p "/live-ws" (hunchentoot:request-uri request)))
      (sb-ext:schedule-timer global-timer 60))
    (server-timing-start "redis-connect")
    (unwind-protect
         (redis:with-connection (:port trackernet::*redis-port* :host trackernet::*redis-host*)
           (server-timing-done "redis-connect")
           (prog1
               (with-server-timing ("handler")
                 (call-next-method acceptor request))
             (setf (hunchentoot:header-out "Server-Timing")
                   (server-timing-formatted))))
      (sb-ext:unschedule-timer global-timer))))

;; Routes

(defun resolve-train-filename (tid)
  (cond
    ;; unambigously a live train
    ((uiop:string-prefix-p "lt:" tid)
     tid)
    ;; a pre-live-train archived train
    ((uiop:string-suffix-p tid ".trn")
     tid)
    ;; also a live train
    ((uiop:string-prefix-p "train_" tid)
     (format nil "lt:~A" tid))
    ;; probably a live train in the new modern format
    (t
     (format nil "lt:train_~A" tid))))

(easy-routes:defroute route-live-train ("/train/:tid") (nojourney)
  (let* ((filename
           (resolve-train-filename tid))
         (lt
           ;; only fetch if it's actually going to be a live train
           (when (uiop:string-prefix-p "lt:" filename)
             (with-server-timing ("fetch-lt")
               (trackernet::redis-raw-get-cpk filename))))
         (clickhouse-lt
           ;; only fetch if we don't have an lt
           (unless lt
             (rate-limit 3 1 "historical")
             (with-server-timing ("fetch-from-clickhouse")
               (trackernet::fetch-clickhouse-live-train filename)))))
    (when lt
      (return-from route-live-train (render/live-train lt)))
    (when clickhouse-lt
      (when nojourney
        (return-from route-live-train (render/historical-train clickhouse-lt)))
      (return-from route-live-train (render/historical-train-journey clickhouse-lt)))
    (return-not-found "Hmm, we don't know where that train's gone." t)))

(easy-routes:defroute route-live-train-journey ("/train/:tid/journey") ()
  (let* ((filename
           (resolve-train-filename tid))
         (lt
           ;; only fetch if it's actually going to be a live train
           (when (uiop:string-prefix-p "lt:" filename)
             (with-server-timing ("fetch-lt")
               (trackernet::redis-raw-get-cpk filename))))
         (clickhouse-lt
           ;; only fetch if we don't have an lt
           (unless lt
             (rate-limit 3 1 "historical")
             (with-server-timing ("fetch-from-clickhouse")
               (trackernet::fetch-clickhouse-live-train filename)))))
    (when lt
      (return-from route-live-train-journey (render/train-journey lt)))
    (when clickhouse-lt
      (return-from route-live-train-journey (render/historical-train-journey clickhouse-lt)))
    (return-not-found "Hmm, we don't know where that train's gone." t)))

(easy-routes:defroute route-live-train-underlying ("/train/:tid/raw") (limit)
  (let* ((filename
           (resolve-train-filename tid))
         (lt
           ;; only fetch if it's actually going to be a live train
           (when (uiop:string-prefix-p "lt:" filename)
             (with-server-timing ("fetch-lt")
               (trackernet::redis-raw-get-cpk filename))))
         (clickhouse-records
           ;; only fetch if we don't have an lt
           (unless lt
             (rate-limit 3 1 "historical")
             (with-server-timing ("fetch-from-clickhouse")
               (trackernet::fetch-clickhouse-observations filename))))
         (raw-train
           (when lt
             (format nil "~A-train-~A"
                     (trackernet::line-code lt)
                     (trackernet::trackernet-id lt)))))
    (when raw-train
      (return-from route-live-train-underlying
        (render/raw-train raw-train limit
                          :orig-ltid tid)))
    (when clickhouse-records
      (return-from route-live-train-underlying
        (render/raw-clickhouse-train clickhouse-records tid)))
    (return-not-found "Hmm, we don't know where that train's gone." t)))

(easy-routes:defroute route-underlying-train ("/raw-train/:tid") (limit)
  (render/raw-train tid limit))

(easy-routes:defroute route-line ("/line/:code") (route fallback)
  (let* ((code (if (and (equal code "A") fallback) "H" code))
         (line-data (tube-line-by-code code)))
    (unless line-data
      (return-bad-request))

    (when (string= fallback "raw")
      (return-from route-line (render/raw-line code)))
    (when (string= fallback "live")
      (return-from route-line (render/live-line code)))
    (when fallback
      (return-bad-request))

    (if (fifth line-data) ; model available
        (render/model-line code route)
        (render/live-line code))))

(easy-routes:defroute route-id-search "/line/:code/search-id" ()
  (unless (tube-line-by-code code)
    (return-bad-request))
  (render/identifier-search code nil nil))

(easy-routes:defroute route-id-search-populated "/line/:code/search-id/:date/:identifier" ()
  (unless (tube-line-by-code code)
    (return-bad-request))
  (let ((around-ts
          (ignore-errors
           (local-time:timestamp-to-universal
            (terrible-date-decoder date)))))
    (unless around-ts
      (return-bad-request "The date was in the wrong format (expected YYYY-MM-DD)."))
    (render/identifier-search code around-ts identifier)))

(easy-routes:defroute route-station "/station/:line/:station" ()
  (unless (tube-line-by-code line)
    (return-bad-request))
  (let ((station (trackernet::station-resolve-slug line station)))
    (unless station
      (return-not-found "Sorry, we can't find that station." t))
    (render/station-calls line station nil)))

(easy-routes:defroute route-station-timed "/station/:line/:station/:date/:time" ()
  (let ((station (trackernet::station-resolve-slug line station))
        (around-ts
          (ignore-errors
           (local-time:timestamp-to-universal
            (terrible-timestamp-decoder-hhmm date time)))))
    (unless station
      (return-not-found "Sorry, we can't find that station." t))
    (unless around-ts
      (return-bad-request "The date and time specified were in the wrong format (expected YYYY-MM-DD HHMM)."))
    (render/station-calls line station around-ts)))

;; Legacy route redirects

(hunchentoot:define-easy-handler (legacy-live-train :uri "/live") (id)
  (relative-redirect (easy-routes:genurl 'route-live-train :tid id)
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (legacy-historical :uri "/historical") (id)
  (relative-redirect (easy-routes:genurl 'route-live-train :tid id)
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (legacy-model-line :uri "/model-line") (code direction)
  (relative-redirect (easy-routes:genurl 'route-line :code code
                                                     :route direction)
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (legacy-line :uri "/line") (code)
  (relative-redirect (easy-routes:genurl 'route-line :code code
                                                     :fallback "raw")
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (legacy-live-line :uri "/live-line") (code)
  (relative-redirect (easy-routes:genurl 'route-line :code code
                                                     :fallback "live")
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (legacy-train :uri "/train") (train limit)
  (relative-redirect (easy-routes:genurl 'route-underlying-train
                                         :tid train :limit limit
                                         :code hunchentoot:+http-moved-permanently+)))

;; What even is this route?? Keep it anyway for legacy's sake, but wow that's
;; a relic from the past
(hunchentoot:define-easy-handler (legacy-train-simple :uri "/train2") (train force)
  (when (and (not force)
             (red:get (format nil "ltmap:~A" train)))
    (relative-redirect
     (easy-routes:genurl 'route-live-train
                         :tid (subseq (red:get (format nil "ltmap:~A" train))
                                      #.(length "lt:train_")))))
  (relative-redirect (easy-routes:genurl 'route-underlying-train
                                         :tid train)
                     :code hunchentoot:+http-moved-permanently+))

(hunchentoot:define-easy-handler (station-calls :uri "/station-calls") (line station around-ts date time)
  (unless station
    (return-bad-request))
  (unless (tube-line-by-code line)
    (return-bad-request))
  (when around-ts
    (unless (setf around-ts (ignore-errors (parse-integer around-ts)))
      (return-bad-request)))
  (when date
    (unless (setf around-ts
                  (ignore-errors
                   (local-time:timestamp-to-universal
                    (terrible-timestamp-decoder date time))))
      (return-bad-request
       "We couldn't understand that date and time. If you filled it in manually, make sure you're using the format <span class=\"!-monospace\">YYYY-MM-DD HH:MM</span>.
<div class=\"govuk-form-group\">
<a href=\"javascript:window.history.back();\" class=\"govuk-button\">Go back and try again</a>
</div>")))
  (let ((new-uri (if around-ts
                    (uri-for-station-timed line station around-ts)
                    (uri-for-station line station))))
    (unless new-uri
      (return-bad-request))
    (relative-redirect new-uri
                       :code hunchentoot:+http-moved-permanently+)))

(hunchentoot:define-easy-handler (identifier-search :uri "/form/identifier-search") (line date identifier)
  (relative-redirect (easy-routes:genurl 'route-id-search-populated
                                         :code line
                                         :date date
                                         :identifier (string-upcase identifier))))

(defun start-webserver ()
  (when (uiop:getenv "INTERTUBE_STAGING")
    (format t "~&*** Running in staging mode ***~%")
    (setf trackernet::*staging-mode-p* t))
  (hunchentoot:start (make-instance 'easy-routes:easy-routes-acceptor
                                    :port 4000
                                    :address "::1"
                                    :error-template-directory nil)))

(defun main ()
  (format t "~&=== intertube-web version ~A / eta.st ===~%" trackernet::*code-version*)
  (let ((*package* (find-package 'intertube-web)))
    (redis:connect)
    (unless (and (boundp 'statsd:*client*)
                 statsd:*client*)
      (setf statsd:*client* (statsd:make-sync-client :host "moorgate.i.eta.st")))
    (start-webserver)
    (unwind-protect
      (sb-impl::toplevel-repl nil)
      (loop (sleep 1)))))
