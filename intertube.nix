{ pkgs, stdenv, codeVersion ? "unknown-nix", ... }:

let
  govuk-frontend = pkgs.fetchurl {
    url = "https://registry.npmjs.org/govuk-frontend/-/govuk-frontend-4.8.0.tgz";
    sha512 = "NOmPJxL8IYq1HSNHYKx9XY2LLTxuwb+IFASiGQO4sgJ8K7AG66SlSeqARrcetevV8zOf+i1z+MbJJ2O7//OxAw==";
  };
  cached-packages = stdenv.mkDerivation {
    name = "intertube-deps";
    src = ./bundle;
    buildInputs = with pkgs; [ sbcl openssl.out ]; 
    unpackPhase = ''
      cp -r $src ./bundle/
      chmod -R +w ./bundle/
    '';
    buildPhase = ''
      export ASDF_OUTPUT_TRANSLATIONS="/:$TMPDIR/asdf-cache"
      export LD_LIBRARY_PATH="${pkgs.openssl.out}/lib/"

      sbcl --disable-debugger --load ${./load-deps.lisp}
    '';

    installPhase = ''
      mkdir -p $out/cache
      cp -r "$TMPDIR/asdf-cache/$(pwd)/bundle" "$out/cache/"
      cp -r "./bundle" "$out/"
    '';

    dontStrip = true;
  };
in
stdenv.mkDerivation {
  name = "intertube";
  src = pkgs.nix-gitignore.gitignoreSource [] ./.;
  buildInputs = with pkgs; [ sbcl nodePackages.sass openssl.out makeWrapper ]; 

  unpackPhase = ''
    cp -r $src/* .
    chmod -R +w ./bundle/
    cp -r ${cached-packages}/bundle .

    mkdir -p node_modules/govuk-frontend/
    tar -xf ${govuk-frontend}
    mv package/* node_modules/govuk-frontend/

    mkdir -p "$TMPDIR/asdf-cache/$(pwd)/bundle"
    cp -r ${cached-packages}/cache/bundle "$TMPDIR/asdf-cache/$(pwd)"
    chmod -R +w "$TMPDIR/asdf-cache/$(pwd)/bundle"
  '';
  buildPhase = ''
    export ASDF_OUTPUT_TRANSLATIONS="/:$TMPDIR/asdf-cache"
    export LD_LIBRARY_PATH="${pkgs.openssl.out}/lib/"
    export INTERTUBE_CODE_VERSION="${codeVersion}"

    chmod +w build/
    sass -I node_modules/govuk-frontend --no-error-css --no-source-map --quiet-deps --style compressed assets/css/main.scss build/dist.css
    make -o build/dist.css
  '';

  installPhase = ''
    export LD_LIBRARY_PATH="${pkgs.openssl.out}/lib/"

    mkdir -p $out/bin/
    cp intertube-scraper $out/bin/intertube-scraper
    cp intertube-web $out/bin/intertube-web
   
    wrapProgram $out/bin/intertube-scraper --set LD_LIBRARY_PATH "$LD_LIBRARY_PATH"
    wrapProgram $out/bin/intertube-web --set LD_LIBRARY_PATH "$LD_LIBRARY_PATH"
  '';

  dontStrip = true;
}
