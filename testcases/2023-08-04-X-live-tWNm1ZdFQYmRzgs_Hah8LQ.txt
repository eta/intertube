-3900152588- 
[train     ] TRAIN-IDENTITY = (NIL NIL)
[train     ] FIRST-UPDATED = 3900152588
[train     ] DESTINATION = Not in Service
[train     ] TRACKERNET-ID = 5U82
[history   ] #<TRAIN-TD-CHANGE at 1643: TD area Q0>
[history   ] #<TRAIN-RESCUE at 1643: new tid 5U82>
[history   ] #<TRAIN-IDENTITY-CHANGE at 1643: WTT ?×?>
[history   ] #<TRAIN-DESTINATION-CHANGE at 1643: destination Not in Service>
[train     ] TD-AREA = Q0
[new event ] #<TRAIN-STATION-STOP from 1643 to 1643: 0s at Abbey Wood Bolthole Berth (plat NIL), 0395>
-3900152833- 
[event     ] END-TS = 3900152833
[track     ] 391A
-3900152841- 
[train     ] MODEL-POINT = At Abbey Wood Platform 3(W)
[history   ] #<TRAIN-STATION-STOP from 1643 to 1647¼: 253s at Abbey Wood Bolthole Berth (plat NIL), 391A -> 0395>
[new event ] #<TRAIN-STATION-STOP from 1647¼ to 1647¼: 0s at Abbey Wood (plat 3), 0391>
-3900152874- 
[history   ] #<TRAIN-STATION-STOP from 1647¼ to 1647¾: 33s at Abbey Wood (plat 3), 0391>
[new event ] #<TRAIN-TRANSIT from 1647¾ to 1647¾: 0391>
-3900152874- -3900153376- 
[train     ] DESTINATION = Heathrow Airport
[train     ] TRACKERNET-ID = 9T82
[train     ] MODEL-POINT = f2c37e3
[history   ] #<TRAIN-RESCUE at 1656¼: new tid 9T82>
[history   ] #<TRAIN-DESTINATION-CHANGE at 1656¼: destination Heathrow Airport>
[event     ] END-TS = 3900153376
[track     ] 375C
-3900153391- 
[event     ] END-TS = 3900153391
[track     ] 375B
-3900153407- 
[event     ] END-TS = 3900153407
[track     ] 0375
-3900153422- 
[event     ] END-TS = 3900153422
[track     ] 371C
-3900153424- 
[event     ] END-TS = 3900153424
[track     ] 0371
-3900153436- 
[event     ] END-TS = 3900153436
[track     ] 359C
-3900153444- 
[event     ] END-TS = 3900153444
[track     ] 0359
-3900153510- 
[event     ] END-TS = 3900153510
[track     ] 355A
-3900153515- 
[train     ] MODEL-POINT = At Woolwich Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1647¾ to 1658½: 0391 -> 375C -> 375B -> 0375 -> 371C -> 0371 -> 359C -> 0359 -> 355A>
[new event ] #<TRAIN-STATION-STOP from 1658½ to 1658½: 0s at Woolwich (plat B), 0355>
-3900153600- 
[train     ] MODEL-POINT = 0dc4885
[history   ] #<TRAIN-STATION-STOP from 1658½ to 1700: 85s at Woolwich (plat B), 0355>
[new event ] #<TRAIN-TRANSIT from 1700 to 1700: 343F>
-3900153608- 
[event     ] END-TS = 3900153608
[track     ] 343D
-3900153609- 
[event     ] END-TS = 3900153609
[track     ] 343C
-3900153672- 
[event     ] END-TS = 3900153672
[track     ] 0343
-3900153691- 
[event     ] END-TS = 3900153691
[track     ] 339C
-3900153692- 
[event     ] END-TS = 3900153692
[track     ] 339B
-3900153747- 
[event     ] END-TS = 3900153747
[track     ] 0339
-3900153760- 
[event     ] END-TS = 3900153760
[track     ] 335A
-3900153771- 
[train     ] MODEL-POINT = At Custom House Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1700 to 1702¾: 343F -> 343D -> 343C -> 0343 -> 339C -> 339B -> 0339 -> 335A>
[new event ] #<TRAIN-STATION-STOP from 1702¾ to 1702¾: 0s at Custom House (plat B), 0335>
-3900153846- 
[train     ] MODEL-POINT = efca6db
[history   ] #<TRAIN-STATION-STOP from 1702¾ to 1704: 75s at Custom House (plat B), 0335>
[new event ] #<TRAIN-TRANSIT from 1704 to 1704: 315D>
-3900153854- 
[event     ] END-TS = 3900153854
[track     ] 315C
-3900153873- 
[event     ] END-TS = 3900153873
[track     ] 0315
-3900153972- 
[event     ] END-TS = 3900153972
[track     ] 311A
-3900153976- 
[train     ] MODEL-POINT = At Canary Wharf Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1704 to 1706¼: 315D -> 315C -> 0315 -> 311A>
[new event ] #<TRAIN-STATION-STOP from 1706¼ to 1706¼: 0s at Canary Wharf (plat B), 0311>
-3900154078- 
[train     ] MODEL-POINT = 8a34484
[history   ] #<TRAIN-STATION-STOP from 1706¼ to 1707¾: 102s at Canary Wharf (plat B), 0311>
[new event ] #<TRAIN-TRANSIT from 1707¾ to 1707¾: 201C>
-3900154087- 
[event     ] END-TS = 3900154087
[track     ] 0201
-3900154171- 
[event     ] END-TS = 3900154171
[track     ] 145E
-3900154172- 
[event     ] END-TS = 3900154172
[track     ] 145C
-3900154178- 
[event     ] END-TS = 3900154178
[track     ] 0145
-3900154204- 
[event     ] END-TS = 3900154204
[track     ] 141A
-3900154213- 
[train     ] MODEL-POINT = At Whitechapel Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1707¾ to 1710: 201C -> 0201 -> 145E -> 145C -> 0145 -> 141A>
[new event ] #<TRAIN-STATION-STOP from 1710 to 1710: 0s at Whitechapel (plat B), 0141>
-3900154282- 
[train     ] MODEL-POINT = 08d5aac
[history   ] #<TRAIN-STATION-STOP from 1710 to 1711¼: 69s at Whitechapel (plat B), 0141>
[new event ] #<TRAIN-TRANSIT from 1711¼ to 1711¼: 125C>
-3900154293- 
[event     ] END-TS = 3900154293
[track     ] 0125
-3900154364- 
[event     ] END-TS = 3900154364
[track     ] 121A
-3900154373- 
[train     ] MODEL-POINT = At Liverpool Street Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1711¼ to 1712¾: 125C -> 0125 -> 121A>
[new event ] #<TRAIN-STATION-STOP from 1712¾ to 1712¾: 0s at Liverpool Street (plat B), 0121>
-3900154464- 
[train     ] MODEL-POINT = 2428e7a
[history   ] #<TRAIN-STATION-STOP from 1712¾ to 1714¼: 91s at Liverpool Street (plat B), 0121>
[new event ] #<TRAIN-TRANSIT from 1714¼ to 1714¼: 105C>
-3900154473- 
[event     ] END-TS = 3900154473
[track     ] 0105
-3900154514- 
[event     ] END-TS = 3900154514
[track     ] 101A
-3900154521- 
[train     ] MODEL-POINT = At Farringdon Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1714¼ to 1715¼: 105C -> 0105 -> 101A>
[new event ] #<TRAIN-STATION-STOP from 1715¼ to 1715¼: 0s at Farringdon (plat B), 0101>
-3900154613- 
[train     ] MODEL-POINT = 7d07385
[history   ] #<TRAIN-STATION-STOP from 1715¼ to 1716¾: 92s at Farringdon (plat B), 0101>
[new event ] #<TRAIN-TRANSIT from 1716¾ to 1716¾: 095C>
-3900154622- 
[event     ] END-TS = 3900154622
[track     ] 0095
-3900154667- 
[event     ] END-TS = 3900154667
[track     ] 085D
-3900154668- 
[event     ] END-TS = 3900154668
[track     ] 085C
-3900154672- 
[event     ] END-TS = 3900154672
[track     ] 0085
-3900154711- 
[event     ] END-TS = 3900154711
[track     ] 081A
-3900154714- 
[train     ] MODEL-POINT = At Tottenham Court Road Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1716¾ to 1718½: 095C -> 0095 -> 085D -> 085C -> 0085 -> 081A>
[new event ] #<TRAIN-STATION-STOP from 1718½ to 1718½: 0s at Tottenham Court Road (plat B), 0081>
-3900154800- 
[train     ] MODEL-POINT = 58d8a33
[history   ] #<TRAIN-STATION-STOP from 1718½ to 1720: 86s at Tottenham Court Road (plat B), 0081>
[new event ] #<TRAIN-TRANSIT from 1720 to 1720: 065C>
-3900154809- 
[event     ] END-TS = 3900154809
[track     ] 0065
-3900154841- 
[event     ] END-TS = 3900154841
[track     ] 061A
-3900154847- 
[train     ] MODEL-POINT = At Bond Street Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1720 to 1720¾: 065C -> 0065 -> 061A>
[new event ] #<TRAIN-STATION-STOP from 1720¾ to 1720¾: 0s at Bond Street (plat B), 0061>
-3900154919- 
[train     ] MODEL-POINT = 4b53b36
[history   ] #<TRAIN-STATION-STOP from 1720¾ to 1721¾: 72s at Bond Street (plat B), 0061>
[new event ] #<TRAIN-TRANSIT from 1721¾ to 1721¾: 045C>
-3900154927- 
[event     ] END-TS = 3900154927
[track     ] 045A
-3900154928- 
[event     ] END-TS = 3900154928
[track     ] 0045
-3900155087- 
[event     ] END-TS = 3900155087
[track     ] 041A
-3900155097- 
[train     ] MODEL-POINT = At Paddington Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1721¾ to 1724¾: 045C -> 045A -> 0045 -> 041A>
[new event ] #<TRAIN-STATION-STOP from 1724¾ to 1724¾: 0s at Paddington (plat B), 0041>
-3900155201- 
[train     ] MODEL-POINT = 98ff7e0
[history   ] #<TRAIN-STATION-STOP from 1724¾ to 1726½: 104s at Paddington (plat B), 0041>
[new event ] #<TRAIN-TRANSIT from 1726½ to 1726½: 025C>
-3900155210- 
[event     ] END-TS = 3900155210
[track     ] 0025
-3900155258- 
[train     ] MODEL-POINT = At Westbourne Park Sidings Platform W
[history   ] #<TRAIN-TRANSIT from 1726½ to 1727½: 025C -> 0025>
[new event ] #<TRAIN-STATION-STOP from 1727½ to 1727½: 0s at Westbourne Park Sidings (plat W), 009B>
-3900155269- 
[event     ] END-TS = 3900155269
[track     ] 009A
-3900155272- 
[event     ] END-TS = 3900155272
[track     ] 0009
-3900155281- 
[train     ] MODEL-POINT = f75fb63
[history   ] #<TRAIN-TD-CHANGE at 1728: TD area D3>
[train     ] TD-AREA = D3
[track     ] D3·0091
-3900155288- 
[history   ] #<TRAIN-STATION-STOP from 1727½ to 1728¼: 45s at Westbourne Park Sidings (plat W), D3·0091 -> 0009 -> 009A -> 009B>
[new event ] #<TRAIN-TRANSIT from 1728¼ to 1728¼: D3·0113 ("Departed Portobello Jn (down from 5)")>
-3900155326- 
[train     ] MODEL-POINT = 4d7e84b
[track     ] D3·0123
-3900155354- 
[track     ] D3·0127
-3900155380- 
[track     ] D3·0137
-3900155396- 
[track     ] D3·0153
-3900155419- 
[track     ] D3·0163
-3900155442- 
[history   ] #<TRAIN-TD-CHANGE at 1730½: TD area DA>
[train     ] TD-AREA = DA
[track     ] DA·0175
-3900155477- 
[train     ] MODEL-POINT = At Acton Main Line Platform 4
[history   ] #<TRAIN-TRANSIT from 1728¼ to 1732: D3·0113 -> D3·0123 -> D3·0127 -> D3·0137 -> D3·0153 -> D3·0163 -> DA·0175 ("Departed Portobello Jn (down from 5)")>
[new event ] #<TRAIN-STATION-STOP from 1732 to 1732: 0s at Acton Main Line (plat 4), DA·0187>
-3900155591- 
[train     ] MODEL-POINT = 0f36f4d
[history   ] #<TRAIN-STATION-STOP from 1732 to 1732¾: 42s at Acton Main Line (plat 4), DA·0187>
[new event ] #<TRAIN-TRANSIT from 1732¾ to 1732¾: DA·0199 ("Departed Acton Main Line platform 4 (down)")>
-3900155615- 
[history   ] #<TRAIN-TRANSIT from 1732¾ to 1733¾: DA·0199 ("Departed Acton Main Line platform 4 (down)")>
[history   ] #<TRAIN-STATION-STOP from 1733¾ to 1733¾: 0s at Acton West (plat down /R), >
[new event ] #<TRAIN-TRANSIT from 1733¾ to 1733¾: DA·0203 ("Departed Acton West (down from R)")>
-3900155659- 
[train     ] MODEL-POINT = At Ealing Broadway Platform 3
[history   ] #<TRAIN-TRANSIT from 1733¾ to 1735: DA·0203 ("Departed Acton West (down from R)")>
[new event ] #<TRAIN-STATION-STOP from 1735 to 1735: 0s at Ealing Broadway (plat 3), DA·0209>
-3900155781- 
[train     ] MODEL-POINT = 8b029ee
[history   ] #<TRAIN-STATION-STOP from 1735 to 1735½: 41s at Ealing Broadway (plat 3), DA·0209>
[new event ] #<TRAIN-TRANSIT from 1735½ to 1735½: DA·0211 ("Departed Ealing Broadway (down)")>
-3900155797- 
[train     ] MODEL-POINT = At West Ealing Platform 3
[history   ] #<TRAIN-TRANSIT from 1735½ to 1737¼: DA·0211 ("Departed Ealing Broadway (down)")>
[new event ] #<TRAIN-STATION-STOP from 1737¼ to 1737¼: 0s at West Ealing (plat 3), DA·0215>
-3900155901- 
[train     ] MODEL-POINT = 469455a
[history   ] #<TRAIN-STATION-STOP from 1737¼ to 1738: 39s at West Ealing (plat 3), DA·0215>
[new event ] #<TRAIN-TRANSIT from 1738 to 1738: DA·0225 ("Departed West Ealing platform 3 (down from D)")>
-3900155935- 
[train     ] MODEL-POINT = At Hanwell Platform 2
[history   ] #<TRAIN-TD-CHANGE at 1738¾: TD area D4>
[history   ] #<TRAIN-TRANSIT from 1738 to 1739½: DA·0225 ("Departed West Ealing platform 3 (down from D)")>
[train     ] TD-AREA = D4
[new event ] #<TRAIN-STATION-STOP from 1739½ to 1739½: 0s at Hanwell (plat 2), D4·0233>
-3900156050- 
[train     ] MODEL-POINT = f78635a
[history   ] #<TRAIN-STATION-STOP from 1739½ to 1740¼: 39s at Hanwell (plat 2), D4·0233>
[new event ] #<TRAIN-TRANSIT from 1740¼ to 1740¼: D4·0239 ("Departed Hanwell platform 2 (down from D)")>
-3900156077- 
[track     ] D4·0243
-3900156104- 
[train     ] MODEL-POINT = At Southall Platform 3
[history   ] #<TRAIN-TRANSIT from 1740¼ to 1742¾: D4·0239 -> D4·0243 ("Departed Hanwell platform 2 (down from D)")>
[new event ] #<TRAIN-STATION-STOP from 1742¾ to 1742¾: 0s at Southall (plat 3), D4·0253>
-3900156240- 
[train     ] MODEL-POINT = dd197d3
[history   ] #<TRAIN-STATION-STOP from 1742¾ to 1743¼: 28s at Southall (plat 3), D4·0253>
[new event ] #<TRAIN-TRANSIT from 1743¼ to 1743¼: D4·0265 ("Departed Southall platform 3 (down)")>
-3900156267- 
[track     ] D4·0273
-3900156298- 
[track     ] D4·0283
-3900156325- 
[train     ] MODEL-POINT = At Hayes & Harlington Platform 3
[history   ] #<TRAIN-TRANSIT from 1743¼ to 1745¾: D4·0265 -> D4·0273 -> D4·0283 ("Departed Southall platform 3 (down)")>
[new event ] #<TRAIN-STATION-STOP from 1745¾ to 1745¾: 0s at Hayes & Harlington (plat 3), D4·0287>
-3900156467- 
[train     ] MODEL-POINT = df22366
[history   ] #<TRAIN-STATION-STOP from 1745¾ to 1747¼: 82s at Hayes & Harlington (plat 3), D4·0287>
[history   ] #<TRAIN-TRANSIT from 1747¼ to 1748: D4·0303 ("Departed Hayes & Harlington platform 3 (down from R)")>
[history   ] #<TRAIN-STATION-STOP from 1748 to 1748: 0s at Heathrow Airport Jn (plat down /R), >
[new event ] #<TRAIN-TRANSIT from 1748 to 1748: D4·0303 ("Departed Heathrow Airport Jn (down from R)")>
-3900156499- 
[track     ] D4·0323
-3900156555- 
[history   ] #<TRAIN-TRANSIT from 1748 to 1749½: D4·0303 -> D4·0323 ("Departed Heathrow Airport Jn (down from R)")>
[history   ] #<TRAIN-STATION-STOP from 1749½ to 1749½: 0s at Heathrow Tunnel Jn (plat down), >
[new event ] #<TRAIN-TRANSIT from 1749½ to 1749½: D4·0327 ("Departed Heathrow Tunnel Jn (down)")>
-3900156628- 
[track     ] D4·0331
-3900156706- 
[train     ] MODEL-POINT = At Heathrow Terminals 2 & 3 Platform 1
[history   ] #<TRAIN-TRANSIT from 1749½ to 1752¾: D4·0327 -> D4·0331 ("Departed Heathrow Tunnel Jn (down)")>
[new event ] #<TRAIN-STATION-STOP from 1752¾ to 1752¾: 0s at Heathrow Terminals 2 & 3 (plat 1), D4·0335>
-3900156850- 
[train     ] MODEL-POINT = 82b9af0
[history   ] #<TRAIN-STATION-STOP from 1752¾ to 1753¾: 63s at Heathrow Terminals 2 & 3 (plat 1), D4·0335>
[new event ] #<TRAIN-TRANSIT from 1753¾ to 1753¾: D4·0345 ("Departed Heathrow Terminals 2 & 3 platform 1 (down)")>
-3900156993- 
[train     ] MODEL-POINT = At Heathrow Terminal 4 Platform 1
[history   ] #<TRAIN-TRANSIT from 1753¾ to 1758: D4·0345 ("Departed Heathrow Terminals 2 & 3 platform 1 (down)")>
[new event ] #<TRAIN-STATION-STOP from 1758 to 1758: 0s at Heathrow Terminal 4 (plat 1), D4·R352>
-3900157001- 
[train     ] DESTINATION = Abbey Wood
[train     ] TRACKERNET-ID = 9U09
[history   ] #<TRAIN-RESCUE at 1756½: new tid 9U09>
[history   ] #<TRAIN-DESTINATION-CHANGE at 1756½: destination Abbey Wood>
[track     ] D4·B352
-3900158017- 
[track     ] D4·A352
-3900158123- 
[train     ] MODEL-POINT = d7df2e3
[history   ] #<TRAIN-STATION-STOP from 1758 to 1815: 1025s at Heathrow Terminal 4 (plat 1), D4·A352 -> D4·B352 -> D4·R352>
[new event ] #<TRAIN-TRANSIT from 1815 to 1815: D4·0340 ("Departed Heathrow Terminal 4 platform 1 (up)")>
-3900158311- 
[train     ] MODEL-POINT = At Heathrow Terminals 2 & 3 Platform 2
[history   ] #<TRAIN-TRANSIT from 1815 to 1820¼: D4·0340 ("Departed Heathrow Terminal 4 platform 1 (up)")>
[new event ] #<TRAIN-STATION-STOP from 1820¼ to 1820¼: 0s at Heathrow Terminals 2 & 3 (plat 2), D4·0336>
-3900158482- 
[train     ] MODEL-POINT = 2807828
[history   ] #<TRAIN-STATION-STOP from 1820¼ to 1821: 46s at Heathrow Terminals 2 & 3 (plat 2), D4·0336>
[new event ] #<TRAIN-TRANSIT from 1821 to 1821: D4·0332 ("Departed Heathrow Terminals 2 & 3 platform 2 (up)")>
-3900158533- 
[track     ] D4·0326
-3900158624- 
[history   ] #<TRAIN-TRANSIT from 1821 to 1824: D4·0332 -> D4·0326 ("Departed Heathrow Terminals 2 & 3 platform 2 (up)")>
[history   ] #<TRAIN-STATION-STOP from 1824 to 1824: 0s at Heathrow Tunnel Jn (plat up /U), >
[new event ] #<TRAIN-TRANSIT from 1824 to 1824: D4·0316 ("Departed Heathrow Tunnel Jn (up from U)")>
-3900158669- 
[track     ] D4·0292
-3900158811- 
[train     ] MODEL-POINT = At Hayes & Harlington Platform 4
[history   ] #<TRAIN-TRANSIT from 1824 to 1827¼: D4·0316 -> D4·0292 ("Departed Heathrow Tunnel Jn (up from U)")>
[history   ] #<TRAIN-STATION-STOP from 1827¼ to 1827¼: 0s at Heathrow Airport Jn (plat up /U), >
[history   ] #<TRAIN-TRANSIT from 1827¼ to 1827¾: D4·0284 ("Departed Heathrow Airport Jn (up from U)")>
[new event ] #<TRAIN-STATION-STOP from 1827¾ to 1827¾: 0s at Hayes & Harlington (plat 4), D4·0284>
-3900158941- 
[train     ] MODEL-POINT = 451ed29
[history   ] #<TRAIN-STATION-STOP from 1827¾ to 1828½: 34s at Hayes & Harlington (plat 4), D4·0284>
[new event ] #<TRAIN-TRANSIT from 1828½ to 1828½: D4·0276 ("Departed Hayes & Harlington platform 4 (up)")>
-3900158980- 
[track     ] D4·0266
-3900159046- 
[track     ] D4·0258
-3900159097- 
[train     ] MODEL-POINT = At Southall Platform 4
[history   ] #<TRAIN-TRANSIT from 1828½ to 1832¼: D4·0276 -> D4·0266 -> D4·0258 ("Departed Hayes & Harlington platform 4 (up)")>
[new event ] #<TRAIN-STATION-STOP from 1832¼ to 1832¼: 0s at Southall (plat 4), D4·0248>
-3900159205- 
[train     ] MODEL-POINT = d097b74
[track     ] D4·0244
-3900159254- 
[history   ] #<TRAIN-STATION-STOP from 1832¼ to 1832¾: 31s at Southall (plat 4), D4·0244 -> D4·0248>
[new event ] #<TRAIN-TRANSIT from 1832¾ to 1832¾: D4·0238 ("Departed Southall platform 4 (up from R)")>
-3900159267- 
[track     ] D4·0232
-3900159309- 
[train     ] MODEL-POINT = At Hanwell Platform 3
[history   ] #<TRAIN-TD-CHANGE at 1835: TD area DA>
[history   ] #<TRAIN-TRANSIT from 1832¾ to 1835½: D4·0238 -> D4·0232 ("Departed Southall platform 4 (up from R)")>
[train     ] TD-AREA = DA
[new event ] #<TRAIN-STATION-STOP from 1835½ to 1835½: 0s at Hanwell (plat 3), DA·0224>
-3900159423- 
[train     ] MODEL-POINT = At West Ealing Platform 4
[history   ] #<TRAIN-STATION-STOP from 1835½ to 1836¼: 39s at Hanwell (plat 3), DA·0224>
[history   ] #<TRAIN-TRANSIT from 1836¼ to 1838: DA·0214 ("Departed Hanwell platform 3 (up from U)")>
[new event ] #<TRAIN-STATION-STOP from 1838 to 1838: 0s at West Ealing (plat 4), DA·0214>
-3900159549- 
[train     ] MODEL-POINT = db9f1df
[history   ] #<TRAIN-STATION-STOP from 1838 to 1838½: 33s at West Ealing (plat 4), DA·0214>
[new event ] #<TRAIN-TRANSIT from 1838½ to 1838½: DA·0210 ("Departed West Ealing platform 4 (up from U)")>
-3900159587- 
[train     ] MODEL-POINT = At Ealing Broadway Platform 4
[history   ] #<TRAIN-TRANSIT from 1838½ to 1840¼: DA·0210 ("Departed West Ealing platform 4 (up from U)")>
[new event ] #<TRAIN-STATION-STOP from 1840¼ to 1840¼: 0s at Ealing Broadway (plat 4), DA·0206>
-3900159685- 
[train     ] MODEL-POINT = 28ad713
[history   ] #<TRAIN-STATION-STOP from 1840¼ to 1841: 48s at Ealing Broadway (plat 4), DA·0206>
[new event ] #<TRAIN-TRANSIT from 1841 to 1841: DA·0202 ("Departed Ealing Broadway platform 4 (up from R)")>
-3900159717- 
[history   ] #<TRAIN-TRANSIT from 1841 to 1842¼: DA·0202 ("Departed Ealing Broadway platform 4 (up from R)")>
[history   ] #<TRAIN-STATION-STOP from 1842¼ to 1842¼: 0s at Acton West (plat up), >
[new event ] #<TRAIN-TRANSIT from 1842¼ to 1842¼: DA·0190 ("Departed Acton West (up)")>
-3900159744- 
[train     ] MODEL-POINT = At Acton Main Line Platform 3
[history   ] #<TRAIN-TRANSIT from 1842¼ to 1843¼: DA·0190 ("Departed Acton West (up)")>
[new event ] #<TRAIN-STATION-STOP from 1843¼ to 1843¼: 0s at Acton Main Line (plat 3), DA·0186>
-3900159852- 
[train     ] MODEL-POINT = 64a6132
[history   ] #<TRAIN-TD-CHANGE at 1844: TD area D3>
[history   ] #<TRAIN-STATION-STOP from 1843¼ to 1844: 38s at Acton Main Line (plat 3), DA·0186>
[train     ] TD-AREA = D3
[new event ] #<TRAIN-TRANSIT from 1844 to 1844: D3·0174 ("Departed Acton Main Line platform 3 (up)")>
-3900159880- 
[track     ] D3·0164
-3900159898- 
[track     ] D3·0156
-3900159917- 
[track     ] D3·0144
-3900159933- 
[track     ] D3·0134
-3900159966- 
[track     ] D3·0114
-3900159998- 
[track     ] D3·0112
-3900160036- 
[track     ] D3·0092
-3900160075- 
[history   ] #<TRAIN-TRANSIT from 1844 to 1847¾: D3·0174 -> D3·0164 -> D3·0156 -> D3·0144 -> D3·0134 -> D3·0114 -> D3·0112 -> D3·0092 ("Departed Acton Main Line platform 3 (up)")>
[history   ] #<TRAIN-STATION-STOP from 1847¾ to 1847¾: 0s at Portobello Jn (plat up /E), >
[new event ] #<TRAIN-TRANSIT from 1847¾ to 1847¾: D3·X18A ("Departed Portobello Jn (up from E)")>
-3900160117- 
[train     ] MODEL-POINT = At Westbourne Park Sidings Platform E
[history   ] #<TRAIN-TD-CHANGE at 1848½: TD area Q0>
[history   ] #<TRAIN-TRANSIT from 1847¾ to 1848½: D3·X18A ("Departed Portobello Jn (up from E)")>
[train     ] TD-AREA = Q0
[new event ] #<TRAIN-STATION-STOP from 1848½ to 1848½: 0s at Westbourne Park Sidings (plat E), 0018>
-3900160130- 
[event     ] END-TS = 3900160130
[track     ] 042D
-3900160132- 
[train     ] MODEL-POINT = dee0711
[history   ] #<TRAIN-STATION-STOP from 1848½ to 1848¾: 15s at Westbourne Park Sidings (plat E), 042D -> 0018>
[new event ] #<TRAIN-TRANSIT from 1848¾ to 1848¾: 042C>
-3900160145- 
[event     ] END-TS = 3900160145
[track     ] 0042
-3900160177- 
[event     ] END-TS = 3900160177
[track     ] 046A
-3900160181- 
[train     ] MODEL-POINT = At Paddington Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1848¾ to 1849½: 042C -> 0042 -> 046A>
[new event ] #<TRAIN-STATION-STOP from 1849½ to 1849½: 0s at Paddington (plat A), 0046>
-3900160289- 
[train     ] MODEL-POINT = 84f1f56
[history   ] #<TRAIN-STATION-STOP from 1849½ to 1851¼: 108s at Paddington (plat A), 0046>
[new event ] #<TRAIN-TRANSIT from 1851¼ to 1851¼: 062C>
-3900160297- 
[event     ] END-TS = 3900160297
[track     ] 0062
-3900160380- 
[event     ] END-TS = 3900160380
[track     ] 066A
-3900160384- 
[train     ] MODEL-POINT = At Bond Street Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1851¼ to 1853: 062C -> 0062 -> 066A>
[new event ] #<TRAIN-STATION-STOP from 1853 to 1853: 0s at Bond Street (plat A), 0066>
-3900160465- 
[train     ] MODEL-POINT = 0fa6f4b
[history   ] #<TRAIN-STATION-STOP from 1853 to 1854¼: 81s at Bond Street (plat A), 0066>
[new event ] #<TRAIN-TRANSIT from 1854¼ to 1854¼: 082C>
-3900160474- 
[event     ] END-TS = 3900160474
[track     ] 082A
-3900160475- 
[event     ] END-TS = 3900160475
[track     ] 0082
-3900160512- 
[event     ] END-TS = 3900160512
[track     ] 086A
-3900160516- 
[train     ] MODEL-POINT = At Tottenham Court Road Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1854¼ to 1855¼: 082C -> 082A -> 0082 -> 086A>
[new event ] #<TRAIN-STATION-STOP from 1855¼ to 1855¼: 0s at Tottenham Court Road (plat A), 0086>
-3900160609- 
[train     ] MODEL-POINT = b2804d9
[history   ] #<TRAIN-STATION-STOP from 1855¼ to 1856¾: 93s at Tottenham Court Road (plat A), 0086>
[new event ] #<TRAIN-TRANSIT from 1856¾ to 1856¾: 092C>
-3900160618- 
[event     ] END-TS = 3900160618
[track     ] 0092
-3900160660- 
[event     ] END-TS = 3900160660
[track     ] 102D
-3900160661- 
[event     ] END-TS = 3900160661
[track     ] 102C
-3900160665- 
[event     ] END-TS = 3900160665
[track     ] 0102
-3900160698- 
[event     ] END-TS = 3900160698
[track     ] 106A
-3900160702- 
[train     ] MODEL-POINT = At Farringdon Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1856¾ to 1858¼: 092C -> 0092 -> 102D -> 102C -> 0102 -> 106A>
[new event ] #<TRAIN-STATION-STOP from 1858¼ to 1858¼: 0s at Farringdon (plat A), 0106>
-3900160790- 
[train     ] MODEL-POINT = 8a37154
[history   ] #<TRAIN-STATION-STOP from 1858¼ to 1859¾: 88s at Farringdon (plat A), 0106>
[new event ] #<TRAIN-TRANSIT from 1859¾ to 1859¾: 122C>
-3900160802- 
[event     ] END-TS = 3900160802
[track     ] 0122
-3900160838- 
[event     ] END-TS = 3900160838
[track     ] 126A
-3900160845- 
[train     ] MODEL-POINT = At Liverpool Street Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1859¾ to 1900¾: 122C -> 0122 -> 126A>
[new event ] #<TRAIN-STATION-STOP from 1900¾ to 1900¾: 0s at Liverpool Street (plat A), 0126>
-3900160942- 
[train     ] MODEL-POINT = 7c3ac67
[history   ] #<TRAIN-STATION-STOP from 1900¾ to 1902¼: 97s at Liverpool Street (plat A), 0126>
[new event ] #<TRAIN-TRANSIT from 1902¼ to 1902¼: 142C>
-3900160951- 
[event     ] END-TS = 3900160951
[track     ] 0142
-3900161015- 
[event     ] END-TS = 3900161015
[track     ] 146B
-3900161017- 
[event     ] END-TS = 3900161017
[track     ] 146A
-3900161028- 
[train     ] MODEL-POINT = At Whitechapel Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1902¼ to 1903¾: 142C -> 0142 -> 146B -> 146A>
[new event ] #<TRAIN-STATION-STOP from 1903¾ to 1903¾: 0s at Whitechapel (plat A), 0146>
-3900161101- 
[train     ] MODEL-POINT = 7d93f91
[history   ] #<TRAIN-STATION-STOP from 1903¾ to 1905: 73s at Whitechapel (plat A), 0146>
[new event ] #<TRAIN-TRANSIT from 1905 to 1905: 202C>
-3900161111- 
[event     ] END-TS = 3900161111
[track     ] 0202
-3900161149- 
[event     ] END-TS = 3900161149
[track     ] 412D
-3900161151- 
[event     ] END-TS = 3900161151
[track     ] 312C
-3900161155- 
[event     ] END-TS = 3900161155
[track     ] 312A
-3900161156- 
[event     ] END-TS = 3900161156
[track     ] 0312
-3900161238- 
[train     ] MODEL-POINT = At Canary Wharf Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1905 to 1907¼: 202C -> 0202 -> 412D -> 312C -> 312A -> 0312>
[new event ] #<TRAIN-STATION-STOP from 1907¼ to 1907¼: 0s at Canary Wharf (plat A), 0316>
-3900161340- 
[train     ] MODEL-POINT = aa41cbc
[history   ] #<TRAIN-STATION-STOP from 1907¼ to 1909: 102s at Canary Wharf (plat A), 0316>
[new event ] #<TRAIN-TRANSIT from 1909 to 1909: 332C>
-3900161348- 
[event     ] END-TS = 3900161348
[track     ] 0332
-3900161453- 
[event     ] END-TS = 3900161453
[track     ] 336B
-3900161459- 
[event     ] END-TS = 3900161459
[track     ] 336A
-3900161473- 
[train     ] MODEL-POINT = At Custom House Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1909 to 1911: 332C -> 0332 -> 336B -> 336A>
[new event ] #<TRAIN-STATION-STOP from 1911 to 1911: 0s at Custom House (plat A), 0336>
-3900161548- 
[train     ] MODEL-POINT = be48fb0
[history   ] #<TRAIN-STATION-STOP from 1911 to 1912¼: 75s at Custom House (plat A), 0336>
[new event ] #<TRAIN-TRANSIT from 1912¼ to 1912¼: 340C>
-3900161561- 
[event     ] END-TS = 3900161561
[track     ] 340B
-3900161568- 
[event     ] END-TS = 3900161568
[track     ] 0340
-3900161580- 
[event     ] END-TS = 3900161580
[track     ] 352F
-3900161581- 
[event     ] END-TS = 3900161581
[track     ] 352E
-3900161582- 
[event     ] END-TS = 3900161582
[track     ] 352C
-3900161636- 
[event     ] END-TS = 3900161636
[track     ] 0352
-3900161657- 
[event     ] END-TS = 3900161657
[track     ] 0356
-3900161713- 
[event     ] END-TS = 3900161713
[track     ] 360A
-3900161718- 
[train     ] MODEL-POINT = At Woolwich Platform A(E)
[history   ] #<TRAIN-TRANSIT from 1912¼ to 1915¼: 340C -> 340B -> 0340 -> 352F -> 352E -> 352C -> 0352 -> 0356 -> 360A>
[new event ] #<TRAIN-STATION-STOP from 1915¼ to 1915¼: 0s at Woolwich (plat A), 0360>
-3900161792- 
[train     ] MODEL-POINT = f2c37e3
[history   ] #<TRAIN-STATION-STOP from 1915¼ to 1916½: 74s at Woolwich (plat A), 0360>
[new event ] #<TRAIN-TRANSIT from 1916½ to 1916½: 372C>
-3900161800- 
[event     ] END-TS = 3900161800
[track     ] 372A
-3900161801- 
[event     ] END-TS = 3900161801
[track     ] 0372
-3900161869- 
[event     ] END-TS = 3900161869
[track     ] 396C
-3900161886- 
[event     ] END-TS = 3900161886
[track     ] 396B
-3900161905- 
[event     ] END-TS = 3900161905
[track     ] 0396
-3900162035- 
[event     ] END-TS = 3900162035
[track     ] 400B
-3900162040- 
[train     ] MODEL-POINT = c8337b9
[event     ] END-TS = 3900162040
[track     ] 398A
-3900162069- 
[train     ] MODEL-POINT = At Abbey Wood Platform 3(E)
[history   ] #<TRAIN-TRANSIT from 1916½ to 1921: 372C -> 372A -> 0372 -> 396C -> 396B -> 0396 -> 400B -> 398A>
[new event ] #<TRAIN-STATION-STOP from 1921 to 1921: 0s at Abbey Wood (plat 3), 0398>
-3900162098- 
[history   ] #<TRAIN-STATION-STOP from 1921 to 1921½: 29s at Abbey Wood (plat 3), 0398>
[new event ] #<TRAIN-TRANSIT from 1921½ to 1921½: 0398>
-3900162099- 
[train     ] DESTINATION = Maidenhead
[train     ] TRACKERNET-ID = 9N40
[history   ] #<TRAIN-RESCUE at 1921½: new tid 9N40>
[history   ] #<TRAIN-DESTINATION-CHANGE at 1921½: destination Maidenhead>
[history   ] #<TRAIN-TRANSIT from 1921½ to 1921½: 0398>
[new event ] #<TRAIN-STATION-STOP from 1921½ to 1921½: 0s at Abbey Wood (plat 3), 0398>
-3900162675- 
[train     ] MODEL-POINT = At Abbey Wood Platform 3(W)
[event     ] END-TS = 3900162675
[track     ] 0391
-3900162760- 
[train     ] MODEL-POINT = f2c37e3
[history   ] #<TRAIN-STATION-STOP from 1921½ to 1932½: 661s at Abbey Wood (plat 3), 0391 -> 0398>
[new event ] #<TRAIN-TRANSIT from 1932½ to 1932½: 375C>
-3900162776- 
[event     ] END-TS = 3900162776
[track     ] 375B
-3900162792- 
[event     ] END-TS = 3900162792
[track     ] 0375
-3900162806- 
[event     ] END-TS = 3900162806
[track     ] 371C
-3900162807- 
[event     ] END-TS = 3900162807
[track     ] 371A
-3900162808- 
[event     ] END-TS = 3900162808
[track     ] 0371
-3900162821- 
[event     ] END-TS = 3900162821
[track     ] 359C
-3900162827- 
[event     ] END-TS = 3900162827
[track     ] 0359
-3900162887- 
[event     ] END-TS = 3900162887
[track     ] 355A
-3900162891- 
[train     ] MODEL-POINT = At Woolwich Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1932½ to 1934¾: 375C -> 375B -> 0375 -> 371C -> 371A -> 0371 -> 359C -> 0359 -> 355A>
[new event ] #<TRAIN-STATION-STOP from 1934¾ to 1934¾: 0s at Woolwich (plat B), 0355>
-3900162957- 
[train     ] MODEL-POINT = 0dc4885
[history   ] #<TRAIN-STATION-STOP from 1934¾ to 1935¾: 66s at Woolwich (plat B), 0355>
[new event ] #<TRAIN-TRANSIT from 1935¾ to 1935¾: 343F>
-3900162966- 
[event     ] END-TS = 3900162966
[track     ] 343C
-3900163030- 
[event     ] END-TS = 3900163030
[track     ] 0343
-3900163051- 
[event     ] END-TS = 3900163051
[track     ] 339B
-3900163105- 
[event     ] END-TS = 3900163105
[track     ] 0339
-3900163118- 
[event     ] END-TS = 3900163118
[track     ] 335A
-3900163129- 
[train     ] MODEL-POINT = At Custom House Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1935¾ to 1938¾: 343F -> 343C -> 0343 -> 339B -> 0339 -> 335A>
[new event ] #<TRAIN-STATION-STOP from 1938¾ to 1938¾: 0s at Custom House (plat B), 0335>
-3900163184- 
[train     ] MODEL-POINT = efca6db
[history   ] #<TRAIN-STATION-STOP from 1938¾ to 1939½: 55s at Custom House (plat B), 0335>
[new event ] #<TRAIN-TRANSIT from 1939½ to 1939½: 315D>
-3900163192- 
[event     ] END-TS = 3900163192
[track     ] 315C
-3900163210- 
[event     ] END-TS = 3900163210
[track     ] 0315
-3900163309- 
[event     ] END-TS = 3900163309
[track     ] 311A
-3900163313- 
[train     ] MODEL-POINT = At Canary Wharf Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1939½ to 1941¾: 315D -> 315C -> 0315 -> 311A>
[new event ] #<TRAIN-STATION-STOP from 1941¾ to 1941¾: 0s at Canary Wharf (plat B), 0311>
-3900163473- 
[train     ] MODEL-POINT = 8a34484
[history   ] #<TRAIN-STATION-STOP from 1941¾ to 1944½: 160s at Canary Wharf (plat B), 0311>
[new event ] #<TRAIN-TRANSIT from 1944½ to 1944½: 201C>
-3900163481- 
[event     ] END-TS = 3900163481
[track     ] 201B
-3900163482- 
[event     ] END-TS = 3900163482
[track     ] 0201
-3900163568- 
[event     ] END-TS = 3900163568
[track     ] 145C
-3900163572- 
[event     ] END-TS = 3900163572
[track     ] 0145
-3900163598- 
[event     ] END-TS = 3900163598
[track     ] 141A
-3900163607- 
[train     ] MODEL-POINT = At Whitechapel Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1944½ to 1946¾: 201C -> 201B -> 0201 -> 145C -> 0145 -> 141A>
[new event ] #<TRAIN-STATION-STOP from 1946¾ to 1946¾: 0s at Whitechapel (plat B), 0141>
-3900163669- 
[train     ] MODEL-POINT = 08d5aac
[history   ] #<TRAIN-STATION-STOP from 1946¾ to 1947¾: 62s at Whitechapel (plat B), 0141>
[new event ] #<TRAIN-TRANSIT from 1947¾ to 1947¾: 125C>
-3900163681- 
[event     ] END-TS = 3900163681
[track     ] 0125
-3900163752- 
[event     ] END-TS = 3900163752
[track     ] 121A
-3900163760- 
[train     ] MODEL-POINT = At Liverpool Street Platform B(W)
[history   ] #<TRAIN-TRANSIT from 1947¾ to 1949¼: 125C -> 0125 -> 121A>
[new event ] #<TRAIN-STATION-STOP from 1949¼ to 1949¼: 0s at Liverpool Street (plat B), 0121>
