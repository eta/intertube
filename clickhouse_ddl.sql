CREATE TABLE default.trains
(

    `observation_ts` DateTime('UTC'),

    `observer` LowCardinality(FixedString(3)),

    `intertube_archived_ts` DateTime('UTC'),

    `intertube_filename` String,

    `intertube_archive_path` LowCardinality(String),

    `train_id` String,

    `lcid` UInt32,

    `set_no` UInt16,

    `trip_no` UInt16,

    `seconds_to` UInt16,

    `location_desc` LowCardinality(String),

    `destination_desc` LowCardinality(String),

    `dest_code` LowCardinality(String),

    `ord` UInt8,

    `depart_time` UInt32,

    `depart_interval` Int16,

    `departed` UInt8,

    `direction` UInt8,

    `is_stalled` UInt8,

    `track_code` LowCardinality(String),

    `line` FixedString(1),

    `input_dest` LowCardinality(String),

    `leading_car_no` UInt32,

    `insertion_date` DateTime MATERIALIZED now(),

    INDEX filename_index intertube_filename TYPE bloom_filter GRANULARITY 8192
)
ENGINE = MergeTree
PARTITION BY line
ORDER BY observation_ts
SETTINGS index_granularity = 8192;
