# intertube models

This directory contains intertube's track graph models, as saved from the `write-json-toposort-model` function.

Please note that this data is to be considered **proprietary**: you may load it into a local database for testing and development,
but running a public intertube instance based off it is prohibited. Please [get in touch](https://eta.st/#contact) if you would
like to use this data.
