(in-package :trackernet)

(defclass toposort-model ()
  ((line
    :initarg :line
    :reader line)
   (generated-ts
    :initarg :generated-ts
    :reader generated)
   (codes
    :initform (make-hash-table :test 'equal)
    :reader codes)
   (links
    :initform (make-hash-table :test 'equal)
    :reader links)
   (parents
    :initform (make-hash-table :test 'equal)
    :reader parents)
   ;; NOTE(eta): The first version of this code mistakenly assumes that
   ;;            each track code has exactly 1 model point. This is wrong.
   ;;            We keep an arbitrary 1:1 mapping for compat, and one that
   ;;            is 1:n as well.
   (code-to-point
    :initform (make-hash-table :test 'equal)
    :reader code-to-point)
   (code-to-points
    :initform (make-hash-table :test 'equal)
    :reader code-to-points)
   (point-to-station
    :initform (make-hash-table :test 'equal)
    :reader point-to-station)
   (station-to-point
    :initform (make-hash-table :test 'equal)
    :reader station-to-point)
   (station-to-slug
    :initform (make-hash-table :test 'equal)
    :reader station-to-slug)
   (slug-to-station
    :initform (make-hash-table :test 'equal)
    :reader slug-to-station)
   (station-pairs-to-inter
    :initform (make-hash-table :test 'equal)
    :reader station-pairs-to-inter)))

(defun slugify (thing)
  "Turn THING, a string, into a slug."
  (cl-ppcre:regex-replace-all
   "\\s+"
   (cl-ppcre:regex-replace-all
    "[^\\w\\s]+"
    (string-downcase thing)
    "")
   "-"))

(defun generate-station-slugs (model)
  "Generate a set of 'station slugs': slug-like URL bits that map to a station name, for each station on the given MODEL."
  (mapcar (lambda (name)
            (list name (slugify name)))
          (remove-duplicates
           (mapcar #'car
                   (alexandria:hash-table-keys
                    (station-to-point
                     model)))
           :test #'string=)))

(defun read-json-toposort-model (file)
  (with-open-file (stream file)
    (let* ((cl-json:*json-identifier-name-to-lisp* #'identity)
           (model (cl-json:decode-json stream))
           (unfucker (lambda (list)
                        (cons (symbol-name (car list)) (cdr list))))
           (line (cdr (assoc :|lineCode|
                             (cdr (assoc :|metadata| model)))))
           (generated-ts (cdr (assoc :|generatedTs|
                                     (cdr (assoc :|metadata| model)))))
           (ret (make-instance 'toposort-model
                               :line line
                               :generated-ts generated-ts))
           (links (mapcar unfucker
                          (cdr (assoc :|links| model))))
           (stations
             (alexandria:alist-hash-table
              (mapcar unfucker
                      (cdr (assoc :|stations| model)))
              :test #'equal))
           (codes
             (alexandria:alist-hash-table
              (mapcar unfucker
                      (cdr (assoc :|codes| model)))
              :test #'equal)))
      (assert line)
      (assert generated-ts)
      (setf (slot-value ret 'codes) codes)
      (setf (slot-value ret 'links)
            (alexandria:alist-hash-table
             links
             :test #'equal))
      (setf (slot-value ret 'point-to-station) stations)
      ;; Create the reverse maps:
      (loop
        for (point . links) in links
        do (loop
             for track-code in (gethash point codes)
             ;; track code -> model points
             do (alexandria:ensure-gethash track-code (slot-value ret 'code-to-points))
             do (push point (gethash track-code (slot-value ret 'code-to-points)))
             ;; track code -> model point (legacy, random one wins)
             do (setf (gethash track-code (slot-value ret 'code-to-point))
                      point))
        do (loop
             for other in links
             ;; model point -> parents
             do (alexandria:ensure-gethash other (slot-value ret 'parents))
             do (push point (gethash other (slot-value ret 'parents)))))
      ;; station name & pfm -> point
      (loop
        for point being the hash-keys of stations
          using (hash-value station)
        do (setf (gethash station (slot-value ret 'station-to-point))
                 point))
      ;; handle slugs
      (dolist (slug-pair (generate-station-slugs ret))
        (setf (gethash (first slug-pair) (slot-value ret 'station-to-slug))
              (second slug-pair))

        ;; check we don't have a conflict in slugs
        (alexandria:when-let
            ((existing
              (gethash (second slug-pair) (slot-value ret 'slug-to-station))))
          (error "Station slug '~A' is both '~A' and '~A'"
                 (second slug-pair) existing (first slug-pair)))

        (setf (gethash (second slug-pair) (slot-value ret 'slug-to-station))
              (first slug-pair)))
      ;; handle mapping station -> station pairs to the 'inter' node
      ;; between them
      (loop
        for point being the hash-keys of stations
          using (hash-value station)
        do (loop
             ;; Look at all neighbours of this station...
             for inter-point in (gethash point (slot-value ret 'links))
             do (alexandria:when-let*
                    ;; ...and then the first neighbour of those neighbours
                    ;; (since there should be only one)
                    ((next-point (first (gethash inter-point (slot-value ret 'links))))
                     ;; ...and check if it's a station
                     (next-station (gethash next-point stations)))
                  ;; If we get here, we've got a link between 2 stations; insert it!
                  (setf (gethash (list station next-station) (slot-value ret 'station-pairs-to-inter))
                        inter-point))))
      ret)))

(defparameter *line-models*
  (alexandria:alist-hash-table
   (mapcar
    (lambda (pair)
      (cons (car pair)
            (read-json-toposort-model (cadr pair))))
    '(("D" "./models/D.json")
      ("J" "./models/J.json")
      ("B" "./models/B.json")
      ("C" "./models/C.json")
      ("P" "./models/P.json")
      ("H" "./models/H.json")
      ("M" "./models/M.json")
      ("N" "./models/N.json")
      ("V" "./models/V.json")
      ("W" "./models/W.json")
      ("X" "./models/X.json")))
   :test 'equal))

(defun line-model-for (code)
  (unless (gethash code *line-models*)
    (error "Attempted to retrieve toposort model for line ~A" code))
  (gethash code *line-models*))

(defun toposort-model-point (line-code track-code)
  (unless (string= line-code "S") ;; FIXME(eta): argh!
    (gethash track-code
             (code-to-point (line-model-for line-code)))))

(defun toposort-model-points (line-code track-code)
  (unless (string= line-code "S") ;; FIXME(eta): argh!
    (gethash track-code
             (code-to-points (line-model-for line-code)))))

(defun toposort-model-point-between-stations (line-code station-1-name station-1-pfm station-2-name station-2-pfm)
  (gethash (list (list station-1-name station-1-pfm)
                 (list station-2-name station-2-pfm))
           (station-pairs-to-inter (line-model-for line-code))))

(defun toposort-model-station-information (line-code model-point)
  (values-list (gethash model-point (point-to-station (line-model-for line-code)))))

(defun toposort-model-neighbouring-station (line-code model-point direction &key (max-hops 5))
  (check-type direction (member :forward :backward))
  (let ((num-hops 0)
        (model (line-model-for line-code)))
    (loop
      (when (> num-hops max-hops)
        (return-from toposort-model-neighbouring-station))
      (multiple-value-bind (station platform)
          (toposort-model-station-information line-code model-point)
        (when station
          (return-from toposort-model-neighbouring-station
            (list station platform num-hops)))
        (let ((parents
                (gethash model-point
                         (if (eql direction :forward)
                             (links model)
                             (parents model)))))
          (unless (eql (length parents) 1)
            ;; No parents, or ambiguous parents
            (return-from toposort-model-neighbouring-station))
          (incf num-hops)
          (setf model-point (first parents)))))))

(defun toposort-model-point-for-station (line-code station-name pfm)
  (gethash (list station-name pfm) (station-to-point (line-model-for line-code))))

(defun toposort-model-points-for-station (line-code station-name)
  (mapcar (function cdr)
          (remove-if-not
           (lambda (x)
             (string= (caar x) station-name))
           (alexandria:hash-table-alist (station-to-point (line-model-for line-code))))))

(defun toposort-model-previous-station (line-code model-point)
  (toposort-model-neighbouring-station line-code model-point :backward
                                       :max-hops 1))

(defun toposort-model-next-station (line-code model-point)
  (toposort-model-neighbouring-station line-code model-point :forward
                                       :max-hops 1))

(defun toposort-model-station-progress (line-code track-code)
  (let ((model-point (toposort-model-point line-code track-code)))
    (when model-point
      (let* ((prev (toposort-model-neighbouring-station line-code model-point
                                                        :backward
                                                        :max-hops 1))
             (next (toposort-model-neighbouring-station line-code model-point
                                                        :forward
                                                        :max-hops 1))
             (codes (gethash model-point (codes (line-model-for line-code))))
             (nth-code (position track-code codes :test #'string=)))
        (when (and prev next nth-code)
          (list prev (/ nth-code (length codes)) next))))))

(defun toposort-model-trace (line-code model-point)
  "Traces the parents and children of MODEL-POINT until we reach a model point with more or less than 1 parent/child."
  (let ((parent-line
          (loop
            with cur-parent = model-point
            with parent-list
            do (setf parent-list (gethash cur-parent
                                          (parents (line-model-for line-code))))
            while (eql (length parent-list) 1)
            do (setf cur-parent (first parent-list))
            collect cur-parent))
        (child-line
          (loop
            with cur-child = model-point
            with children
            do (setf children (gethash cur-child
                                          (links (line-model-for line-code))))
            while (eql (length children) 1)
            do (setf cur-child (first children))
            collect cur-child)))
    (append (nreverse parent-line) (list model-point) child-line)))

(defun station-slug-for (line-code station-name)
  (gethash station-name (station-to-slug (line-model-for line-code))))

(defun station-resolve-slug (line-code slug)
  (gethash slug (slug-to-station (line-model-for line-code))))

;; This is a simple breadth-first search.
(defun toposort-model-path-between (line-code start-model-point end-model-points)
  (let ((model (line-model-for line-code))
        (parents (make-hash-table :test 'equal))
        (end (if (atom end-model-points)
                 (list end-model-points)
                 end-model-points))
        (queue (list start-model-point)))
    (loop
      with cur
      for i from 0
      while (setf cur (pop queue))
      when (eql i 1000)
        do (error "Too many iterations!")
      when (member cur end :test (function equal))
        do (return-from toposort-model-path-between
             (nreverse
              (loop
                with tip = cur
                while tip
                collect tip
                do (setf tip (gethash tip parents)))))
      do (dolist (neighbour (gethash cur (links model)))
           (unless (or
                    (gethash neighbour parents)
                    ;; This causes the back-walker to infinite loop.
                    (equal neighbour cur)
                    (equal neighbour start-model-point))
             (setf (gethash neighbour parents) cur)
             ;; FIXME(eta): this is O(n)
             (setf queue (nconc queue (list neighbour))))))))
