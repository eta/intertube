(load "deploy-common.lisp")
(format t "==> Running tests (test output in ./test-output.tap)~%")
(with-open-file (test-out "./test-output.tap"
                          :if-exists :supersede
                          :if-does-not-exist :create
                          :direction :output)
  (let ((prove:*test-result-output*
          (make-broadcast-stream *standard-output* test-out)))
    (unless (prove:run "./tests.lisp" :reporter :tap)
      (format *error-output* "Tests failed!~%")
      (sb-ext:exit :code 1))))
