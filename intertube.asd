(defsystem "intertube"
  :depends-on ("drakma" "cl-conspack" "cxml" "cl-statsd" "cl-redis" "qbase64"
                        "cl-ansi-text" "gzip-stream" "bobbin"
                        "cl-heap" "hunchentoot" "cl-who" "fuzzy-match"
                        "cl-json" "ironclad" "fast-websocket"
                        "cl-stomp" "trivial-backtrace"
                        "uuid" "easy-routes" "prove")
  :serial t
  :components
  ((:file "packages")
   (:file "itps")
   (:file "models/models")
   (:file "xrcos")
   (:file "xrsmart")
   (:file "trackernet")
   (:file "wtt")
   (:file "wobsite/embed")
   (:file "assets/assets")
   (:file "inter-font/font")
   (:file "branding/branding")
   (:file "wobsite/line-model")
   (:file "wobsite/journeys")
   (:file "wobsite")))
