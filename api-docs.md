## API documentation

Access to this is private; [ask eta](https://eta.st/#contact) if you have an
interesting use for intertube data, and we can work something out.

### Data formats

intertube represents train data with two key datastructures:

*note: the way these get converted to JSON screws with the names; in
particular e.g. `unique-id` will actually be `uniqueId`; look at an
actual JSON object to see what they're actually called*

- the `LIVE-TRAIN` class, representing a currently running train
  - `unique-id`: this train's intertube unique identifier (random string)
  - `train-identity`: a nullable 2-element array, containing the train
    timetable number and trip number
  - `first-updated`: a **universal time** timestamp representing the first
    ever update time
    - "universal time" is "the number of seconds that have elapsed since
      00:00 of January 1, 1900 in the GMT time zone". blame Common Lisp :p
  - `last-updated`: a universal time timestamp representing the last update
  - `destination`: the current Trackernet destination, as a human-readable
    station name string (e.g. "Stanmore")
  - `current-entry`: the `TRAIN-HISTORY-ENTRY` representing what the train
    is currently doing
  - `trackernet-id`: what Trackernet calls this train (usually its
    "LCID" field, or a string of the form "tidXXX" where "XXX" is the train
     ID)
  - `destination-ballot`: ignore this
  - `line-code`: the Trackernet line code, representing which Tube line
    this train is from (e.g. "D" for District)
  - `model-point`: ignore this
  - `history`: an array of `TRAIN-HISTORY-ENTRY` objects for what the train
    *has* done in the past (in reverse chronological order, I think)
- subclasses of `TRAIN-HISTORY-ENTRY`, representing things that a train has
  done or is currently doing
  - *all subclasses contain the following fields*
    - `universal-ts`: a universal time timestamp representing when this
      event happened or started
  - `TRAIN-IDENTITY-CHANGE`: a change in working timetable number
    - `wtt-id`: the new timetable number for this train
    - `wtt-trip`: the new trip number for this train
  - `TRAIN-DESTINATION-CHANGE`: a change in destination
    - `destination`: the new destination
  - `TRAIN-DESTINATION-PLATFORM-CHANGE`: a change in the destination platform number
    - `platform`: the new destination platform number (can be null)
  - `TRAIN-BRANCH-CHANGE`: a change in which branch the train is on (e.g. bank or Charing Cross)
    - `branch`: the new branch (can be null)
  - `TRAIN-RESCUE`: a change in Trackernet identifier
    - `new-key`: the new Trackernet unique identifer for this train (i.e.
      the `trackernet-id`)
  - `TRAIN-INPUT-DEST-CHANGE`: a change in the Trackernet `input_dest` field
    - `input-dest`: the new value
  - `TRAIN-TRANSIT`: transit between two stations
    - `end-ts`: timestamp of the last observed movement
    - `track-codes`: list of Trackernet track codes observed during this
      trip between stations, in *reverse chronological order*
  - `TRAIN-STATION-STOP`: a stop at a station
    - *subclass of `TRAIN-TRANSIT`; inherits its fields* 
    - `station-name`: the station name (string)
    - `platform`: the platform (string)


### Currently implemented methods

All methods take a `token` query string parameter, with the value being the
API token eta gave you.

#### `GET /api/v1/train`

Query string parameters:

- `id`: the live train ID to retrieve

Returns: a JSON object, containing the complete dump of that live train
(as in, `LIVE-TRAIN` object).

#### `GET /api/v1/list-trains`

Query string parameters:

- `history`: if specified, will return the full history of each train object
  instead of setting it to null
  - *WARNING*: this will result in like 14 megabytes of JSON

Returns: a JSON array of every `LIVE-TRAIN` in the database (potentially
without its `history` field).

#### `GET /api/v1/list-trains-on-line`

Query string parameters:

- `history`: if specified, will return the full history of each train object
  instead of setting it to null
  - *WARNING*: this will result in like 14 megabytes of JSON
- `line`: a Trackernet line code (e.g. `D` for District)

Returns: a JSON array of every `LIVE-TRAIN` on that line (potentially
without its `history` field).

#### `GET /api/v1/train-by-wtt-id`

Query string parameters:

- `wtt`: the Trackernet Working Timetable number of the train (e.g. `230`).
- `line`: a Trackernet line code (e.g. `D` for District)

Returns: a `LIVE-TRAIN` object with that working timetable number on that
line, as JSON (or a 404 :p)

#### `GET /api/v1/firehose` (websocket)

Query string parameters:

- `ver`: the version of the format to use (either nothing for the legacy format, or "2" for v2")

This is a websocket that streams literally every event intertube receives.
It's very high-traffic! (You will probably either get disconnected or cause
me problems if your connection is slow, or you don't read from it quickly
enough? I have no idea.)

##### Legacy format

Events are sent as text websocket messages, with the event data encoded
as a JSON array. Currently implemented events:

- `[train-id, "create"]`
  - `train-id` was newly created.
- `[train-id, "rescue-from", old-train-id]`
  - `old-train-id`'s events have been merged into `train-id`, and the old
    train object has been deleted.
    (This happens when Trackernet changes train identifiers and we notice.)
- `[train-id, "add", event]`
  - `event` has been added to `train-id`'s list of historical events.
  - `event` is a `TRAIN-HISTORY-ENTRY` subclass.
- `[train-id, "update", event]`
  - `train-id`'s current event was replaced with `event`.
  - `event` is a `TRAIN-HISTORY-ENTRY` subclass.

##### Version 2

Events are sent as text websocket messages, with the event data encoded
as a JSON object. Fields are as follows:

- `ltid`: the live train ID the event applies to
- `line`: the Trackernet line code of the live train the event applies to
- `trackernetId`: the Trackernet ID of the live train the event applies to
- `kind`: the type of event, see below

If `kind` is `rescue`,

- `fromLtid`: this train ID's events have been merged into `ltid`, and this
  train object has been deleted (as in version 1).

If `kind` is `add`,

- `event`: an event that has been added to the train's list of historical
  events (as in version 1)

If `kind` is `update`,

- `event`: the new current event for the train (as in version 1)
- `reason`: why the update happened, one of:
  - `exchanged`: the old current event was replaced with this one
  - `move`: the train moved to a new track code within the same event
  - `ready-to-depart`: the train was marked as ready to depart
    (for Crossrail trains)
- (if a `move` or `exchanged`) `model-point`: the train's model point after the update
- (if a `move`) `move-code`: the new track code that prompted the update
- (if a `move`) `move-ts`: the time at which the move happened

If `kind` is `create`,

- `object`: the complete live-train object of a newly created train
