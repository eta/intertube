(defpackage :wtt
  (:use :cl))

(in-package :wtt)

(defparameter *wtt-district* "/home/eta/Downloads/wtt-151-dis.pdf")

(defun run-pdftotext (timetable page x y width height)
  "Runs pdftotext to extract data from PAGE of TIMETABLE, with a bounding box starting at (X, Y) with dimensions (WIDTH, HEIGHT)."
  (split-sequence:split-sequence
   #\Newline
   (string-trim
    '(#\Space #\Return #\Newline #\Tab)
    (uiop:run-program
     `("pdftotext" "-r" "300"
                   "-f" ,(princ-to-string page)
                   "-l" ,(princ-to-string page)
                   "-x" ,(princ-to-string x)
                   "-y" ,(princ-to-string y)
                   "-W" ,(princ-to-string width)
                   "-H" ,(princ-to-string height)
                   "-nopgbrk"
                   ,timetable
                   "-")
     :error-output *error-output*
     :output :string))))

(defun run-pdftotext-with-bbox (timetable page bbox)
  (apply #'run-pdftotext (append (list timetable page) bbox)))

(defparameter *header-left-bbox* '(148 190 854 97))
(defparameter *header-right-bbox* '(1475 187 854 97))
(defparameter *train-data-top-y* 300)
(defparameter *train-data-bottom-y* 1820)
(defparameter *train-data-x* 548)
(defparameter *train-data-width* 90)
(defparameter *train-data-per-row* 20)
(defparameter *train-no-delta* 63)
(defparameter *train-no-delta* 63)
(defparameter *deltas* nil)

(defun read-one-train (timetable page n &optional bottom)
  (let ((x (+ *train-data-x*
               (* n *train-data-width*)))
        (y (if bottom
               *train-data-bottom-y*
               *train-data-top-y*)))
    (loop
      for (keyword . y-delta) in *deltas*
      append (list
              (cons keyword
                   (run-pdftotext timetable page x y *train-data-width* y-delta)))
      do (incf y y-delta))))

(defparameter *deltas-eastbound*
  '((:train-no . 63)
    (:trip-no . 49)
    (:notes . 97)
    (:eby-pfm . 35)
    (:eby . 25)
    (:eac . 25)
    (:eac-depot . 25)
    (:acton-town . 35)
    (:richmond . 35)
    (:turnham-green . 25)
    (:hammersmith . 25)
    (:west-kensington . 38)
    (:olympia . 49)
    (:wimbledon . 35)
    (:east-putney . 25)
    (:putney-bridge . 25)
    (:parsons-green . 25)
    (:earls-court . 25)
    (:earls-court-pfm . 35)
    (:hsk . 35)
    (:hsk-pfm . 25)
    (:edgware-road . 25)
    (:edgware-road-pfm . 35)
    (:hsk-inner-rail . 35)
    (:gloucester-road . 25)
    (:south-kensington . 25)
    (:embankment . 25)
    (:mansion-house . 25)
    (:tower-hill . 25)
    (:aldgate . 35)
    (:liverpool-street . 35)
    (:aldgate-east . 25)
    (:whitechapel . 25)
    (:west-ham . 35)
    (:plaistow . 35)
    (:barking . 25)
    (:barking-sidings . 25)
    (:dagenham-east . 25)
    (:upminster . 25)
    (:upminster-pfm . 25)
    (:upminster-depot . 60)
    (:to-form . 60)))

(defparameter *deltas-westbound*
  '((:train-no . 63)
    (:trip-no . 49)
    (:notes . 97)
    (:upminster-depot . 35)
    (:upminster-pfm . 25)
    (:upminster . 25)
    (:dagenham-east . 25)
    (:barking-sidings . 25)
    (:barking . 25)
    (:plaistow . 35)
    (:west-ham . 35)
    (:whitechapel . 25)
    (:aldgate-east . 25)
    (:liverpool-street . 35)
    (:aldgate . 35)
    (:tower-hill . 25)
    (:mansion-house . 25)
    (:embankment . 25)
    (:south-kensington . 25)
    (:gloucester-road-pfm-1 . 25)
    (:gloucester-road-pfm-2 . 25)
    (:hsk-outer-rail . 35)
    (:edgware-road-pfm . 35)
    (:edgware-road . 25)
    (:hsk-pfm . 25)
    (:hsk . 35)
    (:earls-court-pfm . 35)
    (:earls-court . 25)
    (:parsons-green . 25)
    (:putney-bridge . 25)
    (:east-putney . 25)
    (:wimbledon . 35)
    (:olympia . 49)
    (:west-kensington . 38)
    (:hammersmith . 25)
    (:turnham-green . 25)
    (:richmond . 35)
    (:acton-town . 35)
    (:eac-depot . 25)
    (:eac . 25)
    (:eby . 25)
    (:eby-pfm . 60)
    (:to-form . 45)))

(defun print-wtt-keyword (kwd)
  (case kwd
   (:eby "Ealing Broadway")
   (:eac "Ealing Common")
   (:eac-depot "Ealing Common Depot")
   (:acton-town "Acton Town")
   (:richmond "Richmond")
   (:turnham-green "Turnham Green")
   (:hammersmith "Hammersmith")
   (:west-kensington "West Kensington")
   (:olympia "Olympia")
   (:wimbledon "Wimbledon")
   (:east-putney "East Putney")
   (:putney-bridge "Putney Bridge")
   (:parsons-green "Parsons Green")
   (:earls-court "Earl's Court")
   (:hsk "High Street Kensington")
   (:edgware-road "Edgware Road")
   (:hsk-inner-rail "High Street Kensington")
   (:gloucester-road "Gloucester Road")
   (:gloucester-road-pfm-1 "Gloucester Road")
   (:south-kensington "South Kensington")
   (:embankment "Embankment")
   (:mansion-house "Mansion House")
   (:tower-hill "Tower Hill")
   (:aldgate "Aldgate")
   (:liverpool-street "Liverpool Street")
   (:aldgate-east "Aldgate East")
   (:whitechapel "Whitechapel")
   (:west-ham "West Ham")
   (:plaistow "Plaistow")
   (:barking "Barking")
   (:barking-sidings "Barking Sidings")
   (:dagenham-east "Dagenham East")
   (:upminster "Upminster")
   (:upminster-depot "Upminster Depot")
   (t (princ-to-string kwd))))

(defun read-one-page (timetable page)
  (let* ((header-left (run-pdftotext-with-bbox timetable page *header-left-bbox*))
         (header-right (run-pdftotext-with-bbox timetable page *header-right-bbox*))
         (*deltas* (if (search "EASTBOUND" (first header-left))
                       *deltas-eastbound*
                       *deltas-westbound*)))
    (append
     (list page header-left header-right)
     (loop
       for col from 0 below *train-data-per-row*
       append (list (read-one-train timetable page col)))
     (loop
       for col from 0 below *train-data-per-row*
       append (list (read-one-train timetable page col t))))))

(defun main ()
  (loop
    for page from 19 upto 160
    do (format t "~&Processing page ~A~%" page)
       (cpk:encode-to-file
        (read-one-page "./wtt-district.pdf" page)
        (format nil "./wtt-district-~A.cpk" page))))

(defvar *wtt-data* '())

(defun convert-abbreviation-to-offset (abbrev)
  (ecase (elt abbrev 0)
    (#\a 30)
    (#\b 60)
    (#\c 90)
    (#\d 120)
    (#\e 150)
    (#\f 180)
    (#\g 210)
    (#\h 240)
    (#\j 270)
    (#\k 300)
    (#\T 0)
    (#\Space 0)))

(defclass wtt-train ()
  ((train-no
    :initarg :train-no
    :reader train-no)
   (trip-no
    :initarg :trip-no
    :reader trip-no)
   (notes
    :initarg :notes
    :reader notes)
   (calling-points
    :initarg :calling-points
    :reader calling-points)
   (to-form
    :initarg :to-form
    :reader to-form)
   (from-page
    :initarg :from-page
    :reader from-page)
   (runs-when
    :initarg :runs-when
    :reader runs-when)))

(defmethod print-object ((obj wtt-train) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (train-no trip-no notes calling-points to-form from-page runs-when) obj
      (format stream "~A×~A~@[ (notes: ~{~A~^, ~})~] runs ~A with ~A stops (first ~2,'0D~2,'0D at ~A), forms ~2,'0D~2,'0D (src p#~A)"
              train-no trip-no notes runs-when (length calling-points)
              (second (first calling-points)) (third (first calling-points))
              (first (first calling-points))
              (first to-form) (second to-form)
              from-page))))

(defun parse-wtt-time (timestr)
  (cl-ppcre:register-groups-bind (h abbrevs m halves)
      ("(\\d\\d)\\s*([abcdefghjkT ])?\\s*(\\d\\d)(.*)" timestr)
    (let* ((h (parse-integer h))
           (m (parse-integer m))
           (offset (convert-abbreviation-to-offset (or abbrevs " ")))
           (is-half (or
                     (and
                      (position #\1 halves)
                      (position #\2 halves))
                     (position #\1 halves))))
      (list h m (if is-half 30 0) offset))))

(defvar *wtt-hash-table* (make-hash-table :test #'equal))

(defun strconcat (a b)
  (concatenate 'string a b))

(defun parse-wtt-train-from-list (lst &key runs-when from-page)
  (let ((train-no)
        (trip-no)
        (notes)
        (to-form)
        (calling-points))
    (dolist (pair lst)
      (let ((values (reduce #'strconcat (cdr pair))))
        (case (car pair)
          (:train-no
           (setf train-no values))
          (:trip-no
           (setf trip-no (parse-integer values)))
          (:to-form
           (setf to-form (parse-wtt-time values)))
          (:notes
           (setf notes (delete-if
                        (lambda (x) (eql (length x) 0))
                        (cdr pair))))
          (otherwise
           (alexandria:when-let
               ((time (ignore-errors (parse-wtt-time values))))
             (push (cons (car pair) time) calling-points))))))
    (unless (and train-no trip-no calling-points)
      (error "Train is missing data; ~Ax~A #cpts ~A"
             train-no trip-no (length calling-points)))
    (make-instance 'wtt-train
                   :train-no train-no
                   :trip-no trip-no
                   :notes notes
                   :to-form to-form
                   :runs-when runs-when
                   :from-page from-page
                   :calling-points (nreverse calling-points))))

(defun headers-to-runtime (header-left header-right)
  (let ((total-string
          (reduce #'strconcat
                  (reduce #'append (list header-left header-right)))))
    (cond
      ((search "MONDAY" total-string)
       :weekdays)
      ((search "SATURDAY" total-string)
       :saturdays)
      ((search "SUNDAY" total-string)
       :sundays)
      (t
       (error "Unknown headers ~A" total-string)))))

(defun add-wtt-page-to-table (page)
  (destructuring-bind
      (page-no header-left header-right &rest trains)
      page
    (format t "~&processing page #~A: ~A — ~A (~A trains)~%"
            page-no header-left header-right (length trains))
    (let ((runs-when (headers-to-runtime header-left header-right)))
     (dolist (trn trains)
       (handler-case
           (let* ((wtt-train (parse-wtt-train-from-list
                              trn
                              :runs-when runs-when
                              :from-page page-no))
                  (train-no
                    (string-trim
                     '(#\D #\E #\W)
                     (train-no wtt-train))))
             (setf (gethash train-no *wtt-hash-table*)
                   (cons wtt-train
                         (gethash train-no *wtt-hash-table*))))
         (error (e)
           (warn "Failed to parse train: ~A" e)))))))

(defun add-wtt-pages-to-table (pages-list)
  (dolist (page pages-list)
    (with-simple-restart (continue "Skip this page and continue.")
     (add-wtt-page-to-table
      (car
       (cpk:decode-file page))))))

(defun import-wtt-pages (pages-list)
  (clrhash *wtt-hash-table*)
  (add-wtt-pages-to-table pages-list)
  (loop
    for train-no being the hash-keys of *wtt-hash-table*
    do (setf (gethash train-no *wtt-hash-table*)
             (sort
              (gethash train-no *wtt-hash-table*)
              #'<
              :key #'trip-no)))
  (hash-table-count *wtt-hash-table*))

(defun find-wtt-for (train-no trip-no runs-when)
  (alexandria:when-let
      ((trains (gethash train-no *wtt-hash-table*)))
    (find-if
     (lambda (trn)
       (and
        (eql (trip-no trn) trip-no)
        (eql (runs-when trn) runs-when)))
     trains)))

(defun time-compare (timea timeb func)
  (funcall func
           (+ (* (first timea) 60)
              (second timea))
           (+ (* (first timeb) 60)
              (second timeb))))

(defun find-wtt-for-time (train-no time runs-when)
  (alexandria:when-let
      ((trains (gethash train-no *wtt-hash-table*)))
    (find-if
     (lambda (trn)
       (and
        (eql (runs-when trn) runs-when)
        (time-compare
         (cdr (first (calling-points trn)))
         time
         #'<=)
        (time-compare
         (cdr (car (last (calling-points trn))))
         time
         #'>=)))
     trains)))
