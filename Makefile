SCRAPERFILES = packages.lisp itps.lisp trackernet.lisp wtt.lisp deploy.lisp models/*
WEBFILES = assets/* wobsite/* inter-font/* branding/* wobsite.lisp deploy-web.lisp build/dist.css
SASS = node_modules/.bin/sass
YARN = yarn

.PHONY: all web scraper system test css clean

all: web scraper
scraper: intertube-scraper
web: intertube-web
css: build/dist.css
clean:
	rm -f build/* intertube-scraper intertube-web *.fasl

# Install the Sass compiler if it doesn't already exist with yarn.
# 
# yarn fucks around with the file modification times here, so
# make sure the sass binary is newer with 'touch'.
$(SASS): package.json yarn.lock
	$(YARN) install
	touch $(SASS)

build/dist.css: assets/css/* $(SASS)
	$(SASS) -I node_modules/govuk-frontend --no-error-css --quiet-deps --style compressed assets/css/main.scss build/dist.css

test: all tests.lisp
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy-tests.lisp

model-test: all tests.lisp model-tests.lisp
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy-model-tests.lisp

intertube-scraper: $(SCRAPERFILES)
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy.lisp

intertube-web: $(SCRAPERFILES) $(WEBFILES)
	sbcl --noinform --no-sysinit --no-userinit --disable-debugger --script deploy-web.lisp
