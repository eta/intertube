;;;; Berth data for the Crossrail Central Operating Section (Q0)

(defpackage :xrcos
  (:use :cl))

(in-package :xrcos)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *crs-codes*
    '(("PDX" . "Paddington")
      ("BDS" . "Bond Street")
      ("TCR" . "Tottenham Court Road")
      ("FDX" . "Farringdon")
      ("LSX" . "Liverpool Street")
      ("WHX" . "Whitechapel")
      ("CWX" . "Canary Wharf")
      ("CUS" . "Custom House")
      ("WWC" . "Woolwich")
      ("ABX" . "Abbey Wood")))

  (defparameter *xrcos-descriptions* nil)
  (defparameter *xrcos-links* nil)
  (defparameter *xrcos-signal-maps*
    (make-hash-table :test 'equal))

  (defclass signal-map ()
    ((bit-to-signal
      :reader bit-to-signal
      :initarg :bit-to-signal)
     (signal-to-starter
      :reader signal-to-starter
      :initarg :signal-to-starter)))

  (defun describe-crs (crs)
    (let ((ret (cdr (assoc crs *crs-codes* :test #'string=))))
      (unless ret
        (error "Unknown CRS code ~A." crs))
      ret))

  (defun quote-berth (berth)
    (etypecase berth
      (number (format nil "~4,'0D" berth))
      (symbol (symbol-name berth))))

  (defun quote-signal (berth)
    (etypecase berth
      (number (format nil "~3,'0D" berth))
      (symbol (symbol-name berth))))

  (defun link-and-describe-berths (descriptions links berths name)
    "Generate links between the berths in BERTHS, and label them all with NAME."
    (loop
      with berth
      with next
      for _berth in berths
      for i from 0
      do (setf berth (quote-berth _berth))
      do (setf (gethash berth descriptions) name)
         ;;(format t "~&; ~A: ~A~%" berth name)
      when (< (1+ i) (length berths))
        do (setf next (quote-berth (elt berths (1+ i))))
           (push next (gethash berth links))
           ;;(format t "~&; ~A -> ~A [seg]~%" berth next)
      )))

(defmacro seg ((&key name crs pfm nocont) &body berths)
  (declare (ignore name crs pfm nocont berths)))

(defmacro link (a b)
  (declare (ignore a b)))

;; NOTE(eta): Have fun reading this one!
(defmacro define-xrcos-data (() &body body)
  (let ((descriptions (make-hash-table :test 'equal))
        (links (make-hash-table :test 'equal))
        (last-berths nil)
        (last-name nil)
        (last-pfm nil)
        (name-list '()))
    ;; HACK(eta): We need to discover the order of things with names ahead
    ;;            of time for the next bit, so do a first pass to just
    ;;            discover those.
    (dolist (pragma body)
      (when (and
             (eql (car pragma) 'seg)
             (or
              (getf (cadr pragma) :crs)
              (getf (cadr pragma) :name)))
        (setf name-list
              (append name-list (list
                                 (if (getf (cadr pragma) :crs)
                                     (describe-crs (getf (cadr pragma) :crs))
                                     (getf (cadr pragma) :name)))))))
    ;; Right, now we can do the proper sequential pass.
    (dolist (pragma body)
      (ecase (car pragma)
        (link (destructuring-bind (a b)
                  (cdr pragma)
                ;;(format t "~&; ~A -> ~A [manual]~%" (quote-berth a) (quote-berth b))
                (push (quote-berth b) (gethash (quote-berth a) links))))
        (seg (destructuring-bind (&key name crs pfm nocont)
                  (cadr pragma)
                (if (atom (caddr pragma))
                    ;; it's just a simple list of berths
                    (progn
                      (assert name (name)
                              "Name must be provided for simple SEG invocation ~A." pragma)
                      (assert nocont (nocont)
                              "Continuation is not available for simple SEG invocation ~A." pragma)
                      (link-and-describe-berths descriptions links (cddr pragma) name)
                      (setf last-berths nil))
                    ;; it's a complicated eastbound/westbound thing
                    (let* ((eastbound (cdr (assoc :eb (cddr pragma))))
                           (westbound (cdr (assoc :wb (cddr pragma))))
                           (name-list-index (position last-name name-list :test #'string=))
                           (next-name (when (and name-list-index
                                                 (< name-list-index (length name-list)))
                                        (elt name-list (1+ name-list-index)))))
                      (assert (and eastbound westbound (or (not crs)
                                                           (and crs pfm)))
                              ((cddr pragma))
                              "Unknown SEG invocation: ~A." pragma)
                      (assert (or nocont last-berths)
                              (nocont)
                              "Asked to continue, but nothing to continue from in SEG invocation ~A." pragma)
                      (assert (or nocont name last-name) (name)
                              "No way to generate name for SEG invocation ~A." pragma)
                      ;; Link all of the EB berths.
                      (let ((eb-name (or (when name (format nil "~A eastbound" name))
                                         (when crs (format nil "At ~A Platform ~A(E)" (describe-crs crs) pfm))
                                         (format nil "Between ~A~A and ~A"
                                                 last-name
                                                 (if last-pfm (format nil " (~A)" last-pfm) "")
                                                 next-name))))
                        (link-and-describe-berths descriptions links eastbound eb-name)
                        (when (and (not nocont) last-berths)
                          ;;(format t "~&; ~A -> ~A [cont]~%" (car last-berths) (quote-berth (car eastbound)))
                          (push (quote-berth (car eastbound)) (gethash (car last-berths) links))))
                      ;; Link all of the WB berths (copypasta time!)
                      (let ((wb-name (or (when name (format nil "~A westbound" name))
                                         (when crs (format nil "At ~A Platform ~A(W)" (describe-crs crs) pfm))
                                         (format nil "Between ~A and ~A~A"
                                                 next-name
                                                 last-name
                                                 (if last-pfm (format nil " (~A)" last-pfm) ""))))
                            (westbound-rev (reverse westbound))) ; important since they're in EB order
                        (link-and-describe-berths descriptions links westbound-rev wb-name)
                        (when (and (not nocont) last-berths)
                          ;;(format t "~&; ~A -> ~A [cont]~%" (quote-berth (car westbound-rev)) (cdr last-berths))
                          (push (cdr last-berths) (gethash (quote-berth (car westbound-rev)) links))))
                      (setf last-berths (cons
                                         (quote-berth (car (last eastbound)))
                                         (quote-berth (car (last westbound)))))
                      (when crs
                        (setf last-name (describe-crs crs)))
                      (setf last-pfm pfm)
                      (when name
                        (setf last-name name))))))))
    `(progn
       (setf *xrcos-descriptions* ,descriptions)
       (setf *xrcos-links* ,links))))

(define-xrcos-data ()
  (seg (:name "At Westbourne Park Sidings Platform E" :nocont t)
    018A 0018 042D)
  (seg (:name "At Westbourne Park Sidings Platform E(W)" :nocont t)
    017A 0017 0099)
  (seg (:name "At Westbourne Park Sidings Platform C" :nocont t)
    0015 0016)
  (seg (:name "At Westbourne Park Sidings Platform B" :nocont t)
    0013 0014)
  (seg (:name "At Westbourne Park Sidings Platform A" :nocont t)
    0011 0012)
  (seg (:name "At Westbourne Park Sidings Platform W" :nocont t)
    009B 009A 0009 0091)
  (seg (:name "At Westbourne Park Sidings Platform W(E)" :nocont t)
    10RA 010R 0010)
  (seg (:name "Royal Oak Portal entry (A)" :nocont t)
    042C)
  (link 042C 042B) ;; from ROPe to ROP
  ;; links from Royal Oak Portal (A) to turnback sidings
  (link 0027 0015)
  (link 0027 0013)
  (link 0027 0011)
  ;; links from turnback sidings to Royal Oak Portal entry (A)
  (link 0016 042C)
  (link 0014 042C)
  (link 0012 042C)
  ;; BEGIN A-SIDE --- for B side, jump past this section.
  (seg (:name "Royal Oak Portal (A)" :nocont t)
    (:eb 042B 042A 0042 046A)
    (:wb 0027 027A 027B 027C))
  (seg (:crs "PDX" :pfm "A")
    (:eb 0046)
    (:wb 0043))
  (seg ()
    (:eb 062C 062B 062A 0062 066A)
    (:wb 043A 0047 047A 047B 047C))
  (seg (:crs "BDS" :pfm "A")
    (:eb 0066)
    (:wb 0063))
  (seg ()
    (:eb 082C 082B 082A 0082 086A)
    (:wb 063A 0067 067A 067B 067C))
  (seg (:crs "TCR" :pfm "A")
    (:eb 0086)
    (:wb 0083))
  (seg ()
    (:eb 092C 092B 092A 0092 102D 102C 102B 102A 0102 106A)
    (:wb 083A 0087 087A 087B 087C 087D 0097 097A 097B 097C))
  (seg (:crs "FDX" :pfm "A")
    (:eb 0106)
    (:wb 0103))
  (seg ()
    (:eb 122C 122B 122A 0122 126A)
    (:wb 103A 0107 107A 107B 107C))
  (seg (:crs "LSX" :pfm "A")
    (:eb 0126)
    (:wb 0123))
  (seg ()
    (:eb 142C 142B 142A 0142 146B 146A)
    (:wb 123A 0127 127A 127B 127C 127D))
  (seg (:crs "WHX" :pfm "A")
    (:eb 0146)
    (:wb 0143))
  (seg (:name "East of Whitechapel (A)")
    ;; end of this berth is the GEML/CCOS interface
    (:eb 202C 202B 202A 0202 412D)
    (:wb 143A 0147 147A 147B 147C))
  (seg ()
    (:eb 312C 312B 312A 0312 316A)
    (:wb 147E 0203 203A 203B 203C))
  (seg (:crs "CWX" :pfm "A")
    (:eb 0316)
    (:wb 0313))
  (seg ()
    (:eb 332C 332B 332A 0332 336B 336A)
    (:wb 313A 0317 317A 317B 317C 317D))
  (seg (:crs "CUS" :pfm "A")
    (:eb 0336)
    (:wb 0337))
  (seg ()
    (:eb 340C 340B 340A 0340 352F 352E 352D 352C 352B 352A 0352 356B 356A 0356 360A)
    (:wb 337A 337B 0341 341A 341B 341C 341D 341E 0345 345A 345B 345C 345D 345E 345F))
  (seg (:crs "WWC" :pfm "A")
    (:eb 0360)
    (:wb 0357))
  (seg ()
    (:eb 372C 372B 372A 0372 396E 396D 396C 396B 396A 0396 400B 400A)
    (:wb 357A 0361 361A 361B 361C 361D 361E 361F 0377 377A 377B 377C))
  (seg (:crs "ABX" :pfm "4")
    (:eb 0400)
    (:wb 0393))
  ;; END A-SIDE
  ;; BEGIN B-SIDE
  (seg (:name "Royal Oak Portal entry (B)" :nocont t)
    040C)
  (link 040C 040B) ;; from ROPe to ROP
  ;; links from Royal Oak Portal (B) to turnback sidings
  (link 0025 0015)
  (link 0025 0013)
  (link 0025 0011)
  ;; links from turnback sidings to Royal Oak Portal entry (B)
  (link 0016 040C)
  (link 0014 040C)
  (link 0012 040C)
  (seg (:name "Royal Oak Portal (B)" :nocont t)
    (:eb 040B 040A 0040 044A)
    (:wb 0025 025A 025B 025C))
  (seg (:crs "PDX" :pfm "B")
    (:eb 0044)
    (:wb 0041))
  (seg ()
    (:eb 060C 060B 060A 0060 064A)
    (:wb 041A 0045 045A 045B 045C))
  (seg (:crs "BDS" :pfm "B")
    (:eb 0064)
    (:wb 0061))
  (seg ()
    (:eb 080C 080B 080A 0080 084A)
    (:wb 061A 0065 065A 065B 065C))
  (seg (:crs "TCR" :pfm "B")
    (:eb 0084)
    (:wb 0081))
  (seg ()
    (:eb 090C 090B 090A 0090 100D 100C 100B 100A 0100 104A)
    (:wb 081A 0085 085A 085B 085C 085D 0095 095A 095B 095C))
  (seg (:crs "FDX" :pfm "B")
    (:eb 0104)
    (:wb 0101))
  (seg ()
    (:eb 120C 120B 120A 0120 124A)
    (:wb 101A 0105 105A 105B 105C))
  (seg (:crs "LSX" :pfm "B")
    (:eb 0124)
    (:wb 0121))
  (seg ()
    (:eb 140C 140B 140A 0140 144A)
    (:wb 121A 0125 125A 125B 125C))
  (seg (:crs "WHX" :pfm "B")
    (:eb 0144)
    (:wb 0141))
  (seg (:name "East of Whitechapel (B)")
    ;; end of this berth is the GEML/CCOS interface
    (:eb 200C 200B 200A 0200 410D)
    (:wb 141A 0145 145A 145B 145C))
  (seg ()
    (:eb 310C 310B 310A 0310 314A)
    (:wb 145E 0201 201A 201B 201C))
  (seg (:crs "CWX" :pfm "B")
    (:eb 0314)
    (:wb 0311))
  (seg ()
    (:eb 330C 330B 300A 0330 344B 344A)
    (:wb 311A 0315 315A 315B 315C 315D))
  (seg (:crs "CUS" :pfm "B")
    (:eb 0334)
    (:wb 0335))
  (seg ()
    (:eb 338C 338B 338A 0338 350E 350D 350C 350B 350A 0350 354B 354A 0354 358A)
    (:wb 335A 335B 0339 339A 339B 339C 339D 0343 343A 343B 343C 343D 343E 343F))
  (seg (:crs "WWC" :pfm "B")
    (:eb 0358)
    (:wb 0355))
  (seg ()
    (:eb 370C 370B 370A 0370 394F 394E 394D 394C 394B 394A 0394 398B 398A)
    (:wb 355A 0359 359A 359B 359C 0371 371A 371B 371C 0375 375A 375B 375C))
  (seg (:crs "ABX" :pfm "3")
    (:eb 0398)
    (:wb 0391))
  ;; links to/from the bolthole berth
  (link 391A 0391)
  (link 0398 602A)
  (seg (:name "At Abbey Wood Bolthole Berth"
        :nocont t)
    602A 0602 501B 0501 X397 395A 0395 391A)
  ;; END B-SIDE
  )

(defun redis-xrcos-model ()
  "Insert the XR COS model into Redis."
  (length
   (redis:with-pipelining
     ;; TODO(eta): figure out a solution for links?
     (loop
       for berth being the hash-keys of *xrcos-descriptions*
         using (hash-value descr)
       do (red:set (format nil "X-track-desc-~A" berth) descr)))))

(defmacro define-xrcos-signals ((area sig-prefix) &body body)
  (let ((bit-to-signal (make-hash-table))
        (signals-hash (make-hash-table :test 'equal))
        (signal-to-starter (make-hash-table :test 'equal)))
    (dolist (pragma body)
      (ecase (car pragma)
        (map-byte
            (destructuring-bind ((byte &key (start 0)) &rest sigs)
                (cdr pragma)
              (loop
                for sig in sigs
                for bpos from start
                do (let ((bit (+ (* byte 8) bpos))
                         (sig (format nil "~A~A"
                                      sig-prefix (quote-signal sig))))
                     ;;(format t "~A:~A (~A) = ~A~%" byte bpos bit sig)
                     (when (gethash sig signals-hash)
                       (error "Signal ~A is already at bit ~A!"
                              sig (gethash sig signals-hash)))
                     (setf (gethash sig signals-hash) bit)
                     (when (gethash bit bit-to-signal)
                       (error "Bit ~A (~A:~A) redefined from ~A to ~A!"
                              bit byte bpos (gethash bit bit-to-signal)
                              sig))
                     (setf (gethash bit bit-to-signal) sig)))))
        (station
            (let* ((station-crs (cadr pragma))
                   (station-name (describe-crs station-crs))
                   (mappings '()))
              (dolist (subpragma (cddr pragma))
                (ecase (car subpragma)
                  (sig
                   (destructuring-bind (platform name) (cdr subpragma)
                     (let ((sig (format nil "~A~A" sig-prefix (quote-signal name))))
                       (setf mappings
                             (append
                              (list (cons platform sig))
                              mappings)))))
                  (sigs
                   (destructuring-bind (side wb eb) (cdr subpragma)
                     (check-type side (member :eb :wb))
                     (let* ((pfm (if (eql side :eb) "A" "B")))
                       (setf
                        mappings
                        (append
                         (list
                          (cons (format nil "~A(W)" pfm)
                                (format nil "~A~A" sig-prefix (quote-signal wb)))
                          (cons (format nil "~A(E)" pfm)
                                (format nil "~A~A" sig-prefix (quote-signal eb))))
                         mappings)))))))
              (dolist (map mappings)
                (when (gethash (cdr map) signal-to-starter)
                  (error "Signal ~A is already assigned starter for ~A!"
                         (cdr map) (gethash (cdr map) signal-to-starter)))
                ;;(format t "~A is the starter for ~A pfm ~A~%" (cdr map)
                ;;        station-name (car map))
                (setf (gethash (cdr map) signal-to-starter)
                      (list station-name (car map))))))))
    `(setf (gethash ,area *xrcos-signal-maps*)
           (make-instance 'signal-map
                          :signal-to-starter ,signal-to-starter
                          :bit-to-signal ,bit-to-signal))))

(defmacro station (crs &body body)
  (declare (ignore crs body)))

(defmacro sigs (side wb eb)
  (declare (ignore side wb eb)))

(defmacro sig (platform name)
  (declare (ignore platform name)))

(defmacro map-byte ((byte &key start) &body sigs)
  (declare (ignore byte start sigs)))

(define-xrcos-signals ("Q0" "XR")
  (map-byte (27 :start 6)
    062 063)
  (map-byte (28)
    066 067 082 083 086 087 092 097)
  (map-byte (29)
    102 103 106 107 042 061 060 064)
  (map-byte (30)
    065 080 081 084 085 040 090 095)
  (map-byte (31)
    043 041 100 101 104 105 046 047)
  (map-byte (32)
    044 045 013 015 145 147 027 025)
  (map-byte (33)
    018 017 016 014 012 011 010 009)
  (map-byte (34)
    123 121 142 140 127 125 126 124)
  (map-byte (35)
    143 146 141 144 200 202 205 207)
  (map-byte (36)
    410 412 416 415 414 417 413 411)
  (map-byte (37)
    201 203 122 120)
  ;; Platform starter signals
  ;; (arguably we could've just used the berth names, oh well)
  (station "PDX"
    (sigs :eb 043 046)
    (sigs :wb 041 044))
  (station "BDS"
    (sigs :eb 063 066)
    (sigs :wb 061 064))
  (station "TCR"
    (sigs :eb 083 086)
    (sigs :wb 081 084))
  (station "FDX"
    (sigs :eb 103 106)
    (sigs :wb 101 104))
  (station "LSX"
    (sigs :eb 123 126)
    (sigs :wb 121 124))
  (station "WHX"
    (sigs :eb 143 146)
    (sigs :wb 141 144))
  (station "CWX"
    (sigs :eb 313 316)
    (sigs :wb 311 314))
  (station "CUS"
    (sigs :eb 337 336)
    (sigs :wb 335 334))
  (station "WWC"
    (sigs :eb 357 360)
    (sigs :wb 355 358))
  (station "ABX"
    (sig "4(W)" 393)
    (sig "3(W)" 391)
    (sig "3(E)" 398)))

(defun get-starter-for-signal (area signal &key strip-direction)
  "Get the station and platform that SIGNAL is a starter signal for, if one exists."
  (alexandria:when-let ((signal-map (gethash area *xrcos-signal-maps*)))
    (alexandria:when-let ((ret (gethash signal (signal-to-starter signal-map))))
      (if (and strip-direction (position #\( (second ret)))
          (list (first ret)
                (subseq (second ret) 0 1))
          ret))))

(defun get-mentioned-signals (area signalling-state)
  "Given a bitmap of SIGNALLING-STATE, output all signals set to 1 in said state along with their bit indices."
  (alexandria:when-let ((signal-map (gethash area *xrcos-signal-maps*)))
    (loop
      for bit being the hash-keys of (bit-to-signal signal-map)
      using (hash-value signal)
      append (when (logbitp bit signalling-state)
               (list (cons signal bit))))))
