const url_params = new URLSearchParams(window.location.search);
const line_code = url_params.get('line');
var model_log;
var model_log_lines;
var cook;
var download_button;
var cutoff;
var model_container;
var model_container_rect = null;
var model_svg = null;
var model_g = null;
var model_g_style = "";
var model_svg_dimensions = null;
var model_dot = null;
var station_select = null;
var viewbox_scale = { x: 0, y: 0 };
var stations = {};
var station_models = {};
var cooktext = '';

window.model = null;

function populateNodes() {
    station_select.innerHTML = "";
    stations = {};
    let option = document.createElement("option");
    option.value = "none";
    option.disabled = true;
    option.selected = true;
    option.innerHTML = "(select one)";
    station_select.appendChild(option);
    let nodes = document.querySelectorAll('g.node');
    let kv = {};
    let things = [];
    for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        let id = node.id;
        let station = null;
        let pfm = null;
        let text = null;
        for (let j = 0; j < node.children.length; j++) {
            let child = node.children[j];
            if (child.getAttribute("font-weight") == "bold") {
                if (child.getAttribute("font-size") == "14.00") {
                    station = child.innerHTML;
                }
                else if (child.getAttribute("font-size") == "10.00") {
                    pfm = child.innerHTML;
                }
            }
            else if (child.getAttribute("font-size") == "10.00") {
                text = child.innerHTML;
            }
        }
        if (station || text) {
            let thing = null;
            if (station) {
                thing = station + " pfm " + pfm;
                Object.keys(window.model.stations).forEach((s) => {
                    if (window.model.stations[s][0] == station
                       && window.model.stations[s][1] == pfm) {
                        station_models[id] = s;
                    }
                });
                if (!station_models[id]) {
                    console.warn("Couldn't find " + station + ", " + pfm);
                    station_models[id] = "At " + station + " Platform " + pfm;
                }
            }
            else {
                thing = text;
                station_models[id] = text;
            }
            kv[thing] = id;
            things.push(thing);
            stations[id] = node;
        }
    }
    things.sort();
    things.forEach((thing) => {
        let option = document.createElement("option");
        option.value = kv[thing];
        option.innerHTML = thing;
        station_select.appendChild(option);
    });
}
// https://stackoverflow.com/a/19328891, CC BY-SA 3.0
var saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (data, fileName) {
        var json = JSON.stringify(data),
            blob = new Blob([json], {type: "octet/stream"}),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());


function doSave() {
    if (!window.model) {
        alert("no model!");
        return;
    }
    saveData(window.model, "model-" + line_code + ".json");
}

function doRender(model) {
    let model_json = JSON.stringify(model);
    let data = new FormData();
    data.append("model", model_json);

    model_container.innerHTML = "<p class='govuk-body'>Rendering model...</p>";
    console.log("Rendering...");

    fetch("/model-kitchen/render", { method: "POST", body: data })
        .then((response) => response.text())
        .then((svg) => {
            console.log("Got rendered SVG!");
            model_container.innerHTML = svg;
            model_svg = model_container.querySelector('svg');
            model_g = model_container.querySelector('svg g');
            model_g_style = model_g.transform;
            model_svg_dimensions = {
                x: model_svg.width,
                y: model_svg.height
            };
            viewbox_scale.x = model_svg.viewBox.baseVal.width / model_svg.width.baseVal.value;
            viewbox_scale.y = model_svg.viewBox.baseVal.height / model_svg.height.baseVal.value;
            let dot = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
            dot.setAttribute("cx", 0);
            dot.setAttribute("cy", 0);
            dot.setAttribute("r", "5");
            dot.setAttribute("color", "#e00");
            dot.setAttribute("id", "dot");
            model_svg.appendChild(dot);
            model_dot = dot;
            model_container_rect = model_container.getBoundingClientRect();
            populateNodes();
        })
        .catch((e) => {
            model_container.innerHTML = "<p class='govuk-body'>Render failed!</p>";
            console.error(e);
            model_svg = null;
            model_svg_dimensions = null;
            alert('rendering failed: ' + e);
        });
}

function reset() {
    cook.disabled = false;
    cook.innerHTML = cooktext;
    download_button.disabled = true;
}

function doUpload() {
    let input = document.createElement("input");
    input.setAttribute("type", "file");
    input.addEventListener("change", (evt) => {
        let file = input.files[0];
        console.log("got file: " + file);
        if (file) {
            const reader = new FileReader();
            reader.onload = (e) => {
                try {
                    let json = e.target.result;
                    let model = JSON.parse(json);
                    console.log("Got new uploaded model!");
                    window.model = model;
                    doRender(model);
                }
                catch (e) {
                    alert("failed to upload: " + e);
                }
            };
            reader.readAsText(file);
        }
    });
    input.click();
}

function doCook() {
    cook.disabled = true;
    cook.innerHTML = 'Mmm, tasty...';
    console.log("Tasty!");
    fetch("/model-kitchen/generate?line=" + line_code + "&cutoff=" + cutoff.value)
        .then((response) => response.json())
        .then((data) => {
            reset();
            let log = data[0];
            let model = JSON.parse(data[1]);
            window.model = model;
            let log_lines = log.split(/\r\n|\r|\n/).length;
            console.log("Got new model!");
            model_log.innerHTML = "<pre>" + log + "</pre>";
            model_log_lines.innerHTML = log_lines;
            download_button.disabled = false;
            doRender(model);
        })
        .catch((e) => {
            reset();
            console.error(e);
            alert('cooking failed: ' + e);
        });
}

let pos = { top: 0, left: 0, x: 0, y: 0 };
let svg_pos = { x: 0, y: 0 };
let scale_origin = { x: 0, y: 0 };
let scrolling = false;
let scale = 1.0;

function handleMouseMove(e) {
    const dx = e.clientX - pos.x;
    const dy = e.clientY - pos.y;

    if (model_svg) {
        let relative_x = e.offsetX;
        let relative_y = e.offsetY;

        let container_x_unscaled = relative_x * viewbox_scale.x;
        let container_y_unscaled = relative_y * viewbox_scale.y;

        // We also need to apply the current scale transform to our vector.
        // This is equivalent to translation to the origin, scaling, and then
        // moving it back.

        let cx_step1 = container_x_unscaled - scale_origin.x;
        let cy_step1 = container_y_unscaled - scale_origin.y;

        let cx_step2 = cx_step1 * scale;
        let cy_step2 = cy_step1 * scale;

        let cx_step3 = cx_step2 + scale_origin.x;
        let cy_step3 = cy_step2 + scale_origin.y;

        let ox = cx_step3;
        let oy = cy_step3;

        svg_pos.x = ox;
        svg_pos.y = oy;

        model_dot.setAttribute("cx", ox);
        model_dot.setAttribute("cy", oy);
    }

    if (scrolling) {
        model_container.scrollTop = pos.top - dy;
        model_container.scrollLeft = pos.left - dx;
    }
};

function handleMouseUp() {
    document.removeEventListener('mouseup', handleMouseUp);

    model_container.style.cursor = 'grab';
    model_container.style.removeProperty('user-select');
    scrolling = false;
};

function handleMouseDown(e) {
    pos = {
        left: model_container.scrollLeft,
        top: model_container.scrollTop,
        x: e.clientX,
        y: e.clientY,
    };

    model_container.style.cursor = 'grabbing';
    model_container.style.userSelect = 'none';
    document.addEventListener('mouseup', handleMouseUp);
    scrolling = true;
};

function handleWheel(e) {
    if (model_svg) {
        e.preventDefault();
        let delta = 0.05;
        if (e.deltaY < 0) {
            delta = -0.05;
        }
        let old_scale = scale;
        scale += delta;
        scale = Math.min(Math.max(scale, 0.1), 3.0);

        let ox = svg_pos.x;
        let oy = svg_pos.y;

        model_dot.setAttribute("cx", ox);
        model_dot.setAttribute("cy", oy);
        scale_origin.x = ox;
        scale_origin.y = oy;

        model_g.transform = model_g_style + " scale(" + scale + ")";
        model_g.style.transformOrigin = ox + " " + oy;
        console.log("scale: " + old_scale + " -> " + scale + "\nabout " + ox + ", " + oy);
    }
}


document.addEventListener('DOMContentLoaded', function() {
    console.log("Model kitchen for line " + line_code);
    cook = document.getElementById('cook');
    cooktext = cook.innerHTML;
    model_log = document.getElementById('model-log');
    model_log_lines = document.getElementById('model-log-lines');
    download_button = document.getElementById('download');
    cutoff = document.getElementById('cutoff');
    cook.addEventListener("click", doCook);
    download_button.addEventListener("click", doSave);
    model_container = document.getElementById('model-container');
    station_select = document.getElementById('station-select');
    model_container.addEventListener("mousedown", handleMouseDown);
    model_container.addEventListener("wheel", handleWheel);
    model_container.addEventListener('mousemove', handleMouseMove);
    document.getElementById('upload-model').addEventListener('click', (evt) => {
        doUpload();
    });
    station_select.addEventListener('change', (evt) => {
        console.log("Selected " + evt.target.value);
        if (evt.target.value != "none") {
            stations[evt.target.value].focus();
            stations[evt.target.value].scrollIntoView();
            document.getElementById('track-code-list').innerHTML = JSON.stringify(window.model.codes[station_models[evt.target.value]]);
        }
    });
});
