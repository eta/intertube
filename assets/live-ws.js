var current_ul = document.getElementById("live-current");
var history_ul = document.getElementById("live-history");
var share = document.getElementById("share-train");
var share_heading = document.getElementById("share-heading");
var warning = document.getElementById("reconnection-warning");
var warning_text = document.getElementById("reconnection-warning-text");
var wsurl_elt = document.getElementById("live-ws-link");
var fcircle_elt = document.getElementById("freshness-circle");
var ftext_elt = document.getElementById("freshness-text");
var fbtn_elt = document.getElementById("freshness-refresh");

var urlParams = new URLSearchParams(window.location.search);
var ltid = urlParams.get('id');

var loc = (window.location.protocol === "https:" ? "wss://" : "ws://") + window.location.host + wsurl_elt.innerHTML;
console.log("Connecting to " + loc);

var ws = new ReconnectingWebSocket(loc);
var last_update = new Date().getTime();
var connected = false;
var freshnesses = ["live", "mid", "stale"];

function freshness(colour, text) {
    ftext_elt.innerHTML = text;
    fcircle_elt.classList.add("freshness-" + colour);
    freshnesses.forEach(function(f) {
        if (f != colour) {
            fcircle_elt.classList.remove("freshness-" + f);
        }
    });
    /*
    if (colour == "live") {
        fbtn_elt.style.display = "none";
    }
    else {
        fbtn_elt.style.display = "block";
    }
    */
}

ws.onopen = function () {
    console.log("Connected!");
    warning.style.display = "none";
    freshness("mid", "Loading...");
};

ws.onmessage = function (event) {
    var payload = JSON.parse(event.data);
    if (payload[0] == "closed") {
        console.log("Got closed indication!");
        ws.close();
        warning_text.innerHTML = "This train is no longer live. Refresh to view the archived version.";
        secs_ago.innerHTML = "";
        warning.style.display = "block";
        connected = false;
        freshness("stale", "Train no longer live");
        return;
    }
    last_update = new Date().getTime();
    freshness("live", "Live");
    console.log("[" + last_update + "] data of type " + payload[0]);
    switch (payload[0]) {
    case "update":
        current_ul.innerHTML = payload[1];
        break;
    case "add":
        history_ul.insertAdjacentHTML("afterbegin", payload[1]);
        break;
    case "backfill":
        history_ul.innerHTML = payload[1];
    }
    connected = true;
};

ws.onclose = function () {
    connected = false;
    warning.style.display = "block";
    freshness("stale", "Reconnecting...");
};

setInterval(function () {
    if (connected) {
        var now = new Date().getTime();
        var secs_diff = Math.trunc((now - last_update) / 1000);
        if (secs_diff > 15) {
            var state = "live";
            if (secs_diff > 30) {
                state = "mid";
            }
            freshness(state, "Live (" + secs_diff + "s ago)");
        }
    }
}, 1000);

var shareBlob = {
    text: share_heading.textContent + "\n" + window.location,
};

if (navigator.share && navigator.canShare(shareBlob)) {
    console.log("Sharing enabled!");
    share.style = "display: block;";
    share.addEventListener('click', function(e) {
        navigator.share(shareBlob);
        e.preventDefault();
    });
}
else {
    console.log("Sharing disabled: no support");
}
