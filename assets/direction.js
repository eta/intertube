var change_direction = document.getElementById("change-direction-button");
var form = document.getElementById("model-line-form");

var radios = document.getElementsByClassName("direction-radio-button");

function submitForm(e) {
    form.submit();
    e.preventDefault();
}

for (var i = 0; i < radios.length; i++) {
    radios[i].addEventListener("click", submitForm);
}

change_direction.style.display = "none";


var jump_btn = document.getElementById("platform-jump-button");
var jump_form = document.getElementById("platform-jump-form");
var sel = document.getElementById("pfm");


function submitJumpForm(e) {
    jump_form.submit();
    e.preventDefault();
}

sel.addEventListener("change", submitJumpForm);

jump_btn.style.display = "none";