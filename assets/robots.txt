User-agent: *
Disallow: /train/
Disallow: /raw-train/
Disallow: /line/
Disallow: /station/
Disallow: /station-calls
Disallow: /live
Disallow: /historical
Disallow: /live-ws
Disallow: /api/
Disallow: /choose-live
Disallow: /train2
Disallow: /train
Disallow: /live-line
Disallow: /line

User-agent: dotbot
Disallow: /

User-agent: BLEXBot
Disallow: /

User-agent: SemrushBot
Disallow: /

User-agent: AhrefsBot
Disallow: /

User-agent: barkrowler
Disallow: /
