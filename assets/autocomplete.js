function suggest(query, populateResults) {
    fetch("/tracksuggestions?code=" + window.linecode + "&query=" + encodeURIComponent(query))
        .then(function(resp) { return resp.json(); })
        .then(function(data) {
            populateResults(data);
        })
        .catch(function(err) {
            console.error("Request failed: " + err);
        });
}
function inputTemplate(result) {
    if (result) {
        return result.code;
    }
    return undefined;
}
function suggestionTemplate(result) {
    if (result) {
        return "<span class=\"track-suggestion\">" + result.name + " <strong class=\"govuk-tag govuk-tag--grey train-track-code\">" + result.code + "</strong></span>";
    }
    return undefined;
}
function confirmCallback(ret) {
    setTimeout(function() {
        document.querySelector("#line-search-form").submit();
    }, 1);
}
window.onload = function() {
    var linecode = document.querySelector("#linecode");
    window.linecode = linecode.value;
    var input = document.querySelector("#trackcode");
    var value = input.value ? input.value : "";
    var auto = document.querySelector("#trackcode-autocomplete-container");
    auto.innerHTML = "";
    accessibleAutocomplete({
        element: auto,
        id: 'trackcode',
        name: 'trackcode',
        required: false,
        defaultValue: value,
        minLength: 3,
        autoselect: true,
        confirmOnBlur: false,
        onConfirm: confirmCallback,
        templates: {
            suggestion: suggestionTemplate,
            inputValue: inputTemplate,
		    },
		    tNoResults: function() {
			      return "No stations found";
		    },
		    source: suggest
	  });
};

