(in-package :intertube-web)

(defparameter *nr-svg* #.(uiop:read-file-string "./assets/nr.svg"))
(defparameter *metro-svg* #.(uiop:read-file-string "./assets/metro.svg"))
(defparameter *git-branch-svg*
  #.(concatenate 'string
                 "<span class=\"git-branch\">"
                 (uiop:read-file-string "./assets/git-branch.svg")
                 "</span>"))
(defparameter *unlink-svg*
  #.(concatenate 'string
                 "<span class=\"unlink\">"
                 (uiop:read-file-string "./assets/unlink.svg")
                 "</span>"))

(defparameter *bsicons-bhf-svg*
  #.(uiop:read-file-string "./assets/bsicons-bhf.svg"))

(defparameter *bsicons-bhf-alone-svg*
  #.(uiop:read-file-string "./assets/bsicons-bhf-alone.svg"))

(defparameter *bsicons-str-svg*
  #.(uiop:read-file-string "./assets/bsicons-str.svg"))

(defparameter *bsicons-kmw-svg*
  #.(uiop:read-file-string "./assets/bsicons-kmw.svg"))

(defparameter *bsicons-bhf-start-svg*
  #.(uiop:read-file-string "./assets/bsicons-bhf-start.svg"))

(defparameter *bsicons-str-pass-svg*
  #.(uiop:read-file-string "./assets/bsicons-str-pass.svg"))

(defparameter *bsicons-bhf-end-svg*
  #.(uiop:read-file-string "./assets/bsicons-bhf-end.svg"))

(defparameter *bsicons-cont-up-svg*
  #.(uiop:read-file-string "./assets/bsicons-cont-up.svg"))

(defparameter *fa-clock-svg*
  #.(concatenate 'string
                 "<span class=\"fa-clock\">"
                 (uiop:read-file-string "./assets/fa-clock.svg")
                 "</span>"))
(defparameter *govuk-go-arrow-svg*
  (uiop:read-file-string "./assets/govuk-go-arrow.svg"))

(defparameter *govuk-go-arrow-standalone-svg*
  (uiop:read-file-string "./assets/govuk-go-arrow-standalone.svg"))

(defparameter *govuk-go-arrow-small-svg*
  (uiop:read-file-string "./assets/govuk-go-arrow-small.svg"))

(defparameter *autocomplete-js*
  #.(concatenate 'string
                 (uiop:read-file-string "./assets/aa-dist.js")
                 (uiop:read-file-string "./assets/autocomplete.js")))

(defparameter *direction-js*
  #.(uiop:read-file-string "./assets/direction.js"))

(defparameter *model-kitchen-js*
  #.(uiop:read-file-string "./assets/model-kitchen.js"))

(defparameter *live-js*
  #.(concatenate 'string
                 (uiop:read-file-string "./assets/reconnecting-websocket.min.js")
                 (uiop:read-file-string "./assets/live-ws.js")))

(defparameter *website-css*
  #.(progn
      (when trackernet::*staging-mode-p*
        (format t "~&;; Recompiling CSS~%")
        (uiop:run-program "make css"
                          :output t
                          :error-output t))
      (read-file-to-array "./build/dist.css")))
(defparameter *robots-txt* #.(read-file-to-array "./assets/robots.txt"))
